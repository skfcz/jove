/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cadoculus.jove.fileheader;

import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.codec.DefaultCodecFactory;
import de.cadoculus.jove.bibi.codec.FileHeaderCodec;
import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Properties;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import junit.framework.TestCase;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Zerbst
 */
public class FileHeaderTest extends TestCase {

    public static Logger log = LoggerFactory.getLogger( FileHeaderTest.class );
    private static File testDir;

    @BeforeClass
    public static void setUpClass() throws Exception {
        BasicConfigurator.resetConfiguration();

        java.io.File logconfig = new java.io.File( "log4j.properties" );

        if ( logconfig.exists() && logconfig.canRead() ) {

            try {
                URL propsURL = new URL( "file:" + logconfig.getName() );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {

                BasicConfigurator.resetConfiguration();
                BasicConfigurator.configure();

            }
        }

        testDir = new File( "target/test-out/FileHeaderTest" );
//        Files.delete( testDir.toPath() );
        testDir.mkdirs();

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test81() throws Exception {
        setUpClass();
        System.out.println( "test81" + new File( "." ).getAbsolutePath() );

        File f = new File( "src/test/ressources/jt/model1.jt" );
        FileInputStream fis = new FileInputStream( f );
        FileChannel channel = fis.getChannel();
        ByteBuffer bb = channel.map( FileChannel.MapMode.READ_ONLY, 0, f.length() );

        FileHeaderCodec codec = new FileHeaderCodec( FileHeader.class );

        WorkingContext ctx = new WorkingContext( new Properties());
        DefaultCodecFactory dcf = new DefaultCodecFactory();
        ctx.setDefaultCodecFactory( dcf );
        ctx.setByteBuffer( bb );
        FileHeader fileHeader = codec.decode( bb, ctx );
        assertNotNull( "fileHeader", fileHeader );
        assertEquals( "version", 81, fileHeader.getIntVersion() );
        assertEquals( "byteorder", 0, fileHeader.getByteOrder().byteValue() );
        assertEquals( "toc", 105, fileHeader.getTocOffset().intValue() );
        assertEquals( "reserved field", 0, fileHeader.getReservedFieldI().intValue() );
        assertNotNull( "lsg segment id", fileHeader.getLsgSegmentId() );
        assertNull( "reserved field GUID", fileHeader.getReservedFieldG() );

        File file = new File( testDir, "test81.xml" );

        JAXBContext jaxbContext = JAXBContext.newInstance( FileHeader.class );
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
        jaxbMarshaller.marshal( fileHeader, file );

    }

    @Test
    public void test90() throws Exception {

        File f = new File( "src/test/ressources/jt/box1.jt" );
        FileInputStream fis = new FileInputStream( f );
        FileChannel channel = fis.getChannel();
        ByteBuffer bb = channel.map( FileChannel.MapMode.READ_ONLY, 0, f.length() );

        FileHeaderCodec codec = new FileHeaderCodec( FileHeader.class );

        WorkingContext ctx = new WorkingContext( new Properties());
        DefaultCodecFactory dcf = new DefaultCodecFactory();
        ctx.setDefaultCodecFactory( dcf );
        ctx.setByteBuffer( bb );
        FileHeader fileHeader = codec.decode( bb, ctx );

        assertNotNull( "fileHeader", fileHeader );
        assertEquals( "version", 90, fileHeader.getIntVersion() );
        assertEquals( "byteorder", 0, fileHeader.getByteOrder().byteValue() );
        assertEquals( "toc", 105, fileHeader.getTocOffset().intValue() );

        File file = new File( testDir, "test90.xml" );

        JAXBContext jaxbContext = JAXBContext.newInstance( FileHeader.class );
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
        jaxbMarshaller.marshal( fileHeader, file );
     
        

    }
}
