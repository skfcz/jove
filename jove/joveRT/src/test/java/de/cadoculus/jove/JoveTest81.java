/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove;

import de.cadoculus.jove.enums.ObjectBaseType;
import de.cadoculus.jove.enums.SegmentType;
import de.cadoculus.jove.base.BBoxF32;
import de.cadoculus.jove.base.LogicalElementHeaderZLIB;
import de.cadoculus.jove.base.ObjectData;
import de.cadoculus.jove.base.SegmentHeader;
import de.cadoculus.jove.enums.ObjectTypeIdentifier;
import de.cadoculus.jove.fileheader.FileHeader;
import de.cadoculus.jove.lsgsegment.LSGSegment;
import de.cadoculus.jove.lsgsegment.PartitionNodeElement;
import de.cadoculus.jove.metadatasegment.MetaDataSegment;
import de.cadoculus.jove.metadatasegment.PMIEntities;
import de.cadoculus.jove.metadatasegment.PMIManagerMetaDataElement;
import de.cadoculus.jove.metadatasegment.PMIStringTable;
import de.cadoculus.jove.tocsegment.TOCEntry;
import de.cadoculus.jove.tocsegment.TOCSegment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * * Tests the JoveReader with files in version 8.0 and 8.1
 * @author Zerbst
 */
public class JoveTest81 {

    public static Logger log = LoggerFactory.getLogger( JoveTest81.class );
    private static File testDir;

    @BeforeClass
    public static void setUpClass() throws Exception {
        BasicConfigurator.resetConfiguration();

        java.io.File logconfig = new java.io.File( "log4j.properties" );

        if ( logconfig.exists() && logconfig.canRead() ) {

            try {
                URL propsURL = new URL( "file:" + logconfig.getName() );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {

                BasicConfigurator.resetConfiguration();
                BasicConfigurator.configure();

            }
        }

        testDir = new File( "target/test-out/JoveReaderTest" );
//        Files.delete(testDir.toPath() );
        testDir.mkdirs();

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of read method, of class JoveReader.
     */
    @Test
    public void readSpat_LSG() throws Exception {
        File f = new File( "src/test/ressources/jt/spat.jt" );
        FileInputStream fis = new FileInputStream( f );
        FileChannel channel = fis.getChannel();
        ByteBuffer bb = channel.map( FileChannel.MapMode.READ_ONLY, 0, f.length() );
        JoveReader instance = new JoveReader( bb, null );
        Jove jove = instance.read();
        FileHeader fh = jove.getFileHeader();

        File myDir = new File( testDir, "testRead" );
        myDir.mkdir();
        File xml = new File( myDir, "spat_LSG.xml" );
        FileOutputStream fos = new FileOutputStream( xml );
        JAXBContext context = JAXBContext.newInstance( Jove.class.getPackage().getName() + ":" + //
                FileHeader.class.getPackage().getName() + ":" + //
                TOCEntry.class.getPackage().getName() + ":" + //
                LSGSegment.class.getPackage().getName() + ":" + //
                BBoxF32.class.getPackage().getName() + ":" + //
                MetaDataSegment.class.getPackage().getName() + ":" + //
                ObjectTypeIdentifier.class.getPackage().getName() );
        Marshaller m = context.createMarshaller();
        m.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
        m.marshal( jove, fos );
        fos.close();

        // Test the read values and compare them with values read
        // using a hex editor
        assertNotNull( "header", fh );
        assertEquals( "version", 81, fh.getIntVersion() );
        assertEquals( "toc offset", 105, fh.getTocOffset().intValue() );

        TOCSegment toc = jove.getTocSegment();

        assertNotNull( "toc", toc );

        assertEquals( "num sequences ", 3, jove.getSegments().size() );

        for ( SegmentHeader sh : jove.getSegments() ) {
            if ( SegmentType.LOGICAL_SCENE_GRAPH == sh.getSegmentType() ) {
                assertEquals( "length ", 613, sh.getSegmentLength() );

                ObjectData objectData = sh.getObjectData();
                assertNotNull( "objectData", objectData );
                assertEquals( "class", LSGSegment.class, objectData.getClass() );

                LSGSegment lsgSeg = (LSGSegment) objectData;
                assertNotNull( "graph elemements", lsgSeg.getGraphElements() );
                assertEquals( "graph elemements #", 4, lsgSeg.getGraphElements().size() );

                PartitionNodeElement pne = (PartitionNodeElement) lsgSeg.getGraphElements().get( 0 );
                assertEquals( "pne el length", 135, pne.getElementLength() );
                assertEquals( "pne el base type", ObjectBaseType.GROUP_GRAPH_NODE_OBJECT, pne.getObjectBaseType() );

                log.info( "PartitionNodeElement objectID" + pne.getObjectID() );

            }

        }

    }

    /**
     * Test of read method, of class JoveReader.
     */
    @Test
    public void readSpat_PMI_DATA() throws Exception {
        File f = new File( "src/test/ressources/jt/spat.jt" );
        FileInputStream fis = new FileInputStream( f );
        FileChannel channel = fis.getChannel();
        ByteBuffer bb = channel.map( FileChannel.MapMode.READ_ONLY, 0, f.length() );
        JoveReader instance = new JoveReader( bb, null );
        Jove jove = instance.read();

        assertEquals( "num sequences ", 3, jove.getSegments().size() );
        SegmentHeader segmentHeader = null;
        for ( SegmentHeader sh : jove.getSegments() ) {
            if ( SegmentType.PMI_DATA == sh.getSegmentType() ) {
                segmentHeader = sh;
            }
        }
        assertNotNull( "found PMI_DATA segment", segmentHeader );
        assertEquals("segment offset ", 1752, segmentHeader.getOffset());
        log.info( "\nread PMI_DATA segment\n");
        
        SegmentHeader pmiDataSegment = instance.readDataSegment( segmentHeader );

        File myDir = new File( testDir, "testRead" );
        myDir.mkdir();
        File xml = new File( myDir, "spat_PMI_DATA.xml" );
        FileOutputStream fos = new FileOutputStream( xml );
        JAXBContext context = JAXBContext.newInstance( Jove.class.getPackage().getName() + ":" + //
                FileHeader.class.getPackage().getName() + ":" + //
                TOCEntry.class.getPackage().getName() + ":" + //
                LSGSegment.class.getPackage().getName() + ":" + //
                BBoxF32.class.getPackage().getName() + ":" + //
                MetaDataSegment.class.getPackage().getName() + ":" + //
                ObjectTypeIdentifier.class.getPackage().getName() );
        Marshaller m = context.createMarshaller();
        m.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
        m.marshal( jove, fos );
        fos.close();

        ObjectData objectData = pmiDataSegment.getObjectData();
        assertNotNull( "objectData", objectData );
        assertEquals( "class", MetaDataSegment.class, objectData.getClass() );

        MetaDataSegment mdSeg = (MetaDataSegment) objectData;
        List<LogicalElementHeaderZLIB> metaDataElements = mdSeg.getMetaDataElements();
        assertEquals( "metaDataElements #", 1, metaDataElements.size() );

        LogicalElementHeaderZLIB leh = metaDataElements.get( 0 );
        assertEquals( "metaDataElements 0", PMIManagerMetaDataElement.class, leh.getClass() );

        PMIManagerMetaDataElement pmmde = (PMIManagerMetaDataElement) leh;
        assertEquals( "version number", 8, (int) pmmde.getPmiVersionNumber() );

        PMIEntities pmiEntities = pmmde.getPmiEntities();
        assertEquals( "PMIEntities/PMIDimensionEntities#dimensionCount", 0, pmiEntities.getPmiDimensionEntities().getDimensionCount().intValue() );
        assertEquals( "PMIEntities/PMINoteEntities#noteCount", 0, pmiEntities.getPmiNoteEntities().getNoteCount().intValue() );
        assertEquals( "PMIEntities/PMIDatumFeatureSymbolEntities#dfsCount", 0, pmiEntities.getPmiDatumFeatureSymbolEntities().getDfsCount().intValue() );
        assertEquals( "PMIEntities/PMIDatumTargetEntities#datumTargetCount", 0, pmiEntities.getPmiDatumTargetEntities().getDatumTargetCount().intValue() );
        assertEquals( "PMIEntities/PMIFeatureControlFrameEntities#fcfCount", 0, pmiEntities.getPmiFeatureControlFrameEntities().getFcfCount().intValue() );
        assertEquals( "PMIEntities/PMILineWeldEntities#lineWeldCount", 0, pmiEntities.getPmiLineWeldEntities().getLineWeldCount().intValue() );
        assertEquals( "PMIEntities/PMISpotWeldEntities#spotWeldCount", 0, pmiEntities.getPmiSpotWeldEntities().getSpotWeldCount().intValue() );
        assertEquals( "PMIEntities/PMISurfaceFinishEntities#sfCount", 0, pmiEntities.getPmiSurfaceFinishEntities().getSfCount().intValue() );
        assertEquals( "PMIEntities/PMIMeasurementPointEntities#mpCount", 0, pmiEntities.getPmiMeasurementPointEntities().getMpCount().intValue() );
        assertEquals( "PMIEntities/PMILocatorEntities#locatorCount", 0, pmiEntities.getPmiLocatorEntities().getLocatorCount().intValue() );
        assertEquals( "PMIEntities/PMIReferenceGeometryEntities#referenceGeometryCount", 0, pmiEntities.getPmiReferenceGeometryEntities().getReferenceGeometryCount().intValue() );
        assertEquals( "PMIEntities/PMIDesignGroupEntities#designGroupCount", 0, pmiEntities.getPmiDesignGroupEntities().getDesignGroupCount().intValue() );
        assertEquals( "PMIEntities/PMICoordinateSystemEntities#coordSysCount", 0, pmiEntities.getPmiCoordinateSystemEntities().getCoordSysCount().intValue() );

        assertEquals( "PMIAssociations#associationCount", 0, pmmde.getPmiAssociations().getAssociationCount().intValue() );
        assertEquals( "PMIUserAttributes#userAttributeCount", 0, pmmde.getPmiUserAttributes().getUserAttributeCount().intValue() );

        PMIStringTable pmiStringTable = pmmde.getPmiStringTable();
        assertEquals( "PMIStringTable#stringCount", 8, pmmde.getPmiStringTable().getStringCount().intValue() );

        List<String> pmiString = pmmde.getPmiStringTable().getPmiString();
        assertNotNull( "PMIStringTable#pmiString", pmiString );
        assertEquals( "PMIStringTable#pmiString #", 8, pmmde.getPmiStringTable().getPmiString().size() );

    }

   }
