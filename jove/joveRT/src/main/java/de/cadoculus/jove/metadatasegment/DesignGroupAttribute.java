/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.12.1 Design Group Attribute</h4>
The Design Group Attribute data collection defines a group property/attribute.

*/
@XmlRootElement(name="DesignGroupAttribute")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "DesignGroupAttribute",
    propOrder= {"attributeType", "integerValue", "doubleValue", "stringValueStringID", "labelStringID", "descriptionStringID", }
)
public class DesignGroupAttribute {

    public static Logger log = LoggerFactory.getLogger( DesignGroupAttribute.class );

    /**  
     * The variable number 0 : I32 attributeType
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer attributeType;

    /**  
     * The variable number 1 : I32 integerValue
     * <br>options : <pre>{valid : obj.attributeType ==1}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.attributeType ==1" )
    private Integer integerValue;

    /**  
     * The variable number 2 : F64 doubleValue
     * <br>options : <pre>{valid : obj.attributeType ==2}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.F64()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.attributeType ==2" )
    private Double doubleValue;

    /**  
     * The variable number 3 : I32 stringValueStringID
     * <br>options : <pre>{valid : obj.attributeType ==3}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.attributeType ==3" )
    private Integer stringValueStringID;

    /**  
     * The variable number 4 : I32 labelStringID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer labelStringID;

    /**  
     * The variable number 5 : I32 descriptionStringID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer descriptionStringID;



    /** Getter for attributeType.
     *  
     * 
     * @return Integer
     */
    public Integer getAttributeType() {
        return attributeType;
    }

     /** Setter for attributeType.
     *  
     * 
     * @param value Integer
     */
    public void setAttributeType( Integer value ) {
        this.attributeType = value;
    }

    /** Getter for integerValue.
     *  
     * 
     * @return Integer
     */
    public Integer getIntegerValue() {
        return integerValue;
    }

     /** Setter for integerValue.
     *  
     * 
     * @param value Integer
     */
    public void setIntegerValue( Integer value ) {
        this.integerValue = value;
    }

    /** Getter for doubleValue.
     *  
     * 
     * @return Double
     */
    public Double getDoubleValue() {
        return doubleValue;
    }

     /** Setter for doubleValue.
     *  
     * 
     * @param value Double
     */
    public void setDoubleValue( Double value ) {
        this.doubleValue = value;
    }

    /** Getter for stringValueStringID.
     *  
     * 
     * @return Integer
     */
    public Integer getStringValueStringID() {
        return stringValueStringID;
    }

     /** Setter for stringValueStringID.
     *  
     * 
     * @param value Integer
     */
    public void setStringValueStringID( Integer value ) {
        this.stringValueStringID = value;
    }

    /** Getter for labelStringID.
     *  
     * 
     * @return Integer
     */
    public Integer getLabelStringID() {
        return labelStringID;
    }

     /** Setter for labelStringID.
     *  
     * 
     * @param value Integer
     */
    public void setLabelStringID( Integer value ) {
        this.labelStringID = value;
    }

    /** Getter for descriptionStringID.
     *  
     * 
     * @return Integer
     */
    public Integer getDescriptionStringID() {
        return descriptionStringID;
    }

     /** Setter for descriptionStringID.
     *  
     * 
     * @param value Integer
     */
    public void setDescriptionStringID( Integer value ) {
        this.descriptionStringID = value;
    }


    
}

