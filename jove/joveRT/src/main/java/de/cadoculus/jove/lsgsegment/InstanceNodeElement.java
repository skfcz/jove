/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="InstanceNodeElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "InstanceNodeElement",
    propOrder= {"baseNodeData", "versionNumber", "childNodeObjectId", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0x10dd102a, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97" )
public class InstanceNodeElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( InstanceNodeElement.class );

    /**  
     * The variable number 0 : BaseNodeData baseNodeData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="BaseNodeData")
    private BaseNodeData baseNodeData;

    /**  
     * The variable number 1 : I16 versionNumber
     * <br>options : <pre>{valid : jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private Short versionNumber;

    /**  
     * The variable number 2 : I32 childNodeObjectId
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer childNodeObjectId;



    /** Getter for baseNodeData.
     *  
     * 
     * @return BaseNodeData
     */
    public BaseNodeData getBaseNodeData() {
        return baseNodeData;
    }

     /** Setter for baseNodeData.
     *  
     * 
     * @param value BaseNodeData
     */
    public void setBaseNodeData( BaseNodeData value ) {
        this.baseNodeData = value;
    }

    /** Getter for versionNumber.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumber() {
        return versionNumber;
    }

     /** Setter for versionNumber.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumber( Short value ) {
        this.versionNumber = value;
    }

    /** Getter for childNodeObjectId.
     *  
     * 
     * @return Integer
     */
    public Integer getChildNodeObjectId() {
        return childNodeObjectId;
    }

     /** Setter for childNodeObjectId.
     *  
     * 
     * @param value Integer
     */
    public void setChildNodeObjectId( Integer value ) {
        this.childNodeObjectId = value;
    }


    
}

