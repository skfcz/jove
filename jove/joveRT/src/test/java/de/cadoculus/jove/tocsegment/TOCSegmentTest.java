/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cadoculus.jove.tocsegment;

import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.codec.DefaultCodecFactory;
import de.cadoculus.jove.fileheader.FileHeader;
import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.Properties;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Zerbst
 */
public class TOCSegmentTest {

    public static Logger log = LoggerFactory.getLogger( TOCSegmentTest.class );
    
    @BeforeClass
    public static void setUpClass() throws Exception {
        BasicConfigurator.resetConfiguration();

        java.io.File logconfig = new java.io.File( "log4j.properties" );

        if ( logconfig.exists() && logconfig.canRead() ) {

            try {
                URL propsURL = new URL( "file:" + logconfig.getName() );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {

                BasicConfigurator.resetConfiguration();
                BasicConfigurator.configure();

            }
        }
    }
    private File testDir;

    @Before
    public void setUp() throws Exception {

        testDir = new File( "target/test-out/TOCSegmentTest" );
        testDir.mkdirs();
    }

    @Test
    public void test81() throws Exception {

        File f = new File( "src/test/ressources/jt/model1.jt" );
        FileInputStream fis = new FileInputStream( f );
        FileChannel channel = fis.getChannel();
        ByteBuffer buffer = channel.map( FileChannel.MapMode.READ_ONLY, 0, f.length() );

        DefaultCodecFactory cfact = new DefaultCodecFactory();
        WorkingContext ctx = new WorkingContext( new Properties());
        ctx.setDefaultCodecFactory( cfact );
        ctx.setByteBuffer( buffer );

        Codec<FileHeader> codec = cfact.create( null, FileHeader.class, ctx );
        buffer.position( 0 );
        FileHeader fh = codec.decode( buffer, ctx );
        ctx.setFileHeader( fh );

        buffer.position( fh.getTocOffset() );
        Codec<TOCSegment> tocCodec = cfact.create( null, TOCSegment.class, ctx );
        TOCSegment tocSegment = tocCodec.decode( buffer, ctx );

        assertEquals( "tocSegment", 3, tocSegment.getEntryCount().intValue() );
        List<TOCEntry> tocEntries = tocSegment.getTocEntries();
        assertNotNull( "tocEntries", tocEntries );

        assertEquals( "tocEntries #", 3, tocEntries.size() );
        
        File  file = new File(testDir, "test81.xml");

        JAXBContext jaxbContext = JAXBContext.newInstance( TOCSegment.class );
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
        jaxbMarshaller.marshal( tocSegment, file );


    
    }
}
