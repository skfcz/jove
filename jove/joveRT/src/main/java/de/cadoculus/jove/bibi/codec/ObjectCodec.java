/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.BIBIValid;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * The ObjectCodec is used to create an fill an POJO of the given class.
 *
 * @author Zerbst
 */
public class ObjectCodec<T> implements Codec<T> {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( ObjectCodec.class );
    protected final Class<T> type;
    protected T myObject;
    protected final List<Object> alreadyParsedValues;

    /**
     * Construct an ObjectCodec for the given type.
     *
     * @param type
     */
    public ObjectCodec( Class<T> type ) throws BIBIException {
        if ( type == null ) {
            throw new IllegalArgumentException( "expect none null type" );
        }
        this.type = type;
        this.alreadyParsedValues = new ArrayList<>();

    }

    /**
     * Construct an ObjectCodec for the given type, but skip the first
     * <it>n</it>
     * fields.
     *
     * @param type the Class to create in the codec
     * @param alreadyParsedValues a List of already parsed values. Those values
     * are not read by the ObjectCodec, but inserted into the new Object from
     * the List
     */
    public ObjectCodec( Class<T> type, List<Object> alreadyParsedValues ) throws BIBIException {
        if ( type == null ) {
            throw new IllegalArgumentException( "expect none null type" );
        }
        this.type = type;
        this.alreadyParsedValues = alreadyParsedValues;
    }

    @Override
    public T decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        MDC.put( WorkingContext.OBJECT_TYPE, type.getSimpleName() );
        MDC.put( WorkingContext.FIELD, "" );
        MDC.put( WorkingContext.POSITION, Long.toString( input.position() ) );
        MDC.put( WorkingContext.CODEC, getClass().getSimpleName() );

        CodecUtility.checkMaxPosition( input, ctx );

        // Collect information about the bound object
        // and create the needed codecs
        LinkedHashMap<Field, Codec> field2codec = collectBoundedFields( type, ctx );
        if ( log.isDebugEnabled() ) {
            int pos = 0;
            for ( Iterator<Field> it = field2codec.keySet().iterator(); it.hasNext(); ) {
                Field field = it.next();
                log.debug( "    field #" + ( pos++ ) + " " + field.getType() + " " + field.getName() + " : " + field2codec.get( field ) );
            }
        }

        // Create a new Object of the desired type
        myObject = createObject( ctx );
        ctx.setObj( myObject );

        // Read the values from the binary stream and insert
        // into newly created object
        Field field = null;
        Object value = null;
        try {
            Iterator<Field> fieldIt = field2codec.keySet().iterator();
            if ( alreadyParsedValues != null && !alreadyParsedValues.isEmpty() ) {
                for ( Object parsedValue : alreadyParsedValues ) {
                    value = parsedValue;
                    field = fieldIt.next();

                    MDC.put( WorkingContext.FIELD, field.getName() );

                    field.setAccessible( true );
                    field.set( myObject, value );
                    log.info( "put value '" + value + "' into " + field.getName() );
                    postAction( field, value, ctx );
                }
            }

            FIELD:
            while ( fieldIt.hasNext() ) {

                field = fieldIt.next();
                MDC.put( WorkingContext.FIELD, field.getName() );
                MDC.put( WorkingContext.OBJECT_TYPE, type.getSimpleName() );
                MDC.put( WorkingContext.POSITION, Long.toString( input.position() ) );
                MDC.put( WorkingContext.CODEC, getClass().getSimpleName() );

                ctx.setObj( myObject );

                if ( !fieldIsValid( field, ctx ) ) {
                    log.info( "skip " + field.getName() );
                    continue FIELD;
                }

                Codec codec = field2codec.get( field );
                log.info( "field " + field.getName() + ", codec " + codec );

                codec = updateCodec( field, codec, ctx );

                value = codec.decode( input, ctx );
                ctx.setObj( myObject );

                log.info( "read " + value + " (" + ( value != null ? value.getClass() : "no class" ) + ")" );
                field.setAccessible( true );
                field.set( myObject, value );
                log.info( "put value into " + field );

                // log.info( "apply postAction for " + field.getName() );
                postAction( field, value, ctx );
            }
        } catch ( IllegalArgumentException ex ) {
            log.error( "failed to set value of field " + type.getSimpleName() + "#" + field.getName() + " to " + value );
            log.error( "    field " + field.getType() );
            log.error( "    value " + value.getClass() );
            log.error( "    reason ", ex );
            throw new BIBIException( "failed to set value of field " + type.getSimpleName() + "#" + field.getName() + " to " + value, ex );
        } catch ( IllegalAccessException ex ) {
            throw new BIBIException( "denied to set value of field " + type.getSimpleName() + "#" + field.getName() + " to " + value, ex );
        } catch ( BIBIException ex ) {
            handleException( ex, field );
        }

        return myObject;
    }

    private static void recCollectFields( Class type, LinkedHashMap<Field, Codec> field2codec, WorkingContext ctx ) throws BIBIException {

        if ( type.getSuperclass() != null ) {
            recCollectFields( type.getSuperclass(), field2codec, ctx );
        }

        Field[] declaredFields = type.getDeclaredFields();

        FIELDS:
        for ( int i = 0; i < declaredFields.length; i++ ) {
            Field field = declaredFields[i];
            // We do not care about static fields
            if ( Modifier.isStatic( field.getModifiers() ) ) {
                //log.info( "skip static field " + field );
                continue FIELDS;
            }
            // We do not care about fields created by the compiler
            // without underlying code
            if ( field.isSynthetic() ) {
                log.debug( "skip synthetic field " + field );
                continue FIELDS;
            }
            // We do not care about fields without annotation
            if ( !CodecUtility.isBoundField( field ) ) {
                log.debug( "skip unbound field " + field );
                continue FIELDS;
            }

            Class fieldType = field.getType();

            Codec<?> subCodec = ctx.getDefaultCodecFactory().create( field, fieldType, ctx );
            if ( subCodec == null ) {
                log.warn( "found no codec for " + field.getName() );
            }
            //log.info( "created codec " + subCodec + " for " + field.getName() + ", " + Arrays.toString( field.getAnnotations() ) );
            field2codec.put( field, subCodec );
        }

    }

    /**
     * Collect information on the bounded fields
     *
     * @param ctx the WorkingContext
     * @return a map containing the Fields and the codec to use
     * @throws BIBIException
     */
    public static LinkedHashMap<Field, Codec> collectBoundedFields( Class theType, WorkingContext ctx ) throws BIBIException {

        LinkedHashMap<Field, Codec> field2codec = new LinkedHashMap<>();

        recCollectFields( theType, field2codec, ctx );

        return field2codec;
    }

    /**
     * This method is called before the Codec is applied to decode a field. In
     * case that a codec has to be exchanged by another one after the data in
     * the current object has been read in, this is the chance to replace the
     * original codec created by the
     * {@link #collectBoundedFields(de.cadoculus.jove.bibi.WorkingContext)}
     * method by another one. The default implementation simply returns the
     * originalCodec.
     *
     *
     *
     * @param field the next Field to read
     * @param originalCodec the Codec created by {@link #collectBoundedFields(de.cadoculus.jove.bibi.WorkingContext)
     * @param ctx the current WorkingContext
     * @return the Codec to apply. In case that null is returned the complete
     * field is skipped
     */
    protected Codec updateCodec( Field field, Codec originalCodec, WorkingContext ctx ) throws BIBIException {
        return originalCodec;
    }

    /**
     * This method is called after a Field has been decoded. The default
     * implementation does nothing.
     *
     * @param field the last Field read in
     * @param value the value read from the codec
     * @param ctx the current WorkingContext
     */
    protected void postAction( Field field, Object value, WorkingContext ctx ) {
        return;
    }

    /**
     * Create the object to fill
     *
     * @param ctx the WorkingContext
     * @return the object
     * @throws BIBIException
     */
    protected T createObject( WorkingContext ctx ) throws BIBIException {
        T retval = null;
        try {
            retval = type.newInstance();
        } catch ( InstantiationException ex ) {
            throw new BIBIException( "failed to create new object", ex );
        } catch ( IllegalAccessException ex ) {
            throw new BIBIException( "denied to create new object", ex );
        }
        log.info( "created new object of " + type );

        return retval;
    }

    /**
     * Checks whether a given Field is annotated with BIBIValid and evaluates
     * the expression
     *
     * @param field the Field to check
     * @param ctx the current WorkingContext
     * @return true if the field should be parsed
     * @throws BIBIException
     */
    protected boolean fieldIsValid( Field field, WorkingContext ctx ) throws BIBIException {
        BIBIValid valid = field.getAnnotation( BIBIValid.class );
        if ( valid != null ) {
            return CodecUtility.evaluate( valid, ctx );
        }
        return true;
    }

    @Override
    public String toString() {
        return "ObjectCodec " + type.getSimpleName();
    }

    /**
     * This method is called if an error BIBIException ccured when reading the
     * fields of an object. The default implementation simply rethrows the
     * caught BIBIException. Derived codecs may decide to perform specific
     * action.
     *
     * @param ex the caught Exception
     * @param field the field which was read when the error occured
     * @throws BIBIException the exception to be thrown from {@link ObjectCodec#decode
     * } method.
     *
     */
    protected void handleException( BIBIException ex, Field field ) throws BIBIException {
        throw ex;
    }
}
