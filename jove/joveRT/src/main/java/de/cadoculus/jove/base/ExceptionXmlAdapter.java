/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.base;

import de.cadoculus.jove.bibi.BIBIException;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * The class adapts an exception to be written as XML.
 *
 * @author cz
 */
public class ExceptionXmlAdapter extends XmlAdapter< String, BIBIException> {

    @Override
    public String marshal( BIBIException exception ) throws Exception {
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        exception.printStackTrace(new  PrintStream(bos));
        return bos.toString();
        
    }

    @Override
    public BIBIException unmarshal( String v ) throws Exception {
        throw new UnsupportedOperationException( "Not supported yet." ); //To change body of generated methods, choose Tools | Templates.
    }

}
