/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi;

import java.nio.ByteBuffer;

/**
 * This interface describes classes which are used to deserialize a binary
 * stream into a Java object.
 *
 * @param <T> The type of object the {@link Codec} is able to decode/encode.
 * @author cz
 */
public interface Codec<T> {

    /**
     * Deserialize the given binary stream into a Java object.
     *
     * @param input the ByteBuffer to read from
     * @param ctx the WorkingContex to use
     * @return the read Java object
     */
    T decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException;
}
