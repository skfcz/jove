/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.1.1.2.1 Text Box</h4>
The Text Box data collection specifies a 2D box that particular text fits within. All values are with respect to 2D-Reference Frame documented in 7.2.6.2.1.1.1.1.1 2D-Reference Frame.

*/
@XmlRootElement(name="TextBox")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "TextBox",
    propOrder= {"originXCoord", "originYCoord", "lowerRightCornerXCoord", "lowerRightCornerYCoord", "upperLeftCornerXCoord", "upperLeftCornerYCoord", }
)
public class TextBox {

    public static Logger log = LoggerFactory.getLogger( TextBox.class );

    /**  
     * The variable number 0 : F32 originXCoord
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float originXCoord;

    /**  
     * The variable number 1 : F32 originYCoord
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float originYCoord;

    /**  
     * The variable number 2 : F32 lowerRightCornerXCoord
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float lowerRightCornerXCoord;

    /**  
     * The variable number 3 : F32 lowerRightCornerYCoord
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float lowerRightCornerYCoord;

    /**  
     * The variable number 4 : F32 upperLeftCornerXCoord
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float upperLeftCornerXCoord;

    /**  
     * The variable number 5 : F32 upperLeftCornerYCoord
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float upperLeftCornerYCoord;



    /** Getter for originXCoord.
     *  
     * 
     * @return Float
     */
    public Float getOriginXCoord() {
        return originXCoord;
    }

     /** Setter for originXCoord.
     *  
     * 
     * @param value Float
     */
    public void setOriginXCoord( Float value ) {
        this.originXCoord = value;
    }

    /** Getter for originYCoord.
     *  
     * 
     * @return Float
     */
    public Float getOriginYCoord() {
        return originYCoord;
    }

     /** Setter for originYCoord.
     *  
     * 
     * @param value Float
     */
    public void setOriginYCoord( Float value ) {
        this.originYCoord = value;
    }

    /** Getter for lowerRightCornerXCoord.
     *  
     * 
     * @return Float
     */
    public Float getLowerRightCornerXCoord() {
        return lowerRightCornerXCoord;
    }

     /** Setter for lowerRightCornerXCoord.
     *  
     * 
     * @param value Float
     */
    public void setLowerRightCornerXCoord( Float value ) {
        this.lowerRightCornerXCoord = value;
    }

    /** Getter for lowerRightCornerYCoord.
     *  
     * 
     * @return Float
     */
    public Float getLowerRightCornerYCoord() {
        return lowerRightCornerYCoord;
    }

     /** Setter for lowerRightCornerYCoord.
     *  
     * 
     * @param value Float
     */
    public void setLowerRightCornerYCoord( Float value ) {
        this.lowerRightCornerYCoord = value;
    }

    /** Getter for upperLeftCornerXCoord.
     *  
     * 
     * @return Float
     */
    public Float getUpperLeftCornerXCoord() {
        return upperLeftCornerXCoord;
    }

     /** Setter for upperLeftCornerXCoord.
     *  
     * 
     * @param value Float
     */
    public void setUpperLeftCornerXCoord( Float value ) {
        this.upperLeftCornerXCoord = value;
    }

    /** Getter for upperLeftCornerYCoord.
     *  
     * 
     * @return Float
     */
    public Float getUpperLeftCornerYCoord() {
        return upperLeftCornerYCoord;
    }

     /** Setter for upperLeftCornerYCoord.
     *  
     * 
     * @param value Float
     */
    public void setUpperLeftCornerYCoord( Float value ) {
        this.upperLeftCornerYCoord = value;
    }


    
}

