/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="VertexShapeData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "VertexShapeData",
    propOrder= {"versionNumberVSD", "vertexBinding", "quantizationParameters", "VertexBinding2", }
)
public class VertexShapeData extends BaseShapeData {

    public static Logger log = LoggerFactory.getLogger( VertexShapeData.class );

    /**  
     * The variable number 0 : I16 versionNumberVSD
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumberVSD;

    /**  
     * The variable number 1 : U64 vertexBinding
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U64()    
    private Long vertexBinding;

    /**  
     * The variable number 2 : QuantizationParameters quantizationParameters
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="QuantizationParameters")
    private QuantizationParameters quantizationParameters;

    /**  
     * The variable number 3 : U64 VertexBinding2
     * <br>options : <pre>{valid : obj.versionNumberVSD == 1}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.U64()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.versionNumberVSD == 1" )
    private Long VertexBinding2;



    /** Getter for versionNumberVSD.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberVSD() {
        return versionNumberVSD;
    }

     /** Setter for versionNumberVSD.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberVSD( Short value ) {
        this.versionNumberVSD = value;
    }

    /** Getter for vertexBinding.
     *  
     * 
     * @return Long
     */
    public Long getVertexBinding() {
        return vertexBinding;
    }

     /** Setter for vertexBinding.
     *  
     * 
     * @param value Long
     */
    public void setVertexBinding( Long value ) {
        this.vertexBinding = value;
    }

    /** Getter for quantizationParameters.
     *  
     * 
     * @return QuantizationParameters
     */
    public QuantizationParameters getQuantizationParameters() {
        return quantizationParameters;
    }

     /** Setter for quantizationParameters.
     *  
     * 
     * @param value QuantizationParameters
     */
    public void setQuantizationParameters( QuantizationParameters value ) {
        this.quantizationParameters = value;
    }

    /** Getter for VertexBinding2.
     *  
     * 
     * @return Long
     */
    public Long getVertexBinding2() {
        return VertexBinding2;
    }

     /** Setter for VertexBinding2.
     *  
     * 
     * @param value Long
     */
    public void setVertexBinding2( Long value ) {
        this.VertexBinding2 = value;
    }


    
}

