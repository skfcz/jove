/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.BIBICodec;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This factory creates Codecs for all kind of objects contained in the data
 * segments. Either using the default ObjectCodec or when a
 *
 * @BIBICodec annotation is given the Codec named in the annotation.
 *
 * @author Zerbst
 */
public class ObjectCodecFactory extends AbstractCodecFactory {

    private static Logger log = LoggerFactory.getLogger( ObjectCodecFactory.class );

    @Override
    public <T> Codec<T> create( AnnotatedElement metadata, Class<T> type, WorkingContext ctx ) throws BIBIException {
        //log.info( "create codec for " + metadata + ", " + type + ", " + ctx, new Exception() );
        // Do not care about some categories of types
        if ( type.isPrimitive() ) {
            return null;
        }
        if ( type.isEnum() ) {
            return null;
        }
        if ( type.isInterface() ) {
            return null;
        }
        if ( Collection.class.isAssignableFrom( type ) ) {
            return null;
        }
        if ( Array.class.isAssignableFrom( type ) ) {
            return null;
        }
        //log.info( "passed initial tests" );

        // 
        // Check if class has an annotation for a specific codec
        //
        BIBICodec bibiCodec = type.getAnnotation( BIBICodec.class );
        if ( bibiCodec != null ) {
            Class codecClass = null;
            try {
                codecClass = bibiCodec.value();
                Constructor constructor = codecClass.getConstructor( Class.class );
                return (Codec<T>) constructor.newInstance( type );
            } catch ( Exception ex ) {
                log.error( "failed to create codec " + codecClass, ex );
                throw new BIBIException( "failed to create new instance of " + codecClass + " for " + type.getSimpleName(), ex );
            }
        }

        //
        // Check that at least one field is bound
        //
        Field[] fields = type.getDeclaredFields();
        if ( fields.length == 0 ) {
            throw new BIBIException( "could not bind " + type.toString() + " without fields" );
        }
        boolean foundBoundedField = false;
        for ( Field field : fields ) {

            // We do not care about static fields
            if ( Modifier.isStatic( field.getModifiers() ) ) {
                continue;
            }
            // We do not care about fields created by the compiler
            // without underlying code
            if ( field.isSynthetic() ) {
                continue;
            }

            if ( CodecUtility.isBoundField( field ) ) {
                foundBoundedField = true;
                break;
            }
        }

        //log.info( "foundBoundedField " + foundBoundedField );
        if ( foundBoundedField ) {

            return new ObjectCodec<T>( type );
        }
        log.warn( "no codec created for " + type + ", " + metadata, new Exception() );

        return null;

    }
}
