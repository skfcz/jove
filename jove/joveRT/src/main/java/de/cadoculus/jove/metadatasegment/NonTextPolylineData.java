/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.1.1.3 Non-Text Polyline Data</h4>
The Non-Text Polyline Data collection contains all the non-text polylines making up the particular PMI entity. Examples of non-text polylines include line attachments, text boxes, symbol box dividers, etc. The Non-Text Polyline Data collection is made up of an array of indices into an array of polyline segments packed as either 2D or 3D vertex coordinates, specifying where each polyline segment begins and ends. Whether vertex coordinates are 2D or 3D is dependent upon the PMI entity type using this data collection. If it is a 7.2.6.2.6 Generic PMI Entities type then the packed coordinate data is 3D; for all other PMI entity types the packed coordinate data is 2D. Also for Version Number, as defined in 7.2.6.2 PMI Manager Meta Data Element, greater than "4" an array of values that sequentially specify the polyline type in the polyline segments array is included.

*/
@XmlRootElement(name="NonTextPolylineData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "NonTextPolylineData",
    propOrder= {"polylineSegmentIndexCount", "polylineSegmentIndex", "polylineTypeCount", "polylineType", "polylineVertexCoords", }
)
public class NonTextPolylineData {

    public static Logger log = LoggerFactory.getLogger( NonTextPolylineData.class );

    /**  
     * The variable number 0 : I32 polylineSegmentIndexCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer polylineSegmentIndexCount;

    /**  
     * The variable number 1 : I16[] polylineSegmentIndex
     * <br>options : <pre>{length : obj.polylineSegmentIndexCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.polylineSegmentIndexCount" )
    @XmlElementWrapper(name = "polylineSegmentIndex")
        private java.util.List<Short> polylineSegmentIndex;

    /**  
     * The variable number 2 : I32 polylineTypeCount
     * <br>options : <pre>{valid : pmiVersionNumber > 4}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber > 4" )
    private Integer polylineTypeCount;

    /**  
     * The variable number 3 : PMIPolylineType[] polylineType
     * <br>options : <pre>{length : obj.polylineTypeCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.polylineTypeCount" )
    @XmlElementWrapper(name = "polylineType")
        private java.util.List<PMIPolylineType> polylineType;

    /**  
     * The variable number 4 : VecF32 polylineVertexCoords
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.VecF32()    
    private java.util.List<Float> polylineVertexCoords;



    /** Getter for polylineSegmentIndexCount.
     *  
     * 
     * @return Integer
     */
    public Integer getPolylineSegmentIndexCount() {
        return polylineSegmentIndexCount;
    }

     /** Setter for polylineSegmentIndexCount.
     *  
     * 
     * @param value Integer
     */
    public void setPolylineSegmentIndexCount( Integer value ) {
        this.polylineSegmentIndexCount = value;
    }

    /** Getter for polylineSegmentIndex.
     *  
     * 
     * @return java.util.List<Short>
     */
    public java.util.List<Short> getPolylineSegmentIndex() {
        return polylineSegmentIndex;
    }

     /** Setter for polylineSegmentIndex.
     *  
     * 
     * @param value java.util.List<Short>
     */
    public void setPolylineSegmentIndex( java.util.List<Short> value ) {
        this.polylineSegmentIndex = value;
    }

    /** Getter for polylineTypeCount.
     *  
     * 
     * @return Integer
     */
    public Integer getPolylineTypeCount() {
        return polylineTypeCount;
    }

     /** Setter for polylineTypeCount.
     *  
     * 
     * @param value Integer
     */
    public void setPolylineTypeCount( Integer value ) {
        this.polylineTypeCount = value;
    }

    /** Getter for polylineType.
     *  
     * 
     * @return java.util.List<PMIPolylineType>
     */
    public java.util.List<PMIPolylineType> getPolylineType() {
        return polylineType;
    }

     /** Setter for polylineType.
     *  
     * 
     * @param value java.util.List<PMIPolylineType>
     */
    public void setPolylineType( java.util.List<PMIPolylineType> value ) {
        this.polylineType = value;
    }

    /** Getter for polylineVertexCoords.
     *  
     * 
     * @return java.util.List<Float>
     */
    public java.util.List<Float> getPolylineVertexCoords() {
        return polylineVertexCoords;
    }

     /** Setter for polylineVertexCoords.
     *  
     * 
     * @param value java.util.List<Float>
     */
    public void setPolylineVertexCoords( java.util.List<Float> value ) {
        this.polylineVertexCoords = value;
    }


    
}

