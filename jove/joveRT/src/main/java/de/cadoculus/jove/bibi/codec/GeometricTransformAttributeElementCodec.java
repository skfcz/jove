/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.lsgsegment.GeometricTransformAttributeElement;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.List;

/**
 *
 * @author cz
 */
public class GeometricTransformAttributeElementCodec extends ObjectCodec<GeometricTransformAttributeElement> {

    public static final long MOO_MASK = Long.parseLong( "10000000" + "00000000", 2 );
    public static final long MO1_MASK = Long.parseLong( "01000000" + "00000000", 2 );
    public static final long MO2_MASK = Long.parseLong( "00100000" + "00000000", 2 );
    public static final long MO3_MASK = Long.parseLong( "00010000" + "00000000", 2 );

    public static final long M1O_MASK = Long.parseLong( "00001000" + "00000000", 2 );
    public static final long M11_MASK = Long.parseLong( "00000100" + "00000000", 2 );
    public static final long M12_MASK = Long.parseLong( "00000010" + "00000000", 2 );
    public static final long M13_MASK = Long.parseLong( "00000001" + "00000000", 2 );

    public static final long M2O_MASK = Long.parseLong( "00000000" + "10000000", 2 );
    public static final long M21_MASK = Long.parseLong( "00000000" + "01000000", 2 );
    public static final long M22_MASK = Long.parseLong( "00000000" + "00100000", 2 );
    public static final long M23_MASK = Long.parseLong( "00000000" + "00010000", 2 );

    public static final long M3O_MASK = Long.parseLong( "00000000" + "00001000", 2 );
    public static final long M31_MASK = Long.parseLong( "00000000" + "00000100", 2 );
    public static final long M32_MASK = Long.parseLong( "00000000" + "00000010", 2 );
    public static final long M33_MASK = Long.parseLong( "00000000" + "00000001", 2 );

    public GeometricTransformAttributeElementCodec( Class<GeometricTransformAttributeElement> type ) throws BIBIException {
        super( type );
    }

    public GeometricTransformAttributeElementCodec( Class<GeometricTransformAttributeElement> type, List<Object> alreadyParsedValues ) throws BIBIException {
        super( type );
        this.alreadyParsedValues.addAll( alreadyParsedValues );
    }

    @Override
    protected void postAction( Field field, Object value, WorkingContext ctx ) {
        if ( "storedValuesMask".equals( field.getName() ) ) {
            // we first have to calcuate the number of floats to read

            GeometricTransformAttributeElement gta = (GeometricTransformAttributeElement) ctx.getObj();
            gta.getHoco().setIdentity();

            final int storedValuesMask = gta.getStoredValuesMask();

            BitSet bs = new BitSet();

            for ( int i = 15; i >= 0; i-- ) {

                if ( ( ( 1 << i ) & storedValuesMask ) != 0 ) {
                    bs.set( i );
                } else {
                    bs.clear( i );
                }
            }

            String bbits = new String();

            for ( int j = 0; j < 16; j++ ) {
                bbits += ( bs.get( j ) ? "1" : "0" );
            }

            ByteBuffer b = ctx.getByteBuffer();

            log.info( "bit pattern: " + bbits );
            if ( ( storedValuesMask & MOO_MASK ) > 0 ) {

                gta.getHoco().m00 = b.getFloat();
                log.info( "read m00 " + gta.getHoco().m00 );
            }

            if ( ( storedValuesMask & MO1_MASK ) > 0 ) {
                gta.getHoco().m01 = b.getFloat();
                log.info( "read m01 " + gta.getHoco().m01 );
            }

            if ( ( storedValuesMask & MO2_MASK ) > 0 ) {
                gta.getHoco().m02 = b.getFloat();
                log.info( "read m02 " + gta.getHoco().m02 );
            }

            if ( ( storedValuesMask & MO3_MASK ) > 0 ) {
                gta.getHoco().m03 = b.getFloat();
                log.info( "read m03 " + gta.getHoco().m03 );
            }

            if ( ( storedValuesMask & M1O_MASK ) > 0 ) {
                gta.getHoco().m10 = b.getFloat();
                log.info( "read m10 " + gta.getHoco().m10 );
            }

            if ( ( storedValuesMask & M11_MASK ) > 0 ) {
                gta.getHoco().m11 = b.getFloat();
                log.info( "read m11 " + gta.getHoco().m11 );
            }

            if ( ( storedValuesMask & M12_MASK ) > 0 ) {
                gta.getHoco().m12 = b.getFloat();
                log.info( "read m12 " + gta.getHoco().m12 );
            }

            if ( ( storedValuesMask & M13_MASK ) > 0 ) {
                gta.getHoco().m13 = b.getFloat();
                log.info( "read m13 " + gta.getHoco().m13 );
            }

            if ( ( storedValuesMask & M2O_MASK ) > 0 ) {
                gta.getHoco().m20 = b.getFloat();
                log.info( "read m20 " + gta.getHoco().m20 );
            }

            if ( ( storedValuesMask & M21_MASK ) > 0 ) {
                gta.getHoco().m21 = b.getFloat();
                log.info( "read m21 " + gta.getHoco().m21 );
            }

            if ( ( storedValuesMask & M22_MASK ) > 0 ) {
                gta.getHoco().m22 = b.getFloat();
                log.info( "read m22 " + gta.getHoco().m22 );
            }

            if ( ( storedValuesMask & M23_MASK ) > 0 ) {
                gta.getHoco().m23 = b.getFloat();
                log.info( "read m23 " + gta.getHoco().m23 );
            }

            if ( ( storedValuesMask & M3O_MASK ) > 0 ) {
                gta.getHoco().m30 = b.getFloat();
                log.info( "read m30 " + gta.getHoco().m30 );
            }

            if ( ( storedValuesMask & M31_MASK ) > 0 ) {
                gta.getHoco().m31 = b.getFloat();
                log.info( "read m31 " + gta.getHoco().m31 );
            }

            if ( ( storedValuesMask & M32_MASK ) > 0 ) {
                gta.getHoco().m32 = b.getFloat();
                log.info( "read m32 " + gta.getHoco().m32 );
            }

            if ( ( storedValuesMask & M33_MASK ) > 0 ) {
                gta.getHoco().m33 = b.getFloat();
                log.info( "read m33 " + gta.getHoco().m33 );
            }

            log.info( "hoco " + gta.getHoco() );

        }
    }
}
