/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class to hold utility function mainly for test purpose
 *
 * @author Zerbst
 */
public class Utility {

    public static Logger log = LoggerFactory.getLogger( Utility.class );

    /**
     * Copy one file to another location
     *
     * @param in the source file
     * @param out the target file
     *
     * @throws java.lang.Exception all kind off exceptions
     */
    public static void copyFile( File in, File out ) throws Exception {
        FileInputStream fis = new FileInputStream( in );
        FileOutputStream fos = new FileOutputStream( out );

        try {
            byte[] buf = new byte[ 1024 ];
            int i = 0;

            while ( ( i = fis.read( buf ) ) != -1 ) {
                fos.write( buf, 0, i );
            }
        } catch ( Exception e ) {
            throw e;
        } finally {

            if ( fis != null ) {
                fis.close();
            }

            if ( fos != null ) {
                fos.close();
            }
        }
    }

    /**
     * Recursively delete a directory
     *
     * @param path the start dir
     *
     * @return true if delete was successfull
     */
    public static boolean deleteDirectory( File path ) {

        if ( path.exists() ) {
            File[] files = path.listFiles();

            for ( int i = 0; i < files.length; i++ ) {

                if ( files[ i].isDirectory() ) {
                    deleteDirectory( files[ i] );
                } else {
                    files[ i].delete();
                }
            }
        }

        return ( path.delete() );
    }
}
