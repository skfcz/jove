/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="PartNodeElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PartNodeElement",
    propOrder= {"metaNodeData", "versionNumber", "reservedField", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0xce357244, 0x38fb, 0x11d1, 0xa5, 0x6, 0x00, 0x60, 0x97, 0xbd, 0xc6, 0xe1" )
public class PartNodeElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( PartNodeElement.class );

    /**  
     * The variable number 0 : MetaNodeData metaNodeData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="MetaNodeData")
    private MetaNodeData metaNodeData;

    /**  
     * The variable number 1 : I16 versionNumber
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumber;

    /**  
     * The variable number 2 : I32 reservedField
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer reservedField;



    /** Getter for metaNodeData.
     *  
     * 
     * @return MetaNodeData
     */
    public MetaNodeData getMetaNodeData() {
        return metaNodeData;
    }

     /** Setter for metaNodeData.
     *  
     * 
     * @param value MetaNodeData
     */
    public void setMetaNodeData( MetaNodeData value ) {
        this.metaNodeData = value;
    }

    /** Getter for versionNumber.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumber() {
        return versionNumber;
    }

     /** Setter for versionNumber.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumber( Short value ) {
        this.versionNumber = value;
    }

    /** Getter for reservedField.
     *  
     * 
     * @return Integer
     */
    public Integer getReservedField() {
        return reservedField;
    }

     /** Setter for reservedField.
     *  
     * 
     * @param value Integer
     */
    public void setReservedField( Integer value ) {
        this.reservedField = value;
    }


    
}

