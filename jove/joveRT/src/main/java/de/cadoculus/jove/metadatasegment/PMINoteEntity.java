/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="PMINoteEntity")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMINoteEntity",
    propOrder= {"pmi2dData", "urlFlag", }
)
public class PMINoteEntity {

    public static Logger log = LoggerFactory.getLogger( PMINoteEntity.class );

    /**  
     * The variable number 0 : PMI2DData pmi2dData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMI2DData")
    private PMI2DData pmi2dData;

    /**  
     * The variable number 1 : U32 urlFlag
     * <br>options : <pre>{valid : pmiVersionNumber > 5}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber > 5" )
    private Long urlFlag;



    /** Getter for pmi2dData.
     *  
     * 
     * @return PMI2DData
     */
    public PMI2DData getPmi2dData() {
        return pmi2dData;
    }

     /** Setter for pmi2dData.
     *  
     * 
     * @param value PMI2DData
     */
    public void setPmi2dData( PMI2DData value ) {
        this.pmi2dData = value;
    }

    /** Getter for urlFlag.
     *  
     * 
     * @return Long
     */
    public Long getUrlFlag() {
        return urlFlag;
    }

     /** Setter for urlFlag.
     *  
     * 
     * @param value Long
     */
    public void setUrlFlag( Long value ) {
        this.urlFlag = value;
    }


    
}

