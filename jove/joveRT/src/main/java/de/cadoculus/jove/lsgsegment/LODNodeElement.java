/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="LODNodeElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "LODNodeElement",
    propOrder= {"lodNodeData", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0x10dd102c, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97" )
public class LODNodeElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( LODNodeElement.class );

    /**  
     * The variable number 0 : LODNodeData lodNodeData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="LODNodeData")
    private LODNodeData lodNodeData;



    /** Getter for lodNodeData.
     *  
     * 
     * @return LODNodeData
     */
    public LODNodeData getLodNodeData() {
        return lodNodeData;
    }

     /** Setter for lodNodeData.
     *  
     * 
     * @param value LODNodeData
     */
    public void setLodNodeData( LODNodeData value ) {
        this.lodNodeData = value;
    }


    
}

