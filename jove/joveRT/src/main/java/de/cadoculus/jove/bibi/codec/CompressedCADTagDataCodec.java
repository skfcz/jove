/*
 * Copyright (C) 2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.metadatasegment.CompressedCADTagData;
import java.lang.reflect.Field;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cz
 */
public class CompressedCADTagDataCodec extends ObjectCodec<CompressedCADTagData> {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( CompressedCADTagDataCodec.class );

    public CompressedCADTagDataCodec( Class<CompressedCADTagData> type ) throws BIBIException {
        super( type );
    }

    public CompressedCADTagDataCodec( Class<CompressedCADTagData> type, List<Object> alreadyParsedValues ) throws BIBIException {
        super( type );
        this.alreadyParsedValues.addAll( alreadyParsedValues );
    }

    @Override
    protected Codec updateCodec( Field field, Codec originalCodec, WorkingContext ctx ) throws BIBIException {
        log.info( "updateCodec " + field + ", " + originalCodec );
        return originalCodec;
    }

    @Override
    protected void postAction( Field field, Object value, WorkingContext ctx ) {
        log.info( "postAction " + field + ", " + value );
    }

}
