/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.1.2.1.1 Base Property Atom Data</h4>

*/
@XmlRootElement(name="BasePropertyAtomData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "BasePropertyAtomData",
    propOrder= {"objectID", "versionNumber", "stateFlags", }
)
public class BasePropertyAtomData {

    public static Logger log = LoggerFactory.getLogger( BasePropertyAtomData.class );

    /** 
     * Object ID is the identifier for this Object. Other objects referencing this particular object do so using the
Object ID.
<p> 
     * The variable number 0 : ObjectID objectID
     * <br>options : <pre>{valid : jtVersion <= 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.ObjectID()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion <= 81" )
    private ObjectID objectID;

    /** 
     * Version Number is the version identifier for this data collection. Version number "0x0001" is currently
the only valid value for Base Property Atom Data.
<p> 
     * The variable number 1 : I16 versionNumber
     * <br>options : <pre>{valid : jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private Short versionNumber;

    /** 
     * State Flags is a collection of flags. The flags are combined using the binary OR operator
and store various state information for property atoms. Bits 0 â€" 7 are freely available for an
application to store whatever property atom information desired. All other bits are reserved
* for future expansion of the file format.<p> 
     * The variable number 2 : U32 stateFlags
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    private Long stateFlags;



    /** Getter for objectID.
     * Object ID is the identifier for this Object. Other objects referencing this particular object do so using the
Object ID.
<p> 
     * 
     * @return ObjectID
     */
    public ObjectID getObjectID() {
        return objectID;
    }

     /** Setter for objectID.
     * Object ID is the identifier for this Object. Other objects referencing this particular object do so using the
Object ID.
<p> 
     * 
     * @param value ObjectID
     */
    public void setObjectID( ObjectID value ) {
        this.objectID = value;
    }

    /** Getter for versionNumber.
     * Version Number is the version identifier for this data collection. Version number "0x0001" is currently
the only valid value for Base Property Atom Data.
<p> 
     * 
     * @return Short
     */
    public Short getVersionNumber() {
        return versionNumber;
    }

     /** Setter for versionNumber.
     * Version Number is the version identifier for this data collection. Version number "0x0001" is currently
the only valid value for Base Property Atom Data.
<p> 
     * 
     * @param value Short
     */
    public void setVersionNumber( Short value ) {
        this.versionNumber = value;
    }

    /** Getter for stateFlags.
     * State Flags is a collection of flags. The flags are combined using the binary OR operator
and store various state information for property atoms. Bits 0 â€" 7 are freely available for an
application to store whatever property atom information desired. All other bits are reserved
* for future expansion of the file format.<p> 
     * 
     * @return Long
     */
    public Long getStateFlags() {
        return stateFlags;
    }

     /** Setter for stateFlags.
     * State Flags is a collection of flags. The flags are combined using the binary OR operator
and store various state information for property atoms. Bits 0 â€" 7 are freely available for an
application to store whatever property atom information desired. All other bits are reserved
* for future expansion of the file format.<p> 
     * 
     * @param value Long
     */
    public void setStateFlags( Long value ) {
        this.stateFlags = value;
    }


    
}

