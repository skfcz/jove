/*
 * Copyright (C) 2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */

package de.cadoculus.jove.bibi.annotation;

/**
 *
 * @author Zerbst
 */
public enum PredictorType {
    
    Lag1,
    Lag2,
    Stride1,
    Stride2,
    StripIndex,
    Ramp,
    Xor1,
    Xor2,
    NULL_PRED
}
