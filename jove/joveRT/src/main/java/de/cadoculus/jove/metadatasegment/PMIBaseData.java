/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.1.1.1 PMI Base Data</h4>
The PMI Base Data collection defines the basic/common data that every 2D and 3D PMI entity contains

*/
@XmlRootElement(name="PMIBaseData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIBaseData",
    propOrder= {"userLabel", "j2DFrameFlag", "j2DReferenceFrame", "textHeight", "symbolValidFlag", }
)
public class PMIBaseData {

    public static Logger log = LoggerFactory.getLogger( PMIBaseData.class );

    /**  
     * The variable number 0 : I32 userLabel
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer userLabel;

    /**  
     * The variable number 1 : U8 j2DFrameFlag
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U8()    
    private Short j2DFrameFlag;

    /**  
     * The variable number 2 : J2DReferenceFrame j2DReferenceFrame
     * <br>options : <pre>{valid : obj.j2DFrameFlag != 0}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="J2DReferenceFrame")
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.j2DFrameFlag != 0" )
    private J2DReferenceFrame j2DReferenceFrame;

    /**  
     * The variable number 3 : F32 textHeight
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float textHeight;

    /**  
     * The variable number 4 : U8 symbolValidFlag
     * <br>options : <pre>{valid : pmiVersionNumber > 4}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.U8()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber > 4" )
    private Short symbolValidFlag;



    /** Getter for userLabel.
     *  
     * 
     * @return Integer
     */
    public Integer getUserLabel() {
        return userLabel;
    }

     /** Setter for userLabel.
     *  
     * 
     * @param value Integer
     */
    public void setUserLabel( Integer value ) {
        this.userLabel = value;
    }

    /** Getter for j2DFrameFlag.
     *  
     * 
     * @return Short
     */
    public Short getJ2DFrameFlag() {
        return j2DFrameFlag;
    }

     /** Setter for j2DFrameFlag.
     *  
     * 
     * @param value Short
     */
    public void setJ2DFrameFlag( Short value ) {
        this.j2DFrameFlag = value;
    }

    /** Getter for j2DReferenceFrame.
     *  
     * 
     * @return J2DReferenceFrame
     */
    public J2DReferenceFrame getJ2DReferenceFrame() {
        return j2DReferenceFrame;
    }

     /** Setter for j2DReferenceFrame.
     *  
     * 
     * @param value J2DReferenceFrame
     */
    public void setJ2DReferenceFrame( J2DReferenceFrame value ) {
        this.j2DReferenceFrame = value;
    }

    /** Getter for textHeight.
     *  
     * 
     * @return Float
     */
    public Float getTextHeight() {
        return textHeight;
    }

     /** Setter for textHeight.
     *  
     * 
     * @param value Float
     */
    public void setTextHeight( Float value ) {
        this.textHeight = value;
    }

    /** Getter for symbolValidFlag.
     *  
     * 
     * @return Short
     */
    public Short getSymbolValidFlag() {
        return symbolValidFlag;
    }

     /** Setter for symbolValidFlag.
     *  
     * 
     * @param value Short
     */
    public void setSymbolValidFlag( Short value ) {
        this.symbolValidFlag = value;
    }


    
}

