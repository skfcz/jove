/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="MetaNodeData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "MetaNodeData",
    propOrder= {"versionNumberMND", }
)
public class MetaNodeData extends GroupNodeData {

    public static Logger log = LoggerFactory.getLogger( MetaNodeData.class );

    /**  
     * The variable number 0 : I16 versionNumberMND
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumberMND;



    /** Getter for versionNumberMND.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberMND() {
        return versionNumberMND;
    }

     /** Setter for versionNumberMND.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberMND( Short value ) {
        this.versionNumberMND = value;
    }


    
}

