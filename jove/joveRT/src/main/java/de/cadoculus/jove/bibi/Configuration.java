/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi;

import java.util.Enumeration;
import java.util.Properties;
import org.slf4j.LoggerFactory;

/**
 * This is a small wrapper for configuration. It offers convenience methods to
 * get configuration values. It is implemented (again) to avoid dependency on
 * apache commons-conf :-)
 *
 * @author cz
 */
public class Configuration {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( Configuration.class );
    private final Properties props = new Properties();

    /**
     * Create a new Configuration from scratch. It automatically loads the build
     * in default values from "default.properties".
     */
    public Configuration() {
        try {
            props.load( Configuration.class.getResourceAsStream( "default.properties" ) );
        } catch ( Exception exp ) {
            log.error( "failed to load default configuration", exp );
        }
    }

    /**
     * A copy constructor for Configurations.
     *
     * @param src the Configurations to copy from
     */
    public Configuration( Configuration src ) {
        this();
        for ( Enumeration<?> enu = src.props.propertyNames(); enu.hasMoreElements(); ) {
            String key = (String) enu.nextElement();
            this.props.setProperty( key, src.props.getProperty( key ) );
        }
    }

    /**
     * Set a property.
     *
     * @param key the key
     * @param value the value
     * @throws IllegalArgumentException if no valid key is given
     */
    public void setProperty( String key, String value ) {
        if ( key == null || key.trim().length() == 0 ) {
            throw new IllegalArgumentException( "expect non null and none empty key, got '" + key + "'" );
        }
        this.props.setProperty( key, value );
    }

    /**
     * Add properties.
     *
     * @param other the properties to add
     * @throws IllegalArgumentException if null properties are given
     */
    public void addProperties( Properties other ) {
        if ( other == null ) {
            throw new IllegalArgumentException( "expect non null properties" );
        }
        this.props.putAll( other );
    }

    /**
     * Get a String property
     *
     * @param key the value name to get
     * @return the configured value or null
     */
    public String getProperty( String key ) {
        if ( key == null || key.trim().length() == 0 ) {
            throw new IllegalArgumentException( "expect non null and none empty key, got '" + key + "'" );
        }
        return props.getProperty( key );
    }

    /**
     * Get a String property.
     *
     * @param key the value name to get
     * @param defaultValue the value to return if nothing is contained in
     * configuration
     * @return the configured value or null
     * @throws IllegalArgumentException if no valid key is given
     */
    public String getProperty( String key, String defaultValue ) {
        if ( key == null || key.trim().length() == 0 ) {
            throw new IllegalArgumentException( "expect non null and none empty key, got '" + key + "'" );
        }
        return props.getProperty( key, defaultValue );
    }

    /**
     * Get a value parsed as boolean flag using {@link Boolean#valueOf(java.lang.String)
     * }.
     *
     * @param key the value name to get
     * @return true or false
     * @throws IllegalArgumentException if no valid key is given
     */
    public boolean getBooleanProperty( String key ) {
        if ( key == null || key.trim().length() == 0 ) {
            throw new IllegalArgumentException( "expect non null and none empty key, got '" + key + "'" );
        }
        return Boolean.valueOf( props.containsKey( key )
                ? props.getProperty( key ) : "false" );
    }

    /**
     * Get a value parsed as boolean flag using {@link Boolean#valueOf(java.lang.String)
     * }.
     *
     * @param key the value name to get
     * @param defaultValue the value to return if nothing is contained in
     * configuration
     * @return true or false
     * @throws IllegalArgumentException if no valid key is given
     */
    public boolean getBooleanProperty( String key, boolean defaultValue ) {
        if ( key == null || key.trim().length() == 0 ) {
            throw new IllegalArgumentException( "expect non null and none empty key, got '" + key + "'" );
        }
        if ( props.containsKey( key ) ) {
            return Boolean.valueOf( props.getProperty( key ) );
        }
        return defaultValue;
    }

    /**
     * Get a value parsed as int value using {@link Integer#valueOf(java.lang.String)
     * }.
     *
     * @param key the value name to get
     * @return the parsed value or 0 if nothing was configured
     * @throws IllegalArgumentException if no valid key is given
     */
    public int getIntegerProperty( String key ) {
        if ( key == null || key.trim().length() == 0 ) {
            throw new IllegalArgumentException( "expect non null and none empty key, got '" + key + "'" );
        }
        return Integer.valueOf( props.containsKey( key )
                ? props.getProperty( key ) : "0" );
    }

    /**
     * Get a value parsed as int value using {@link Integer#valueOf(java.lang.String)
     * }.
     *
     * @param key the value name to get
     * @param defaultValue the value to return if nothing is contained in
     * configuration
     * @return the parsed value or the givend defaultValue
     * @throws IllegalArgumentException if no valid key is given
     */
    public int getIntegerProperty( String key, int defaultValue ) {
        if ( key == null || key.trim().length() == 0 ) {
            throw new IllegalArgumentException( "expect non null and none empty key, got '" + key + "'" );
        }
        if ( props.containsKey( key ) ) {
            return Integer.valueOf( props.getProperty( key ) );
        }
        return defaultValue;
    }

    /**
     * Get a value parsed as int value using {@link Double#valueOf(java.lang.String)
     * }.
     *
     * @param key the value name to get
     * @return true or false
     */
    public double getDoubleProperty( String key ) {
        if ( key == null || key.trim().length() == 0 ) {
            throw new IllegalArgumentException( "expect non null and none empty key, got '" + key + "'" );
        }
        return Double.valueOf( props.containsKey( key )
                ? props.getProperty( key ) : "0.0" );

    }

    /**
     * Get a value parsed as int value using {@link Double#valueOf(java.lang.String)
     * }.
     *
     * @param key the value name to get
     * @param defaultValue the value to return if nothing is contained in
     * configuration
     * @return true or false
     */
    public double getDoubleProperty( String key, double defaultValue ) {
        if ( key == null || key.trim().length() == 0 ) {
            throw new IllegalArgumentException( "expect non null and none empty key, got '" + key + "'" );
        }
        if ( props.containsKey( key ) ) {
            return Double.valueOf( props.getProperty( key ) );
        }
        return defaultValue;
    }
}
