/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.1.1.1.1.1 Base Node Data </h4>

*/
@XmlRootElement(name="BaseNodeData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "BaseNodeData",
    propOrder= {"versionNumber", "nodeFlags", "attributeCount", "attributeObjectID", }
)
public class BaseNodeData {

    public static Logger log = LoggerFactory.getLogger( BaseNodeData.class );

    /**  
     * The variable number 0 : I16 versionNumber
     * <br>options : <pre>{valid : jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private Short versionNumber;

    /**  
     * The variable number 1 : U32 nodeFlags
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    private Long nodeFlags;

    /**  
     * The variable number 2 : I32 attributeCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer attributeCount;

    /**  
     * The variable number 3 : I32[] attributeObjectID
     * <br>options : <pre>{length : obj.attributeCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.attributeCount" )
    @XmlElementWrapper(name = "attributeObjectID")
        private java.util.List<Integer> attributeObjectID;



    /** Getter for versionNumber.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumber() {
        return versionNumber;
    }

     /** Setter for versionNumber.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumber( Short value ) {
        this.versionNumber = value;
    }

    /** Getter for nodeFlags.
     *  
     * 
     * @return Long
     */
    public Long getNodeFlags() {
        return nodeFlags;
    }

     /** Setter for nodeFlags.
     *  
     * 
     * @param value Long
     */
    public void setNodeFlags( Long value ) {
        this.nodeFlags = value;
    }

    /** Getter for attributeCount.
     *  
     * 
     * @return Integer
     */
    public Integer getAttributeCount() {
        return attributeCount;
    }

     /** Setter for attributeCount.
     *  
     * 
     * @param value Integer
     */
    public void setAttributeCount( Integer value ) {
        this.attributeCount = value;
    }

    /** Getter for attributeObjectID.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getAttributeObjectID() {
        return attributeObjectID;
    }

     /** Setter for attributeObjectID.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setAttributeObjectID( java.util.List<Integer> value ) {
        this.attributeObjectID = value;
    }


    
}

