/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="PMIModelView")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIModelView",
    propOrder= {"eyeDirection", "angle", "eyePosition", "targetPoint", "viewAngle", "viewportDiameter", "reservedField1", "reservedField2", "activeFlag", "viewID", "viewNameStringID", }
)
public class PMIModelView {

    public static Logger log = LoggerFactory.getLogger( PMIModelView.class );

    /**  
     * The variable number 0 : DirF32 eyeDirection
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.DirF32()    
    private javax.vecmath.Vector3f eyeDirection;

    /**  
     * The variable number 1 : F32 angle
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float angle;

    /**  
     * The variable number 2 : CoordF32 eyePosition
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.CoordF32()    
    private javax.vecmath.Point3f eyePosition;

    /**  
     * The variable number 3 : CoordF32 targetPoint
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.CoordF32()    
    private javax.vecmath.Point3f targetPoint;

    /**  
     * The variable number 4 : CoordF32 viewAngle
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.CoordF32()    
    private javax.vecmath.Point3f viewAngle;

    /**  
     * The variable number 5 : F32 viewportDiameter
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float viewportDiameter;

    /**  
     * The variable number 6 : F32 reservedField1
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float reservedField1;

    /**  
     * The variable number 7 : I32 reservedField2
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer reservedField2;

    /**  
     * The variable number 8 : I32 activeFlag
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer activeFlag;

    /**  
     * The variable number 9 : I32 viewID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer viewID;

    /**  
     * The variable number 10 : I32 viewNameStringID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer viewNameStringID;



    /** Getter for eyeDirection.
     *  
     * 
     * @return javax.vecmath.Vector3f
     */
    public javax.vecmath.Vector3f getEyeDirection() {
        return eyeDirection;
    }

     /** Setter for eyeDirection.
     *  
     * 
     * @param value javax.vecmath.Vector3f
     */
    public void setEyeDirection( javax.vecmath.Vector3f value ) {
        this.eyeDirection = value;
    }

    /** Getter for angle.
     *  
     * 
     * @return Float
     */
    public Float getAngle() {
        return angle;
    }

     /** Setter for angle.
     *  
     * 
     * @param value Float
     */
    public void setAngle( Float value ) {
        this.angle = value;
    }

    /** Getter for eyePosition.
     *  
     * 
     * @return javax.vecmath.Point3f
     */
    public javax.vecmath.Point3f getEyePosition() {
        return eyePosition;
    }

     /** Setter for eyePosition.
     *  
     * 
     * @param value javax.vecmath.Point3f
     */
    public void setEyePosition( javax.vecmath.Point3f value ) {
        this.eyePosition = value;
    }

    /** Getter for targetPoint.
     *  
     * 
     * @return javax.vecmath.Point3f
     */
    public javax.vecmath.Point3f getTargetPoint() {
        return targetPoint;
    }

     /** Setter for targetPoint.
     *  
     * 
     * @param value javax.vecmath.Point3f
     */
    public void setTargetPoint( javax.vecmath.Point3f value ) {
        this.targetPoint = value;
    }

    /** Getter for viewAngle.
     *  
     * 
     * @return javax.vecmath.Point3f
     */
    public javax.vecmath.Point3f getViewAngle() {
        return viewAngle;
    }

     /** Setter for viewAngle.
     *  
     * 
     * @param value javax.vecmath.Point3f
     */
    public void setViewAngle( javax.vecmath.Point3f value ) {
        this.viewAngle = value;
    }

    /** Getter for viewportDiameter.
     *  
     * 
     * @return Float
     */
    public Float getViewportDiameter() {
        return viewportDiameter;
    }

     /** Setter for viewportDiameter.
     *  
     * 
     * @param value Float
     */
    public void setViewportDiameter( Float value ) {
        this.viewportDiameter = value;
    }

    /** Getter for reservedField1.
     *  
     * 
     * @return Float
     */
    public Float getReservedField1() {
        return reservedField1;
    }

     /** Setter for reservedField1.
     *  
     * 
     * @param value Float
     */
    public void setReservedField1( Float value ) {
        this.reservedField1 = value;
    }

    /** Getter for reservedField2.
     *  
     * 
     * @return Integer
     */
    public Integer getReservedField2() {
        return reservedField2;
    }

     /** Setter for reservedField2.
     *  
     * 
     * @param value Integer
     */
    public void setReservedField2( Integer value ) {
        this.reservedField2 = value;
    }

    /** Getter for activeFlag.
     *  
     * 
     * @return Integer
     */
    public Integer getActiveFlag() {
        return activeFlag;
    }

     /** Setter for activeFlag.
     *  
     * 
     * @param value Integer
     */
    public void setActiveFlag( Integer value ) {
        this.activeFlag = value;
    }

    /** Getter for viewID.
     *  
     * 
     * @return Integer
     */
    public Integer getViewID() {
        return viewID;
    }

     /** Setter for viewID.
     *  
     * 
     * @param value Integer
     */
    public void setViewID( Integer value ) {
        this.viewID = value;
    }

    /** Getter for viewNameStringID.
     *  
     * 
     * @return Integer
     */
    public Integer getViewNameStringID() {
        return viewNameStringID;
    }

     /** Setter for viewNameStringID.
     *  
     * 
     * @param value Integer
     */
    public void setViewNameStringID( Integer value ) {
        this.viewNameStringID = value;
    }


    
}

