/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import java.nio.ByteBuffer;
import org.slf4j.LoggerFactory;

/**
 * Reads a single byte from the given ByteBuffer into a Character.
 *
 * @author Zerbst
 */
public class CharacterCodec implements Codec<Character> {

    private static org.slf4j.Logger log = LoggerFactory.getLogger( CharacterCodec.class );

    public CharacterCodec() {
    }

    @Override
    public Character decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        CodecUtility.checkMaxPosition( input, ctx );
        byte b = input.get();
        log.info( "read " + b );
        return new Character( ( char ) b );
    }

    @Override
    public String toString() {
        return "CharacterCodec";
    }
}
