/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.F32;
import de.cadoculus.jove.bibi.annotation.F64;
import de.cadoculus.jove.bibi.annotation.I16;
import de.cadoculus.jove.bibi.annotation.I32;
import de.cadoculus.jove.bibi.annotation.U16;
import de.cadoculus.jove.bibi.annotation.U32;
import de.cadoculus.jove.bibi.annotation.U8;
import java.lang.reflect.AnnotatedElement;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.LoggerFactory;

/**
 * Reads
 *
 * @author Zerbst
 */
public class NumberCodecFactory extends AbstractCodecFactory {

    private static org.slf4j.Logger log = LoggerFactory.getLogger( NumberCodecFactory.class );
    private static Map<String, Codec> cache = new HashMap<>();

    @Override
    public <T> Codec<T> create( AnnotatedElement metadata, Class<T> type, WorkingContext ctx ) {

        if ( !( Number.class.isAssignableFrom( type ) ) ) {
            return null;
        }

        // only one annotation may be applied on one number
        Object lastFound = null;
        for ( Class an : CodecUtility.NUMERIC_ANNOTATIONS ) {
            if ( metadata.getAnnotation( an ) != null ) {
                if ( lastFound != null ) {
                    log.info( "found annotations " + lastFound + " and " + metadata.getAnnotation( an ) + " on " + metadata );
                    throw new IllegalStateException( "found multiple numeric annotations on " + metadata.toString() );
                }
                lastFound = metadata.getAnnotation( an );
            }
        }

        // Check first for cached codec
        String key = "key_" + lastFound + "_" + type;
        if ( cache.containsKey( key ) ) {
            return (Codec<T>) cache.get( key );
        }

        // Construct new Codec        
        // TODO: reuse lastFound instead of querying again for annotation
        F32 f32 = metadata.getAnnotation( F32.class );
        F64 f64 = metadata.getAnnotation( F64.class );
        I16 i16 = metadata.getAnnotation( I16.class );
        I32 i32 = metadata.getAnnotation( I32.class );
        U16 u16 = metadata.getAnnotation( U16.class );
        U32 u32 = metadata.getAnnotation( U32.class );
        U8 u8 = metadata.getAnnotation( U8.class );

        Codec<T> retval = null;

        if ( f32 != null && Float.class.isAssignableFrom( type ) ) {
            retval = new NumberCodec( F32.class, type );
        } else if ( f64 != null && Double.class.isAssignableFrom( type ) ) {
            retval = new NumberCodec( F64.class, type );
        } else if ( i16 != null && ( Short.class.isAssignableFrom( type ) ) ) {
            retval = new NumberCodec( I16.class, Short.class );
        } else if ( i32 != null && Integer.class.isAssignableFrom( type ) ) {
            retval = new NumberCodec( I32.class, Integer.class );
        } else if ( u16 != null
                && ( Integer.class.isAssignableFrom( type )
                || Long.class.isAssignableFrom( type ) ) ) {
            retval = new NumberCodec( U16.class, type );
        } else if ( u32 != null
                && ( Long.class.isAssignableFrom( type ) ) ) {
            retval = new NumberCodec( U32.class, type );
        } else if ( u8 != null
                && ( Short.class.isAssignableFrom( type )
                || Integer.class.isAssignableFrom( type )
                || Long.class.isAssignableFrom( type ) )
                || Byte.class.isAssignableFrom( type ) ) {
            retval = new NumberCodec( U8.class, type );
        } else {

            log.warn( "no codec available for combination type " + type.getSimpleName()
                    + " and annotation " + lastFound );
        }
        if ( retval != null ) {
            cache.put( key, retval );
        }
        return retval;

    }
}
