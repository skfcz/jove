/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.BIBILength;
import de.cadoculus.jove.bibi.annotation.MbString;
import de.cadoculus.jove.bibi.annotation.UChar;
import java.lang.reflect.AnnotatedElement;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This factory creates codecs for character and string like values. Supported
 * annotation / Java types:</p>
 *
 * <ul> <li>UChar / Character</li> <li>UChar , BIBILength/ String</li></ul>
 *
 * @author Zerbst
 */
public class StringCodecFactory extends AbstractCodecFactory {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( StringCodecFactory.class );

    public StringCodecFactory() {
    }

    @Override
    public <T> Codec<T> create( AnnotatedElement metadata, Class<T> type, WorkingContext ctx ) throws BIBIException {

        if ( !( String.class == type || Character.class == type || Character.TYPE == type ) ) {
            return null;
        }
        if ( metadata == null ) {
            return null;
        }

        UChar ucharA = metadata.getAnnotation( UChar.class );
        BIBILength lengthA = metadata.getAnnotation( BIBILength.class );
        MbString mbStringA = metadata.getAnnotation( MbString.class );
        de.cadoculus.jove.bibi.annotation.String stringA = metadata.getAnnotation( de.cadoculus.jove.bibi.annotation.String.class );

        Codec<T> retval = null;

        if ( mbStringA != null && String.class == type ) {
            retval = (Codec<T>) new StringCodec( metadata, type );
        } else if ( stringA != null && String.class == type) {
            retval = (Codec<T>) new StringCodec( metadata, type );
        } else if ( ucharA != null && lengthA != null && String.class == type ) {
            retval = (Codec<T>) new StringCodec( metadata, type );
        } else if ( ucharA != null && Character.class == type ) {
            retval = (Codec<T>) new CharacterCodec();
        } else if ( ucharA != null && Character.TYPE == type ) {
            retval = (Codec<T>) new CharacterCodec();        
        } else {

            log.warn( "no codec available for combination type " + type.getSimpleName()
                    + ", UChar " + ucharA + ", BIBILength " + lengthA + ", MbString " + mbStringA );
        }

        return retval;

    }
}
