/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="BaseShapeData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "BaseShapeData",
    propOrder= {"versionNumberBSD", "reservedField", "untransformedBox", "area", "vertexCountRange", "nodeCountRange", "polygonCountRange", "size", "compressionLevel", }
)
public class BaseShapeData extends BaseNodeData {

    public static Logger log = LoggerFactory.getLogger( BaseShapeData.class );

    /**  
     * The variable number 0 : I16 versionNumberBSD
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumberBSD;

    /**  
     * The variable number 1 : BBoxF32 reservedField
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BBoxF32()    
    private BBoxF32 reservedField;

    /**  
     * The variable number 2 : BBoxF32 untransformedBox
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BBoxF32()    
    private BBoxF32 untransformedBox;

    /**  
     * The variable number 3 : F32 area
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float area;

    /**  
     * The variable number 4 : VertexCountRange vertexCountRange
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="VertexCountRange")
    private VertexCountRange vertexCountRange;

    /**  
     * The variable number 5 : NodeCountRange nodeCountRange
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="NodeCountRange")
    private NodeCountRange nodeCountRange;

    /**  
     * The variable number 6 : PolygonCountRange polygonCountRange
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PolygonCountRange")
    private PolygonCountRange polygonCountRange;

    /**  
     * The variable number 7 : I32 size
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer size;

    /**  
     * The variable number 8 : F32 compressionLevel
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float compressionLevel;



    /** Getter for versionNumberBSD.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberBSD() {
        return versionNumberBSD;
    }

     /** Setter for versionNumberBSD.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberBSD( Short value ) {
        this.versionNumberBSD = value;
    }

    /** Getter for reservedField.
     *  
     * 
     * @return BBoxF32
     */
    public BBoxF32 getReservedField() {
        return reservedField;
    }

     /** Setter for reservedField.
     *  
     * 
     * @param value BBoxF32
     */
    public void setReservedField( BBoxF32 value ) {
        this.reservedField = value;
    }

    /** Getter for untransformedBox.
     *  
     * 
     * @return BBoxF32
     */
    public BBoxF32 getUntransformedBox() {
        return untransformedBox;
    }

     /** Setter for untransformedBox.
     *  
     * 
     * @param value BBoxF32
     */
    public void setUntransformedBox( BBoxF32 value ) {
        this.untransformedBox = value;
    }

    /** Getter for area.
     *  
     * 
     * @return Float
     */
    public Float getArea() {
        return area;
    }

     /** Setter for area.
     *  
     * 
     * @param value Float
     */
    public void setArea( Float value ) {
        this.area = value;
    }

    /** Getter for vertexCountRange.
     *  
     * 
     * @return VertexCountRange
     */
    public VertexCountRange getVertexCountRange() {
        return vertexCountRange;
    }

     /** Setter for vertexCountRange.
     *  
     * 
     * @param value VertexCountRange
     */
    public void setVertexCountRange( VertexCountRange value ) {
        this.vertexCountRange = value;
    }

    /** Getter for nodeCountRange.
     *  
     * 
     * @return NodeCountRange
     */
    public NodeCountRange getNodeCountRange() {
        return nodeCountRange;
    }

     /** Setter for nodeCountRange.
     *  
     * 
     * @param value NodeCountRange
     */
    public void setNodeCountRange( NodeCountRange value ) {
        this.nodeCountRange = value;
    }

    /** Getter for polygonCountRange.
     *  
     * 
     * @return PolygonCountRange
     */
    public PolygonCountRange getPolygonCountRange() {
        return polygonCountRange;
    }

     /** Setter for polygonCountRange.
     *  
     * 
     * @param value PolygonCountRange
     */
    public void setPolygonCountRange( PolygonCountRange value ) {
        this.polygonCountRange = value;
    }

    /** Getter for size.
     *  
     * 
     * @return Integer
     */
    public Integer getSize() {
        return size;
    }

     /** Setter for size.
     *  
     * 
     * @param value Integer
     */
    public void setSize( Integer value ) {
        this.size = value;
    }

    /** Getter for compressionLevel.
     *  
     * 
     * @return Float
     */
    public Float getCompressionLevel() {
        return compressionLevel;
    }

     /** Setter for compressionLevel.
     *  
     * 
     * @param value Float
     */
    public void setCompressionLevel( Float value ) {
        this.compressionLevel = value;
    }


    
}

