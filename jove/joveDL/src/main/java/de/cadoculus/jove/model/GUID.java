/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.model;

import java.security.SecureRandom;

/**
 * The GUID type is a 16 byte (128-bit) number. GUID is stored/written to the JT
 * file using a four-byte word (U32), 2 two-byte words (U16), and 8 one-byte
 * words (U8) such as: {3F2504E0-4F89-11D3-9A-0C-03-05-E8-2C-33-01} In the JT
 * format GUIDs are used as unique identifiers (e.g. Data Segment ID, Object
 * Type ID, etc.)
 *
 * @author Zerbst
 */
public final class GUID {

    /**
     * GUID used to indicate end of elements
     */
    public static final GUID END_OF_ELEMENTS = new GUID(
            "0xffffffff, 0xffff, 0xffff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff" );
    public static final int BYTES_PER_OBJECT = 16;

    /*
     * The random number generator used by this class to create random
     * based UUIDs.
     */
    private static volatile SecureRandom numberGenerator = null;
    private Long a;
    private Integer b1;
    private Integer b2;
    private Byte c1;
    private Byte c2;
    private Byte c3;
    private Byte c4;
    private Byte c5;
    private Byte c6;
    private Byte c7;
    private Byte c8;

    /**
     * Create a new , random GUID. This might collide with the GUID used as
     * identifier.
     */
    public GUID() {

        // Lazy creation of SecureRandom
        SecureRandom ng = numberGenerator;

        if ( ng == null ) {
            numberGenerator = ng = new SecureRandom();
        }

        a = ng.nextLong();
        b1 = ng.nextInt();
        b2 = ng.nextInt();

        byte[] bytes = new byte[ 8 ];
        ng.nextBytes( bytes );
        c1 = bytes[ 0];
        c2 = bytes[ 1];
        c3 = bytes[ 2];
        c4 = bytes[ 3];
        c5 = bytes[ 4];
        c6 = bytes[ 5];
        c7 = bytes[ 6];
        c8 = bytes[ 7];

    }

    /**
     * Create a GUID from a String representation as found in the JT
     * specification doc. This is a comma (,) separated String with 11 hex
     * values, e.g.
     * <code>0xce357245, 0x38fb, 0x11d1, 0xa5,
     * 0x6, 0x0, 0x60, 0x97, 0xbd, 0xc6, 0xe1</code>
     *
     * @param stringRep the GUIDs string representation
     */
    public GUID( String stringRep ) {

        String[] parts = stringRep.split( "," );

        if ( parts.length != 11 ) {
            throw new IllegalArgumentException( "expect 11 comma separated hex values, got "
                    + stringRep );
        }

        long v0 = Long.decode( parts[ 0].trim() );
        a = ( v0 > Integer.MAX_VALUE ) ? new Long( v0 ) : new Integer( (int) v0 );
        b1 = Integer.decode( parts[ 1].trim() ).intValue();
        b2 = Integer.decode( parts[ 2].trim() ).intValue();
        c1 = (byte) Integer.decode( parts[ 3].trim() ).intValue();
        c2 = (byte) Integer.decode( parts[ 4].trim() ).intValue();
        c3 = (byte) Integer.decode( parts[ 5].trim() ).intValue();
        c4 = (byte) Integer.decode( parts[ 6].trim() ).intValue();
        c5 = (byte) Integer.decode( parts[ 7].trim() ).intValue();
        c6 = (byte) Integer.decode( parts[ 8].trim() ).intValue();
        c7 = (byte) Integer.decode( parts[ 9].trim() ).intValue();
        c8 = (byte) Integer.decode( parts[ 10].trim() ).intValue();
    }

    /**
     * Create a new GUID object from the given attributes
     *
     * @param a the first long
     * @param b1 the first int
     * @param b2 the second int
     * @param c1 the first byte
     * @param c2 the second byte
     * @param c3 the third byte
     * @param c4 the fourth byte
     * @param c5 the fivth byte
     * @param c6 the sixth byte
     * @param c7 the seventh byte
     * @param c8 the eighth byte
     */
    public GUID( long a, int b1, int b2, byte c1, byte c2, byte c3, byte c4, byte c5, byte c6,
            byte c7, byte c8 ) {

        this.a = a;
        this.b1 = b1;
        this.b2 = b2;
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
        this.c4 = c4;
        this.c5 = c5;
        this.c6 = c6;
        this.c7 = c7;
        this.c8 = c8;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals( Object obj ) {
        boolean retval = false;

        if ( obj instanceof GUID ) {
            retval = toString().equals( obj.toString() );
        }

        return retval;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

        return toString().hashCode();
    }

    /**
     * Create a string representation as found in the JT spec with comma
     * separated hex values
     *
     * @return the string representation
     */
    public String toString() {
        StringBuilder buff = new StringBuilder();
        buff.append( "0x" );
        buff.append( Long.toHexString( a.longValue() ) );
        buff.append( ", 0x" );
        buff.append( Integer.toHexString( b1 ) );
        buff.append( ", 0x" );
        buff.append( Integer.toHexString( b2 ) );
        buff.append( ", 0x" );
        buff.append( toHexString( c1 ) );
        buff.append( ", 0x" );
        buff.append( toHexString( c2 ) );
        buff.append( ", 0x" );
        buff.append( toHexString( c3 ) );
        buff.append( ", 0x" );
        buff.append( toHexString( c4 ) );
        buff.append( ", 0x" );
        buff.append( toHexString( c5 ) );
        buff.append( ", 0x" );
        buff.append( toHexString( c6 ) );
        buff.append( ", 0x" );
        buff.append( toHexString( c7 ) );
        buff.append( ", 0x" );
        buff.append( toHexString( c8 ) );

        return buff.toString();
    }

    public static String toHexString( Byte b ) {
        String retval = null;

        if ( b == 0 ) {
            retval = "00";
        } else {
            retval = Integer.toHexString( b & 0xFF );
        }

        return retval;

    }
}
