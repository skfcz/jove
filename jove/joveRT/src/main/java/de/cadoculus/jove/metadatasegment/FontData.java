/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
This entity collects a number of elements listed individually in PMIManagerMetaDataElement.
*/
@XmlRootElement(name="FontData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "FontData",
    propOrder= {"fontName", "characterSet", "pmiPolygonData", }
)
public class FontData {

    public static Logger log = LoggerFactory.getLogger( FontData.class );

    /**  
     * The variable number 0 : String fontName
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.String()    
    private String fontName;

    /**  
     * The variable number 1 : VecI32 characterSet
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.VecI32()    
    private java.util.List<Integer> characterSet;

    /**  
     * The variable number 2 : PMIPolygonData pmiPolygonData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIPolygonData")
    private PMIPolygonData pmiPolygonData;



    /** Getter for fontName.
     *  
     * 
     * @return String
     */
    public String getFontName() {
        return fontName;
    }

     /** Setter for fontName.
     *  
     * 
     * @param value String
     */
    public void setFontName( String value ) {
        this.fontName = value;
    }

    /** Getter for characterSet.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getCharacterSet() {
        return characterSet;
    }

     /** Setter for characterSet.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setCharacterSet( java.util.List<Integer> value ) {
        this.characterSet = value;
    }

    /** Getter for pmiPolygonData.
     *  
     * 
     * @return PMIPolygonData
     */
    public PMIPolygonData getPmiPolygonData() {
        return pmiPolygonData;
    }

     /** Setter for pmiPolygonData.
     *  
     * 
     * @param value PMIPolygonData
     */
    public void setPmiPolygonData( PMIPolygonData value ) {
        this.pmiPolygonData = value;
    }


    
}

