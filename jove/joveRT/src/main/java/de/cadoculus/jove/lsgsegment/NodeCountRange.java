/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="NodeCountRange")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "NodeCountRange",
    propOrder= {"minCount", "maxCount", }
)
public class NodeCountRange {

    public static Logger log = LoggerFactory.getLogger( NodeCountRange.class );

    /**  
     * The variable number 0 : I32 minCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer minCount;

    /**  
     * The variable number 1 : I32 maxCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer maxCount;



    /** Getter for minCount.
     *  
     * 
     * @return Integer
     */
    public Integer getMinCount() {
        return minCount;
    }

     /** Setter for minCount.
     *  
     * 
     * @param value Integer
     */
    public void setMinCount( Integer value ) {
        this.minCount = value;
    }

    /** Getter for maxCount.
     *  
     * 
     * @return Integer
     */
    public Integer getMaxCount() {
        return maxCount;
    }

     /** Setter for maxCount.
     *  
     * 
     * @param value Integer
     */
    public void setMaxCount( Integer value ) {
        this.maxCount = value;
    }


    
}

