/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.enums;

import de.cadoculus.jove.base.*;

/** 
Segment Type defines a broad classification of the segment contents. For example, a Segment Type of "1" denotes that the
segment contains Logical Scene Graph material; "2" denotes contents of a B-Rep, etc.<p>
Note: Segment Types 7-16 all identify the contents as LOD Shape data, where the increasing type number is intended to
convey some notion of how high an LOD the specific shape segment represents. The lower the type in this 7-16 range the
more detailed the Shape LOD (i.e. Segment Type 7 is the most detailed Shape LOD Segment). For the rare case when there
are more than 10 LODs, LOD9 and greater are all assigned Segment Type 16. <p>
Note: The more generic Shape Segment type (i.e. Segment Type 6) is used when the Shape Segment has one or more of the
following characteristics:<ul>
<li>Not a descendant of an LOD node,<li>
<li>Is referenced by (i.e. is a child of) more than one LOD node,</li>
<li>Shape has its own built-in LODs, and</li>
<li>No way to determine what LOD a Shape Segment represents.</li></ul>

 * 
 * This enumeration contains 19 values of type I32
*/

public enum SegmentType {
   
    LOGICAL_SCENE_GRAPH( 1 ),
    JT_BREP( 2 ),
    PMI_DATA( 3 ),
    META_DATA( 4 ),
    SHAPE( 6 ),
    SHAPE_LOD0( 7 ),
    SHAPE_LOD1( 8 ),
    SHAPE_LOD2( 9 ),
    SHAPE_LOD3( 10 ),
    SHAPE_LOD4( 11 ),
    SHAPE_LOD5( 12 ),
    SHAPE_LOD6( 13 ),
    SHAPE_LOD7( 14 ),
    SHAPE_LOD8( 15 ),
    SHAPE_LOD9( 16 ),
    XT_BREP( 17 ),
    WIREFRAME_REPRESENTATION( 18 ),
    ULP( 20 ),
    LWPA( 24 );

    
    private final Integer enumValue;
    private SegmentType( Integer v ) {
        this.enumValue = v;
    }

    /**
     * Get the integer value contained in the enumeration
     */
    public int getValue() {
        return enumValue;
    }
    /**
     * Get a SegmentType for the given enumeration value
     * @param v the enumeration value
     * @return the found value
     * @throws IllegalArgumentException  if no SegmentType was found for the given value
     */
    public static SegmentType valueOf( Integer v ) throws IllegalArgumentException {
        for ( SegmentType test : SegmentType.values() ) {
            if ( test.getValue() == v ) {
                return test;
            }
        }
        throw new IllegalArgumentException( "could not find SegmentType for value '" + v + "'" );

    }       
    
}

