/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.base.GUID;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.CodecFactory;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.I16;
import de.cadoculus.jove.bibi.annotation.I32;
import de.cadoculus.jove.bibi.annotation.UChar;
import java.lang.reflect.AnnotatedElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This factory creates codecs for Enumerations. Supported annotation / Java
 * types:</p>
 *
 * <ul>
 * <li>GUID / enum based on GUID</li>
 * <li>I32 / enum based on integer</li>
 * </ul>
 *
 * @author cz
 */
class EnumerationCodecFactory implements CodecFactory {

    private static Logger log = LoggerFactory.getLogger( EnumerationCodecFactory.class );

    @Override
    public <T> Codec<T> create( AnnotatedElement metadata, Class<T> type, WorkingContext ctx ) throws BIBIException {

        log.debug( "create " + metadata + ", " + type );
        log.debug( "    enum ass" + Enum.class.isAssignableFrom( type ) + ",  " + type.isEnum() );

        if ( !type.isEnum() ) {
            log.debug( "no enum" );
            return null;
        }
        if ( metadata == null ) {
            log.debug( "no metadata" );
            return null;
        }

        de.cadoculus.jove.bibi.annotation.GUID guidA = metadata.getAnnotation( de.cadoculus.jove.bibi.annotation.GUID.class );
        if ( guidA != null ) {
            log.debug( "create GUID based Enum Codec for " + type );
            return (Codec<T>) new EnumerationCodec( new GUIDCodec( metadata, GUID.class ), metadata, type );
        }
        de.cadoculus.jove.bibi.annotation.I32 intA = metadata.getAnnotation( de.cadoculus.jove.bibi.annotation.I32.class );
        if ( intA != null ) {
            log.debug( "create Int based Enum Codec for " + type );
            return (Codec<T>) new EnumerationCodec( new NumberCodec( I32.class, Integer.class ), metadata, type );
        }
        de.cadoculus.jove.bibi.annotation.I16 shortA = metadata.getAnnotation( de.cadoculus.jove.bibi.annotation.I16.class );
        if ( shortA != null ) {
            log.debug( "create short based Enum Codec for " + type );
            return (Codec<T>) new EnumerationCodec( new NumberCodec( I16.class, Short.class ), metadata, type );
        }
        de.cadoculus.jove.bibi.annotation.UChar charA = metadata.getAnnotation( de.cadoculus.jove.bibi.annotation.UChar.class );
        if ( charA != null ) {
            log.debug( "create char based Enum Codec for " + type );
            return (Codec<T>) new EnumerationCodec( new NumberCodec( UChar.class, Character.class ), metadata, type );
        }
        return null;
    }

}
