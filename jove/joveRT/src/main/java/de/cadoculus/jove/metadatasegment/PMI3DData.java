/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.7.1 PMI 3D Data</h4>
The PMI 3D Data collection defines a data format common to all 3D based PMI entities.
Along with the PMI Base Data and String identifier, this data collection also includes non-text polyline data defined by an array of indices into an array of polyline segments packed as 2D/3D vertex coordinates, specifying where each polyline
segment begins and ends. How polylines are constructed from this index array and packed vertex coordinates array is the same as that illustrated in Figure 144 of 7.2.6.2.1.1.1.2.2 Text Polyline Data.

*/
@XmlRootElement(name="PMI3DData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMI3DData",
    propOrder= {"pmiBaseData", "stringID", "polylineDimensionality", "polylineSegmentIndexCount", "polylineSegmentIndex", "polylineVertexCoords", }
)
public class PMI3DData {

    public static Logger log = LoggerFactory.getLogger( PMI3DData.class );

    /**  
     * The variable number 0 : PMIBaseData pmiBaseData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIBaseData")
    private PMIBaseData pmiBaseData;

    /**  
     * The variable number 1 : I32 stringID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer stringID;

    /**  
     * The variable number 2 : PMIPolylineDimensionality polylineDimensionality
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private PMIPolylineDimensionality polylineDimensionality;

    /**  
     * The variable number 3 : I32 polylineSegmentIndexCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer polylineSegmentIndexCount;

    /**  
     * The variable number 4 : I16[] polylineSegmentIndex
     * <br>options : <pre>{length : obj.polylineSegmentIndexCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.polylineSegmentIndexCount" )
    @XmlElementWrapper(name = "polylineSegmentIndex")
        private java.util.List<Short> polylineSegmentIndex;

    /**  
     * The variable number 5 : VecF32 polylineVertexCoords
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.VecF32()    
    private java.util.List<Float> polylineVertexCoords;



    /** Getter for pmiBaseData.
     *  
     * 
     * @return PMIBaseData
     */
    public PMIBaseData getPmiBaseData() {
        return pmiBaseData;
    }

     /** Setter for pmiBaseData.
     *  
     * 
     * @param value PMIBaseData
     */
    public void setPmiBaseData( PMIBaseData value ) {
        this.pmiBaseData = value;
    }

    /** Getter for stringID.
     *  
     * 
     * @return Integer
     */
    public Integer getStringID() {
        return stringID;
    }

     /** Setter for stringID.
     *  
     * 
     * @param value Integer
     */
    public void setStringID( Integer value ) {
        this.stringID = value;
    }

    /** Getter for polylineDimensionality.
     *  
     * 
     * @return PMIPolylineDimensionality
     */
    public PMIPolylineDimensionality getPolylineDimensionality() {
        return polylineDimensionality;
    }

     /** Setter for polylineDimensionality.
     *  
     * 
     * @param value PMIPolylineDimensionality
     */
    public void setPolylineDimensionality( PMIPolylineDimensionality value ) {
        this.polylineDimensionality = value;
    }

    /** Getter for polylineSegmentIndexCount.
     *  
     * 
     * @return Integer
     */
    public Integer getPolylineSegmentIndexCount() {
        return polylineSegmentIndexCount;
    }

     /** Setter for polylineSegmentIndexCount.
     *  
     * 
     * @param value Integer
     */
    public void setPolylineSegmentIndexCount( Integer value ) {
        this.polylineSegmentIndexCount = value;
    }

    /** Getter for polylineSegmentIndex.
     *  
     * 
     * @return java.util.List<Short>
     */
    public java.util.List<Short> getPolylineSegmentIndex() {
        return polylineSegmentIndex;
    }

     /** Setter for polylineSegmentIndex.
     *  
     * 
     * @param value java.util.List<Short>
     */
    public void setPolylineSegmentIndex( java.util.List<Short> value ) {
        this.polylineSegmentIndex = value;
    }

    /** Getter for polylineVertexCoords.
     *  
     * 
     * @return java.util.List<Float>
     */
    public java.util.List<Float> getPolylineVertexCoords() {
        return polylineVertexCoords;
    }

     /** Setter for polylineVertexCoords.
     *  
     * 
     * @param value java.util.List<Float>
     */
    public void setPolylineVertexCoords( java.util.List<Float> value ) {
        this.polylineVertexCoords = value;
    }


    
}

