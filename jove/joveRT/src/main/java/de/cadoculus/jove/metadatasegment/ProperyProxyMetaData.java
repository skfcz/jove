/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="ProperyProxyMetaData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "ProperyProxyMetaData",
    propOrder= {"propertyKey", "propertyValueType", "stringPropertyValue", "integerPropertyValue", "floatPropertyValue", "datePropertyValue", }
)
public class ProperyProxyMetaData {

    public static Logger log = LoggerFactory.getLogger( ProperyProxyMetaData.class );

    /**  
     * The variable number 0 : MbString propertyKey
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.MbString()    
    private String propertyKey;

    /**  
     * The variable number 1 : U8 propertyValueType
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U8()    
    private Short propertyValueType;

    /**  
     * The variable number 2 : MbString stringPropertyValue
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.MbString()    
    private String stringPropertyValue;

    /**  
     * The variable number 3 : I32 integerPropertyValue
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer integerPropertyValue;

    /**  
     * The variable number 4 : F32 floatPropertyValue
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float floatPropertyValue;

    /**  
     * The variable number 5 : DatePropertyValue datePropertyValue
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="DatePropertyValue")
    private DatePropertyValue datePropertyValue;



    /** Getter for propertyKey.
     *  
     * 
     * @return String
     */
    public String getPropertyKey() {
        return propertyKey;
    }

     /** Setter for propertyKey.
     *  
     * 
     * @param value String
     */
    public void setPropertyKey( String value ) {
        this.propertyKey = value;
    }

    /** Getter for propertyValueType.
     *  
     * 
     * @return Short
     */
    public Short getPropertyValueType() {
        return propertyValueType;
    }

     /** Setter for propertyValueType.
     *  
     * 
     * @param value Short
     */
    public void setPropertyValueType( Short value ) {
        this.propertyValueType = value;
    }

    /** Getter for stringPropertyValue.
     *  
     * 
     * @return String
     */
    public String getStringPropertyValue() {
        return stringPropertyValue;
    }

     /** Setter for stringPropertyValue.
     *  
     * 
     * @param value String
     */
    public void setStringPropertyValue( String value ) {
        this.stringPropertyValue = value;
    }

    /** Getter for integerPropertyValue.
     *  
     * 
     * @return Integer
     */
    public Integer getIntegerPropertyValue() {
        return integerPropertyValue;
    }

     /** Setter for integerPropertyValue.
     *  
     * 
     * @param value Integer
     */
    public void setIntegerPropertyValue( Integer value ) {
        this.integerPropertyValue = value;
    }

    /** Getter for floatPropertyValue.
     *  
     * 
     * @return Float
     */
    public Float getFloatPropertyValue() {
        return floatPropertyValue;
    }

     /** Setter for floatPropertyValue.
     *  
     * 
     * @param value Float
     */
    public void setFloatPropertyValue( Float value ) {
        this.floatPropertyValue = value;
    }

    /** Getter for datePropertyValue.
     *  
     * 
     * @return DatePropertyValue
     */
    public DatePropertyValue getDatePropertyValue() {
        return datePropertyValue;
    }

     /** Setter for datePropertyValue.
     *  
     * 
     * @param value DatePropertyValue
     */
    public void setDatePropertyValue( DatePropertyValue value ) {
        this.datePropertyValue = value;
    }


    
}

