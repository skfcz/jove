/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Data class represents any kind of content found after the SegmentHeader
 * in a segment. For known and supported segments like the LSG Segment, there
 * needs to exists a specialized class with associated code.
 *
 * @author Zerbst
 */
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType(  name = "Data" )
@XmlRootElement(name = "Data")
@   de.cadoculus.jove.bibi.annotation.BIBICodec( de.cadoculus.jove.bibi.codec.ObjectDataCodec.class )
public class ObjectData {
}
