/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Aüache License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl;

import de.cadoculus.jove.model.GUID;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * The SegmentDeclaration represents a segment in the JT file. Unlike a Block a
 * segment is identified by a GUID ?
 *
 * @author Carsten Zerbst
 */
public class SegmentDeclaration extends BlockDeclaration {

    private GUID guidID;
    private final Set<EnumerationDeclaration> enumerations = new HashSet<>();

    @Override
    public String toString() {

        StringBuffer buff = new StringBuffer();
        buff.append( "SegmentDeclaration{ name=" ).append( name ).append( ", entities=[ " );
        for ( Iterator<EntityDeclaration> it = entities.iterator(); it.hasNext(); ) {
            EntityDeclaration entityDeclaration = it.next();
            buff.append( entityDeclaration.getName() );
            if ( it.hasNext() ) {
                buff.append( ", " );
            }
        }
        buff.append( "], properties=" );
        buff.append( properties.toString() );
        buff.append( "}" );
        return buff.toString();
    }

    /**
     * @return the guidID
     */
    public GUID getGuidID() {
        return guidID;
    }

    /**
     * @param guidID the guidID to set
     */
    public void setGuidID( GUID guidID ) {
        this.guidID = guidID;
    }

    /**
     * Get the list of defined enumerations.
     *
     * @return the defined enumerations
     */
    public Set<EnumerationDeclaration> getEnumerations() {

        return enumerations;
    }

}
