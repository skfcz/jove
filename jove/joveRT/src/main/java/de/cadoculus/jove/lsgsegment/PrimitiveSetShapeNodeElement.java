/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="PrimitiveSetShapeNodeElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PrimitiveSetShapeNodeElement",
    propOrder= {"baseShapeData", "versionNumberPSSN1", "textureCoordBinding", "colorBinding", "textureCoordGenType", "versionNumberPSSN2", "primitiveSetQuantizationParameters", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0xe40373c1, 0x1ad9, 0x11d3, 0x9d, 0xaf, 0x00, 0xa0, 0xc9, 0xc7, 0xdd, 0xc2" )
public class PrimitiveSetShapeNodeElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( PrimitiveSetShapeNodeElement.class );

    /**  
     * The variable number 0 : BaseShapeData baseShapeData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="BaseShapeData")
    private BaseShapeData baseShapeData;

    /**  
     * The variable number 1 : I16 versionNumberPSSN1
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumberPSSN1;

    /**  
     * The variable number 2 : I32 textureCoordBinding
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer textureCoordBinding;

    /**  
     * The variable number 3 : I32 colorBinding
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer colorBinding;

    /**  
     * The variable number 4 : I32 textureCoordGenType
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer textureCoordGenType;

    /**  
     * The variable number 5 : I16 versionNumberPSSN2
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumberPSSN2;

    /**  
     * The variable number 6 : PrimitiveSetQuantizationParameters primitiveSetQuantizationParameters
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PrimitiveSetQuantizationParameters")
    private PrimitiveSetQuantizationParameters primitiveSetQuantizationParameters;



    /** Getter for baseShapeData.
     *  
     * 
     * @return BaseShapeData
     */
    public BaseShapeData getBaseShapeData() {
        return baseShapeData;
    }

     /** Setter for baseShapeData.
     *  
     * 
     * @param value BaseShapeData
     */
    public void setBaseShapeData( BaseShapeData value ) {
        this.baseShapeData = value;
    }

    /** Getter for versionNumberPSSN1.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberPSSN1() {
        return versionNumberPSSN1;
    }

     /** Setter for versionNumberPSSN1.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberPSSN1( Short value ) {
        this.versionNumberPSSN1 = value;
    }

    /** Getter for textureCoordBinding.
     *  
     * 
     * @return Integer
     */
    public Integer getTextureCoordBinding() {
        return textureCoordBinding;
    }

     /** Setter for textureCoordBinding.
     *  
     * 
     * @param value Integer
     */
    public void setTextureCoordBinding( Integer value ) {
        this.textureCoordBinding = value;
    }

    /** Getter for colorBinding.
     *  
     * 
     * @return Integer
     */
    public Integer getColorBinding() {
        return colorBinding;
    }

     /** Setter for colorBinding.
     *  
     * 
     * @param value Integer
     */
    public void setColorBinding( Integer value ) {
        this.colorBinding = value;
    }

    /** Getter for textureCoordGenType.
     *  
     * 
     * @return Integer
     */
    public Integer getTextureCoordGenType() {
        return textureCoordGenType;
    }

     /** Setter for textureCoordGenType.
     *  
     * 
     * @param value Integer
     */
    public void setTextureCoordGenType( Integer value ) {
        this.textureCoordGenType = value;
    }

    /** Getter for versionNumberPSSN2.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberPSSN2() {
        return versionNumberPSSN2;
    }

     /** Setter for versionNumberPSSN2.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberPSSN2( Short value ) {
        this.versionNumberPSSN2 = value;
    }

    /** Getter for primitiveSetQuantizationParameters.
     *  
     * 
     * @return PrimitiveSetQuantizationParameters
     */
    public PrimitiveSetQuantizationParameters getPrimitiveSetQuantizationParameters() {
        return primitiveSetQuantizationParameters;
    }

     /** Setter for primitiveSetQuantizationParameters.
     *  
     * 
     * @param value PrimitiveSetQuantizationParameters
     */
    public void setPrimitiveSetQuantizationParameters( PrimitiveSetQuantizationParameters value ) {
        this.primitiveSetQuantizationParameters = value;
    }


    
}

