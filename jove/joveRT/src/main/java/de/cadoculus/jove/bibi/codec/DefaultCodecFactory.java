/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.CodecFactory;
import de.cadoculus.jove.bibi.WorkingContext;
import java.lang.reflect.AnnotatedElement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * This class is the default entry point to create {@link Codec Codecs}. It
 * works as mere delegate to the more specific
 * {@link CodecFactory CodecFactories} for Strings, Numbers, Objects, ...
 *
 * @author Zerbst
 */
public class DefaultCodecFactory implements CodecFactory {

    private static Logger log = LoggerFactory.getLogger( DefaultCodecFactory.class );
    private final List<CodecFactory> factories = new ArrayList<>();

    public DefaultCodecFactory() {
        factories.add( new ByteCodecFactory() );
        factories.add( new GUIDCodecFactory() );
        factories.add( new StringCodecFactory() );
        factories.add( new NumberCodecFactory() );
        factories.add( new EnumerationCodecFactory() );
        factories.add( new ObjectIDCodecFactory() );
        factories.add( new CompositeTypeCodecFactory() );
        factories.add( new ObjectCodecFactory() );
        factories.add( new ListCodecFactory() );

    }

    @Override
    public <T> Codec<T> create( AnnotatedElement metadata, Class<T> type, WorkingContext ctx ) throws BIBIException {
        Codec<T> retval = null;

        //log.debug( "create codec for " + type );
        for ( CodecFactory fact : factories ) {
            retval = fact.create( metadata, type, ctx );
            if ( retval != null ) {
                MDC.put( WorkingContext.CODEC, retval.getClass().getSimpleName() );
                return retval;
            }
        }
        throw new BIBIException( "no factory found to create codec for type " + type.getSimpleName() + " (" + metadata + ")" );

    }
}
