/*
 * Copyright (C) 2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.base;

/**
 * This enumeration contains the loading status of an element
 *
 * @author cz
 */
public enum LoadingStatus {
    
    /**
     * Status is unknown, e.g. element is currently loading but not finished
     */
    UNKNOWN,

    /**
     * Loading completed successfully
     */
    LOADING_FINISHED,
    /**
     * Loading failed
     */
    LOADING_FAILED,

    /**
     * Loading was not performed, as the element is postphoned as late loaded
     * element.
     */
    LOADING_POSTPHONED;
}
