/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>8.1.16.1 Compressed CAD Tag Type-2 Dat</h4>
Compressed CAD Tag Type-2 Data collection contains the Type-2 (i.e. 64 Bit integer Type) CAD Tag data for a list of CAD
Tags.
The Compressed CAD Tag Type-2 Data collection is only present if there are Type-2 CAD Tags in the CAD Tag Types
vector. Thus a loader/reader of JT file must first uncompress/decode and evaluate the previously read CAD Tag Types vector
to determine if there are any Type-2 CAD Tags and if so, then the Compressed CAD Tag Type-2 Data collection is present.

*/
@XmlRootElement(name="CompressedCADTagType2Data")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "CompressedCADTagType2Data",
    propOrder= {"firstI32", "secondI32", }
)
public class CompressedCADTagType2Data {

    public static Logger log = LoggerFactory.getLogger( CompressedCADTagType2Data.class );

    /**  
     * The variable number 0 : VecI32 firstI32
     * <br>options : <pre>{compression : {Int32CDP2, Lag1}}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.VecI32()    
    private java.util.List<Integer> firstI32;

    /**  
     * The variable number 1 : VecI32 secondI32
     * <br>options : <pre>{compression : {Int32CDP2, Lag1}}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.VecI32()    
    private java.util.List<Integer> secondI32;



    /** Getter for firstI32.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getFirstI32() {
        return firstI32;
    }

     /** Setter for firstI32.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setFirstI32( java.util.List<Integer> value ) {
        this.firstI32 = value;
    }

    /** Getter for secondI32.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getSecondI32() {
        return secondI32;
    }

     /** Setter for secondI32.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setSecondI32( java.util.List<Integer> value ) {
        this.secondI32 = value;
    }


    
}

