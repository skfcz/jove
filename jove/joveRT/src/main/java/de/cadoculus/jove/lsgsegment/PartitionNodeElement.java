/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.1.1.1.2 Partition Node Element </h4>
A Partition Node represents an external JT file reference and provides a means to partition a model into multiple physical JT
files (e.g. separate JT file per part in an assembly). When the referenced JT file is opened, the Partition Node‟s children are
really the children of the LSG root node for the underlying JT file. Usage of Partition Nodes in LSG also aids in supporting
JT file loader/reader "best practice" of late loading data (i.e. can delay opening and loading the externally referenced JT file
until the data is needed).

*/
@XmlRootElement(name="PartitionNodeElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PartitionNodeElement",
    propOrder= {"groupNodeData", "partitionFlags", "fileName", "reservedField", "transformedBox", "area", "vertexCountRange", "nodeCountRange", "polygonCountRange", "untransformedBox", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0x10dd103e, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97" )
public class PartitionNodeElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( PartitionNodeElement.class );

    /**  
     * The variable number 0 : GroupNodeData groupNodeData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="GroupNodeData")
    private GroupNodeData groupNodeData;

    /**  
     * The variable number 1 : I32 partitionFlags
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer partitionFlags;

    /**  
     * The variable number 2 : MbString fileName
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.MbString()    
    private String fileName;

    /**  
     * The variable number 3 : BBoxF32 reservedField
     * <br>options : <pre>{valid : (obj.partitionFlags & 0x00000001) != 0}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BBoxF32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "(obj.partitionFlags & 0x00000001) != 0" )
    private BBoxF32 reservedField;

    /**  
     * The variable number 4 : BBoxF32 transformedBox
     * <br>options : <pre>{valid : (obj.partitionFlags & 0x00000001) == 0}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BBoxF32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "(obj.partitionFlags & 0x00000001) == 0" )
    private BBoxF32 transformedBox;

    /**  
     * The variable number 5 : F32 area
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float area;

    /**  
     * The variable number 6 : VertexCountRange vertexCountRange
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="VertexCountRange")
    private VertexCountRange vertexCountRange;

    /**  
     * The variable number 7 : NodeCountRange nodeCountRange
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="NodeCountRange")
    private NodeCountRange nodeCountRange;

    /**  
     * The variable number 8 : PolygonCountRange polygonCountRange
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PolygonCountRange")
    private PolygonCountRange polygonCountRange;

    /**  
     * The variable number 9 : BBoxF32 untransformedBox
     * <br>options : <pre>{valid : (obj.partitionFlags & 0x00000001) != 0}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BBoxF32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "(obj.partitionFlags & 0x00000001) != 0" )
    private BBoxF32 untransformedBox;



    /** Getter for groupNodeData.
     *  
     * 
     * @return GroupNodeData
     */
    public GroupNodeData getGroupNodeData() {
        return groupNodeData;
    }

     /** Setter for groupNodeData.
     *  
     * 
     * @param value GroupNodeData
     */
    public void setGroupNodeData( GroupNodeData value ) {
        this.groupNodeData = value;
    }

    /** Getter for partitionFlags.
     *  
     * 
     * @return Integer
     */
    public Integer getPartitionFlags() {
        return partitionFlags;
    }

     /** Setter for partitionFlags.
     *  
     * 
     * @param value Integer
     */
    public void setPartitionFlags( Integer value ) {
        this.partitionFlags = value;
    }

    /** Getter for fileName.
     *  
     * 
     * @return String
     */
    public String getFileName() {
        return fileName;
    }

     /** Setter for fileName.
     *  
     * 
     * @param value String
     */
    public void setFileName( String value ) {
        this.fileName = value;
    }

    /** Getter for reservedField.
     *  
     * 
     * @return BBoxF32
     */
    public BBoxF32 getReservedField() {
        return reservedField;
    }

     /** Setter for reservedField.
     *  
     * 
     * @param value BBoxF32
     */
    public void setReservedField( BBoxF32 value ) {
        this.reservedField = value;
    }

    /** Getter for transformedBox.
     *  
     * 
     * @return BBoxF32
     */
    public BBoxF32 getTransformedBox() {
        return transformedBox;
    }

     /** Setter for transformedBox.
     *  
     * 
     * @param value BBoxF32
     */
    public void setTransformedBox( BBoxF32 value ) {
        this.transformedBox = value;
    }

    /** Getter for area.
     *  
     * 
     * @return Float
     */
    public Float getArea() {
        return area;
    }

     /** Setter for area.
     *  
     * 
     * @param value Float
     */
    public void setArea( Float value ) {
        this.area = value;
    }

    /** Getter for vertexCountRange.
     *  
     * 
     * @return VertexCountRange
     */
    public VertexCountRange getVertexCountRange() {
        return vertexCountRange;
    }

     /** Setter for vertexCountRange.
     *  
     * 
     * @param value VertexCountRange
     */
    public void setVertexCountRange( VertexCountRange value ) {
        this.vertexCountRange = value;
    }

    /** Getter for nodeCountRange.
     *  
     * 
     * @return NodeCountRange
     */
    public NodeCountRange getNodeCountRange() {
        return nodeCountRange;
    }

     /** Setter for nodeCountRange.
     *  
     * 
     * @param value NodeCountRange
     */
    public void setNodeCountRange( NodeCountRange value ) {
        this.nodeCountRange = value;
    }

    /** Getter for polygonCountRange.
     *  
     * 
     * @return PolygonCountRange
     */
    public PolygonCountRange getPolygonCountRange() {
        return polygonCountRange;
    }

     /** Setter for polygonCountRange.
     *  
     * 
     * @param value PolygonCountRange
     */
    public void setPolygonCountRange( PolygonCountRange value ) {
        this.polygonCountRange = value;
    }

    /** Getter for untransformedBox.
     *  
     * 
     * @return BBoxF32
     */
    public BBoxF32 getUntransformedBox() {
        return untransformedBox;
    }

     /** Setter for untransformedBox.
     *  
     * 
     * @param value BBoxF32
     */
    public void setUntransformedBox( BBoxF32 value ) {
        this.untransformedBox = value;
    }


    
}

