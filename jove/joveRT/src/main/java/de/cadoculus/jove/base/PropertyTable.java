/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.base;

import de.cadoculus.jove.lsgsegment.BaseAttributeElement;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cz
 */
@XmlRootElement( name = "PropertyTable" )
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType(
         name = "PropertyTable",
        propOrder = { "versionNumber", "elementPropertyTableCount", "objectId2properties" }
)

public class PropertyTable {

    private static org.slf4j.Logger log = LoggerFactory.getLogger( PropertyTable.class );

    private short versionNumber;

    private int elementPropertyTableCount;

    @XmlElementWrapper( name = "objectId2properties" )
    @XmlElements( {
        @XmlElement( name = "Element2Properties", type = Element2Properties.class )
    } )
    private List<Element2Properties> objectId2properties = new ArrayList<>();

    /**
     * @return the versionNumber
     */
    public short getVersionNumber() {
        return versionNumber;
    }

    /**
     * @param versionNumber the versionNumber to set
     */
    public void setVersionNumber( short versionNumber ) {
        this.versionNumber = versionNumber;
    }

    /**
     * @return the elementPropertyTableCount
     */
    public int getElementPropertyTableCount() {
        return elementPropertyTableCount;
    }

    /**
     * @param elementPropertyTableCount the elementPropertyTableCount to set
     */
    public void setElementPropertyTableCount( int elementPropertyTableCount ) {
        this.elementPropertyTableCount = elementPropertyTableCount;
    }

    /**
     * @return the properties
     */
    public List<Element2Properties> getProperties() {
        return objectId2properties;
    }

}
