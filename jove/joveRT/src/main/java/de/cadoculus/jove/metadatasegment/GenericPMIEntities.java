/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.6 Generic PMI Entities</h4>
The Generic PMI Entities data collection provides a "generic" format for defining various PMI entity types, including user defined types. The generic format defines the data making up the PMI Entity through a combination of the PMI 2D Data collection and a list of PMI Property data collections.

*/
@XmlRootElement(name="GenericPMIEntities")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "GenericPMIEntities",
    propOrder= {"genericEntityCount", "genericEntities", }
)
public class GenericPMIEntities {

    public static Logger log = LoggerFactory.getLogger( GenericPMIEntities.class );

    /**  
     * The variable number 0 : I32 genericEntityCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer genericEntityCount;

    /**  
     * The variable number 1 : GenericPMIEntity[] genericEntities
     * <br>options : <pre>{length : obj.genericEntityCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.genericEntityCount" )
    @XmlElementWrapper(name = "genericEntities")
    @XmlElementRef()    private java.util.List<GenericPMIEntity> genericEntities;



    /** Getter for genericEntityCount.
     *  
     * 
     * @return Integer
     */
    public Integer getGenericEntityCount() {
        return genericEntityCount;
    }

     /** Setter for genericEntityCount.
     *  
     * 
     * @param value Integer
     */
    public void setGenericEntityCount( Integer value ) {
        this.genericEntityCount = value;
    }

    /** Getter for genericEntities.
     *  
     * 
     * @return java.util.List<GenericPMIEntity>
     */
    public java.util.List<GenericPMIEntity> getGenericEntities() {
        return genericEntities;
    }

     /** Setter for genericEntities.
     *  
     * 
     * @param value java.util.List<GenericPMIEntity>
     */
    public void setGenericEntities( java.util.List<GenericPMIEntity> value ) {
        this.genericEntities = value;
    }


    
}

