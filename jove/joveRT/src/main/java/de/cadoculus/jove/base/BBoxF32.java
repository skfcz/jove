/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.base;

import javax.vecmath.Point3f;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * This class represents a bounding box by the lower left and upper right
 * corner.<p> The BBoxF32 type defines a bounding box using two CoordF32 types
 * to store the XYZ coordinates for the bounding box minimum and maximum corner
 * points.
 *
 * @author Zerbst
 */
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType(
name = "BBox",
propOrder = { "min", "max" } )
public class BBoxF32 {
    
    private Point3f max;    
    private Point3f min;

    @Override
    public String toString() {
        return "BoundingBox [" + getMin() + " , " + getMax() + "]";
    }

    /**
     * @return the max
     */
    public Point3f getMax() {
        return max;
    }

    /**
     * @param max the max to set
     */
    public void setMax( Point3f max ) {
        this.max = max;
    }

    /**
     * @return the min
     */
    public Point3f getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin( Point3f min ) {
        this.min = min;
    }

}
