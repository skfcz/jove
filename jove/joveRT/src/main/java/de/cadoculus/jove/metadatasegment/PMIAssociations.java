/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.2 PMI Associations</h4>
The PMI Associations data collection defines data for a list of associations. An association defines a link ("relationship") between two PMI, B-Rep, or Wireframe Rep entities where one entity is defined as the "source" and the other entity is defined as the "destination".

*/
@XmlRootElement(name="PMIAssociations")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIAssociations",
    propOrder= {"associationCount", "associations", }
)
public class PMIAssociations {

    public static Logger log = LoggerFactory.getLogger( PMIAssociations.class );

    /**  
     * The variable number 0 : I32 associationCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer associationCount;

    /**  
     * The variable number 1 : PMIAssociation[] associations
     * <br>options : <pre>{length : obj.associationCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.associationCount" )
    @XmlElementWrapper(name = "associations")
    @XmlElementRef()    private java.util.List<PMIAssociation> associations;



    /** Getter for associationCount.
     *  
     * 
     * @return Integer
     */
    public Integer getAssociationCount() {
        return associationCount;
    }

     /** Setter for associationCount.
     *  
     * 
     * @param value Integer
     */
    public void setAssociationCount( Integer value ) {
        this.associationCount = value;
    }

    /** Getter for associations.
     *  
     * 
     * @return java.util.List<PMIAssociation>
     */
    public java.util.List<PMIAssociation> getAssociations() {
        return associations;
    }

     /** Setter for associations.
     *  
     * 
     * @param value java.util.List<PMIAssociation>
     */
    public void setAssociations( java.util.List<PMIAssociation> value ) {
        this.associations = value;
    }


    
}

