/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.13 PMI Coordinate System Entities</h4>
The PMI Coordinate System Entities data collection defines data for a list of Coordinate Systems.

*/
@XmlRootElement(name="PMICoordinateSystemEntities")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMICoordinateSystemEntities",
    propOrder= {"coordSysCount", "coordinateSystemEntities", }
)
public class PMICoordinateSystemEntities {

    public static Logger log = LoggerFactory.getLogger( PMICoordinateSystemEntities.class );

    /**  
     * The variable number 0 : I32 coordSysCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer coordSysCount;

    /**  
     * The variable number 1 : PMICoordinateSystemEntity[] coordinateSystemEntities
     * <br>options : <pre>{length : obj.coordSysCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.coordSysCount" )
    @XmlElementWrapper(name = "coordinateSystemEntities")
    @XmlElementRef()    private java.util.List<PMICoordinateSystemEntity> coordinateSystemEntities;



    /** Getter for coordSysCount.
     *  
     * 
     * @return Integer
     */
    public Integer getCoordSysCount() {
        return coordSysCount;
    }

     /** Setter for coordSysCount.
     *  
     * 
     * @param value Integer
     */
    public void setCoordSysCount( Integer value ) {
        this.coordSysCount = value;
    }

    /** Getter for coordinateSystemEntities.
     *  
     * 
     * @return java.util.List<PMICoordinateSystemEntity>
     */
    public java.util.List<PMICoordinateSystemEntity> getCoordinateSystemEntities() {
        return coordinateSystemEntities;
    }

     /** Setter for coordinateSystemEntities.
     *  
     * 
     * @param value java.util.List<PMICoordinateSystemEntity>
     */
    public void setCoordinateSystemEntities( java.util.List<PMICoordinateSystemEntity> value ) {
        this.coordinateSystemEntities = value;
    }


    
}

