/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.7 PMI CAD Tag Data</h4>
The PMI CAD Tag Data collection contains the list of persistent IDs, as defined in the CAD System, to uniquely identify individual PMI entities. The existence of this PMI CAD Tag Data collection is dependent upon the value of previously read data field CAD Tags Flag as documented in 7.2.6.2 PMI Manager Meta Data Element.
If PMI CAD Tag Data collection is present, there will be a CAD Tag for each PMI entity as specified by the below documented CAD Tag Index Count formula.

*/
@XmlRootElement(name="PMICADTagData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMICADTagData",
    propOrder= {"cadTagIndexCount", "cadTagIndex", "cadTagData", }
)
public class PMICADTagData {

    public static Logger log = LoggerFactory.getLogger( PMICADTagData.class );

    /**  
     * The variable number 0 : I32 cadTagIndexCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer cadTagIndexCount;

    /**  
     * The variable number 1 : I32[] cadTagIndex
     * <br>options : <pre>{length : obj.cadTagIndexCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.cadTagIndexCount" )
    @XmlElementWrapper(name = "cadTagIndex")
        private java.util.List<Integer> cadTagIndex;

    /**  
     * The variable number 2 : CompressedCADTagData cadTagData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="CompressedCADTagData")
    private CompressedCADTagData cadTagData;



    /** Getter for cadTagIndexCount.
     *  
     * 
     * @return Integer
     */
    public Integer getCadTagIndexCount() {
        return cadTagIndexCount;
    }

     /** Setter for cadTagIndexCount.
     *  
     * 
     * @param value Integer
     */
    public void setCadTagIndexCount( Integer value ) {
        this.cadTagIndexCount = value;
    }

    /** Getter for cadTagIndex.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getCadTagIndex() {
        return cadTagIndex;
    }

     /** Setter for cadTagIndex.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setCadTagIndex( java.util.List<Integer> value ) {
        this.cadTagIndex = value;
    }

    /** Getter for cadTagData.
     *  
     * 
     * @return CompressedCADTagData
     */
    public CompressedCADTagData getCadTagData() {
        return cadTagData;
    }

     /** Setter for cadTagData.
     *  
     * 
     * @param value CompressedCADTagData
     */
    public void setCadTagData( CompressedCADTagData value ) {
        this.cadTagData = value;
    }


    
}

