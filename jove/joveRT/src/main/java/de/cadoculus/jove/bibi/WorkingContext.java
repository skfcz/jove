/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi;

import de.cadoculus.jove.base.SegmentHeader;
import de.cadoculus.jove.fileheader.FileHeader;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * The WorkingContext class contains a number of important values for the
 * current state of the loading process.
 *
 * @author cz
 */
public class WorkingContext {

    public static final String FILEHEADER = "fileheader";
    public static final String JT_VERSION = "jtVersion";
    public static final String BYTE_ORDER = "byteOrder";
    public static final String SEGMENT_HEADER = "segementHeader";
    public static final String CODEC = "codec";
    public static final String OBJECT = "obj";
    public static final String OBJECT_TYPE = "type";
    public static final String FIELD = "field";
    public static final String POSITION = "position";
    public static final String PMI_VERSION_NUMBER = "pmiVersionNumber";

    private CodecFactory defaultCodecFactory;
    private FileHeader fileHeader;
    private Object current;
    private ByteBuffer byteBuffer;
    private SegmentHeader segmentHeader;
    private long maxPosition = -1;
    private Exception exception;
    private Configuration conf;
    private short pmiVersionNumber = -1;
    private boolean lateLoading = true;

    /**
     * Create a new WorkingContext based on given properties
     *
     * @param cfg the given properties, may be null
     */
    public WorkingContext( Properties cfg ) {
        this.conf = new Configuration();
        if ( cfg != null ) {
            this.conf.addProperties( cfg );
        }
    }

    /**
     * A copy constructor.
     *
     * @param src the WorkingContext to copy all attributes from
     */
    public WorkingContext( WorkingContext src ) {
        this.defaultCodecFactory = src.getDefaultCodecFactory();
        this.fileHeader = src.getFileHeader();
        this.current = src.getObj();
        this.byteBuffer = src.getByteBuffer();
        this.segmentHeader = src.getSegmentHeader();
        this.maxPosition = src.getMaxPosition();
        this.conf = new Configuration( src.getConf() );
        this.pmiVersionNumber = src.getPmiVersionNumber();
        this.lateLoading = src.lateLoading;
    }

    public void setDefaultCodecFactory( CodecFactory dcf ) {
        this.defaultCodecFactory = dcf;
    }

    public CodecFactory getDefaultCodecFactory() {
        return defaultCodecFactory;
    }

    public void setFileHeader( FileHeader header ) {
        this.fileHeader = header;
    }

    public FileHeader getFileHeader() {
        return fileHeader;
    }

    /**
     * Get the current object
     *
     * @return the current
     */
    public Object getObj() {
        return current;
    }

    /**
     * Set the current obj
     *
     * @param current the current to set
     */
    public void setObj( Object current ) {
        this.current = current;
    }

    /**
     * Creates a Map of Key-Value Pairs with WorkingContext settings that should
     * be exposed to template processing. Each Entry corresponds to a variable;
     * key names are the public constants VAR_XYZ.... Put all variables into a
     * map typically used during template evaluation.
     *
     * @return the Map
     */
    public Map<String, Object> setupMap() {
        Map<String, Object> variables = new HashMap<>();

        variables.put( FILEHEADER, fileHeader );
        variables.put( JT_VERSION, fileHeader != null ? Integer.toString( fileHeader.getIntVersion() ) : "00" );
        if ( fileHeader != null && fileHeader.getByteOrder() != null ) {
            variables.put( BYTE_ORDER, ( ( 0 == fileHeader.getByteOrder() ) ? "LITTLE_ENDIAN" : "BIG_ENDIAN" ) );
        } else {
            variables.put( BYTE_ORDER, "BIG_ENDIAN" );
        }
        variables.put( SEGMENT_HEADER, segmentHeader );
        variables.put( OBJECT, current );
        variables.put( PMI_VERSION_NUMBER, Short.toString( pmiVersionNumber ) );

        return variables;
    }

    /**
     * @return the byteBuffer
     */
    public ByteBuffer getByteBuffer() {
        return byteBuffer;
    }

    /**
     * @param byteBuffer the byteBuffer to set
     */
    public void setByteBuffer( ByteBuffer byteBuffer ) {
        this.byteBuffer = byteBuffer;
    }

    /**
     * @return the segementHeader
     */
    public SegmentHeader getSegmentHeader() {
        return segmentHeader;
    }

    /**
     * @param segementHeader the segementHeader to set
     */
    public void setSegmentHeader( SegmentHeader segementHeader ) {
        this.segmentHeader = segementHeader;
    }

    /**
     * Get the latest postion a Codec may not pass in the current ByteBuffer.
     *
     * @return the maxPosition the position or a negativ number If the position
     * is unknown
     */
    public long getMaxPosition() {
        return maxPosition;
    }

    /**
     * Set the latest postion a Codec may not pass in the current ByteBuffer.
     *
     * @param maxPosition the maxPosition to set
     */
    public void setMaxPosition( long maxPosition ) {
        this.maxPosition = maxPosition;
    }

    /**
     * Set an exception which occured during reading / writing
     */
    public void setException( BIBIException exp ) {
        this.exception = exp;
    }

    /**
     * Get the exception which occured during reading / writing
     *
     * @return the caught exception
     */
    public Exception getException() {
        return exception;
    }

    /**
     * Get the configuration
     *
     * @return the configuration
     */
    public Configuration getConf() {
        return this.conf;
    }

    /**
     * @return the pmiVersionNumber
     */
    public short getPmiVersionNumber() {
        return pmiVersionNumber;
    }

    /**
     * @param pmiVersionNumber the pmiVersionNumber to set
     */
    public void setPmiVersionNumber( short pmiVersionNumber ) {
        this.pmiVersionNumber = pmiVersionNumber;
    }

    public void addLoadInformation( String level, String msg ) {

    }

    /**
     * @return the lateLoading
     */
    public boolean isLateLoading() {
        return lateLoading;
    }

    /**
     * @param lateLoading the lateLoading to set
     */
    public void setLateLoading( boolean lateLoading ) {
        this.lateLoading = lateLoading;
    }

}
