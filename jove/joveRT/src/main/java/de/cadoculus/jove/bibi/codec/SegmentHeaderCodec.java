/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.base.LoadingStatus;
import de.cadoculus.jove.enums.SegmentType;
import de.cadoculus.jove.base.SegmentHeader;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.lsgsegment.LSGSegment;
import de.cadoculus.jove.metadatasegment.MetaDataSegment;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 * The SegmentHeaderCodec is used to read the complete segment header.
 *
 * This codec skips reading the object data by default unless the configuration
 * contains the value "true" for the key "read.segment.${segementType}". E.g.
 * read.segment.LOGICAL_SCENE_GRAPH=true.
 *
 * @author Zerbst
 */
public class SegmentHeaderCodec extends ObjectCodec<SegmentHeader> {

    private static org.slf4j.Logger log = LoggerFactory.getLogger( SegmentHeaderCodec.class );

    public SegmentHeaderCodec( Class clazz ) throws BIBIException {
        super( SegmentHeader.class );
    }

    /**
     * Adds a segment data specific codec to read the object data. E.g. a codec
     * for LSGSegment if segment type is LOGICAL_SCENE_GRAPH.
     *
     * @param field
     * @param originalCodec
     * @param ctx
     * @return
     */
    @Override
    protected Codec updateCodec( Field field, Codec originalCodec, WorkingContext ctx ) throws BIBIException {
        ctx.setSegmentHeader( this.myObject );
        if ( "objectData".equals( field.getName() ) ) {
            SegmentHeader segHeader = (SegmentHeader) ctx.getObj();
            boolean readData = ctx.isLateLoading() 
                    ? ctx.getConf().getBooleanProperty( "read.segment." + segHeader.getSegmentType(), false )
                    : true;
            log.info( ( readData ? "read " : "skip " ) + " data from " + segHeader.getSegmentType() + " segment" );
            if ( readData ) {
                if ( SegmentType.LOGICAL_SCENE_GRAPH == segHeader.getSegmentType() ) {
                    return ctx.getDefaultCodecFactory().create( null, LSGSegment.class, ctx );
                } else if ( SegmentType.META_DATA == segHeader.getSegmentType() ) {
                    return ctx.getDefaultCodecFactory().create( null, MetaDataSegment.class, ctx );
                } else if ( SegmentType.PMI_DATA == segHeader.getSegmentType() ) {
                    return ctx.getDefaultCodecFactory().create( null, MetaDataSegment.class, ctx );
                } else {
                    throw new IllegalArgumentException( "reading data from " + segHeader.getSegmentType()
                            + " segment not implemented yet" );
                }
            } else {
                this.myObject.setLoadingStatus( LoadingStatus.LOADING_POSTPHONED );
            }
        }
        return originalCodec;
    }

    @Override
    protected void handleException( BIBIException ex, Field field ) throws BIBIException {
        log.error( "an error occured when reading a segment", ex );
        myObject.setLoadingStatus( LoadingStatus.LOADING_FAILED );
        myObject.setException( ex);
    }

    @Override
    protected void postAction( Field field, Object value, WorkingContext ctx ) {
//        if ( "objectData".equals( field.getName() ) ) {
//            log.info( "objectData value " + this.myObject.getObjectData() );
//
//            if ( false && this.myObject.getObjectData() instanceof LSGSegment ) {
//                try {
//                    JAXBContext context = JAXBContext.newInstance( LSGSegment.class );
//                    Marshaller marshaller = context.createMarshaller();
//                    marshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
//                    marshaller.setProperty( Marshaller.JAXB_ENCODING, "UTF-8" );
//
//                    StringWriter sw = new StringWriter();
//                    marshaller.marshal( (LSGSegment) myObject.getObjectData(), sw );
//                    log.info( "LSG\n" + sw.toString() );
//
//                } catch ( Exception exp ) {
//                    log.error( "", exp );
//                }
//            }
//        }
    }
}
