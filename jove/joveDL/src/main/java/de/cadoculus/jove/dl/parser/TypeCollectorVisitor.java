/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl.parser;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class collects several type related information from the AST: <ul>
 * <li>The defined blocks and segments</li> <li>The entities defined per block /
 * segment</li> <li>The entities referenced per block / segment</li>
 *
 * @author Carsten Zerbst
 */
public class TypeCollectorVisitor implements JoveDLParserVisitor {

    public static Logger log = LoggerFactory.getLogger( TypeCollectorVisitor.class );
    public static final String CURRENT_BL_OR_SEG = "currentBlockOrSegment";
    public static final String DEF_ENTITIES = "defEntities";
    public static final String REF_TYPES = "refTypes";
    public static final String DEF_ENUMS = "defEnums";
    private int indent = 0;
    private Set<String> blockNames = new HashSet<String>();
    private Set<String> segmentNames = new HashSet<String>();
    private Set<String> enumNames = new HashSet<String>();
    private Map<String, Set<String>> blOrSeg2refEntityNames = new HashMap<String, Set<String>>();
    private Map<String, Set<String>> blOrSeg2defEntityNames = new HashMap<String, Set<String>>();
    private Map<String, Set<String>> blOrSeg2defEnumNames = new HashMap<String, Set<String>>();
    // Contains types which are available but defined outside JoveDL, e.g. the ElementHeader
    private static List<String> KNOWN_TYPES = Arrays.asList( new String[]{ "LogicalElementHeader", "LogicalElementHeaderZLIB" } );

    public Map<String, Set<String>> getAllDefinedTypes() {
        return blOrSeg2defEntityNames;
    }

    public Map<String, Set<String>> getAllDefinedEnums() {
        return blOrSeg2defEnumNames;
    }

    private String indent( int delta ) {
        indent += delta;
        StringBuffer buff = new StringBuffer();

        for ( int i = 0; i < indent; i++ ) {
            buff.append( "    " );
        }
        return buff.toString();
    }

    @Override
    public Object visit( SimpleNode node, java.util.Map<String, Object> data ) {

        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    private void check() {

        for ( Iterator<String> it = segmentNames.iterator(); it.hasNext(); ) {
            String segmentName = it.next();
            log.info( "check " + segmentName );

            Set<String> refTypes = blOrSeg2refEntityNames.get( segmentName );
            Set<String> defEntities = blOrSeg2defEntityNames.get( segmentName );
            Set<String> defEnums = blOrSeg2defEnumNames.get( segmentName );
            log.info( "defined entities " + defEntities );
            log.info( "defined enums  " + defEnums );
            log.info( "referenced " + refTypes );

            for ( Iterator<String> it1 = refTypes.iterator(); it1.hasNext(); ) {
                String referenced = it1.next();

                if ( defEntities.contains( referenced ) ) {
                    // ok
                } else if ( enumNames.contains( referenced ) ) {
                    // ok
                } else if ( defEnums.contains( referenced ) ) {
                    // ok
                } else if ( KNOWN_TYPES.contains( referenced ) ) {
                    // ok
                } else {
                    throw new IllegalArgumentException( "definition for segment " + segmentName + " references unknown entity " + referenced );

                }

            }

        }

    }

    public Set<String> getBlockNames() {
        return blockNames;
    }

    public Set<String> getSegmentNames() {
        return segmentNames;
    }

    public Set<String> getDefinedTypeNames( String blOrSeg ) {
        return blOrSeg2defEntityNames.get( blOrSeg );
    }

    public Set<String> getDefinedEnumsNames( String blOrSeg ) {
        return blOrSeg2defEnumNames.get( blOrSeg );
    }

    public Set<String> getEnumerationNames() {
        return enumNames;
    }

    @Override
    public Object visit( ASTJoveDL node, java.util.Map<String, Object> data ) {

        log.info( "start traversal" );

        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        data = new HashMap<String, Object>();

        node.childrenAccept( this, data );
        indent( -1 );

        log.info( "finished traversal" );
        log.info( "    enumerations " + enumNames );
        log.info( "    blocks " + blockNames );
        log.info( "    segments " + segmentNames );
        log.info( "    defined entities " + blOrSeg2defEntityNames );
        log.info( "    defined enumerations " + blOrSeg2defEnumNames );
        log.info( "    referenced " + blOrSeg2refEntityNames );

        check();
        return null;

    }

    @Override
    public Object visit( ASTSegmentDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        String segmentName = (String) node.jjtGetValue();
        if ( !( segmentNames.add( segmentName ) ) ) {
            throw new IllegalStateException( "found multiple segments with name '" + segmentName + "'" );
        }

        Set<String> defEntities = new HashSet<String>();
        Set<String> refTypes = new HashSet<String>();
        Set<String> defEnums = new HashSet<String>();

        blOrSeg2defEntityNames.put( segmentName, defEntities );
        blOrSeg2defEnumNames.put( segmentName, defEnums );
        blOrSeg2refEntityNames.put( segmentName, refTypes );

        // prepare context valid for sub nodes
        data.put( CURRENT_BL_OR_SEG, segmentName );
        data.put( DEF_ENTITIES, defEntities );
        data.put( REF_TYPES, refTypes );
        data.put( DEF_ENUMS, defEnums );

        // traverse into AST
        node.childrenAccept( this, data );
        indent( -1 );

        // finished traversal, remove objects from context
        data.remove( CURRENT_BL_OR_SEG );
        data.remove( DEF_ENTITIES );
        data.remove( DEF_ENUMS );
        data.remove( REF_TYPES );

        log.debug( "def " + defEntities );
        log.debug( "enu " + defEnums );
        log.debug( "ref " + refTypes );

        return null;
    }

    @Override
    public Object visit( ASTVariableDeclaration node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTSimpleTypeDeclaration node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTGenericParameterDeclaration node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTParameterNameAndValue node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTParameterValue node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTBlockDeclaration node, java.util.Map<String, Object> data ) {
        String blockName = (String) node.jjtGetValue();
        if ( !( blockNames.add( blockName ) ) ) {
            throw new IllegalStateException( "found multiple blocks with name '" + blockName + "'" );
        }

        data.put( CURRENT_BL_OR_SEG, blockName );
        Set<String> defTypes = new HashSet<String>();
        data.put( DEF_ENTITIES, defTypes );
        Set<String> refTypes = new HashSet<String>();
        data.put( REF_TYPES, refTypes );

        node.childrenAccept( this, data );
        indent( -1 );

        log.debug( "def " + defTypes );
        log.debug( "ref " + refTypes );

        blOrSeg2defEntityNames.put( blockName, defTypes );
        blOrSeg2refEntityNames.put( blockName, refTypes );

        data.remove( CURRENT_BL_OR_SEG );
        data.remove( DEF_ENTITIES );
        data.remove( REF_TYPES );

        return null;

    }

    @Override
    public Object visit( ASTInheritanceDeclaration node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        if ( node.jjtGetValue() != null ) {
            Set<String> refTypes = (Set<String>) data.get( REF_TYPES );
            refTypes.add( (String) node.jjtGetValue() );
        }

        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTArrayDeclaration node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTEntityDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        Set<String> defTypes = (Set<String>) data.get( DEF_ENTITIES );
        if ( !defTypes.add( (String) node.jjtGetValue() ) ) {
            throw new IllegalArgumentException( "found duplicate definition of entity '" + node.jjtGetValue() + "'" );
        }
        log.info( "defTypes  " + defTypes );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTFormalComment node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTGUIDId node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTReferencedTypeDeclaration node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        Set<String> refTypes = (Set<String>) data.get( REF_TYPES );
        refTypes.add( (String) node.jjtGetValue() );

        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTJavaCode node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTEnumerationDeclaration node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        // are we in a segment  ?
        if ( data.get( CURRENT_BL_OR_SEG ) != null ) {
            Set<String> segEnumNames = (Set<String>) data.get( DEF_ENUMS );
            if ( !segEnumNames.add( (String) node.jjtGetValue() ) ) {
                throw new IllegalArgumentException( "found duplicate definition of enumeration '" + node.jjtGetValue()
                        + "' in segment '" + data.get( CURRENT_BL_OR_SEG ) + "'" );
            }
        } else {
            // we are in a global context            
            if ( !enumNames.add( (String) node.jjtGetValue() ) ) {
                throw new IllegalArgumentException( "found duplicate definition of enumeration '" + node.jjtGetValue() + "'" );
            }
        }

        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTEnumConstant node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTEnumIntegerValue node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTEnumGUIDValue node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTVectorCompression node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTCompressionDeclaration node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTPredictorDeclaration node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

}
