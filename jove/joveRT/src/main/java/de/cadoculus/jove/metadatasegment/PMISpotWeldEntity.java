/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="PMISpotWeldEntity")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMISpotWeldEntity",
    propOrder= {"pmi3dData", "weldPoint", "approachDirection", "clampingDirection", "normalDirection", }
)
public class PMISpotWeldEntity {

    public static Logger log = LoggerFactory.getLogger( PMISpotWeldEntity.class );

    /**  
     * The variable number 0 : PMI3DData pmi3dData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMI3DData")
    private PMI3DData pmi3dData;

    /**  
     * The variable number 1 : CoordF32 weldPoint
     * <br>options : <pre>{valid : pmiVersionNumber >= 4}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.CoordF32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber >= 4" )
    private javax.vecmath.Point3f weldPoint;

    /**  
     * The variable number 2 : DirF32 approachDirection
     * <br>options : <pre>{valid : pmiVersionNumber >= 4}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.DirF32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber >= 4" )
    private javax.vecmath.Vector3f approachDirection;

    /**  
     * The variable number 3 : DirF32 clampingDirection
     * <br>options : <pre>{valid : pmiVersionNumber >= 4}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.DirF32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber >= 4" )
    private javax.vecmath.Vector3f clampingDirection;

    /**  
     * The variable number 4 : DirF32 normalDirection
     * <br>options : <pre>{valid : pmiVersionNumber >= 4}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.DirF32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber >= 4" )
    private javax.vecmath.Vector3f normalDirection;



    /** Getter for pmi3dData.
     *  
     * 
     * @return PMI3DData
     */
    public PMI3DData getPmi3dData() {
        return pmi3dData;
    }

     /** Setter for pmi3dData.
     *  
     * 
     * @param value PMI3DData
     */
    public void setPmi3dData( PMI3DData value ) {
        this.pmi3dData = value;
    }

    /** Getter for weldPoint.
     *  
     * 
     * @return javax.vecmath.Point3f
     */
    public javax.vecmath.Point3f getWeldPoint() {
        return weldPoint;
    }

     /** Setter for weldPoint.
     *  
     * 
     * @param value javax.vecmath.Point3f
     */
    public void setWeldPoint( javax.vecmath.Point3f value ) {
        this.weldPoint = value;
    }

    /** Getter for approachDirection.
     *  
     * 
     * @return javax.vecmath.Vector3f
     */
    public javax.vecmath.Vector3f getApproachDirection() {
        return approachDirection;
    }

     /** Setter for approachDirection.
     *  
     * 
     * @param value javax.vecmath.Vector3f
     */
    public void setApproachDirection( javax.vecmath.Vector3f value ) {
        this.approachDirection = value;
    }

    /** Getter for clampingDirection.
     *  
     * 
     * @return javax.vecmath.Vector3f
     */
    public javax.vecmath.Vector3f getClampingDirection() {
        return clampingDirection;
    }

     /** Setter for clampingDirection.
     *  
     * 
     * @param value javax.vecmath.Vector3f
     */
    public void setClampingDirection( javax.vecmath.Vector3f value ) {
        this.clampingDirection = value;
    }

    /** Getter for normalDirection.
     *  
     * 
     * @return javax.vecmath.Vector3f
     */
    public javax.vecmath.Vector3f getNormalDirection() {
        return normalDirection;
    }

     /** Setter for normalDirection.
     *  
     * 
     * @param value javax.vecmath.Vector3f
     */
    public void setNormalDirection( javax.vecmath.Vector3f value ) {
        this.normalDirection = value;
    }


    
}

