/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.1.2.7 Late Loaded Property Atom Element</h4>
<p>Late Loaded Property Atom Element is a property atom type used to reference an associated piece of atomic data
in a separate addressable segment of the JT file. The "Late Loaded" connotation derives from the associated data
being stored in a separate addressable segment of the JT file, and thus a JT file reader can be structured to support
the "best practice" of delaying the loading/reading of the associated data until it is actually needed.</p>
<p>Late Loaded Property Atom Elements are used to store a variety of data, including, but not limited to,
Shape LOD Segments and B-Rep Segments (see 7.2.2 Shape LOD Element and 7.2.3 JT B-Rep Segment).

*/
@XmlRootElement(name="LateLoadedPropertyAtomElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "LateLoadedPropertyAtomElement",
    propOrder= {"basePropertyAtomData", "versionNumber", "segmentID", "segmentType", "payloadObjectID", "reserved", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0xe0b05be5, 0xfbbd, 0x11d1, 0xa3, 0xa7, 0x00, 0xaa, 0x00, 0xd1, 0x9, 0x54" )
public class LateLoadedPropertyAtomElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( LateLoadedPropertyAtomElement.class );

    /**  
     * The variable number 0 : BasePropertyAtomData basePropertyAtomData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="BasePropertyAtomData")
    private BasePropertyAtomData basePropertyAtomData;

    /**  
     * The variable number 1 : I16 versionNumber
     * <br>options : <pre>{valid : jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private Short versionNumber;

    /**  
     * The variable number 2 : GUID segmentID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.GUID()    
    private GUID segmentID;

    /**  
     * The variable number 3 : I32 segmentType
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer segmentType;

    /**  
     * The variable number 4 : ObjectID payloadObjectID
     * <br>options : <pre>{valid : jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.ObjectID()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private ObjectID payloadObjectID;

    /**  
     * The variable number 5 : I32 reserved
     * <br>options : <pre>{valid : jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private Integer reserved;



    /** Getter for basePropertyAtomData.
     *  
     * 
     * @return BasePropertyAtomData
     */
    public BasePropertyAtomData getBasePropertyAtomData() {
        return basePropertyAtomData;
    }

     /** Setter for basePropertyAtomData.
     *  
     * 
     * @param value BasePropertyAtomData
     */
    public void setBasePropertyAtomData( BasePropertyAtomData value ) {
        this.basePropertyAtomData = value;
    }

    /** Getter for versionNumber.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumber() {
        return versionNumber;
    }

     /** Setter for versionNumber.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumber( Short value ) {
        this.versionNumber = value;
    }

    /** Getter for segmentID.
     *  
     * 
     * @return GUID
     */
    public GUID getSegmentID() {
        return segmentID;
    }

     /** Setter for segmentID.
     *  
     * 
     * @param value GUID
     */
    public void setSegmentID( GUID value ) {
        this.segmentID = value;
    }

    /** Getter for segmentType.
     *  
     * 
     * @return Integer
     */
    public Integer getSegmentType() {
        return segmentType;
    }

     /** Setter for segmentType.
     *  
     * 
     * @param value Integer
     */
    public void setSegmentType( Integer value ) {
        this.segmentType = value;
    }

    /** Getter for payloadObjectID.
     *  
     * 
     * @return ObjectID
     */
    public ObjectID getPayloadObjectID() {
        return payloadObjectID;
    }

     /** Setter for payloadObjectID.
     *  
     * 
     * @param value ObjectID
     */
    public void setPayloadObjectID( ObjectID value ) {
        this.payloadObjectID = value;
    }

    /** Getter for reserved.
     *  
     * 
     * @return Integer
     */
    public Integer getReserved() {
        return reserved;
    }

     /** Setter for reserved.
     *  
     * 
     * @param value Integer
     */
    public void setReserved( Integer value ) {
        this.reserved = value;
    }


    
}

