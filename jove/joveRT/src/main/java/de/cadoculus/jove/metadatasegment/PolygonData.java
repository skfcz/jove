/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="PolygonData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PolygonData",
    propOrder= {"normalBinding", "colorBinding", "textureBinding", "polygonDimension", "primTypes", "primIndices", "vertIndices", "vertices", "normals", "colors", "reservedField", }
)
public class PolygonData {

    public static Logger log = LoggerFactory.getLogger( PolygonData.class );

    /**  
     * The variable number 0 : I32 normalBinding
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer normalBinding;

    /**  
     * The variable number 1 : I32 colorBinding
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer colorBinding;

    /**  
     * The variable number 2 : I32 textureBinding
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer textureBinding;

    /**  
     * The variable number 3 : I32 polygonDimension
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer polygonDimension;

    /**  
     * The variable number 4 : VecI32 primTypes
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.VecI32()    
    private java.util.List<Integer> primTypes;

    /**  
     * The variable number 5 : VecI32 primIndices
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.VecI32()    
    private java.util.List<Integer> primIndices;

    /**  
     * The variable number 6 : VecI32 vertIndices
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.VecI32()    
    private java.util.List<Integer> vertIndices;

    /**  
     * The variable number 7 : VecF32 vertices
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.VecF32()    
    private java.util.List<Float> vertices;

    /**  
     * The variable number 8 : VecF32 normals
     * <br>options : <pre>{valid : obj.normalBinding ==1}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.VecF32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.normalBinding ==1" )
    private java.util.List<Float> normals;

    /**  
     * The variable number 9 : VecF32 colors
     * <br>options : <pre>{valid : obj.colorBinding ==1}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.VecF32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.colorBinding ==1" )
    private java.util.List<Float> colors;

    /**  
     * The variable number 10 : I16 reservedField
     * <br>options : <pre>{valid : obj.textureBinding ==1}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.textureBinding ==1" )
    private Short reservedField;



    /** Getter for normalBinding.
     *  
     * 
     * @return Integer
     */
    public Integer getNormalBinding() {
        return normalBinding;
    }

     /** Setter for normalBinding.
     *  
     * 
     * @param value Integer
     */
    public void setNormalBinding( Integer value ) {
        this.normalBinding = value;
    }

    /** Getter for colorBinding.
     *  
     * 
     * @return Integer
     */
    public Integer getColorBinding() {
        return colorBinding;
    }

     /** Setter for colorBinding.
     *  
     * 
     * @param value Integer
     */
    public void setColorBinding( Integer value ) {
        this.colorBinding = value;
    }

    /** Getter for textureBinding.
     *  
     * 
     * @return Integer
     */
    public Integer getTextureBinding() {
        return textureBinding;
    }

     /** Setter for textureBinding.
     *  
     * 
     * @param value Integer
     */
    public void setTextureBinding( Integer value ) {
        this.textureBinding = value;
    }

    /** Getter for polygonDimension.
     *  
     * 
     * @return Integer
     */
    public Integer getPolygonDimension() {
        return polygonDimension;
    }

     /** Setter for polygonDimension.
     *  
     * 
     * @param value Integer
     */
    public void setPolygonDimension( Integer value ) {
        this.polygonDimension = value;
    }

    /** Getter for primTypes.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getPrimTypes() {
        return primTypes;
    }

     /** Setter for primTypes.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setPrimTypes( java.util.List<Integer> value ) {
        this.primTypes = value;
    }

    /** Getter for primIndices.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getPrimIndices() {
        return primIndices;
    }

     /** Setter for primIndices.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setPrimIndices( java.util.List<Integer> value ) {
        this.primIndices = value;
    }

    /** Getter for vertIndices.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getVertIndices() {
        return vertIndices;
    }

     /** Setter for vertIndices.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setVertIndices( java.util.List<Integer> value ) {
        this.vertIndices = value;
    }

    /** Getter for vertices.
     *  
     * 
     * @return java.util.List<Float>
     */
    public java.util.List<Float> getVertices() {
        return vertices;
    }

     /** Setter for vertices.
     *  
     * 
     * @param value java.util.List<Float>
     */
    public void setVertices( java.util.List<Float> value ) {
        this.vertices = value;
    }

    /** Getter for normals.
     *  
     * 
     * @return java.util.List<Float>
     */
    public java.util.List<Float> getNormals() {
        return normals;
    }

     /** Setter for normals.
     *  
     * 
     * @param value java.util.List<Float>
     */
    public void setNormals( java.util.List<Float> value ) {
        this.normals = value;
    }

    /** Getter for colors.
     *  
     * 
     * @return java.util.List<Float>
     */
    public java.util.List<Float> getColors() {
        return colors;
    }

     /** Setter for colors.
     *  
     * 
     * @param value java.util.List<Float>
     */
    public void setColors( java.util.List<Float> value ) {
        this.colors = value;
    }

    /** Getter for reservedField.
     *  
     * 
     * @return Short
     */
    public Short getReservedField() {
        return reservedField;
    }

     /** Setter for reservedField.
     *  
     * 
     * @param value Short
     */
    public void setReservedField( Short value ) {
        this.reservedField = value;
    }


    
}

