/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl.parser;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Carsten Zerbst
 */
public class JoveDLVisitorAdapter implements JoveDLParserVisitor {

    private int indent = 0;
    public static Logger log = LoggerFactory.getLogger( JoveDLVisitorAdapter.class );

    private String indent( int delta ) {
        indent += delta;
        StringBuffer buff = new StringBuffer();

        for ( int i = 0; i < indent; i++ ) {
            buff.append( "    " );
        }
        return buff.toString();
    }

    @Override
    public Object visit( SimpleNode node, java.util.Map<String, Object> data ) {
        log.info( "SimpleNode " + node.toString() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTJoveDL node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTSegmentDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTVariableDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTSimpleTypeDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTGenericParameterDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTParameterNameAndValue node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTParameterValue node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTBlockDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTInheritanceDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTArrayDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTEntityDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTFormalComment node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTGUIDId node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTReferencedTypeDeclaration node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTJavaCode node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", \"" + node.jjtGetValue() + "\"" );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTEnumerationDeclaration node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", \"" + node.jjtGetValue() + "\"" );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTEnumConstant node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", \"" + node.jjtGetValue() + "\"" );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTEnumIntegerValue node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", \"" + node.jjtGetValue() + "\"" );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTEnumGUIDValue node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", \"" + node.jjtGetValue() + "\"" );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTVectorCompression node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", \"" + node.jjtGetValue() + "\"" );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTCompressionDeclaration node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", \"" + node.jjtGetValue() + "\"" );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTPredictorDeclaration node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", \"" + node.jjtGetValue() + "\"" );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

}
