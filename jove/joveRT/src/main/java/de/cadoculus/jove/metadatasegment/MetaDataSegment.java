/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.metadatasegment;

import de.cadoculus.jove.base.ObjectData;
import de.cadoculus.jove.base.LogicalElementHeaderZLIB;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Meta Data Segments are used to store large collections of meta-data in
 * separate addressable segments of the JT File. Storing meta-data in a separate
 * addressable segment allows references (from within the JT file) to these
 * segments to be constructed such that the meta-data can be late-loaded (i.e.
 * JT file reader can be structured to support the “best practice” of delaying
 * the loading/reading of the referenced meta-data segment until it is actually
 * needed).<br>
 * Meta Data Segments are typically referenced by Part Node Elements (see
 * 7.2.1.1.1.5Part Node Element) using Late Loaded Property Atom Elements (see 0
 * Late Loaded Property Atom ElementSecond specifies the date Second value.
 * Valid values are [0, 59] inclusive.<br>
 * The Meta Data Segment type supports ZLIB compression on all element data, so
 * all elements in Meta Data Segment use the Logical Element Header ZLIB form of
 * element header data.
 *
 * <p>
 * The PMI Manager Meta Data Element (as documented in 7.2.6.2 PMI Manager Meta
 * Data Element) can sometimes also be represented in a PMI Data Segment. This
 * can occur when a pre JT 8 version file is migrated to JT 9.5 version file. So
 * from a parsing point of view a PMI Data Segment should be treated exactly the
 * same as a 7.2.6 Meta Data Segment.
 *
 */
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( name = "MetaDataSegment" )
@XmlRootElement( name = "MetaDataSegment" )
@   de.cadoculus.jove.bibi.annotation.BIBICodec( de.cadoculus.jove.bibi.codec.MetaDataSegmentCodec.class )
public class MetaDataSegment extends ObjectData {

    @XmlElementWrapper( name = "metaDataElements" )
    @XmlElements( {
        @XmlElement( name = "PropertyProxyMetaDataElement", type = PropertyProxyMetaDataElement.class ),
        @XmlElement( name = "PMIManagerMetaDataElement", type = PMIManagerMetaDataElement.class ), } )
    private List<LogicalElementHeaderZLIB> metaDataElements = new ArrayList<>();

    /**
     * @return the metaDataElements
     */
    public List<LogicalElementHeaderZLIB> getMetaDataElements() {
        return metaDataElements;
    }

}
