/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2 PMI Manager Meta Data Element</h4>
Object Type ID: 0xce357249, 0x38fb, 0x11d1, 0xa5, 0x6, 0x0, 0x60, 0x97, 0xbd, 0xc6, 0xe1
The PMI Manager Meta Data Element data collection is a type of I32 : Texture Coord Channel which contains the Product and Manufacturing Information for a part/assembly.

*/
@XmlRootElement(name="PMIManagerMetaDataElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIManagerMetaDataElement",
    propOrder= {"versionNumber", "pmiVersionNumber", "reservedField", "pmiEntities", "pmiAssociations", "pmiUserAttributes", "pmiStringTable", "pmiModelViews", "genericPmiEntities", "cadTagsFlag", "pmiCadTagData", "mvPropertyCount", "pmiPropertiesI32", "polygonData", "fontCount", "fontData", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0xce357249, 0x38fb, 0x11d1, 0xa5, 0x6, 0x00, 0x60, 0x97, 0xbd, 0xc6, 0xe1" )
@de.cadoculus.jove.bibi.annotation.BIBICodec( de.cadoculus.jove.bibi.codec.PMIManagerMetaDataElementCodec.class )
public class PMIManagerMetaDataElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( PMIManagerMetaDataElement.class );

    /** 
     * this field was introduced in JT 95 . The original versionNumber field was renamed to pmiVersionNumber<p> 
     * The variable number 0 : I16 versionNumber
     * <br>options : <pre>{valid : jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private Short versionNumber;

    /** 
     * The original versionNumber field was renamed to pmiVersionNumber with JT 95<p> 
     * The variable number 1 : I16 pmiVersionNumber
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short pmiVersionNumber;

    /**  
     * The variable number 2 : I16 reservedField
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short reservedField;

    /**  
     * The variable number 3 : PMIEntities pmiEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIEntities")
    private PMIEntities pmiEntities;

    /**  
     * The variable number 4 : PMIAssociations pmiAssociations
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIAssociations")
    private PMIAssociations pmiAssociations;

    /**  
     * The variable number 5 : PMIUserAttributes pmiUserAttributes
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIUserAttributes")
    private PMIUserAttributes pmiUserAttributes;

    /**  
     * The variable number 6 : PMIStringTable pmiStringTable
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIStringTable")
    private PMIStringTable pmiStringTable;

    /**  
     * The variable number 7 : PMIModelViews pmiModelViews
     * <br>options : <pre>{valid : obj.pmiVersionNumber > 5}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIModelViews")
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.pmiVersionNumber > 5" )
    private PMIModelViews pmiModelViews;

    /**  
     * The variable number 8 : GenericPMIEntities genericPmiEntities
     * <br>options : <pre>{valid : obj.pmiVersionNumber > 5}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="GenericPMIEntities")
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.pmiVersionNumber > 5" )
    private GenericPMIEntities genericPmiEntities;

    /**  
     * The variable number 9 : U32 cadTagsFlag
     * <br>options : <pre>{valid : obj.pmiVersionNumber > 7}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.pmiVersionNumber > 7" )
    private Long cadTagsFlag;

    /** 
     * Reading of PMICADTagData fails currently, disable the element<p> 
     * The variable number 10 : PMICADTagData pmiCadTagData
     * <br>options : <pre>{valid : obj.pmiVersionNumber > 7 && obj.cadTagsFlag ==1 && jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMICADTagData")
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.pmiVersionNumber > 7 && obj.cadTagsFlag ==1 && jtVersion > 81" )
    private PMICADTagData pmiCadTagData;

    /**  
     * The variable number 11 : I32 mvPropertyCount
     * <br>options : <pre>{valid : jtVersion > 81 && obj.versionNumber >1}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81 && obj.versionNumber >1" )
    private Integer mvPropertyCount;

    /**  
     * The variable number 12 : PMIProperty[] pmiPropertiesI32
     * <br>options : <pre>{valid : jtVersion > 81 && obj.versionNumber >1,length : obj.mvPropertyCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.mvPropertyCount" )
    @XmlElementWrapper(name = "pmiPropertiesI32")
    @XmlElementRef()    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81 && obj.versionNumber >1" )
    private java.util.List<PMIProperty> pmiPropertiesI32;

    /**  
     * The variable number 13 : PolygonData polygonData
     * <br>options : <pre>{valid : jtVersion > 81 && obj.versionNumber >1}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PolygonData")
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81 && obj.versionNumber >1" )
    private PolygonData polygonData;

    /**  
     * The variable number 14 : I32 fontCount
     * <br>options : <pre>{valid : jtVersion > 81 && obj.versionNumber >1}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81 && obj.versionNumber >1" )
    private Integer fontCount;

    /**  
     * The variable number 15 : FontData[] fontData
     * <br>options : <pre>{valid : jtVersion > 81 && obj.versionNumber >1,length : obj.fontCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.fontCount" )
    @XmlElementWrapper(name = "fontData")
    @XmlElementRef()    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81 && obj.versionNumber >1" )
    private java.util.List<FontData> fontData;



    /** Getter for versionNumber.
     * this field was introduced in JT 95 . The original versionNumber field was renamed to pmiVersionNumber<p> 
     * 
     * @return Short
     */
    public Short getVersionNumber() {
        return versionNumber;
    }

     /** Setter for versionNumber.
     * this field was introduced in JT 95 . The original versionNumber field was renamed to pmiVersionNumber<p> 
     * 
     * @param value Short
     */
    public void setVersionNumber( Short value ) {
        this.versionNumber = value;
    }

    /** Getter for pmiVersionNumber.
     * The original versionNumber field was renamed to pmiVersionNumber with JT 95<p> 
     * 
     * @return Short
     */
    public Short getPmiVersionNumber() {
        return pmiVersionNumber;
    }

     /** Setter for pmiVersionNumber.
     * The original versionNumber field was renamed to pmiVersionNumber with JT 95<p> 
     * 
     * @param value Short
     */
    public void setPmiVersionNumber( Short value ) {
        this.pmiVersionNumber = value;
    }

    /** Getter for reservedField.
     *  
     * 
     * @return Short
     */
    public Short getReservedField() {
        return reservedField;
    }

     /** Setter for reservedField.
     *  
     * 
     * @param value Short
     */
    public void setReservedField( Short value ) {
        this.reservedField = value;
    }

    /** Getter for pmiEntities.
     *  
     * 
     * @return PMIEntities
     */
    public PMIEntities getPmiEntities() {
        return pmiEntities;
    }

     /** Setter for pmiEntities.
     *  
     * 
     * @param value PMIEntities
     */
    public void setPmiEntities( PMIEntities value ) {
        this.pmiEntities = value;
    }

    /** Getter for pmiAssociations.
     *  
     * 
     * @return PMIAssociations
     */
    public PMIAssociations getPmiAssociations() {
        return pmiAssociations;
    }

     /** Setter for pmiAssociations.
     *  
     * 
     * @param value PMIAssociations
     */
    public void setPmiAssociations( PMIAssociations value ) {
        this.pmiAssociations = value;
    }

    /** Getter for pmiUserAttributes.
     *  
     * 
     * @return PMIUserAttributes
     */
    public PMIUserAttributes getPmiUserAttributes() {
        return pmiUserAttributes;
    }

     /** Setter for pmiUserAttributes.
     *  
     * 
     * @param value PMIUserAttributes
     */
    public void setPmiUserAttributes( PMIUserAttributes value ) {
        this.pmiUserAttributes = value;
    }

    /** Getter for pmiStringTable.
     *  
     * 
     * @return PMIStringTable
     */
    public PMIStringTable getPmiStringTable() {
        return pmiStringTable;
    }

     /** Setter for pmiStringTable.
     *  
     * 
     * @param value PMIStringTable
     */
    public void setPmiStringTable( PMIStringTable value ) {
        this.pmiStringTable = value;
    }

    /** Getter for pmiModelViews.
     *  
     * 
     * @return PMIModelViews
     */
    public PMIModelViews getPmiModelViews() {
        return pmiModelViews;
    }

     /** Setter for pmiModelViews.
     *  
     * 
     * @param value PMIModelViews
     */
    public void setPmiModelViews( PMIModelViews value ) {
        this.pmiModelViews = value;
    }

    /** Getter for genericPmiEntities.
     *  
     * 
     * @return GenericPMIEntities
     */
    public GenericPMIEntities getGenericPmiEntities() {
        return genericPmiEntities;
    }

     /** Setter for genericPmiEntities.
     *  
     * 
     * @param value GenericPMIEntities
     */
    public void setGenericPmiEntities( GenericPMIEntities value ) {
        this.genericPmiEntities = value;
    }

    /** Getter for cadTagsFlag.
     *  
     * 
     * @return Long
     */
    public Long getCadTagsFlag() {
        return cadTagsFlag;
    }

     /** Setter for cadTagsFlag.
     *  
     * 
     * @param value Long
     */
    public void setCadTagsFlag( Long value ) {
        this.cadTagsFlag = value;
    }

    /** Getter for pmiCadTagData.
     * Reading of PMICADTagData fails currently, disable the element<p> 
     * 
     * @return PMICADTagData
     */
    public PMICADTagData getPmiCadTagData() {
        return pmiCadTagData;
    }

     /** Setter for pmiCadTagData.
     * Reading of PMICADTagData fails currently, disable the element<p> 
     * 
     * @param value PMICADTagData
     */
    public void setPmiCadTagData( PMICADTagData value ) {
        this.pmiCadTagData = value;
    }

    /** Getter for mvPropertyCount.
     *  
     * 
     * @return Integer
     */
    public Integer getMvPropertyCount() {
        return mvPropertyCount;
    }

     /** Setter for mvPropertyCount.
     *  
     * 
     * @param value Integer
     */
    public void setMvPropertyCount( Integer value ) {
        this.mvPropertyCount = value;
    }

    /** Getter for pmiPropertiesI32.
     *  
     * 
     * @return java.util.List<PMIProperty>
     */
    public java.util.List<PMIProperty> getPmiPropertiesI32() {
        return pmiPropertiesI32;
    }

     /** Setter for pmiPropertiesI32.
     *  
     * 
     * @param value java.util.List<PMIProperty>
     */
    public void setPmiPropertiesI32( java.util.List<PMIProperty> value ) {
        this.pmiPropertiesI32 = value;
    }

    /** Getter for polygonData.
     *  
     * 
     * @return PolygonData
     */
    public PolygonData getPolygonData() {
        return polygonData;
    }

     /** Setter for polygonData.
     *  
     * 
     * @param value PolygonData
     */
    public void setPolygonData( PolygonData value ) {
        this.polygonData = value;
    }

    /** Getter for fontCount.
     *  
     * 
     * @return Integer
     */
    public Integer getFontCount() {
        return fontCount;
    }

     /** Setter for fontCount.
     *  
     * 
     * @param value Integer
     */
    public void setFontCount( Integer value ) {
        this.fontCount = value;
    }

    /** Getter for fontData.
     *  
     * 
     * @return java.util.List<FontData>
     */
    public java.util.List<FontData> getFontData() {
        return fontData;
    }

     /** Setter for fontData.
     *  
     * 
     * @param value java.util.List<FontData>
     */
    public void setFontData( java.util.List<FontData> value ) {
        this.fontData = value;
    }


    
}

