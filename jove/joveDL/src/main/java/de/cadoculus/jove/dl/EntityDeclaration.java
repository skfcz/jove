/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Aüache License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl;

import de.cadoculus.jove.model.GUID;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Entity Declaration represents an entity (or C structure) from a Block or Segment.
 * @author Carsten Zerbst
 */
public class EntityDeclaration extends DataType {

    private String comment;
    private EntityDeclaration parentType;
    private Map<String, Object> properties = new HashMap<>();
    private List<VariableDeclaration> variables = new ArrayList<>();
    private GUID guid;
    private String javaCode;

   
    /**
     * @return the parentType
     */
    public EntityDeclaration getParentType() {
        return parentType;
    }

    /**
     * @param parentType the parentType to set
     */
    public void setParentType( EntityDeclaration parentType ) {
        this.parentType = parentType;
    }

    /**
     * @return the properties
     */
    public Map<String, Object> getProperties() {
        return properties;
    }

    /**
     * @return the variables
     */
    public List<VariableDeclaration> getVariables() {
        return variables;
    }

    /**
     * @return the guid
     */
    public GUID getGuid() {
        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid( GUID guid ) {
        this.guid = guid;
    }

    @Override
    public String toString() {
        return "EntityDeclaration{" + "name=" + name +
                ( parentType != null ? ", parentType=" + parentType.getName() : "") + //
                 ", properties=" + properties + ", variables=" + variables + ", guid=" + guid + //
                ( javaCode != null ? ", javaCode='" + javaCode + "'" : "") + "}";
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment( String comment ) {
        this.comment = comment;
    }

    /**
     * @return the javaCode
     */
    public String getJavaCode() {
        return javaCode;
    }

    /**
     * @param javaCode the javaCode to set
     */
    public void setJavaCode( String javaCode ) {
        this.javaCode = javaCode;
    }
}
