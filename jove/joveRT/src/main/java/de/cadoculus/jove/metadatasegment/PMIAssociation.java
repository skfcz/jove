/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="PMIAssociation")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIAssociation",
    propOrder= {"sourceData", "destinationData", "reasonCode", "sourceOwningEntityStringID", "destinationOwningEntityStringID", }
)
public class PMIAssociation {

    public static Logger log = LoggerFactory.getLogger( PMIAssociation.class );

    /**  
     * The variable number 0 : I32 sourceData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer sourceData;

    /**  
     * The variable number 1 : I32 destinationData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer destinationData;

    /**  
     * The variable number 2 : PMIReasonCode reasonCode
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private PMIReasonCode reasonCode;

    /**  
     * The variable number 3 : I32 sourceOwningEntityStringID
     * <br>options : <pre>{valid : pmiVersionNumber > 5}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber > 5" )
    private Integer sourceOwningEntityStringID;

    /**  
     * The variable number 4 : I32 destinationOwningEntityStringID
     * <br>options : <pre>{valid : pmiVersionNumber > 5}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber > 5" )
    private Integer destinationOwningEntityStringID;



    /** Getter for sourceData.
     *  
     * 
     * @return Integer
     */
    public Integer getSourceData() {
        return sourceData;
    }

     /** Setter for sourceData.
     *  
     * 
     * @param value Integer
     */
    public void setSourceData( Integer value ) {
        this.sourceData = value;
    }

    /** Getter for destinationData.
     *  
     * 
     * @return Integer
     */
    public Integer getDestinationData() {
        return destinationData;
    }

     /** Setter for destinationData.
     *  
     * 
     * @param value Integer
     */
    public void setDestinationData( Integer value ) {
        this.destinationData = value;
    }

    /** Getter for reasonCode.
     *  
     * 
     * @return PMIReasonCode
     */
    public PMIReasonCode getReasonCode() {
        return reasonCode;
    }

     /** Setter for reasonCode.
     *  
     * 
     * @param value PMIReasonCode
     */
    public void setReasonCode( PMIReasonCode value ) {
        this.reasonCode = value;
    }

    /** Getter for sourceOwningEntityStringID.
     *  
     * 
     * @return Integer
     */
    public Integer getSourceOwningEntityStringID() {
        return sourceOwningEntityStringID;
    }

     /** Setter for sourceOwningEntityStringID.
     *  
     * 
     * @param value Integer
     */
    public void setSourceOwningEntityStringID( Integer value ) {
        this.sourceOwningEntityStringID = value;
    }

    /** Getter for destinationOwningEntityStringID.
     *  
     * 
     * @return Integer
     */
    public Integer getDestinationOwningEntityStringID() {
        return destinationOwningEntityStringID;
    }

     /** Setter for destinationOwningEntityStringID.
     *  
     * 
     * @param value Integer
     */
    public void setDestinationOwningEntityStringID( Integer value ) {
        this.destinationOwningEntityStringID = value;
    }


    
}

