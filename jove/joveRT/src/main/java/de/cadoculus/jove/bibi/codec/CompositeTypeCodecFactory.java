/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.CodecFactory;
import de.cadoculus.jove.bibi.WorkingContext;
import java.lang.reflect.AnnotatedElement;
import java.util.Arrays;
import org.slf4j.LoggerFactory;

/**
 * Factory for types defined in the first chapter of the JT spec beyond simple
 * types, e.g. vectors.
 *
 * @author Zerbst
 */
public class CompositeTypeCodecFactory implements CodecFactory {

    private static org.slf4j.Logger log = LoggerFactory.getLogger( CompositeTypeCodecFactory.class );

    private final Class[] COMPOSITE_ANNOTATIONS = new Class[]{
        // Composite Data Types
        de.cadoculus.jove.bibi.annotation.BBoxF32.class,
        de.cadoculus.jove.bibi.annotation.CoordF32.class, //
        de.cadoculus.jove.bibi.annotation.CoordF64.class, //
        de.cadoculus.jove.bibi.annotation.DirF32.class, //        
        de.cadoculus.jove.bibi.annotation.HCoordF32.class, //
        de.cadoculus.jove.bibi.annotation.HCoordF64.class, //       
        de.cadoculus.jove.bibi.annotation.Mx4F32.class, //
        de.cadoculus.jove.bibi.annotation.PlaneF32.class, //
        de.cadoculus.jove.bibi.annotation.Quaternion.class, //
        de.cadoculus.jove.bibi.annotation.RGB.class, //
        de.cadoculus.jove.bibi.annotation.RGBA.class, //        
        de.cadoculus.jove.bibi.annotation.VecF32.class, //
        de.cadoculus.jove.bibi.annotation.VecF64.class, //
        de.cadoculus.jove.bibi.annotation.VecI32.class, //
        de.cadoculus.jove.bibi.annotation.VecU32.class, //
    };

    @Override
    public <T> Codec<T> create( AnnotatedElement metadata, Class<T> type, WorkingContext ctx ) throws BIBIException {
        if ( metadata == null || type == null ) {
            return null;
        }
        // only one annotation may be applied on one field
        Object lastFound = null;
        for ( Class an : COMPOSITE_ANNOTATIONS ) {
            if ( metadata.getAnnotation( an ) != null ) {
                if ( lastFound != null ) {
                    log.info( "found annotations " + lastFound + " and " + metadata.getAnnotation( an ) + " on " + metadata );
                    throw new IllegalStateException( "found multiple numeric annotations on " + metadata.toString() );
                }
                lastFound = metadata.getAnnotation( an );
            }
        }
        if ( lastFound == null ) {
            return null;
        }
        // TODO : add caching for all but vectors (due to possible compression) 

        if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.BBoxF32
                && de.cadoculus.jove.base.BBoxF32.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.BBoxF32.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.CoordF32
                && javax.vecmath.Point3f.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.CoordF32.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.CoordF64
                && javax.vecmath.Point3d.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.CoordF64.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.DirF32
                && javax.vecmath.Vector3f.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.DirF32.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.HCoordF32
                && javax.vecmath.Point4f.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.HCoordF32.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.HCoordF64
                && javax.vecmath.Point4d.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.HCoordF64.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.Mx4F32
                && javax.vecmath.Matrix4f.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.Mx4F32.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.PlaneF32
                && javax.vecmath.Vector4f.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.PlaneF32.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.Quaternion
                && javax.vecmath.Vector4f.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.Quaternion.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.RGB
                && java.awt.Color.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.RGB.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.RGBA
                && java.awt.Color.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.RGBA.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.VecF32
                && java.util.List.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.VecF32.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.VecF64
                && java.util.List.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.VecF64.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.VecI32
                && java.util.List.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.VecI32.class, type );
        } else if ( lastFound instanceof de.cadoculus.jove.bibi.annotation.VecU32
                && java.util.List.class.isAssignableFrom( type ) ) {
            return new CompositeTypeCodec<T>( metadata, de.cadoculus.jove.bibi.annotation.VecU32.class, type );
        } else {
            log.error( "found annotation " + lastFound + " " + metadata );
            log.error( "   from list of composite annotations " + Arrays.toString( COMPOSITE_ANNOTATIONS ) );
            log.error( "   but do not know valid combination with field type " + type );
            throw new IllegalStateException( "found invalid combination of composite annotation " + lastFound + " and java " + type );

        }

    }
}
