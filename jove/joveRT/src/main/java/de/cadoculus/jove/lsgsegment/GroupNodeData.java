/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="GroupNodeData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "GroupNodeData",
    propOrder= {"versionNumberGND", "childCount", "childNodeObjectID", }
)
public class GroupNodeData extends BaseNodeData {

    public static Logger log = LoggerFactory.getLogger( GroupNodeData.class );

    /**  
     * The variable number 0 : I16 versionNumberGND
     * <br>options : <pre>{valid : jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private Short versionNumberGND;

    /**  
     * The variable number 1 : I32 childCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer childCount;

    /**  
     * The variable number 2 : I32[] childNodeObjectID
     * <br>options : <pre>{length : obj.childCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.childCount" )
    @XmlElementWrapper(name = "childNodeObjectID")
        private java.util.List<Integer> childNodeObjectID;



    /** Getter for versionNumberGND.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberGND() {
        return versionNumberGND;
    }

     /** Setter for versionNumberGND.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberGND( Short value ) {
        this.versionNumberGND = value;
    }

    /** Getter for childCount.
     *  
     * 
     * @return Integer
     */
    public Integer getChildCount() {
        return childCount;
    }

     /** Setter for childCount.
     *  
     * 
     * @param value Integer
     */
    public void setChildCount( Integer value ) {
        this.childCount = value;
    }

    /** Getter for childNodeObjectID.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getChildNodeObjectID() {
        return childNodeObjectID;
    }

     /** Setter for childNodeObjectID.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setChildNodeObjectID( java.util.List<Integer> value ) {
        this.childNodeObjectID = value;
    }


    
}

