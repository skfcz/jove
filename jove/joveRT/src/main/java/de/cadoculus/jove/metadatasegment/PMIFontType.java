/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;

import de.cadoculus.jove.base.*;

/** 
Font identifies the font to be used for this text. Valid values include the following:
 * 
 * This enumeration contains 14 values of type I32
*/

public enum PMIFontType {
   
    SIMPLEX( 1 ),
    DIN( 2 ),
    MILITARY( 3 ),
    ISO( 4 ),
    LIGHTLINE( 5 ),
    IGES_1001( 6 ),
    CENTURY( 7 ),
    IGES_1002( 8 ),
    IGES_1003( 9 ),
    JAPANESE_JISX_0208_CODED_CHARACTER_SET( 101 ),
    JAPANESE_EXTENDED_UNIX_CODES_JISX_0208_CODED_CHARACTER_SET( 102 ),
    CHINESE_GB_2312_1980_SIMPLIFIED_CODED_CHARACTER_SET( 103 ),
    KOREAN_KSC_5601_CODED_CHARACTER_SET( 104 ),
    CHINESE_BIG5_TRADITIONAL_CODED_CHARACTER_SET( 105 );

    
    private final Integer enumValue;
    private PMIFontType( Integer v ) {
        this.enumValue = v;
    }

    /**
     * Get the integer value contained in the enumeration
     */
    public int getValue() {
        return enumValue;
    }
    /**
     * Get a PMIFontType for the given enumeration value
     * @param v the enumeration value
     * @return the found value
     * @throws IllegalArgumentException  if no PMIFontType was found for the given value
     */
    public static PMIFontType valueOf( Integer v ) throws IllegalArgumentException {
        for ( PMIFontType test : PMIFontType.values() ) {
            if ( test.getValue() == v ) {
                return test;
            }
        }
        throw new IllegalArgumentException( "could not find PMIFontType for value '" + v + "'" );

    }       
    
}

