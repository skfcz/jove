/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.base.GUID;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import org.slf4j.LoggerFactory;

/**
 * A codec for enumerations wrapping integer or GUIDs.
 *
 * @author cz
 */
public class EnumerationCodec<T> implements Codec<T> {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( EnumerationCodec.class );
    private final Codec delegate;
    private final Class type;

    public EnumerationCodec( Codec delegate, AnnotatedElement metadata, Class type ) throws BIBIException {
        this.delegate = delegate;
        this.type = type;
        if ( ! type.isEnum() ) {
            throw new BIBIException( "given " + type + " is not an enumeration" );
        }

    }

    @Override
    public T decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        log.debug("decode using " + delegate);
        Object serializedValue = delegate.decode( input, ctx );
        log.debug("    read " + serializedValue.getClass());
        Method method = null;
        try {
            log.debug("    convert into " + type);
            method = type.getMethod( "valueOf", delegate instanceof NumberCodec ? Integer.class : GUID.class );
        } catch ( Exception exp ) {
            throw new BIBIException( "could not find method 'valueOf' in " + type, exp );
        }

        try {
            return (T) method.invoke( null, serializedValue );
        } catch ( IllegalArgumentException exp ) {
            throw new BIBIException( "could not find enumeration for value " + serializedValue + " in " + type, exp );
        } catch ( Exception exp ) {
            throw new BIBIException( "an error occured creating enumeration " + type + " for value " + serializedValue, exp );
        }

    }

}
