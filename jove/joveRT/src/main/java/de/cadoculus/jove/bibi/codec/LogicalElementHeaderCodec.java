/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.base.GUID;
import de.cadoculus.jove.base.LogicalElementHeader;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.WorkingContext;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cz
 */
public class LogicalElementHeaderCodec extends ObjectCodec<LogicalElementHeader> {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( LogicalElementHeaderCodec.class );

    /**
     * Construct an ObjectCodec for the given type.
     *
     * @param type
     */
    public LogicalElementHeaderCodec( Class type ) throws BIBIException {
        super( LogicalElementHeader.class );
    }

    @Override
    protected void postAction( Field field, Object value, WorkingContext ctx ) {
        if ( field.getName().equals( "objectId" ) ) {

            int bytesToSkip = myObject.getElementLength() - GUID.BYTES_PER_OBJECT + 1 + 4;
            log.error( "skip " + bytesToSkip + " bytes" );

            for ( int i = 0; i < bytesToSkip; i++ ) {
                ctx.getByteBuffer().get();
            }
        }
    }
}
