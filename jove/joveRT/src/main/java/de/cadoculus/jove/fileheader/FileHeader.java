/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.fileheader;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
The FileHeader block contains only one entity

*/
@XmlRootElement(name="FileHeader")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "FileHeader",
    propOrder= {"version", "byteOrder", "reservedFieldI", "tocOffset", "lsgSegmentId", "reservedFieldG", }
)
@de.cadoculus.jove.bibi.annotation.BIBICodec( de.cadoculus.jove.bibi.codec.FileHeaderCodec.class )
public class FileHeader {

    public static Logger log = LoggerFactory.getLogger( FileHeader.class );

    /** 
     * An 80-character version string defining the version of the file format used to write this file.
The Version string has the following format: Version M.n Comment<br>
Where M is replaced by the major version number, n is replaced by the minor verson number,
and Comment provides other unspecified reserved information.
<p> 
     * The variable number 0 : UChar[] version
     * <br>options : <pre>{length : 80}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.UChar()    @de.cadoculus.jove.bibi.annotation.BIBILength( "80" )
    
        private String version;

    /**  
     * The variable number 1 : UChar byteOrder
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.UChar()    
    private Byte byteOrder;

    /**  
     * The variable number 2 : I32 reservedFieldI
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer reservedFieldI;

    /**  
     * The variable number 3 : I32 tocOffset
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer tocOffset;

    /**  
     * The variable number 4 : GUID lsgSegmentId
     * <br>options : <pre>{valid : obj.reservedFieldI == 0}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.GUID()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.reservedFieldI == 0" )
    private GUID lsgSegmentId;

    /**  
     * The variable number 5 : GUID reservedFieldG
     * <br>options : <pre>{valid : obj.reservedFieldI != 0}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.GUID()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.reservedFieldI != 0" )
    private GUID reservedFieldG;



    /** Getter for version.
     * An 80-character version string defining the version of the file format used to write this file.
The Version string has the following format: Version M.n Comment<br>
Where M is replaced by the major version number, n is replaced by the minor verson number,
and Comment provides other unspecified reserved information.
<p> 
     * 
     * @return String
     */
    public String getVersion() {
        return version;
    }

     /** Setter for version.
     * An 80-character version string defining the version of the file format used to write this file.
The Version string has the following format: Version M.n Comment<br>
Where M is replaced by the major version number, n is replaced by the minor verson number,
and Comment provides other unspecified reserved information.
<p> 
     * 
     * @param value String
     */
    public void setVersion( String value ) {
        this.version = value;
    }

    /** Getter for byteOrder.
     *  
     * 
     * @return Byte
     */
    public Byte getByteOrder() {
        return byteOrder;
    }

     /** Setter for byteOrder.
     *  
     * 
     * @param value Byte
     */
    public void setByteOrder( Byte value ) {
        this.byteOrder = value;
    }

    /** Getter for reservedFieldI.
     *  
     * 
     * @return Integer
     */
    public Integer getReservedFieldI() {
        return reservedFieldI;
    }

     /** Setter for reservedFieldI.
     *  
     * 
     * @param value Integer
     */
    public void setReservedFieldI( Integer value ) {
        this.reservedFieldI = value;
    }

    /** Getter for tocOffset.
     *  
     * 
     * @return Integer
     */
    public Integer getTocOffset() {
        return tocOffset;
    }

     /** Setter for tocOffset.
     *  
     * 
     * @param value Integer
     */
    public void setTocOffset( Integer value ) {
        this.tocOffset = value;
    }

    /** Getter for lsgSegmentId.
     *  
     * 
     * @return GUID
     */
    public GUID getLsgSegmentId() {
        return lsgSegmentId;
    }

     /** Setter for lsgSegmentId.
     *  
     * 
     * @param value GUID
     */
    public void setLsgSegmentId( GUID value ) {
        this.lsgSegmentId = value;
    }

    /** Getter for reservedFieldG.
     *  
     * 
     * @return GUID
     */
    public GUID getReservedFieldG() {
        return reservedFieldG;
    }

     /** Setter for reservedFieldG.
     *  
     * 
     * @param value GUID
     */
    public void setReservedFieldG( GUID value ) {
        this.reservedFieldG = value;
    }


    // Verbatim Java Code from DL
      
    public static final int BYTES_PER_OBJECT= 80+1+4+4+GUID.BYTES_PER_OBJECT;

                        
    /**
     * This is a valued derived by parsing the version string and extracting the
     * version. If this could be extracted, get int version returns the version
     * multiplied by ten as integer value. E.g. the string "Version 8.1" found
     * in the header will result in an integer version of 81.
     *
     * @return the tenfold of the version found in the string. if parsing failed
     * returns 0
     */
    public int getIntVersion() {
        if ( version == null ) {
            return 0;
        }
        java.util.regex.Pattern pat = java.util.regex.Pattern.compile( "Version ([0-9])\\.([0-9])+(.*)" );
        java.util.regex.Matcher match = pat.matcher( version.trim() );

        if ( !match.matches() ) {
            throw new IllegalArgumentException(
                    "Illegal file start, expect 80 character string like 'Version 8.1   ...', got '"
                    + version.trim() + "'" );
        }

        int retval = Integer.parseInt( match.group( 1 ) ) * 10;
        retval += Integer.parseInt( match.group( 2 ) );

        return retval;

    }
            
    
}

