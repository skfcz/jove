/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.base.ObjectData;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import java.nio.ByteBuffer;

/**
 *
 * @author Zerbst
 */
public class ObjectDataCodec implements Codec<ObjectData> {
    
    public ObjectDataCodec( Class clazz) {
        super();
    }

    @Override
    public ObjectData decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        CodecUtility.checkMaxPosition( input, ctx );
        return new ObjectData();
    }
}
