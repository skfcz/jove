/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
7.2.1.1.2.1.1 Base Attribute Data
<p>Attribute Elements (e.g. color, texture, material, lights, etc.) are
placed in LSG as objects associated with nodes. Attribute Elements are not
nodes themselves, but can be associated with any node.</p>
<p>For applications producing or consuming JT format data, it is important
that the JT format semantics of how attributes are meant to be applied and
accumulated down the LSG are followed. If not followed, then consistency
between the applications in terms of 3D positioning and rendering of LSG
model data will not be achieved.</p>
<p>To that end each attribute type defines its own application and
accumulation semantics, but in general attributes at lower levels in the LSG
take precedence and replace or accumulate with attributes set at higher
levels. Nodes without associated attributes inherit those of their parents.
Attributes inherit only from their parents, thus a node's attributes do not
affect that node's siblings. The root of a partition inherits the attributes
in effect at the referring partition node.</p>
<p>Attributes can be declared "final" (see 7.2.1.1.2.1.1 Base Attribute Data),
which terminates accumulation of that attribute type at that attribute and
propagates the accumulated values there to all descendants of the associated
node. Descendants can explicitly do a one-shot override of "final" using the
attribute "force" flag (see 7.2.1.1.2.1.1Base Attribute Data), but do not by
default. Note that "force" does not turn OFF "final" - it is simply a
one-shot override of "final" for the specific attribute marked as "forcing."
An analogy for this "force" and "final" interaction is that "final" is a
back-door in the attribute accumulation semantics, and that "force" is a
doggy-door in the back-door!


*/
@XmlRootElement(name="BaseAttributeData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "BaseAttributeData",
    propOrder= {"versionNumber", "stateFlags", "fieldInhibitFlags", }
)
public class BaseAttributeData {

    public static Logger log = LoggerFactory.getLogger( BaseAttributeData.class );

    /** 
     * Version Number is the version identifier for this node. Version number
"0x0001" is currently the only valid value for Base Shape Data.<p>
The variable number 0 : I16 versionNumber
<br>no options defined
<p> 
     * The variable number 0 : I16 versionNumber
     * <br>options : <pre>{valid : jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private Short versionNumber;

    /** 
     * State Flags is a collection of flags. The flags are combined using the binary
OR operator and store various state information for Attribute Elements;
such as indicating that the attributes accumulation is final. All undocumented bits are reserved.
<p> 
     * The variable number 1 : U8 stateFlags
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U8()    
    private Short stateFlags;

    /** 
     * Field Inhibit Flags is a collection of flags. The flags are combined
using the binary OR operator and store the per attribute value
accumulation flag. Each value present in an Attribute Element is given a
field number ranging from 0 to 31. If the fieldâ€Ÿs corresponding bit in
Inhibit Flags is set, then the field should not participate in attribute
accumulation. All bits are reserved. See each particular Attribute
Element (e.g. Material Attribute Element) for a description of bit field
assignments for each attribute value.
<p>
The variable number 2 : U32 filedInhibitFlags
<br>no options defined
<p> 
     * The variable number 2 : U32 fieldInhibitFlags
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    private Long fieldInhibitFlags;



    /** Getter for versionNumber.
     * Version Number is the version identifier for this node. Version number
"0x0001" is currently the only valid value for Base Shape Data.<p>
The variable number 0 : I16 versionNumber
<br>no options defined
<p> 
     * 
     * @return Short
     */
    public Short getVersionNumber() {
        return versionNumber;
    }

     /** Setter for versionNumber.
     * Version Number is the version identifier for this node. Version number
"0x0001" is currently the only valid value for Base Shape Data.<p>
The variable number 0 : I16 versionNumber
<br>no options defined
<p> 
     * 
     * @param value Short
     */
    public void setVersionNumber( Short value ) {
        this.versionNumber = value;
    }

    /** Getter for stateFlags.
     * State Flags is a collection of flags. The flags are combined using the binary
OR operator and store various state information for Attribute Elements;
such as indicating that the attributes accumulation is final. All undocumented bits are reserved.
<p> 
     * 
     * @return Short
     */
    public Short getStateFlags() {
        return stateFlags;
    }

     /** Setter for stateFlags.
     * State Flags is a collection of flags. The flags are combined using the binary
OR operator and store various state information for Attribute Elements;
such as indicating that the attributes accumulation is final. All undocumented bits are reserved.
<p> 
     * 
     * @param value Short
     */
    public void setStateFlags( Short value ) {
        this.stateFlags = value;
    }

    /** Getter for fieldInhibitFlags.
     * Field Inhibit Flags is a collection of flags. The flags are combined
using the binary OR operator and store the per attribute value
accumulation flag. Each value present in an Attribute Element is given a
field number ranging from 0 to 31. If the fieldâ€Ÿs corresponding bit in
Inhibit Flags is set, then the field should not participate in attribute
accumulation. All bits are reserved. See each particular Attribute
Element (e.g. Material Attribute Element) for a description of bit field
assignments for each attribute value.
<p>
The variable number 2 : U32 filedInhibitFlags
<br>no options defined
<p> 
     * 
     * @return Long
     */
    public Long getFieldInhibitFlags() {
        return fieldInhibitFlags;
    }

     /** Setter for fieldInhibitFlags.
     * Field Inhibit Flags is a collection of flags. The flags are combined
using the binary OR operator and store the per attribute value
accumulation flag. Each value present in an Attribute Element is given a
field number ranging from 0 to 31. If the fieldâ€Ÿs corresponding bit in
Inhibit Flags is set, then the field should not participate in attribute
accumulation. All bits are reserved. See each particular Attribute
Element (e.g. Material Attribute Element) for a description of bit field
assignments for each attribute value.
<p>
The variable number 2 : U32 filedInhibitFlags
<br>no options defined
<p> 
     * 
     * @param value Long
     */
    public void setFieldInhibitFlags( Long value ) {
        this.fieldInhibitFlags = value;
    }


    
}

