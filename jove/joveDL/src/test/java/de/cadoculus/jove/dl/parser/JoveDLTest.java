/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cadoculus.jove.dl.parser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.*;
/**
 *
 * @author Carsten Zerbst
 */
public class JoveDLTest {

    public static Logger log = LoggerFactory.getLogger( JoveDLTest.class );

    @BeforeClass
    public static void setUpClass() throws Exception {
        BasicConfigurator.resetConfiguration();

        java.io.File logconfig = new java.io.File( "log4j.properties" );

        if ( logconfig.exists() && logconfig.canRead() ) {

            try {
                URL propsURL = new URL( "file:" + logconfig.getName() );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {

                BasicConfigurator.resetConfiguration();
                BasicConfigurator.configure();

            }
        }


    }

    @Test
    public void parseGUID() throws Exception {
        log.info( "parseGUID" );

        String g = "0x10dd1035, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97";
        ByteArrayInputStream in = new ByteArrayInputStream( g.getBytes() );
        JoveDLParser parser = new JoveDLParser( in );
        Token tok = parser.getNextToken();

        log.info( "tok " + tok.image );
        assertEquals( "guid", g, tok.image );
    }

    @Test
    public void parseClass() throws Exception {
        log.info( "parseClass" );
        String c = this.getClass().getName() + ".class";
        ByteArrayInputStream in = new ByteArrayInputStream( c.getBytes() );
        JoveDLParser parser = new JoveDLParser( in );
       
        Token tok = parser.getNextToken();
        log.info( "tok " + tok.image );
        
        assertEquals( "class",c, tok.image );
    }

    // @Test
    public void test() throws Exception {
        log.info( "test" );
        SimpleNode node;
        try ( InputStream in = getClass().getResourceAsStream( "/jovedl/simple.jdl" ) ) {
            JoveDLParser parser = new JoveDLParser( in );
            node = parser.JoveDL();
        }

        JoveDLVisitorAdapter vis = new JoveDLVisitorAdapter();
        node.jjtAccept( vis, null );
    }
}
