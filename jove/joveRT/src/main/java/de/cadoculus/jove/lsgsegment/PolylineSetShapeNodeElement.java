/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="PolylineSetShapeNodeElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PolylineSetShapeNodeElement",
    propOrder= {"vertexShapeData", "versionNumberPSSN", "areaFactor", "vertexBindings", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0x10dd1046, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97" )
public class PolylineSetShapeNodeElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( PolylineSetShapeNodeElement.class );

    /**  
     * The variable number 0 : VertexShapeData vertexShapeData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="VertexShapeData")
    private VertexShapeData vertexShapeData;

    /**  
     * The variable number 1 : I16 versionNumberPSSN
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumberPSSN;

    /**  
     * The variable number 2 : F32 areaFactor
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float areaFactor;

    /**  
     * The variable number 3 : U64 vertexBindings
     * <br>options : <pre>{valid : obj.versionNumberPSSN == 1}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.U64()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.versionNumberPSSN == 1" )
    private Long vertexBindings;



    /** Getter for vertexShapeData.
     *  
     * 
     * @return VertexShapeData
     */
    public VertexShapeData getVertexShapeData() {
        return vertexShapeData;
    }

     /** Setter for vertexShapeData.
     *  
     * 
     * @param value VertexShapeData
     */
    public void setVertexShapeData( VertexShapeData value ) {
        this.vertexShapeData = value;
    }

    /** Getter for versionNumberPSSN.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberPSSN() {
        return versionNumberPSSN;
    }

     /** Setter for versionNumberPSSN.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberPSSN( Short value ) {
        this.versionNumberPSSN = value;
    }

    /** Getter for areaFactor.
     *  
     * 
     * @return Float
     */
    public Float getAreaFactor() {
        return areaFactor;
    }

     /** Setter for areaFactor.
     *  
     * 
     * @param value Float
     */
    public void setAreaFactor( Float value ) {
        this.areaFactor = value;
    }

    /** Getter for vertexBindings.
     *  
     * 
     * @return Long
     */
    public Long getVertexBindings() {
        return vertexBindings;
    }

     /** Setter for vertexBindings.
     *  
     * 
     * @param value Long
     */
    public void setVertexBindings( Long value ) {
        this.vertexBindings = value;
    }


    
}

