/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import java.lang.reflect.AnnotatedElement;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 * The factory for ListCodecs.<p> Creates ListCodecs if the given metadata if a
 * Field for a generic List.
 *
 * @author Zerbst
 */
public class ListCodecFactory extends AbstractCodecFactory {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( ListCodecFactory.class );

    @Override
    public <T> Codec<T> create( AnnotatedElement metadata, Class<T> type, WorkingContext ctx ) throws BIBIException {

        if ( metadata == null ) {
            log.warn( "no metadata passed to  ListCodecFactory#create" );
            return null;
        }
        if ( !( List.class.isAssignableFrom( type ) ) ) {
            return null;
        }

        Class itemClass = CodecUtility.getItemClass( metadata, type );
        if ( itemClass == null ) {
            return null;
        }

        return (Codec<T>) new ListCodec( metadata, type );
    }
}
