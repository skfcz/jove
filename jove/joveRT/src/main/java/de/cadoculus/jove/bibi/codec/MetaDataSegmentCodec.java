/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.base.LogicalElementHeaderZLIB;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.io.ZLIBCompression;
import de.cadoculus.jove.metadatasegment.MetaDataSegment;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import org.slf4j.LoggerFactory;

/**
 * This codec is used to handle the MetaDataSegmentCodec. The codec dumps the
 * decompressed binary data to the file "./meta.bin" if configured with key
 * "dump.segment.META_DATA".
 *
 * @author Zerbst
 */
public class MetaDataSegmentCodec implements Codec<MetaDataSegment> {

    private static org.slf4j.Logger log = LoggerFactory.getLogger( MetaDataSegmentCodec.class );

    public MetaDataSegmentCodec( Class clazz ) throws BIBIException {
    }

    @Override
    public MetaDataSegment decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        log.info( "" );
        log.info( "start reading " + ctx.getSegmentHeader().getSegmentType() );
        log.info( "" );

        MetaDataSegment retval = new MetaDataSegment();

        //
        // 1. Read the Graph elements until we find the End-Of-Elements marker
        //
        // The Meta Data Segement supports compression, therefore we have to read the 
        // first ElementHeaderZLIB
        int compressionFlag = input.getInt();
        log.info( "compressionFlag " + compressionFlag );
        int compressedDataLength = input.getInt() - 1;// Note that data field compression Algorithm is included in this count.
        log.info( "compressedDataLength " + compressedDataLength );
        byte compressionAlgorithm = input.get();
        log.info( "compressionAlgorithm " + compressionAlgorithm );

        ByteBuffer lsgInput = input;

        if ( compressionFlag == 2 ) {
            byte[] compressed = new byte[ compressedDataLength ];
            input.get( compressed );

            byte[] uncompressed = ZLIBCompression.decompressByZLIB( compressed );

            writeSegment( uncompressed, ctx );

            // Get the decompressed data
            int capa = uncompressed.length;
            ByteBuffer uncompressedData = ByteBuffer.allocate( capa );
            uncompressedData.put( uncompressed );
            uncompressedData.position( 0 );
            uncompressedData.order( input.order() );

            lsgInput = uncompressedData;
            log.info( "decompressed Meta Data Segement from " + compressedDataLength + " to " + capa + " bytes" );

            // Create a copy from the WorkingContext to contain the new ByteBuffer
            ctx = new WorkingContext( ctx );
            ctx.setByteBuffer( lsgInput );
            ctx.setMaxPosition( capa );
        } else {
            log.info( "found Meta Data Segement without compression" );
        }

        log.info( "start decoding at " + ctx.getByteBuffer().position() );
        Codec<LogicalElementHeaderZLIB> lehCodec = ctx.getDefaultCodecFactory().create( null, LogicalElementHeaderZLIB.class, ctx );
        LogicalElementHeaderZLIB leh = lehCodec.decode( lsgInput, ctx );
        leh.setCompressedDataLength( compressedDataLength );
        leh.setCompressionAlgorithm( compressionAlgorithm );
        leh.setCompressionFlag( compressionFlag );

        log.info( "finished reading meta data " + leh );

        retval.getMetaDataElements().add( leh );

        // propagate the reading status from the LogicalElementHeaderZLIB  
        // to the segment
        ctx.getSegmentHeader().setLoadingStatus( leh.getLoadingStatus() );

        return retval;
    }

    private void writeSegment( byte[] uncompressed, WorkingContext ctx ) {
        if ( ctx.getConf().getBooleanProperty( "dump.segment." + ctx.getSegmentHeader().getSegmentType(), false ) ) {
            try {
                try ( FileOutputStream fos = new FileOutputStream(
                        ctx.getSegmentHeader().getSegmentType().toString().toLowerCase() + ".bin" ) ) {
                    fos.write( uncompressed );
                }
            } catch ( Exception exp ) {
                log.error( "failed to dump segment", exp );
            }
        }
    }
}
