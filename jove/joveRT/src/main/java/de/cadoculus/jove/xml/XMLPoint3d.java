/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * A holder class to transfer points as XML.
 *
 * @author Carsten Zerbst
 */
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( name = "Point3d" )
public class XMLPoint3d {

    @XmlAttribute
    private Float x;
    @XmlAttribute
    private Float y;
    @XmlAttribute
    private Float z;

    /**
     * @return the x
     */
    public Float getX() {
        return x;
    }

    /**
     * @return the y
     */
    public Float getY() {
        return y;
    }

    /**
     * @return the z
     */
    public Float getZ() {
        return z;
    }

    /**
     * @param x the x to set
     */
    public void setX( Float x ) {
        this.x = x;
    }

    /**
     * @param y the y to set
     */
    public void setY( Float y ) {
        this.y = y;
    }

    /**
     * @param z the z to set
     */
    public void setZ( Float z ) {
        this.z = z;
    }
}
