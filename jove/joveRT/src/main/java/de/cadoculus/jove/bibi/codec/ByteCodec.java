/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.BIBILength;
import de.cadoculus.jove.bibi.annotation.MbString;
import de.cadoculus.jove.bibi.annotation.UChar;
import java.lang.reflect.AnnotatedElement;
import java.nio.ByteBuffer;
import org.slf4j.LoggerFactory;

/**
 * Reads bytes annotated with UChar from the given ByteBuffer into a bytes.
 *
 * @author Zerbst
 */
public class ByteCodec implements Codec<Byte> {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( StringCodecFactory.class );
    private final Class type;
    private final AnnotatedElement metadata;
    
    /**
     * 
     */
    public ByteCodec( AnnotatedElement metadata, Class type ) {
        this.metadata = metadata;
        this.type = type;

        if ( ( metadata.getAnnotation( UChar.class ) ) != null ) {
            // Ok, use UChar
        } else {
            throw new IllegalArgumentException( "expect UChar annotation on " + metadata );
        }
        if ( ! ( Byte.class == type )) {
            throw new IllegalArgumentException("expect Byte as target type");
        }
    }

    @Override
    public Byte decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {

        CodecUtility.checkMaxPosition( input, ctx );
        return input.get();

    }

    @Override
    public String toString() {
        return "ByteCodec";
    }
}
