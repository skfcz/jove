/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.tocsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
Each TOC Entry represents a Data Segment within the JT File. The essential function of a TOC Entry is to map a Segment
ID to an absolute byte offset within the file.

*/
@XmlRootElement(name="TOCEntry")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "TOCEntry",
    propOrder= {"segmentID", "segmentOffset", "segmentLength", "segmentAttributes", }
)
public class TOCEntry {

    public static Logger log = LoggerFactory.getLogger( TOCEntry.class );

    /** 
     * Segment ID is the globally unique identifier for the segment.<p> 
     * The variable number 0 : GUID segmentID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.GUID()    
    private GUID segmentID;

    /** 
     * Segment Offset defines the byte offset from the top of the file to start of the segment.<p> 
     * The variable number 1 : I32 segmentOffset
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer segmentOffset;

    /** 
     * Segment Length is the total size of the segment in bytes.<p> 
     * The variable number 2 : I32 segmentLength
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer segmentLength;

    /** 
     * Segment Attributes is a collection of segment information encoded within a single U32 using the following bit allocation.
<table><tr><td>Bits 0-23</td><td>Reserved for future use.</td></tr><tr><td>Bits 24-32</td><td>Segment type.Complete list of Segment types can be found
in Table 3: Segment Types. </td></tr></table>
<p> 
     * The variable number 3 : U32 segmentAttributes
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    private Long segmentAttributes;



    /** Getter for segmentID.
     * Segment ID is the globally unique identifier for the segment.<p> 
     * 
     * @return GUID
     */
    public GUID getSegmentID() {
        return segmentID;
    }

     /** Setter for segmentID.
     * Segment ID is the globally unique identifier for the segment.<p> 
     * 
     * @param value GUID
     */
    public void setSegmentID( GUID value ) {
        this.segmentID = value;
    }

    /** Getter for segmentOffset.
     * Segment Offset defines the byte offset from the top of the file to start of the segment.<p> 
     * 
     * @return Integer
     */
    public Integer getSegmentOffset() {
        return segmentOffset;
    }

     /** Setter for segmentOffset.
     * Segment Offset defines the byte offset from the top of the file to start of the segment.<p> 
     * 
     * @param value Integer
     */
    public void setSegmentOffset( Integer value ) {
        this.segmentOffset = value;
    }

    /** Getter for segmentLength.
     * Segment Length is the total size of the segment in bytes.<p> 
     * 
     * @return Integer
     */
    public Integer getSegmentLength() {
        return segmentLength;
    }

     /** Setter for segmentLength.
     * Segment Length is the total size of the segment in bytes.<p> 
     * 
     * @param value Integer
     */
    public void setSegmentLength( Integer value ) {
        this.segmentLength = value;
    }

    /** Getter for segmentAttributes.
     * Segment Attributes is a collection of segment information encoded within a single U32 using the following bit allocation.
<table><tr><td>Bits 0-23</td><td>Reserved for future use.</td></tr><tr><td>Bits 24-32</td><td>Segment type.Complete list of Segment types can be found
in Table 3: Segment Types. </td></tr></table>
<p> 
     * 
     * @return Long
     */
    public Long getSegmentAttributes() {
        return segmentAttributes;
    }

     /** Setter for segmentAttributes.
     * Segment Attributes is a collection of segment information encoded within a single U32 using the following bit allocation.
<table><tr><td>Bits 0-23</td><td>Reserved for future use.</td></tr><tr><td>Bits 24-32</td><td>Segment type.Complete list of Segment types can be found
in Table 3: Segment Types. </td></tr></table>
<p> 
     * 
     * @param value Long
     */
    public void setSegmentAttributes( Long value ) {
        this.segmentAttributes = value;
    }


    
}

