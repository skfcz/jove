/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.xml;


import de.cadoculus.jove.base.GUID;
import javax.xml.bind.annotation.adapters.XmlAdapter;


/**
 * The GUIDAdapater is used serialize and deserialize GUID objects into a compact string
 * representation in XML.
 *
 * @see     GUID#GUID(java.lang.String)
 * @see     GUID#toString()
 * @author  Carsten Zerbst
 */
public class GUIDAdapter extends XmlAdapter< String, GUID > {

    @Override public String marshal( GUID v ) throws Exception {
        return v.toString();
    }

    @Override public GUID unmarshal( String v ) throws Exception {
        return new GUID( v );
    }

}
