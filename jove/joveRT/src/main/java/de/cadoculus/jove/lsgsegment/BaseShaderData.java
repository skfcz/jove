/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
7.2.1.1.2.1.2 Base Shader Data
The JT v9 file format is able to represent vertex- and fragment shader programs in GLSL source code form together with parameter bindings for both. The shader source code can be specified inline directly in the JT file, or as a filename containing the shader source code.

*/
@XmlRootElement(name="BaseShaderData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "BaseShaderData",
    propOrder= {"versionNumberBSD", "shaderLanguage", "inlineSourceFlag", "sourceCodeLoc", "sourceCode", "shaderParamCount", "shaderParameter", }
)
public class BaseShaderData {

    public static Logger log = LoggerFactory.getLogger( BaseShaderData.class );

    /**  
     * The variable number 0 : I16 versionNumberBSD
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumberBSD;

    /**  
     * The variable number 1 : I32 shaderLanguage
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer shaderLanguage;

    /**  
     * The variable number 2 : U32 inlineSourceFlag
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    private Long inlineSourceFlag;

    /**  
     * The variable number 3 : MbString sourceCodeLoc
     * <br>options : <pre>{valid : obj.inlineSourceFlag != 1}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.MbString()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.inlineSourceFlag != 1" )
    private String sourceCodeLoc;

    /**  
     * The variable number 4 : MbString sourceCode
     * <br>options : <pre>{valid : obj.inlineSourceFlag == 1}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.MbString()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.inlineSourceFlag == 1" )
    private String sourceCode;

    /**  
     * The variable number 5 : I32 shaderParamCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer shaderParamCount;

    /**  
     * The variable number 6 : ShaderParameter[] shaderParameter
     * <br>options : <pre>{length : obj.shaderParamCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.shaderParamCount" )
    @XmlElementWrapper(name = "shaderParameter")
    @XmlElementRef()    private java.util.List<ShaderParameter> shaderParameter;



    /** Getter for versionNumberBSD.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberBSD() {
        return versionNumberBSD;
    }

     /** Setter for versionNumberBSD.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberBSD( Short value ) {
        this.versionNumberBSD = value;
    }

    /** Getter for shaderLanguage.
     *  
     * 
     * @return Integer
     */
    public Integer getShaderLanguage() {
        return shaderLanguage;
    }

     /** Setter for shaderLanguage.
     *  
     * 
     * @param value Integer
     */
    public void setShaderLanguage( Integer value ) {
        this.shaderLanguage = value;
    }

    /** Getter for inlineSourceFlag.
     *  
     * 
     * @return Long
     */
    public Long getInlineSourceFlag() {
        return inlineSourceFlag;
    }

     /** Setter for inlineSourceFlag.
     *  
     * 
     * @param value Long
     */
    public void setInlineSourceFlag( Long value ) {
        this.inlineSourceFlag = value;
    }

    /** Getter for sourceCodeLoc.
     *  
     * 
     * @return String
     */
    public String getSourceCodeLoc() {
        return sourceCodeLoc;
    }

     /** Setter for sourceCodeLoc.
     *  
     * 
     * @param value String
     */
    public void setSourceCodeLoc( String value ) {
        this.sourceCodeLoc = value;
    }

    /** Getter for sourceCode.
     *  
     * 
     * @return String
     */
    public String getSourceCode() {
        return sourceCode;
    }

     /** Setter for sourceCode.
     *  
     * 
     * @param value String
     */
    public void setSourceCode( String value ) {
        this.sourceCode = value;
    }

    /** Getter for shaderParamCount.
     *  
     * 
     * @return Integer
     */
    public Integer getShaderParamCount() {
        return shaderParamCount;
    }

     /** Setter for shaderParamCount.
     *  
     * 
     * @param value Integer
     */
    public void setShaderParamCount( Integer value ) {
        this.shaderParamCount = value;
    }

    /** Getter for shaderParameter.
     *  
     * 
     * @return java.util.List<ShaderParameter>
     */
    public java.util.List<ShaderParameter> getShaderParameter() {
        return shaderParameter;
    }

     /** Setter for shaderParameter.
     *  
     * 
     * @param value java.util.List<ShaderParameter>
     */
    public void setShaderParameter( java.util.List<ShaderParameter> value ) {
        this.shaderParameter = value;
    }


    
}

