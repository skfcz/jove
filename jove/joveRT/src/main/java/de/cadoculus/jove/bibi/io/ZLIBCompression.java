/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.slf4j.LoggerFactory;

/**
 * Implements the ZLIB compression for byte[] --> byte[]
 *
 * @see Deflater
 * @see Inflater
 * @author Carsten Zerbst
 */
public class ZLIBCompression {

    private static org.slf4j.Logger log = LoggerFactory.getLogger( ZLIBCompression.class );

    /**
     * Compresses the given byte array by the ZLIB algorithm.
     *
     * @param uncompressedBytes the bytes to compress
     *
     * @return compressed bytes
     */
    public static byte[] compressByZLIB( byte[] uncompressedBytes ) {

        Deflater compressor = new Deflater();
        compressor.setLevel( 1 );

        // Give the compressor the data to compress
        compressor.setInput( uncompressedBytes );
        compressor.finish();

        // Create an expandable byte array to hold the compressed data.
        // You cannot use an array that's the same size as the orginal because
        // there is no guarantee that the compressed data will be smaller than
        // the uncompressed data.
        ByteArrayOutputStream bos = new ByteArrayOutputStream( uncompressedBytes.length );

        // Compress the data
        byte[] buf = new byte[ 1024 ];

        while ( !compressor.finished() ) {
            int count = compressor.deflate( buf );
            bos.write( buf, 0, count );
        }

        try {
            bos.close();
        } catch ( IOException exp ) {
            log.error( "should not happen", exp );
        }

        // Get the compressed data
        return bos.toByteArray();
    }

    /**
     * Uncompresses the given byte array by the ZLIB algorithm.
     *
     * @param compressedBytes Compressed bytes
     *
     * @return Uncompressed bytes
     */
    public static byte[] decompressByZLIB( byte[] compressedBytes ) {

        try {
            Inflater inflater = new Inflater();
            inflater.setInput( compressedBytes );

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
                    compressedBytes.length );
            byte[] buffer = new byte[ 1024 ];

            while ( !inflater.finished() ) {
                int count = inflater.inflate( buffer );
                byteArrayOutputStream.write( buffer, 0, count );
            }

            byteArrayOutputStream.close();

            return byteArrayOutputStream.toByteArray();

        } catch ( Exception exception ) {
            throw new IllegalStateException( exception.getMessage() );
        }
    }
}
