/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.base;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.enums.ObjectBaseType;
import de.cadoculus.jove.enums.ObjectTypeIdentifier;
import de.cadoculus.jove.lsgsegment.BaseAttributeElement;
import de.cadoculus.jove.lsgsegment.BaseNodeElement;
import de.cadoculus.jove.lsgsegment.BasePropertyAtomElement;
import de.cadoculus.jove.lsgsegment.BaseShapeNodeElement;
import de.cadoculus.jove.lsgsegment.GroupNodeElement;
import de.cadoculus.jove.lsgsegment.JTObjectReferencePropertyAtomElement;
import de.cadoculus.jove.lsgsegment.LateLoadedPropertyAtomElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.slf4j.LoggerFactory;

/**
 * Element Header contains data describing the object type contained in the
 * Element. It is a combination of the "Logical Element Header" and the "Element
 * Header"
 *
 *
 * <h4>Object Type ID</h4>
 *
 * Object Type ID is the globally unique identifier for the object type.
 *
 * <h4>Object Base Type</h4>
 *
 * Object Base Type identifies the base object type. This is useful when an
 * unknown element type is encountered and thus the best the loader can do is to
 * read the known Object Base Type data bytes (base type object data is always
 * written first) and then skip (read pass) the bytes of unknown data using
 * knowledge of number of bytes encompassing the Object Base Type data and the
 * unknown types Length field. If the Object Base Type is unknown then the
 * loader should simply skip (read pass) Element Length number of bytes.
 *
 * <h4>Object Id</h4>
 *
 * Object ID is the identifier for this Object. Other objects referencing this
 * particular object do so using the Object ID.
 *
 * @author Zerbst
 */
@XmlRootElement
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType(
         name = "LogicalElementHeader",
        propOrder = { "loadingException", "elementLength", "objectTypeId", "objectBaseType", "objectID" } )
public class LogicalElementHeader implements RecoverableElement {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( LogicalElementHeader.class );
    @XmlAttribute
    private Long elementStart;
    @de.cadoculus.jove.bibi.annotation.I32()
    private Integer elementLength;
    @de.cadoculus.jove.bibi.annotation.GUID()
    private ObjectTypeIdentifier objectTypeId;
    @de.cadoculus.jove.bibi.annotation.UChar
    private ObjectBaseType objectBaseType;
    @de.cadoculus.jove.bibi.annotation.I32()
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private Integer objectID;

    @XmlAttribute( name = "loadingStatus" )
    private LoadingStatus loadingStatus = LoadingStatus.UNKNOWN;
    
    @XmlJavaTypeAdapter( ExceptionXmlAdapter.class )
    private BIBIException loadingException;

    /**
     * @return the objectTypeId
     */
    public ObjectTypeIdentifier getObjectTypeId() {
        return objectTypeId;
    }

    /**
     * Object Base Type identifies the base object type. This is useful when an
     * unknown element type is encountered and thus the best the loader can do
     * is to read the known Object Base Type data bytes (base type object data
     * is always written first) and then skip (read pass) the bytes of unknown
     * data using knowledge of number of bytes encompassing the Object Base Type
     * data and the unknown types Length field. If the Object Base Type is
     * unknown then the loader should simply skip (read pass) Element Length
     * number of bytes.
     *
     * <table border="1"> <thead> <tr><th>Base
     * Type</th><th>Description</th><th>Base Type’s Data Format</th></tr>
     * </thead>
     *
     * <tbody>
     *
     * <tr><td>255</td><td>Unknown Graph Node Object</td><td>none</td></tr>
     *
     * <tr><td>0</td><td>Base Graph Node Object</td><td>7.2.1.1.1.1.1 Base Node
     * Data</td></tr>
     *
     * <tr><td>1</td><td>Group Graph Node Object</td><td>7.2.1.1.1.3.1 Group
     * Node Data</td></tr>
     *
     * <tr><td>2</td><td>Shape Graph Node Object</td><td>7.2.1.1.1.10.1.1 Base
     * Shape Data</td></tr>
     *
     * <tr><td>3</td><td>Base Attribute Object</td><td>7.2.1.1.2.1.1 Base
     * Attribute Data</td></tr>
     *
     * <tr><td>4</td><td>Shape LOD</td><td>none</td></tr>
     *
     * <tr><td>5</td><td>Base Property Object</td><td>7.2.1.2.1.1 Base Property
     * Atom Data</td></tr>
     *
     * <tr><td>6</td><td>JT Object Reference Object</td><td>7.2.1.2.5 JT Object
     * Reference Property Atom Element without the Logical Element Header ZLIB
     * data collection.</td></tr>
     *
     * <tr><td>8</td><td>JT Late Loaded Property Object</td><td>0 Late Loaded
     * Property Atom Element without the Logical Element Header ZLIB data
     * collection.</td></tr>
     *
     * <tr><td>9</td><td>JtBase (none)</td><td>none</td></tr>
     *
     * </tbody></table>
     *
     */
    public ObjectBaseType getObjectBaseType() {
        return objectBaseType;
    }

    /**
     * Resolve the object base type as GUID using the Table 4: Object Base Type.
     * See {@link  #getObjectBaseTypeC() }
     *
     * @param baseObjectType the LogicalElementHeader#objectBaseType
     * @return the GUID of the base type or null
     */
    public static Class resolveObjectBaseType( ObjectBaseType baseObjectType ) {

        switch ( baseObjectType ) {
            case UNKNOWN_GRAPH_NODE_OBJECT:
                return null;
            case BASE_GRAPH_NODE_OBJECT:
                return BaseNodeElement.class;
            case GROUP_GRAPH_NODE_OBJECT:
                return GroupNodeElement.class;
            case SHAPE_GRAPH_NODE_OBJECT:
                return BaseShapeNodeElement.class;
            case BASE_ATTRIBUTE_OBJECT:
                return BaseAttributeElement.class;
            case SHAPE_LOD:
                return null;
            case BASE_PROPERTY_OBJECT:
                return BasePropertyAtomElement.class;
            case JT_OBJECT_REFERENCE_OBJECT:
                return JTObjectReferencePropertyAtomElement.class;
            case JT_LATE_LOADED_PROPERTY_OBJECT:
                return LateLoadedPropertyAtomElement.class;
            case JTBASE:
                return null;
            default:
                log.warn( "got unknown object base type " + baseObjectType );
                return null;
        }

    }

    /**
     * @return the objectId
     */
    public Integer getObjectID() {
        return objectID;
    }

    /**
     * @return the elementLength
     */
    public int getElementLength() {
        return elementLength;
    }

    /**
     * @param elementLength the elementLength to set
     */
    public void setElementLength( int elementLength ) {
        this.elementLength = elementLength;
    }

    /**
     * @param objectTypeId the objectTypeId to set
     */
    public void setObjectTypeId( ObjectTypeIdentifier objectTypeId ) {
        this.objectTypeId = objectTypeId;
    }

    /**
     * @param objectBaseType the objectBaseType to set
     */
    public void setObjectBaseType( ObjectBaseType objectBaseType ) {
        this.objectBaseType = objectBaseType;
    }

    /**
     * @param objectId the objectId to set
     */
    public void setObjectID( Integer objectId ) {
        this.objectID = objectId;
    }

    /**
     * @return the elementStart
     */
    public Long getElementStart() {
        return elementStart;
    }

    /**
     * @param elementStart the elementStart to set
     */
    public void setElementStart( Long elementStart ) {
        this.elementStart = elementStart;
    }

    @Override
    public LoadingStatus getLoadingStatus() {
        return loadingStatus;
    }

    public void setLoadingStatus( LoadingStatus status ) {
        this.loadingStatus = status;
    }

    /**
     * Get the exception which occured when loading this SegmentHeader.
     *
     * @return the exception, may be null
     */
    @Override
    public BIBIException getException() {
        return loadingException;
    }

    /**
     * Set the exception which occured when loading this SegmentHeader.
     *
     * @param the exception
     */
    public void setException( BIBIException ex ) {
        this.loadingException = ex;
    }
}
