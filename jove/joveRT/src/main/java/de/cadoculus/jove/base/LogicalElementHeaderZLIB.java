/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Logical Element Header ZLIB data collection is the format of Element Header
 * data used by all Elements within Segment Types that support ZLIB compression
 * on all data in the Segment. See Table 3: Segment Types for information on
 * whether a particular Segment Type supports ZLIB compression on all data in
 * the Segment.
 *
 * <h4>CompressionFlag</h4>
 *
 * Compression Flag is a flag indicating whether ZLIB compression is ON/OFF for
 * all data elements in the file Segment. Valid values include the following:
 *
 *
 * <ul> <li>compressionFlag == 2 : ZLIB compression is ON</li>
 * <li>compressionFlag != 2 : ZLIB compression is OFF</li> </ul>
 *
 *
 * <h4>Compressed Data Length</h4>
 *
 * Compressed Data Length specifies the compressed data length in number of
 * bytes. Note that data field Compression Algorithm is included in this count.
 *
 * <h4>Compression Algorithm</h4>
 *
 * Compression Algorithm specifies the compression algorithm applied to all data
 * in the Segment. Valid values include the following:
 *
 * <ul><li>compressionAlgorithm == 1 : No
 * compression</li><li>compressionAlgorithm == 2 : ZLIB compression</li></ul>
 *
 * @author Carsten Zerbst
 */
@XmlRootElement
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType(
        name = "LogicalElementHeaderZLIB",
        propOrder = {"compressionFlag", "compressedDataLength", "compressionAlgorithm" } )
@de.cadoculus.jove.bibi.annotation.BIBICodec( de.cadoculus.jove.bibi.codec.LogicalElementHeaderZLIBCodec.class )
public class LogicalElementHeaderZLIB extends LogicalElementHeader {

    private int compressionFlag;
    private int compressedDataLength;
    private short compressionAlgorithm;

    /**
     * @return the compressionFlag
     */
    public int getCompressionFlag() {
        return compressionFlag;
    }

    /**
     * @param compressionFlag the compressionFlag to set
     */
    public void setCompressionFlag( int compressionFlag ) {
        this.compressionFlag = compressionFlag;
    }

    /**
     * @return the compressedDataLength
     */
    public int getCompressedDataLength() {
        return compressedDataLength;
    }

    /**
     * @param compressedDataLength the compressedDataLength to set
     */
    public void setCompressedDataLength( int compressedDataLength ) {
        this.compressedDataLength = compressedDataLength;
    }

    /**
     * @return the compressionAlgorithm
     */
    public short getCompressionAlgorithm() {
        return compressionAlgorithm;
    }

    /**
     * @param compressionAlgorithm the compressionAlgorithm to set
     */
    public void setCompressionAlgorithm( short compressionAlgorithm ) {
        this.compressionAlgorithm = compressionAlgorithm;
    }
}
