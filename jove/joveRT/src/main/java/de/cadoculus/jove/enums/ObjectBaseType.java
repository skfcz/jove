/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.enums;

import de.cadoculus.jove.base.*;

/** 
Object Base Type identifies the base object type. This is useful when an unknown element type is encountered and thus the
best the loader can do is to read the known Object Base Type data bytes (base type object data is always written first) and
then skip (read pass) the bytes of unknown data using knowledge of number of bytes encompassing the Object Base Type
data and the unknown types Length field. If the Object Base Type is unknown then the loader should simply skip (read pass)
Element Length number of bytes.

 * 
 * This enumeration contains 10 values of type I32
*/

public enum ObjectBaseType {
   
    UNKNOWN_GRAPH_NODE_OBJECT( 255 ),
    BASE_GRAPH_NODE_OBJECT( 0 ),
    GROUP_GRAPH_NODE_OBJECT( 1 ),
    SHAPE_GRAPH_NODE_OBJECT( 2 ),
    BASE_ATTRIBUTE_OBJECT( 3 ),
    SHAPE_LOD( 4 ),
    BASE_PROPERTY_OBJECT( 5 ),
    JT_OBJECT_REFERENCE_OBJECT( 6 ),
    JT_LATE_LOADED_PROPERTY_OBJECT( 8 ),
    JTBASE( 9 );

    
    private final Integer enumValue;
    private ObjectBaseType( Integer v ) {
        this.enumValue = v;
    }

    /**
     * Get the integer value contained in the enumeration
     */
    public int getValue() {
        return enumValue;
    }
    /**
     * Get a ObjectBaseType for the given enumeration value
     * @param v the enumeration value
     * @return the found value
     * @throws IllegalArgumentException  if no ObjectBaseType was found for the given value
     */
    public static ObjectBaseType valueOf( Integer v ) throws IllegalArgumentException {
        for ( ObjectBaseType test : ObjectBaseType.values() ) {
            if ( test.getValue() == v ) {
                return test;
            }
        }
        throw new IllegalArgumentException( "could not find ObjectBaseType for value '" + v + "'" );

    }       
    
}

