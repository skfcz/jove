/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cadoculus.jove.dl.parser;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Check correct working of TypeCollectorVisitor
 *
 * @author Carsten Zerbst
 */
public class TypeCollectorVisitorTest {

    public static Logger log = LoggerFactory.getLogger( TypeCollectorVisitorTest.class );

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println( "setUpClass" );
        BasicConfigurator.resetConfiguration();

        java.io.File logconfig = new java.io.File( "log4j.properties" );
        System.out.println( "setUpClass " + logconfig.getAbsolutePath() );
        if ( logconfig.exists() && logconfig.canRead() ) {

            try {
                URL propsURL = new URL( "file:" + logconfig.getName() );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {

                BasicConfigurator.resetConfiguration();
                BasicConfigurator.configure();

            }
        } else {
            BasicConfigurator.resetConfiguration();
            BasicConfigurator.configure();

        }

    }

    @Test
    public void parseEnums() throws Exception {
        System.out.println( "parseEnums" );
        log.info( "parseEnums" );

        InputStream in = getClass().getResourceAsStream( "/jovedl/enums.jdl" );
        JoveDLParser parser = new JoveDLParser( in );
        SimpleNode node = parser.JoveDL();
        in.close();

        TypeCollectorVisitor vis = new TypeCollectorVisitor();
        node.jjtAccept( vis, null );

        Set<String> enumerationNames = vis.getEnumerationNames();
        assertNotNull( "enumerationNames", enumerationNames );
        assertEquals( "enumerationNames #", 3, enumerationNames.size() );
        assertTrue( "enumerationNames contains SegmentType", enumerationNames.contains( "SegmentType" ) );
        assertTrue( "enumerationNames contains ObjectTypeIdentifier", enumerationNames.contains( "ObjectTypeIdentifier" ) );
        assertTrue( "enumerationNames contains BaseType", enumerationNames.contains( "BaseType" ) );
        assertFalse( "enumerationNames contains PMIFontType", enumerationNames.contains( "PMIFontType" ) );

    }

    @Test
    public void parseSimple() throws Exception {
        System.out.println( "parseSimple" );
        log.info( "parseSimple" );

        InputStream in = getClass().getResourceAsStream( "/jovedl/medium.jdl" );
        JoveDLParser parser = new JoveDLParser( in );
        SimpleNode node = parser.JoveDL();
        in.close();

        TypeCollectorVisitor vis = new TypeCollectorVisitor();
        node.jjtAccept( vis, null );

        Set<String> enumerationNames = vis.getEnumerationNames();
        assertNotNull( "enumerationNames", enumerationNames );
        assertEquals( "enumerationNames #", 1, enumerationNames.size() );
        assertTrue( "enumerationNames contains SegmentType", enumerationNames.contains( "SegmentType" ) );

        Map<String, Set<String>> allDefinedTypes = vis.getAllDefinedTypes();
        assertNotNull( "allDefinedTypes", allDefinedTypes );
        assertEquals( "allDefinedTypes #keys", 3, allDefinedTypes.size() );

        assertTrue( "found FileHeader", allDefinedTypes.keySet().contains( "FileHeader" ) );

        Set<String> types = allDefinedTypes.get( "FileHeader" );
        assertNotNull( "types FileHeader", types );
        assertEquals( "types FileHeader #", 1, types.size() );
        assertEquals( "type FileHeader", "FileHeader", types.iterator().next() );

        assertTrue( "found TOCSegment", allDefinedTypes.keySet().contains( "TOCSegment" ) );
        types = allDefinedTypes.get( "TOCSegment" );
        assertNotNull( "types TOCSegment", types );
        assertEquals( "types TOCSegment #", 2, types.size() );
        assertTrue( "conatains TCOEntry", types.contains( "TOCEntry" ) );
        assertTrue( "conatains TCOEntry", types.contains( "TOCSegment" ) );

        assertTrue( "found LSGSegment", allDefinedTypes.keySet().contains( "LSGSegment" ) );
        types = allDefinedTypes.get( "LSGSegment" );
        assertNotNull( "types LSGSegment", types );

        log.info( "LSGSegment types\n" + types );

        String[] enames = new String[]{
            "BaseNodeElement",
            "BaseNodeData",
            "PartitionNodeElement",
            "VertexCountRange",
            "NodeCountRange",
            "PolygonCountRange",
            "GroupNodeElement",
            "GroupNodeData",
            "InstanceNodeElement",
            "PartNodeElement",
            "MetaDataNodeElement",
            "MetaNodeData",
            "LODNodeElement",
            "LODNodeData",
            "RangeLODNodeElement",
            "SwitchNodeElement",
            "BaseShapeNodeElement",
            "BaseShapeData",
            "VertexShapeNodeElement",
            "VertexShapeData",
            "QuantizationParameters",
            "BaseAttributeData",
            "JTObjectReferencePropertyAtomElement",
            "LateLoadedPropertyAtomElement",
            "BaseAttributeElement",
            "BasePropertyAtomData",
            "BasePropertyAtomElement" };

        for ( int i = 0; i < enames.length; i++ ) {
            String name = enames[i];
            assertTrue( "entities in  LSGSegment contains " + name, types.contains( name ) );
            types.remove( name );

        }
        log.info( "remaining " + types );
        assertEquals( "remaining types #", 0, types.size() );

    }

    @Test
    public void duplicateType() throws Exception {
        log.info( "duplicateType" );

        InputStream in = getClass().getResourceAsStream( "/jovedl/duplicateTypes.jdl" );
        JoveDLParser parser = new JoveDLParser( in );
        SimpleNode node = parser.JoveDL();
        in.close();

        try {
            TypeCollectorVisitor vis = new TypeCollectorVisitor();
            node.jjtAccept( vis, null );
            fail( "no error on duplicated type" );
        } catch ( IllegalArgumentException exp ) {
            // OK
        }
    }

    @Test
    public void missingDefinition() throws Exception {
        log.info( "missingDefinition" );

        InputStream in = getClass().getResourceAsStream( "/jovedl/missingDefinition.jdl" );
        JoveDLParser parser = new JoveDLParser( in );
        SimpleNode node = parser.JoveDL();
        in.close();

        try {
            TypeCollectorVisitor vis = new TypeCollectorVisitor();
            node.jjtAccept( vis, null );
            fail( "no error on missing refenced type" );
        } catch ( IllegalArgumentException exp ) {
            // OK
        }
    }
}
