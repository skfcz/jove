/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.12 PMI Design Group Entities</h4>
The PMI Design Group Entities data collection defines data for a list of Design Groups. Design Groups are collections of PMI created to organize a model into smaller subsets of information. This organization is achieved via PMI Associations (see 7.2.6.2.2 PMI Associations), where specific PMI entities are associated as "destinations" to a "source" PMI Design Group.

*/
@XmlRootElement(name="PMIDesignGroupEntities")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIDesignGroupEntities",
    propOrder= {"designGroupCount", "designGroupEntities", }
)
public class PMIDesignGroupEntities {

    public static Logger log = LoggerFactory.getLogger( PMIDesignGroupEntities.class );

    /**  
     * The variable number 0 : I32 designGroupCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer designGroupCount;

    /**  
     * The variable number 1 : PMIDesignGroupEntity[] designGroupEntities
     * <br>options : <pre>{length : obj.designGroupCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.designGroupCount" )
    @XmlElementWrapper(name = "designGroupEntities")
    @XmlElementRef()    private java.util.List<PMIDesignGroupEntity> designGroupEntities;



    /** Getter for designGroupCount.
     *  
     * 
     * @return Integer
     */
    public Integer getDesignGroupCount() {
        return designGroupCount;
    }

     /** Setter for designGroupCount.
     *  
     * 
     * @param value Integer
     */
    public void setDesignGroupCount( Integer value ) {
        this.designGroupCount = value;
    }

    /** Getter for designGroupEntities.
     *  
     * 
     * @return java.util.List<PMIDesignGroupEntity>
     */
    public java.util.List<PMIDesignGroupEntity> getDesignGroupEntities() {
        return designGroupEntities;
    }

     /** Setter for designGroupEntities.
     *  
     * 
     * @param value java.util.List<PMIDesignGroupEntity>
     */
    public void setDesignGroupEntities( java.util.List<PMIDesignGroupEntity> value ) {
        this.designGroupEntities = value;
    }


    
}

