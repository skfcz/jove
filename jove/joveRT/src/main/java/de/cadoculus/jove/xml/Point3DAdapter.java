/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.xml;

import de.cadoculus.jove.bibi.annotation.CoordF32;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * The adapter to convert javax.vecmath.Point3d to and from XMLPoint3d
 *
 * @author Carsten Zerbst
 */
public class Point3DAdapter extends XmlAdapter< XMLPoint3d, Point3f> {

    @Override
    public XMLPoint3d marshal( Point3f v ) throws Exception {

        XMLPoint3d retval = new XMLPoint3d();
        retval.setX( v.x );
        retval.setY( v.y );
        retval.setZ( v.z );

        return retval;
    }

    @Override
    public Point3f unmarshal( XMLPoint3d v ) throws Exception {
        Point3f retval = new Point3f( v.getX(), v.getY(), v.getZ() );

        return retval;

    }
}
