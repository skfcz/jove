/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
7.2.6.2.1 PMI Entities

*/
@XmlRootElement(name="PMIEntities")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIEntities",
    propOrder= {"pmiDimensionEntities", "pmiNoteEntities", "pmiDatumFeatureSymbolEntities", "pmiDatumTargetEntities", "pmiFeatureControlFrameEntities", "pmiLineWeldEntities", "pmiSpotWeldEntities", "pmiSurfaceFinishEntities", "pmiMeasurementPointEntities", "pmiLocatorEntities", "pmiReferenceGeometryEntities", "pmiDesignGroupEntities", "pmiCoordinateSystemEntities", }
)
public class PMIEntities {

    public static Logger log = LoggerFactory.getLogger( PMIEntities.class );

    /**  
     * The variable number 0 : PMIDimensionEntities pmiDimensionEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIDimensionEntities")
    private PMIDimensionEntities pmiDimensionEntities;

    /**  
     * The variable number 1 : PMINoteEntities pmiNoteEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMINoteEntities")
    private PMINoteEntities pmiNoteEntities;

    /**  
     * The variable number 2 : PMIDatumFeatureSymbolEntities pmiDatumFeatureSymbolEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIDatumFeatureSymbolEntities")
    private PMIDatumFeatureSymbolEntities pmiDatumFeatureSymbolEntities;

    /**  
     * The variable number 3 : PMIDatumTargetEntities pmiDatumTargetEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIDatumTargetEntities")
    private PMIDatumTargetEntities pmiDatumTargetEntities;

    /**  
     * The variable number 4 : PMIFeatureControlFrameEntities pmiFeatureControlFrameEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIFeatureControlFrameEntities")
    private PMIFeatureControlFrameEntities pmiFeatureControlFrameEntities;

    /**  
     * The variable number 5 : PMILineWeldEntities pmiLineWeldEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMILineWeldEntities")
    private PMILineWeldEntities pmiLineWeldEntities;

    /**  
     * The variable number 6 : PMISpotWeldEntities pmiSpotWeldEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMISpotWeldEntities")
    private PMISpotWeldEntities pmiSpotWeldEntities;

    /**  
     * The variable number 7 : PMISurfaceFinishEntities pmiSurfaceFinishEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMISurfaceFinishEntities")
    private PMISurfaceFinishEntities pmiSurfaceFinishEntities;

    /**  
     * The variable number 8 : PMIMeasurementPointEntities pmiMeasurementPointEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIMeasurementPointEntities")
    private PMIMeasurementPointEntities pmiMeasurementPointEntities;

    /**  
     * The variable number 9 : PMILocatorEntities pmiLocatorEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMILocatorEntities")
    private PMILocatorEntities pmiLocatorEntities;

    /**  
     * The variable number 10 : PMIReferenceGeometryEntities pmiReferenceGeometryEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIReferenceGeometryEntities")
    private PMIReferenceGeometryEntities pmiReferenceGeometryEntities;

    /**  
     * The variable number 11 : PMIDesignGroupEntities pmiDesignGroupEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIDesignGroupEntities")
    private PMIDesignGroupEntities pmiDesignGroupEntities;

    /**  
     * The variable number 12 : PMICoordinateSystemEntities pmiCoordinateSystemEntities
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMICoordinateSystemEntities")
    private PMICoordinateSystemEntities pmiCoordinateSystemEntities;



    /** Getter for pmiDimensionEntities.
     *  
     * 
     * @return PMIDimensionEntities
     */
    public PMIDimensionEntities getPmiDimensionEntities() {
        return pmiDimensionEntities;
    }

     /** Setter for pmiDimensionEntities.
     *  
     * 
     * @param value PMIDimensionEntities
     */
    public void setPmiDimensionEntities( PMIDimensionEntities value ) {
        this.pmiDimensionEntities = value;
    }

    /** Getter for pmiNoteEntities.
     *  
     * 
     * @return PMINoteEntities
     */
    public PMINoteEntities getPmiNoteEntities() {
        return pmiNoteEntities;
    }

     /** Setter for pmiNoteEntities.
     *  
     * 
     * @param value PMINoteEntities
     */
    public void setPmiNoteEntities( PMINoteEntities value ) {
        this.pmiNoteEntities = value;
    }

    /** Getter for pmiDatumFeatureSymbolEntities.
     *  
     * 
     * @return PMIDatumFeatureSymbolEntities
     */
    public PMIDatumFeatureSymbolEntities getPmiDatumFeatureSymbolEntities() {
        return pmiDatumFeatureSymbolEntities;
    }

     /** Setter for pmiDatumFeatureSymbolEntities.
     *  
     * 
     * @param value PMIDatumFeatureSymbolEntities
     */
    public void setPmiDatumFeatureSymbolEntities( PMIDatumFeatureSymbolEntities value ) {
        this.pmiDatumFeatureSymbolEntities = value;
    }

    /** Getter for pmiDatumTargetEntities.
     *  
     * 
     * @return PMIDatumTargetEntities
     */
    public PMIDatumTargetEntities getPmiDatumTargetEntities() {
        return pmiDatumTargetEntities;
    }

     /** Setter for pmiDatumTargetEntities.
     *  
     * 
     * @param value PMIDatumTargetEntities
     */
    public void setPmiDatumTargetEntities( PMIDatumTargetEntities value ) {
        this.pmiDatumTargetEntities = value;
    }

    /** Getter for pmiFeatureControlFrameEntities.
     *  
     * 
     * @return PMIFeatureControlFrameEntities
     */
    public PMIFeatureControlFrameEntities getPmiFeatureControlFrameEntities() {
        return pmiFeatureControlFrameEntities;
    }

     /** Setter for pmiFeatureControlFrameEntities.
     *  
     * 
     * @param value PMIFeatureControlFrameEntities
     */
    public void setPmiFeatureControlFrameEntities( PMIFeatureControlFrameEntities value ) {
        this.pmiFeatureControlFrameEntities = value;
    }

    /** Getter for pmiLineWeldEntities.
     *  
     * 
     * @return PMILineWeldEntities
     */
    public PMILineWeldEntities getPmiLineWeldEntities() {
        return pmiLineWeldEntities;
    }

     /** Setter for pmiLineWeldEntities.
     *  
     * 
     * @param value PMILineWeldEntities
     */
    public void setPmiLineWeldEntities( PMILineWeldEntities value ) {
        this.pmiLineWeldEntities = value;
    }

    /** Getter for pmiSpotWeldEntities.
     *  
     * 
     * @return PMISpotWeldEntities
     */
    public PMISpotWeldEntities getPmiSpotWeldEntities() {
        return pmiSpotWeldEntities;
    }

     /** Setter for pmiSpotWeldEntities.
     *  
     * 
     * @param value PMISpotWeldEntities
     */
    public void setPmiSpotWeldEntities( PMISpotWeldEntities value ) {
        this.pmiSpotWeldEntities = value;
    }

    /** Getter for pmiSurfaceFinishEntities.
     *  
     * 
     * @return PMISurfaceFinishEntities
     */
    public PMISurfaceFinishEntities getPmiSurfaceFinishEntities() {
        return pmiSurfaceFinishEntities;
    }

     /** Setter for pmiSurfaceFinishEntities.
     *  
     * 
     * @param value PMISurfaceFinishEntities
     */
    public void setPmiSurfaceFinishEntities( PMISurfaceFinishEntities value ) {
        this.pmiSurfaceFinishEntities = value;
    }

    /** Getter for pmiMeasurementPointEntities.
     *  
     * 
     * @return PMIMeasurementPointEntities
     */
    public PMIMeasurementPointEntities getPmiMeasurementPointEntities() {
        return pmiMeasurementPointEntities;
    }

     /** Setter for pmiMeasurementPointEntities.
     *  
     * 
     * @param value PMIMeasurementPointEntities
     */
    public void setPmiMeasurementPointEntities( PMIMeasurementPointEntities value ) {
        this.pmiMeasurementPointEntities = value;
    }

    /** Getter for pmiLocatorEntities.
     *  
     * 
     * @return PMILocatorEntities
     */
    public PMILocatorEntities getPmiLocatorEntities() {
        return pmiLocatorEntities;
    }

     /** Setter for pmiLocatorEntities.
     *  
     * 
     * @param value PMILocatorEntities
     */
    public void setPmiLocatorEntities( PMILocatorEntities value ) {
        this.pmiLocatorEntities = value;
    }

    /** Getter for pmiReferenceGeometryEntities.
     *  
     * 
     * @return PMIReferenceGeometryEntities
     */
    public PMIReferenceGeometryEntities getPmiReferenceGeometryEntities() {
        return pmiReferenceGeometryEntities;
    }

     /** Setter for pmiReferenceGeometryEntities.
     *  
     * 
     * @param value PMIReferenceGeometryEntities
     */
    public void setPmiReferenceGeometryEntities( PMIReferenceGeometryEntities value ) {
        this.pmiReferenceGeometryEntities = value;
    }

    /** Getter for pmiDesignGroupEntities.
     *  
     * 
     * @return PMIDesignGroupEntities
     */
    public PMIDesignGroupEntities getPmiDesignGroupEntities() {
        return pmiDesignGroupEntities;
    }

     /** Setter for pmiDesignGroupEntities.
     *  
     * 
     * @param value PMIDesignGroupEntities
     */
    public void setPmiDesignGroupEntities( PMIDesignGroupEntities value ) {
        this.pmiDesignGroupEntities = value;
    }

    /** Getter for pmiCoordinateSystemEntities.
     *  
     * 
     * @return PMICoordinateSystemEntities
     */
    public PMICoordinateSystemEntities getPmiCoordinateSystemEntities() {
        return pmiCoordinateSystemEntities;
    }

     /** Setter for pmiCoordinateSystemEntities.
     *  
     * 
     * @param value PMICoordinateSystemEntities
     */
    public void setPmiCoordinateSystemEntities( PMICoordinateSystemEntities value ) {
        this.pmiCoordinateSystemEntities = value;
    }


    
}

