/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cadoculus.jove.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author cz
 */
@XmlRootElement( name = "Key2ValueObjectId" )
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType(
         name = "Key2ValueObjectId",
        propOrder = { "keyPropertyAtomObjectId", "valuePropertyAtomObjectId" }
)
public class Key2ValueObjectId {

    private int keyPropertyAtomObjectId;
    private int valuePropertyAtomObjectId;

    /**
     * @return the keyPropertyAtomObjectId
     */
    public int getKeyPropertyAtomObjectId() {
        return keyPropertyAtomObjectId;
    }

    /**
     * @param keyPropertyAtomObjectId the keyPropertyAtomObjectId to set
     */
    public void setKeyPropertyAtomObjectId( int keyPropertyAtomObjectId ) {
        this.keyPropertyAtomObjectId = keyPropertyAtomObjectId;
    }

    /**
     * @return the valuePropertyAtomObjectId
     */
    public int getValuePropertyAtomObjectId() {
        return valuePropertyAtomObjectId;
    }

    /**
     * @param valuePropertyAtomObjectId the valuePropertyAtomObjectId to set
     */
    public void setValuePropertyAtomObjectId( int valuePropertyAtomObjectId ) {
        this.valuePropertyAtomObjectId = valuePropertyAtomObjectId;
    }

}
