/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.base.ObjectID;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.UChar;
import java.lang.reflect.AnnotatedElement;
import java.nio.ByteBuffer;
import org.slf4j.LoggerFactory;

/**
 * Reads bytes annotated with UChar from the given ByteBuffer into a bytes.
 *
 * @author Zerbst
 */
public class ObjectIDCodec implements Codec<ObjectID> {

    /**
     *
     */
    public ObjectIDCodec( AnnotatedElement metadata, Class type ) {

        if ( ( metadata.getAnnotation( de.cadoculus.jove.bibi.annotation.ObjectID.class ) ) != null ) {
            // Ok, use ObjectID
        } else {
            throw new IllegalArgumentException( "expect ObjectID annotation on " + metadata );
        }
        if ( !( ObjectID.class == type ) ) {
            throw new IllegalArgumentException( "expect ObjectID as target type" );
        }
    }

    @Override
    public ObjectID decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {

        CodecUtility.checkMaxPosition( input, ctx );
        return new ObjectID( input.getInt() );

    }

    @Override
    public String toString() {
        return "ObjectIDCodec";
    }
}
