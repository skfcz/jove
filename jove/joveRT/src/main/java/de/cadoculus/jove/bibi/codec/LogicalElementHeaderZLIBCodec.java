/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.enums.ObjectBaseType;
import de.cadoculus.jove.enums.ObjectTypeIdentifier;
import de.cadoculus.jove.base.GUID;
import de.cadoculus.jove.base.LoadingStatus;
import de.cadoculus.jove.base.LogicalElementHeader;
import de.cadoculus.jove.base.LogicalElementHeaderZLIB;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.BIBICodec;
import de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.apache.log4j.MDC;
import org.slf4j.LoggerFactory;

/**
 * This Codec is used to decode any descendants of the LogicalElementHeaderZLIB.
 * It first reads the fields declared in LogicalElementHeader and then tries to
 * lookup the object class based on the information contained in
 * {@link LogicalElementHeader#objectTypeId}. In case that the class is not
 * already implemented, it tries to use a fallback based on {@link LogicalElementHeader#objectBaseType
 * }. If even this fails, it will directly read in
 * {@link LogicalElementHeaderZLIB}.
 * <p>
 * The target object is read using a normal {@link ObjectCodec}.
 *
 * <p>
 * To lookup the target class for the objectTypeId the
 * LogicalElementHeaderZLIBCodec creates a lookup table based on the
 * BIBIObjectTypeID annotation found for all clases in the
 * de.cadoculus.jove.lsgsegment package. The code to lookup classes for a given
 * package is extracted from
 * http://www.dzone.com/snippets/get-all-classes-within-package.
 *
 * @author cz
 */
public class LogicalElementHeaderZLIBCodec implements Codec<LogicalElementHeaderZLIB> {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( LogicalElementHeaderZLIBCodec.class );
    private static Map<GUID, Class> objectTypeId2Class = new HashMap<>();
    private Class type;
    private List<Object> alreadyParsedValues;

    /**
     * Construct an ObjectCodec for the given type.
     *
     * @param type
     */
    public LogicalElementHeaderZLIBCodec( Class type ) throws BIBIException {
        if ( type == null ) {
            throw new IllegalArgumentException( "expect none null type" );
        }
        this.type = type;
        this.alreadyParsedValues = new ArrayList<>();
    }

    /**
     * Construct an ObjectCodec for the given type.
     *
     * @param type
     */
    public LogicalElementHeaderZLIBCodec( Class type, List<Object> alreadyParsedValues ) throws BIBIException {
        if ( type == null ) {
            throw new IllegalArgumentException( "expect none null type" );
        }
        this.type = type;
        this.alreadyParsedValues = alreadyParsedValues;

    }

    @Override
    public LogicalElementHeaderZLIB decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        MDC.put( WorkingContext.CODEC, LogicalElementHeaderZLIB.class.getSimpleName() );
        CodecUtility.checkMaxPosition( input, ctx );

        final long startPosition = input.position();
        // First decode the first fields defined in LogicalElementHeader and Element Header
        Integer elementLength = input.getInt();

        ctx = new WorkingContext( ctx );
        final long maxPosition = startPosition + elementLength + 4; // 4 bytes for elementLength 
        ctx.setMaxPosition( maxPosition );

        GUID objectTypeIDg = ctx.getDefaultCodecFactory().create( null, GUID.class, ctx ).decode( input, ctx );
        ObjectTypeIdentifier objectTypeID = ObjectTypeIdentifier.valueOf( objectTypeIDg );
        char objectBaseTypeC = (char) input.get();
        ObjectBaseType objectBaseType = ObjectBaseType.valueOf( (int) objectBaseTypeC );

        Integer objectId = Integer.MIN_VALUE;
        if ( ctx.getFileHeader().getIntVersion() > 81 ) {
            objectId = input.getInt();
        }

        // Now check if there is a known class for the ObjectType
        Class objectClass = null;

        while ( true ) {
            objectClass = lookupClass( objectTypeIDg );
            if ( objectClass != null ) {
                log.info( "found " + objectClass.getSimpleName() + " for GUID " + objectTypeIDg );
                break;
            }
            try {
                ObjectTypeIdentifier oti = ObjectTypeIdentifier.valueOf( objectTypeIDg );
                log.info( "failed to find class for " + oti );
            } catch ( IllegalArgumentException exp ) {
                log.info( "failed to find class for " + objectTypeIDg );
            }

            objectClass = lookupBaseClass( objectBaseType );
            if ( objectClass != null ) {
                log.warn( "use " + objectClass + " as fallback for objectBaseType " + objectBaseType );
                break;
            }
            objectClass = LogicalElementHeaderZLIB.class;
            log.warn( "found not fallback for objectBaseType " + objectBaseType + ", read just the LogicalElementHeaderZLIB" );
            break;
        }

        log.info( "read into " + objectClass );

        List parsedValues = Arrays.asList( new Object[]{ elementLength, objectTypeID,
            objectBaseType, objectId } );

        // See if we have a dedicated parser for that type
        Codec codec = null;
        BIBICodec bibiCodec = (BIBICodec) objectClass.getAnnotation( BIBICodec.class );

        LogicalElementHeaderZLIB retval = null;

        if ( bibiCodec != null ) {
            Class codecClass = null;
            try {
                codecClass = bibiCodec.value();
                Constructor constructor = codecClass.getConstructor( Class.class, List.class );
                codec = (Codec) constructor.newInstance( objectClass, parsedValues );
            } catch ( Exception ex ) {
                log.error( "failed to create codec " + codecClass, ex );
                throw new IllegalStateException( "failed to create new instance of " + codecClass + " for " + objectClass.getSimpleName(), ex );
            }
        } else {
            codec = new ObjectCodec( objectClass, parsedValues );
        }

        try {
            retval = (LogicalElementHeaderZLIB) codec.decode( input, ctx );
            retval.setLoadingStatus( LoadingStatus.LOADING_FINISHED );
            retval.setElementStart( startPosition );
            retval.setElementLength( elementLength );
            log.info( "finished reading " + retval );
        } catch ( BIBIException exp ) {
            log.error( "an error occured reading the LogicalElementHeaderZLIB " + objectClass, exp );
            log.error( "   from " + startPosition + ", length " + elementLength );
            log.error( "   replace by dummy" );
            retval = new LogicalElementHeaderZLIB();
            retval.setLoadingStatus( LoadingStatus.LOADING_FAILED );
            retval.setElementStart( startPosition );
            retval.setElementLength( elementLength );
            retval.setException( exp );
        } 

        final long endPosition = input.position();
        log.info( "read from " + startPosition + " to " + endPosition );
        log.info( "    element length " + elementLength );

        // reposition  
        if ( input.position() != maxPosition ) {
            log.warn( "reposition from " + endPosition + " to " + maxPosition );
            input.position( (int) maxPosition );
        }

        return retval;
    }

    /**
     * Lookup the class for the given objectTypeID. The lookup is performed on
     * all classes found in the <code>de.cadoculus.jove.lsgsegment</code>
     * package using the
     * {@link de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID} annotation.
     *
     * @param objectTypeID the GUID used for the lookup
     * @return the found class or null
     */
    public static Class lookupClass( GUID objectTypeID ) {
        initializeGuid2Class();

        if ( objectTypeId2Class.containsKey( objectTypeID ) ) {
            return objectTypeId2Class.get( objectTypeID );
        }
        return null;
    }

    /**
     * Lookup the fallback class based on the given objectBaseType. The lookup
     * is performed using {@link de.cadoculus.jove.base.LogicalElementHeader#resolveObjectBaseType(char)
     * }.
     *
     * @param objectBaseType the char used for the lookup
     * @return
     */
    public static Class lookupBaseClass( ObjectBaseType objectBaseType ) {
        return LogicalElementHeader.resolveObjectBaseType( objectBaseType );
    }

    private static void initializeGuid2Class() {
        if ( !( objectTypeId2Class.isEmpty() ) ) {
            return;
        }
        try {
            List<Class> classes = new ArrayList<>();
            classes.addAll( getClasses( "de.cadoculus.jove.lsgsegment" ) );
            classes.addAll( getClasses( "de.cadoculus.jove.metadatasegment" ) );

            for ( Class clazz : classes ) {
                BIBIObjectTypeID oti = (BIBIObjectTypeID) clazz.getAnnotation( BIBIObjectTypeID.class );
                if ( oti == null ) {
                    continue;
                }
                GUID guid = new GUID( (java.lang.String) oti.value() );

                objectTypeId2Class.put( guid, clazz );
            }
            log.info( "initialized lookup\n" + objectTypeId2Class );

        } catch ( Exception exp ) {
            log.error( "failed to lookup classes from lsgsegment package" );
        }
    }

    /**
     * Scans all classes accessible from the context class loader which belong
     * to the given package and subpackages.
     *
     * @param packageName The base package
     * @return The classes
     * @throws ClassNotFoundException
     * @throws IOException
     */
    private static List<Class> getClasses( String packageName )
            throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace( '.', '/' );
        Enumeration<URL> resources = classLoader.getResources( path );
        List<File> dirs = new ArrayList<File>();
        while ( resources.hasMoreElements() ) {
            URL resource = resources.nextElement();
            dirs.add( new File( resource.getFile() ) );
        }
        ArrayList<Class> classes = new ArrayList<Class>();
        for ( File directory : dirs ) {
            classes.addAll( findClasses( directory, packageName ) );
        }
        return classes;
    }

    /**
     * Recursive method used to find all classes in a given directory and
     * subdirs.
     *
     * @param directory The base directory
     * @param packageName The package name for classes found inside the base
     * directory
     * @return The classes
     * @throws ClassNotFoundException
     */
    private static List<Class> findClasses( File directory, String packageName ) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<Class>();
        if ( !directory.exists() ) {
            return classes;
        }
        File[] files = directory.listFiles();
        for ( File file : files ) {
            if ( file.isDirectory() ) {
                assert !file.getName().contains( "." );
                classes.addAll( findClasses( file, packageName + "." + file.getName() ) );
            } else if ( file.getName().endsWith( ".class" ) ) {
                classes.add( Class.forName( packageName + '.' + file.getName().substring( 0, file.getName().length() - 6 ) ) );
            }
        }
        return classes;
    }
}
