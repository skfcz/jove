/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.1 Property Proxy Meta Data Element</h4>
Object Type ID: 0xce357247, 0x38fb, 0x11d1, 0xa5, 0x6, 0x0, 0x60, 0x97, 0xbd, 0xc6, 0xe1
A Property Proxy Meta Data Element serves as a "proxy" for all meta-data properties associated with a particular Meta Data Node Element (see 7.2.1.1.1.6 Meta Data Node Element). The proxy is in the form of a list of key/value property pairs where the key identifies the type and meaning of the value. Although the property key is always in the form of a String data type, the value can be one of several data types.

*/
@XmlRootElement(name="PropertyProxyMetaDataElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PropertyProxyMetaDataElement",
    propOrder= {"versionNumber", "properties", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0xce357247, 0x38fb, 0x11d1, 0xa5, 0x6, 0x00, 0x60, 0x97, 0xbd, 0xc6, 0xe1" )
@de.cadoculus.jove.bibi.annotation.BIBICodec( de.cadoculus.jove.bibi.codec.PropertyProxyMetaDataElementCodec.class )
public class PropertyProxyMetaDataElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( PropertyProxyMetaDataElement.class );

    /**  
     * The variable number 0 : I16 versionNumber
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumber;

    /**  
     * The variable number 1 : ProperyProxyMetaData[] properties
     * <br>options : <pre>{length : 0}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "0" )
    @XmlElementWrapper(name = "properties")
    @XmlElementRef()    private java.util.List<ProperyProxyMetaData> properties;



    /** Getter for versionNumber.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumber() {
        return versionNumber;
    }

     /** Setter for versionNumber.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumber( Short value ) {
        this.versionNumber = value;
    }

    /** Getter for properties.
     *  
     * 
     * @return java.util.List<ProperyProxyMetaData>
     */
    public java.util.List<ProperyProxyMetaData> getProperties() {
        return properties;
    }

     /** Setter for properties.
     *  
     * 
     * @param value java.util.List<ProperyProxyMetaData>
     */
    public void setProperties( java.util.List<ProperyProxyMetaData> value ) {
        this.properties = value;
    }


    
}

