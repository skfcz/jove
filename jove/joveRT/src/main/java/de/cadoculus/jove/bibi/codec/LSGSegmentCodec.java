/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.base.Element2Properties;
import de.cadoculus.jove.base.GUID;
import de.cadoculus.jove.base.Key2ValueObjectId;
import de.cadoculus.jove.base.LogicalElementHeaderZLIB;
import de.cadoculus.jove.base.PropertyTable;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.io.ZLIBCompression;
import de.cadoculus.jove.lsgsegment.LSGSegment;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import org.slf4j.LoggerFactory;

/**
 * This codec is used to handle the LSGSegments. The codec dumps the
 * decompressed binary data to the file "./lsg.bin" if configured with key
 * "dump.segment.LOGICAL_SCENE_GRAPH".
 *
 * @author Zerbst
 */
public class LSGSegmentCodec implements Codec<LSGSegment> {

    private static org.slf4j.Logger log = LoggerFactory.getLogger( LSGSegmentCodec.class );

    public LSGSegmentCodec( Class clazz ) throws BIBIException {
    }

    @Override
    public LSGSegment decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        log.info( "" );
        log.info( "start reading LSGSegment" );
        log.info( "" );

        LSGSegment retval = new LSGSegment();

        //
        // 1. Read the Graph elements until we find the End-Of-Elements marker
        //
        // The LSG Segement supports compression, therefore we have to read the 
        // first ElementHeaderZLIB
        int compressionFlag = input.getInt();
        log.info( "compressionFlag " + compressionFlag );
        int compressedDataLength = input.getInt() - 1;// Note that data field compression Algorithm is included in this count.
        log.info( "compressedDataLength " + compressedDataLength );
        byte compressionAlgorithm = input.get();
        log.info( "compressionAlgorithm " + compressionAlgorithm );

        ByteBuffer lsgInput = input;

        if ( compressionFlag == 2 ) {
            byte[] compressed = new byte[ compressedDataLength ];
            input.get( compressed );

            byte[] uncompressed = ZLIBCompression.decompressByZLIB( compressed );

            writeSegment( uncompressed, ctx );

            // Get the decompressed data
            int capa = uncompressed.length;
            ByteBuffer uncompressedData = ByteBuffer.allocate( capa );
            uncompressedData.put( uncompressed );
            uncompressedData.position( 0 );
            uncompressedData.order( input.order() );

            lsgInput = uncompressedData;
            log.info( "decompressed LSG Segement from " + compressedDataLength + " to " + capa + " bytes" );

            // Create a copy from the WorkingContext to contain the new ByteBuffer
            ctx = new WorkingContext( ctx );
            ctx.setByteBuffer( lsgInput );
            ctx.setMaxPosition( capa );
        } else {
            log.info( "found LSG Segement without compression" );
        }

        log.info( "start decoding at " + ctx.getByteBuffer().position() );
        Codec<LogicalElementHeaderZLIB> lehCodec = ctx.getDefaultCodecFactory().create( null, LogicalElementHeaderZLIB.class, ctx );
        LogicalElementHeaderZLIB leh = lehCodec.decode( lsgInput, ctx );
        leh.setCompressedDataLength( compressedDataLength );
        leh.setCompressionAlgorithm( compressionAlgorithm );
        leh.setCompressionFlag( compressionFlag );

        retval.getGraphElements().add( leh );

        // currently I expect at least two graph elements 
        log.info( "finished reading first LogicalElementHeaderZLIB @" + ctx.getByteBuffer().position() );
        log.info( "    continue with remaining graph elements" );
        boolean stopReading = false;

        boolean goOn = true;
        GRAPH_ELEMENTS:
        while ( goOn ) {

            try {
                CodecUtility.checkMaxPosition( lsgInput, ctx );
                lehCodec = ctx.getDefaultCodecFactory().create( null, LogicalElementHeaderZLIB.class, ctx );
                leh = lehCodec.decode( lsgInput, ctx );
                retval.getGraphElements().add( leh );
            } catch ( BIBIException exp ) {
                log.error( "an error occured reading the LogicalElementHeaderZLIB", exp );
                log.error( "skip reading remaining property atom elements and property table" );
                ctx.setException( exp );
                stopReading = true;
                break GRAPH_ELEMENTS;
            }

            log.info( "read graph element #" + retval.getGraphElements().size() );

            // Read next ObjectTypeID and check for end           
            int curpos = lsgInput.position();
            int elLength = lsgInput.getInt();
            Codec<GUID> guidCodec = ctx.getDefaultCodecFactory().create( null, GUID.class, ctx );
            GUID next = guidCodec.decode( lsgInput, ctx );
            log.info( "look ahead @" + curpos + " " + next );

            if ( GUID.END_OF_ELEMENTS.equals( next ) ) {
                log.info( "finished reading graph elements" );
                break GRAPH_ELEMENTS;
            }

            // scroll back into the buffer before reading the next LogicalElementHeaderZLIB
            lsgInput.position( curpos );
            log.info( "read next graph element starting at @" + ctx.getByteBuffer().position() );

        }
        if ( stopReading ) {
            return retval;
        }

        log.info( "" );
        log.info( "finished reading LSGSegment#graph" );
        log.info( "" );
        log.info( "start reading LSGSegment#propertyAtomElements" );
        log.info( "" );

        goOn = true;
        PROPERTY_ATOM_ELEMENTS:
        while ( goOn ) {

            try {
                CodecUtility.checkMaxPosition( lsgInput, ctx );
                lehCodec = ctx.getDefaultCodecFactory().create( null, LogicalElementHeaderZLIB.class, ctx );
                leh = lehCodec.decode( lsgInput, ctx );
                retval.getPropertyAtomElements().add( leh );
            } catch ( BIBIException exp ) {
                log.error( "an error occured reading the LogicalElementHeaderZLIB", exp );
                log.error( "skip reading remaining property table" );
                ctx.setException( exp );
                stopReading = true;
                break PROPERTY_ATOM_ELEMENTS;
            }

            // Read next ObjectTypeID and check for end
            int curpos = lsgInput.position();
            int elLength = lsgInput.getInt();
            Codec<GUID> guidCodec = ctx.getDefaultCodecFactory().create( null, GUID.class, ctx );
            GUID next = guidCodec.decode( lsgInput, ctx );
            log.info( "look ahead @" + curpos + " " + next );

            if ( GUID.END_OF_ELEMENTS.equals( next ) ) {
                log.info( "finished reading property atom elements" );
                break PROPERTY_ATOM_ELEMENTS;
            }

            // scroll back into the buffer before reading the next LogicalElementHeaderZLIB
            lsgInput.position( curpos );
            log.info( "read next property atom element starting at " + ctx.getByteBuffer().position() );

        }
        if ( stopReading ) {
            return retval;
        }

        log.info( "" );
        log.info( "finished reading LSGSegment#propertyAtomElements @" + ctx.getByteBuffer().position() );
        log.info( "" );

        short versionNumberPT = ctx.getByteBuffer().getShort();
        log.info( "versionNumberPT " + versionNumberPT );
        int elementPropertyTableCount = ctx.getByteBuffer().getInt();
        log.info( "elementPropertyTableCount " + elementPropertyTableCount );

        PropertyTable propertyTable = new PropertyTable();
        retval.setPropertyTable( propertyTable );
        propertyTable.setVersionNumber( versionNumberPT );
        propertyTable.setElementPropertyTableCount( elementPropertyTableCount );

        PROPERTY_TABLE:
        for ( int i = 0; i < elementPropertyTableCount; i++ ) {
            try {
                CodecUtility.checkMaxPosition( lsgInput, ctx );

                int elementObjectId = ctx.getByteBuffer().getInt();

                Element2Properties e2p = new Element2Properties();
                e2p.setElementObjectId( elementObjectId );
                propertyTable.getProperties().add( e2p );
                KV_LOOP:
                while ( true ) {
                    int keyPropertyAtomObjectId = ctx.getByteBuffer().getInt();
                    if ( keyPropertyAtomObjectId == 0 ) {
                        break KV_LOOP;
                    }
                    int valuePropertyAtomObjectId = ctx.getByteBuffer().getInt();
                    Key2ValueObjectId kv = new Key2ValueObjectId();
                    kv.setKeyPropertyAtomObjectId( keyPropertyAtomObjectId );
                    kv.setValuePropertyAtomObjectId( valuePropertyAtomObjectId );
                    e2p.getProperties().add( kv );
                }

            } catch ( BIBIException exp ) {
                log.error( "an error occured reading the LogicalElementHeaderZLIB", exp );
                log.error( "skip reading remaining property table" );
                ctx.setException( exp );
                stopReading = true;
                break PROPERTY_TABLE;
            }

        }

        return retval;
    }

    private void writeSegment( byte[] uncompressed, WorkingContext ctx ) {
        if ( ctx.getConf().getBooleanProperty( "dump.segment.LOGICAL_SCENE_GRAPH", false ) ) {
            try {
                try ( FileOutputStream fos = new FileOutputStream( "lsg.bin" ) ) {
                    fos.write( uncompressed );
                }
            } catch ( Exception exp ) {
                log.error( "failed to dump segment", exp );
            }
        }
    }
}
