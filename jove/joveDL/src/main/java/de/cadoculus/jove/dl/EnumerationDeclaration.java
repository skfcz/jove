/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Aüache License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl;

import java.util.ArrayList;
import java.util.List;

/**
 * The EnumerationDeclaration represents an enumeration used in Blocks or
 * Segments.
 *
 * @author Carsten Zerbst
 */
public class EnumerationDeclaration extends DataType {

    private DataType type;
    private String comment;
    private List<EnumValue> values = new ArrayList<>();

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment( String comment ) {
        this.comment = comment;
    }

    /**
     * @return the values
     */
    public List<EnumValue> getValues() {
        return values;
    }

    /**
     * @return the type
     */
    public DataType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType( DataType type ) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Enum " + getName() + " (" + getType() + ")";
    }

}
