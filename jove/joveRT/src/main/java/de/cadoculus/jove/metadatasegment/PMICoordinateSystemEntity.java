/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="PMICoordinateSystemEntity")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMICoordinateSystemEntity",
    propOrder= {"nameStringID", "origin", "xAxisPoint", "yAxisPoint", }
)
public class PMICoordinateSystemEntity {

    public static Logger log = LoggerFactory.getLogger( PMICoordinateSystemEntity.class );

    /**  
     * The variable number 0 : I32 nameStringID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer nameStringID;

    /**  
     * The variable number 1 : CoordF32 origin
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.CoordF32()    
    private javax.vecmath.Point3f origin;

    /**  
     * The variable number 2 : CoordF32 xAxisPoint
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.CoordF32()    
    private javax.vecmath.Point3f xAxisPoint;

    /**  
     * The variable number 3 : CoordF32 yAxisPoint
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.CoordF32()    
    private javax.vecmath.Point3f yAxisPoint;



    /** Getter for nameStringID.
     *  
     * 
     * @return Integer
     */
    public Integer getNameStringID() {
        return nameStringID;
    }

     /** Setter for nameStringID.
     *  
     * 
     * @param value Integer
     */
    public void setNameStringID( Integer value ) {
        this.nameStringID = value;
    }

    /** Getter for origin.
     *  
     * 
     * @return javax.vecmath.Point3f
     */
    public javax.vecmath.Point3f getOrigin() {
        return origin;
    }

     /** Setter for origin.
     *  
     * 
     * @param value javax.vecmath.Point3f
     */
    public void setOrigin( javax.vecmath.Point3f value ) {
        this.origin = value;
    }

    /** Getter for xAxisPoint.
     *  
     * 
     * @return javax.vecmath.Point3f
     */
    public javax.vecmath.Point3f getXAxisPoint() {
        return xAxisPoint;
    }

     /** Setter for xAxisPoint.
     *  
     * 
     * @param value javax.vecmath.Point3f
     */
    public void setXAxisPoint( javax.vecmath.Point3f value ) {
        this.xAxisPoint = value;
    }

    /** Getter for yAxisPoint.
     *  
     * 
     * @return javax.vecmath.Point3f
     */
    public javax.vecmath.Point3f getYAxisPoint() {
        return yAxisPoint;
    }

     /** Setter for yAxisPoint.
     *  
     * 
     * @param value javax.vecmath.Point3f
     */
    public void setYAxisPoint( javax.vecmath.Point3f value ) {
        this.yAxisPoint = value;
    }


    
}

