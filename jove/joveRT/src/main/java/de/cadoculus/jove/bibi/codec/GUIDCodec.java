/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Reads a GUID from the given ByteBuffer. It is based on the ObjectCodec but
 * caches the annotations and issues less log messages.
 *
 * @author Zerbst
 */
public class GUIDCodec extends ObjectCodec<de.cadoculus.jove.base.GUID> implements Codec<de.cadoculus.jove.base.GUID> {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( StringCodecFactory.class );
    
    private AnnotatedElement metadata;
    private static LinkedHashMap<Field, Codec> field2codec;

    public GUIDCodec( Class type ) throws BIBIException {
        super( type );
        if ( !( de.cadoculus.jove.base.GUID.class == type ) ) {
            throw new IllegalArgumentException( "expect GUID as target type" );
        }
    }

    /**
     *
     */
    public GUIDCodec( AnnotatedElement metadata, Class type ) throws BIBIException {
        super( de.cadoculus.jove.base.GUID.class );
        this.metadata = metadata;
    
        if ( ( metadata.getAnnotation( de.cadoculus.jove.bibi.annotation.GUID.class ) ) != null ) {
            // Ok, use UChar
        } else {
            throw new IllegalArgumentException( "expect GUID annotation on " + metadata );
        }
        if ( !( de.cadoculus.jove.base.GUID.class == type ) ) {
            throw new IllegalArgumentException( "expect GUID as target type" );
        }
    }

    @Override
    public de.cadoculus.jove.base.GUID decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        initialize( ctx );

        de.cadoculus.jove.base.GUID myObject = new de.cadoculus.jove.base.GUID();
        Field field = null;
        Object value = null;
        try {

            Iterator<Field> fieldIt = field2codec.keySet().iterator();

            FIELD:
            while ( fieldIt.hasNext() ) {

                field = fieldIt.next();
                MDC.put( WorkingContext.FIELD, field.getName() );
                MDC.put( WorkingContext.OBJECT_TYPE, type.getSimpleName() );
                MDC.put( WorkingContext.POSITION, Long.toString( input.position() ) );
                MDC.put( WorkingContext.CODEC, getClass().getSimpleName() );

                ctx.setObj( myObject );

                Codec codec = field2codec.get( field );

                value = codec.decode( input, ctx );
                ctx.setObj( myObject );


                field.setAccessible( true );
                field.set( myObject, value );

            }
        } catch ( IllegalArgumentException ex ) {
            log.error( "failed to set value of field " + type.getSimpleName() + "#" + field.getName() + " to " + value, ex );
            log.error( "field " + field.getType() );
            log.error( "value " + value.getClass() );
            throw new BIBIException( "failed to set value of field " + type.getSimpleName() + "#" + field.getName() + " to " + value, ex );
        } catch ( IllegalAccessException ex ) {
            throw new BIBIException( "denied to set value of field " + type.getSimpleName() + "#" + field.getName() + " to " + value, ex );
        }
        return myObject;

    }

    @Override
    public String toString() {
        return "GUIDCodec";
    }

    private final static void initialize( WorkingContext ctx ) throws BIBIException {
        if ( field2codec != null ) {
            return;

        }
        field2codec = collectBoundedFields( de.cadoculus.jove.base.GUID.class, ctx );
    }
}
