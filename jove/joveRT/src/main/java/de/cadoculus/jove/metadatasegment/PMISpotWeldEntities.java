/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.7 PMI Spot Weld Entities</h4>
The PMI Spot Weld Entities data collection defines data for a list of Spot Weld Symbols. Spot Weld symbols describe the specifications for welding sheet metal.
Several data fields of the PMI Spot Weld Entities data collection are only present if Version Number, as defined in 7.2.6.2PMI Manager Meta Data Element, is greater than or equal to "4".

*/
@XmlRootElement(name="PMISpotWeldEntities")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMISpotWeldEntities",
    propOrder= {"spotWeldCount", "spotWeldEntities", }
)
public class PMISpotWeldEntities {

    public static Logger log = LoggerFactory.getLogger( PMISpotWeldEntities.class );

    /**  
     * The variable number 0 : I32 spotWeldCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer spotWeldCount;

    /**  
     * The variable number 1 : PMISpotWeldEntity[] spotWeldEntities
     * <br>options : <pre>{length : obj.spotWeldCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.spotWeldCount" )
    @XmlElementWrapper(name = "spotWeldEntities")
    @XmlElementRef()    private java.util.List<PMISpotWeldEntity> spotWeldEntities;



    /** Getter for spotWeldCount.
     *  
     * 
     * @return Integer
     */
    public Integer getSpotWeldCount() {
        return spotWeldCount;
    }

     /** Setter for spotWeldCount.
     *  
     * 
     * @param value Integer
     */
    public void setSpotWeldCount( Integer value ) {
        this.spotWeldCount = value;
    }

    /** Getter for spotWeldEntities.
     *  
     * 
     * @return java.util.List<PMISpotWeldEntity>
     */
    public java.util.List<PMISpotWeldEntity> getSpotWeldEntities() {
        return spotWeldEntities;
    }

     /** Setter for spotWeldEntities.
     *  
     * 
     * @param value java.util.List<PMISpotWeldEntity>
     */
    public void setSpotWeldEntities( java.util.List<PMISpotWeldEntity> value ) {
        this.spotWeldEntities = value;
    }


    
}

