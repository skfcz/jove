/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
7.2.6.2.1.1.1.2 2D Text Data
The 2D Text Data collection defines a 2D text entity/primitive.

*/
@XmlRootElement(name="J2DTextData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "J2DTextData",
    propOrder= {"stringID", "font", "reservedField1", "reservedField2", "textBox", "textPolylineData", }
)
public class J2DTextData {

    public static Logger log = LoggerFactory.getLogger( J2DTextData.class );

    /**  
     * The variable number 0 : I32 stringID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer stringID;

    /**  
     * The variable number 1 : PMIFontType font
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private PMIFontType font;

    /**  
     * The variable number 2 : I32 reservedField1
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer reservedField1;

    /**  
     * The variable number 3 : F32 reservedField2
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.F32()    
    private Float reservedField2;

    /**  
     * The variable number 4 : TextBox textBox
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="TextBox")
    private TextBox textBox;

    /**  
     * The variable number 5 : TextPolylineData textPolylineData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="TextPolylineData")
    private TextPolylineData textPolylineData;



    /** Getter for stringID.
     *  
     * 
     * @return Integer
     */
    public Integer getStringID() {
        return stringID;
    }

     /** Setter for stringID.
     *  
     * 
     * @param value Integer
     */
    public void setStringID( Integer value ) {
        this.stringID = value;
    }

    /** Getter for font.
     *  
     * 
     * @return PMIFontType
     */
    public PMIFontType getFont() {
        return font;
    }

     /** Setter for font.
     *  
     * 
     * @param value PMIFontType
     */
    public void setFont( PMIFontType value ) {
        this.font = value;
    }

    /** Getter for reservedField1.
     *  
     * 
     * @return Integer
     */
    public Integer getReservedField1() {
        return reservedField1;
    }

     /** Setter for reservedField1.
     *  
     * 
     * @param value Integer
     */
    public void setReservedField1( Integer value ) {
        this.reservedField1 = value;
    }

    /** Getter for reservedField2.
     *  
     * 
     * @return Float
     */
    public Float getReservedField2() {
        return reservedField2;
    }

     /** Setter for reservedField2.
     *  
     * 
     * @param value Float
     */
    public void setReservedField2( Float value ) {
        this.reservedField2 = value;
    }

    /** Getter for textBox.
     *  
     * 
     * @return TextBox
     */
    public TextBox getTextBox() {
        return textBox;
    }

     /** Setter for textBox.
     *  
     * 
     * @param value TextBox
     */
    public void setTextBox( TextBox value ) {
        this.textBox = value;
    }

    /** Getter for textPolylineData.
     *  
     * 
     * @return TextPolylineData
     */
    public TextPolylineData getTextPolylineData() {
        return textPolylineData;
    }

     /** Setter for textPolylineData.
     *  
     * 
     * @param value TextPolylineData
     */
    public void setTextPolylineData( TextPolylineData value ) {
        this.textPolylineData = value;
    }


    
}

