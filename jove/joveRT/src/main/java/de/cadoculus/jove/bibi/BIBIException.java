/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi;

/**
 * This exception is used to encapsulate all kind off errors (IOException, ...)
 * which occure during the deserialization process.
 *
 * @author Zerbst
 */
public class BIBIException extends Exception {

    /**
     * Constructor for the BIBIException
     *
     * @param msg a human readable description
     * @param cause the underlying cause
     */
    public BIBIException( String msg, Exception cause ) {
        super( msg, cause );
    }

    /**
     * Constructor for the BIBIException
     *
     * @param msg a human readable description
     */
    public BIBIException( String msg ) {
        super( msg );
    }
}
