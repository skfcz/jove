/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove;

import de.cadoculus.jove.base.SegmentHeader;
import de.cadoculus.jove.fileheader.FileHeader;
import de.cadoculus.jove.lsgsegment.LSGSegment;
import de.cadoculus.jove.tocsegment.TOCSegment;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class represents a JT file.
 *
 * @author Zerbst
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Jove")
public class Jove {

    @XmlElement(name = "FileHeader", type = FileHeader.class)
    private FileHeader fileHeader;
    @XmlElement(name = "TOCSegment", type = TOCSegment.class)
    private TOCSegment tocSegment;
    @XmlElementWrapper(name = "segments")
    @XmlElementRef()
    private List<SegmentHeader> segments = new ArrayList<>();

    /**
     * Get the file header
     *
     * @return the fileHeader
     */
    public FileHeader getFileHeader() {
        return fileHeader;
    }

    /**
     * Set the FileHeader
     *
     * @param fileHeader the fileHeader to set
     */
    public void setFileHeader(FileHeader fileHeader) {
        this.fileHeader = fileHeader;
    }

    /**
     * Get the TOCSegment
     *
     * @return the tocSegment
     */
    public TOCSegment getTocSegment() {
        return tocSegment;
    }

    /**
     * Set the TOCSegment
     *
     * @param tocSegment the tocSegment to set
     */
    public void setTocSegment(TOCSegment tocSegment) {
        this.tocSegment = tocSegment;
    }

    /**
     * Get a list of SegmentHeader
     *
     * @return
     */
    public List<SegmentHeader> getSegments() {
        return segments;
    }

    /**
     * Get the Logical Scene Graph. This is a convenience function using the ID
     * of the LSG from the FileHeader and the ID from the SegmentHeader.
     *
     * @throws IllegalStateException if no LSGSegment could be found
     */
    public LSGSegment getLSGSegment() {
        if (fileHeader == null) {
            throw new IllegalStateException("called getLSGSegment before file was parsed, no FileHeader available");
        }
        if (segments == null || segments.isEmpty()) {
            throw new IllegalStateException("called getLSGSegment before file was parsed, no SegmentHeaders available");
        }
        for (Iterator<SegmentHeader> it = segments.iterator(); it.hasNext();) {
            SegmentHeader segmentHeader = it.next();
            if (segmentHeader.getSegmentID().equals(fileHeader.getLsgSegmentId())) {
                return (LSGSegment) segmentHeader.getObjectData();
            }

        }

        throw new IllegalStateException("could not find SegmentHeader with ID " + fileHeader.getLsgSegmentId());
    }

}
