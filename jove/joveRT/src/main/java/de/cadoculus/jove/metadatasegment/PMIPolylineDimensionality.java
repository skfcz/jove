/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;

import de.cadoculus.jove.base.*;

/** 
Polyline Dimensionality
Polyline Dimensionality specifies the dimensionality of the polyline coordinates packed in Polyline Vertex Coords. Valid values include the following:

 * 
 * This enumeration contains 2 values of type I16
*/

public enum PMIPolylineDimensionality {
   
    TWODIMENSIONALDATAPACKAGING( (short) 2 ),
    THREEDIMENSIONALDATAPACKAGING( (short) 3 );
    
    
    private final Short enumValue;
    private PMIPolylineDimensionality( Short v ) {
        this.enumValue = v;
    }

    /**
     * Get the integer value contained in the enumeration
     */
    public short getValue() {
        return enumValue;
    }
    /**
     * Get a PMIPolylineDimensionality for the given enumeration value
     * @param v the enumeration value
     * @return the found value
     * @throws IllegalArgumentException  if no PMIPolylineDimensionality was found for the given value
     */
    public static PMIPolylineDimensionality valueOf( Short v ) throws IllegalArgumentException {
        for ( PMIPolylineDimensionality test : PMIPolylineDimensionality.values() ) {
            if ( test.getValue() == v ) {
                return test;
            }
        }
        throw new IllegalArgumentException( "could not find PMIPolylineDimensionality for value '" + v + "'" );

    }       
    
}

