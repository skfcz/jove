/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
7.2.6.2.1.2 PMI Note Entities
The PMI Note Entities data collection defines data for a list of Notes. Notes are used to connect textual information to specific Part entities.

*/
@XmlRootElement(name="PMINoteEntities")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMINoteEntities",
    propOrder= {"noteCount", "notes", }
)
public class PMINoteEntities {

    public static Logger log = LoggerFactory.getLogger( PMINoteEntities.class );

    /**  
     * The variable number 0 : I32 noteCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer noteCount;

    /**  
     * The variable number 1 : PMINoteEntity[] notes
     * <br>options : <pre>{length : obj.noteCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.noteCount" )
    @XmlElementWrapper(name = "notes")
    @XmlElementRef()    private java.util.List<PMINoteEntity> notes;



    /** Getter for noteCount.
     *  
     * 
     * @return Integer
     */
    public Integer getNoteCount() {
        return noteCount;
    }

     /** Setter for noteCount.
     *  
     * 
     * @param value Integer
     */
    public void setNoteCount( Integer value ) {
        this.noteCount = value;
    }

    /** Getter for notes.
     *  
     * 
     * @return java.util.List<PMINoteEntity>
     */
    public java.util.List<PMINoteEntity> getNotes() {
        return notes;
    }

     /** Setter for notes.
     *  
     * 
     * @param value java.util.List<PMINoteEntity>
     */
    public void setNotes( java.util.List<PMINoteEntity> value ) {
        this.notes = value;
    }


    
}

