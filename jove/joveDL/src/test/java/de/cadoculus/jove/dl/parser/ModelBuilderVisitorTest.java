/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cadoculus.jove.dl.parser;

import de.cadoculus.jove.dl.BlockDeclaration;
import de.cadoculus.jove.dl.DataType;
import de.cadoculus.jove.dl.EntityDeclaration;
import de.cadoculus.jove.dl.EnumValue;
import de.cadoculus.jove.dl.EnumerationDeclaration;
import de.cadoculus.jove.dl.SegmentDeclaration;
import de.cadoculus.jove.dl.SimpleDataType;
import de.cadoculus.jove.dl.VariableDeclaration;
import de.cadoculus.jove.model.GUID;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Carsten Zerbst
 */
public class ModelBuilderVisitorTest {

    public static Logger log = LoggerFactory.getLogger( ModelBuilderVisitorTest.class );
    private ModelBuilderVisitor mvis;

    @BeforeClass
    public static void setUpClass() throws Exception {
        BasicConfigurator.resetConfiguration();

        java.io.File logconfig = new java.io.File( "log4j.properties" );

        if ( logconfig.exists() && logconfig.canRead() ) {

            try {
                URL propsURL = new URL( "file:" + logconfig.getName() );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {

                BasicConfigurator.resetConfiguration();
                BasicConfigurator.configure();

            }
        }
    }

    @Before
    public void setUp() throws Exception {
        SimpleNode node;
        try ( InputStream in = getClass().getResourceAsStream( "/jovedl/enums.jdl" ) ) {
            JoveDLParser parser = new JoveDLParser( in );
            node = parser.JoveDL();
        }

        TypeCollectorVisitor vis = new TypeCollectorVisitor();
        node.jjtAccept( vis, null );

        mvis = new ModelBuilderVisitor( vis.getAllDefinedTypes(), vis.getAllDefinedEnums(), vis.getEnumerationNames() );
        node.jjtAccept( mvis, null );

    }

    //@Test
    public void enums() throws Exception {
        log.info( "enums" );

        Collection<EnumerationDeclaration> enumerations = mvis.getEnumerations();

        assertNotNull( "enumerations", enumerations );
        assertEquals( "enumerations #", 3, enumerations.size() );

        for ( Iterator<EnumerationDeclaration> it = enumerations.iterator(); it.hasNext(); ) {
            EnumerationDeclaration enumerationDeclaration = it.next();

            if ( "SegmentType".equals( enumerationDeclaration.getName() ) ) {
                assertNotNull( "enum comment", enumerationDeclaration.getComment() );
                log.info( "SegmentType type " + enumerationDeclaration.getType() );
                assertEquals( "SegmentType type",
                        SimpleDataType.getSimpleType( SimpleDataType.SDTType.I32 ),
                        enumerationDeclaration.getType() );

                assertNotNull( "enum values", enumerationDeclaration.getValues() );
                assertEquals( "enum values #", 19, enumerationDeclaration.getValues().size() );
            } else if ( "BaseType".equals( enumerationDeclaration.getName() ) ) {

            } else if ( "ObjectTypeIdentifier".equals( enumerationDeclaration.getName() ) ) {
                log.info( "ObjectTypeIdentifier type " + enumerationDeclaration.getType() );
                assertEquals( "ObjectTypeIdentifier type",
                        SimpleDataType.getSimpleType( SimpleDataType.SDTType.GUID ),
                        enumerationDeclaration.getType() );
            } else {
                fail( "got unknonw EnumerationDeclaration " + enumerationDeclaration.getName() );
                assertNotNull( "enum values", enumerationDeclaration.getValues() );
                assertEquals( "enum values #", 2, enumerationDeclaration.getValues().size() );
                List<EnumValue> values = enumerationDeclaration.getValues();
                EnumValue constant = values.get( 0 );
                assertEquals( "End_of_Elements", constant.getName() );
                assertEquals( "End_of_Elements type", SimpleDataType.getSimpleType( SimpleDataType.SDTType.GUID ),
                        constant.getType() );
                assertEquals( "End_of_Elements value", new GUID( "0xffffffff, 0xffff, 0xffff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff" ), constant.getValue() );

                constant = values.get( 1 );
                assertEquals( "Base_Node_Element", constant.getName() );
                assertEquals( "Base_Node_Element type", SimpleDataType.getSimpleType( SimpleDataType.SDTType.GUID ),
                        constant.getType() );
                assertEquals( "Base_Node_Element value", new GUID( "0x10dd1035, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb,0x59, 0x97" ), constant.getValue() );

            }

        }

        Set<SegmentDeclaration> segments = mvis.getSegments();
        assertEquals( "one segment", 1, segments.size() );

        SegmentDeclaration seg = segments.iterator().next();
        assertEquals( "segment name", "MetaDataSegment", seg.getName() );
        Set<EnumerationDeclaration> enumerations1 = seg.getEnumerations();
        assertEquals( "MetaDataSegment enumerations #", 1, enumerations1.size() );
        EnumerationDeclaration enumerationDeclaration = enumerations1.iterator().next();
        assertEquals( "MetaDataSegment enum name", "PMIFontType", enumerationDeclaration.getName() );
        assertEquals( "PMIFontType type", SimpleDataType.getSimpleType( SimpleDataType.SDTType.I32 ), enumerationDeclaration.getType() );
    }

    @Test
    public void compression() throws Exception {
        SimpleNode node;
        try ( InputStream in = getClass().getResourceAsStream( "/jovedl/compression.jdl" ) ) {
            JoveDLParser parser = new JoveDLParser( in );
            node = parser.JoveDL();
        }

        TypeCollectorVisitor vis = new TypeCollectorVisitor();
        node.jjtAccept( vis, null );

        mvis = new ModelBuilderVisitor( vis.getAllDefinedTypes(), vis.getAllDefinedEnums(), vis.getEnumerationNames() );
        node.jjtAccept( mvis, null );

        SegmentDeclaration seg = mvis.getSegments().iterator().next();
        EntityDeclaration entity = seg.getEntities().iterator().next();
        assertEquals( "entity", "CompressedCADTagData", entity.getName() );

        VariableDeclaration var = entity.getVariables().get( 0 );
        assertEquals( "var name", "cadTag", var.getName() );
        assertEquals( "var type", SimpleDataType.getSimpleType( SimpleDataType.SDTType.VecI32.toString() ), var.getType() );
        assertEquals( "var compression", "Int32CDP2", var.getCompression() );
        assertEquals( "var predictor", "Lag1", var.getPredictor() );

        var = entity.getVariables().get( 1 );
        assertEquals( "var name", "cadTag2", var.getName() );
        assertEquals( "var type", SimpleDataType.getSimpleType( SimpleDataType.SDTType.VecU32.toString() ), var.getType() );
        assertEquals( "var compression", "Int32CDP", var.getCompression() );
        assertEquals( "var predictor", "Lag2", var.getPredictor() );

        var = entity.getVariables().get( 2 );
        assertEquals( "var name", "cadTag3", var.getName() );
        assertEquals( "var type", SimpleDataType.getSimpleType( SimpleDataType.SDTType.VecF32.toString() ), var.getType() );
        assertEquals( "var compression", "Float64CDP", var.getCompression() );
        assertEquals( "var predictor", "Stride1", var.getPredictor() );
    }

    //@Test
    public void blocks() throws Exception {
        log.info( "blocks" );

        Set<BlockDeclaration> blocks = mvis.getBlocks();
        assertNotNull( "blocks", blocks );
        assertEquals( "blocks #", 2, blocks.size() );
    }

    //@Test
    public void fileHeader() throws Exception {
        BlockDeclaration fileHeader = findBlockDeclaration( "FileHeader" );
        assertNotNull( "FileHeader", fileHeader );

        assertNotNull( "comment", fileHeader.getComment() );

        assertNotNull( "entities ", fileHeader.getEntities() );
        assertEquals( "entities #", 1, fileHeader.getEntities().size() );

    }

    //@Test
    public void fileHeaderEntity() throws Exception {

        SimpleNode node;
        try ( InputStream in = getClass().getResourceAsStream( "/jovedl/simple.jdl" ) ) {
            JoveDLParser parser = new JoveDLParser( in );
            node = parser.JoveDL();
        }

        TypeCollectorVisitor vis = new TypeCollectorVisitor();
        node.jjtAccept( vis, null );

        mvis = new ModelBuilderVisitor( vis.getAllDefinedTypes(), vis.getAllDefinedEnums(), vis.getEnumerationNames() );
        node.jjtAccept( mvis, null );

        BlockDeclaration fileHeaderBlock = findBlockDeclaration( "FileHeader" );
        EntityDeclaration fileHeader = fileHeaderBlock.getEntities().get( 0 );

        assertEquals( "name", "FileHeader", fileHeader.getName() );
        assertNotNull( "comment", fileHeader.getComment() );
        assertNotNull( "vars", fileHeader.getVariables() );
        assertEquals( "vars #", 4, fileHeader.getVariables().size() );

        //  { codec: FileHeaderCodec.class, string: "some string", boolVal : true, intVal : 12 , floatVal: 3.14 };
        assertNotNull( "properties", fileHeader.getProperties() );
        assertTrue( "found properties.codec", fileHeader.getProperties().containsKey( "codec" ) );
        assertEquals( "properties.codec value", "FileHeaderCodec.class", fileHeader.getProperties().get( "codec" ) );
        assertTrue( "found properties.string", fileHeader.getProperties().containsKey( "string" ) );
        assertEquals( "properties.string value", "some string", fileHeader.getProperties().get( "string" ) );
        assertTrue( "found properties.boolVal", fileHeader.getProperties().containsKey( "boolVal" ) );
        assertEquals( "properties.boolVal value", true, fileHeader.getProperties().get( "boolVal" ) );
        assertTrue( "found properties.intVal", fileHeader.getProperties().containsKey( "intVal" ) );
        assertEquals( "properties.intVal value", 12, fileHeader.getProperties().get( "intVal" ) );
        assertTrue( "found properties.floatVal", fileHeader.getProperties().containsKey( "floatVal" ) );
        assertEquals( "properties.floatVal value", 3.14, fileHeader.getProperties().get( "floatVal" ) );

    }

    //@Test
    public void fileHeaderEntityVersion() throws Exception {

        BlockDeclaration fileHeaderBlock = findBlockDeclaration( "FileHeader" );
        EntityDeclaration fileHeader = fileHeaderBlock.getEntities().get( 0 );

        VariableDeclaration var = fileHeader.getVariables().get( 0 );
        assertEquals( "version", var.getName() );
        assertEquals( "versions type", SimpleDataType.getSimpleType( SimpleDataType.SDTType.UChar.toString() ), var.getType() );
        assertTrue( "version array", var.isArray() );
        assertNotNull( "version properties", var.getProperties() );
        assertEquals( "version properties#", 1, var.getProperties().size() );
        assertTrue( "length", var.getProperties().containsKey( "length" ) );
        log.info( "length type " + var.getProperties().get( "length" ).getClass() );
        assertEquals( "length", "80", var.getProperties().get( "length" ) );
    }

    //@Test
    public void fileHeaderVars() throws Exception {

        BlockDeclaration fileHeaderBlock = findBlockDeclaration( "FileHeader" );
        EntityDeclaration fileHeader = fileHeaderBlock.getEntities().get( 0 );

        VariableDeclaration var = fileHeader.getVariables().get( 0 );

        assertEquals( "version", var.getName() );
        var = fileHeader.getVariables().get( 1 );
        assertEquals( "byteOrder", var.getName() );
        var = fileHeader.getVariables().get( 2 );
        assertEquals( "reservedFieldI", var.getName() );
        var = fileHeader.getVariables().get( 3 );
        assertEquals( "tocOffset", var.getName() );
        var = fileHeader.getVariables().get( 4 );
        assertEquals( "lsgSegmentId", var.getName() );
        var = fileHeader.getVariables().get( 5 );
        assertEquals( "reservedFieldG", var.getName() );

    }

    //@Test
    public void tocSegmentI() throws Exception {
        BlockDeclaration tocSegment = findBlockDeclaration( "TOCSegment" );
        // Ensure that both entities are parsed
        assertEquals( "entities #", 2, tocSegment.getEntities().size() );

        DataType tocEntry = tocSegment.getEntities().get( 0 ).getVariables().get( 1 ).getType();

        assertEquals( "forward declaration works", tocEntry, tocSegment.getEntities().get( 1 ) );
    }

    //@Test
    public void objectId() throws Exception {
        SegmentDeclaration lsg = mvis.getSegments().iterator().next();
        EntityDeclaration baseNodeElement = lsg.getEntities().get( 0 );
        assertNotNull( "objectId", baseNodeElement.getGuid() );
        assertEquals( "objectId", new GUID( "0x10dd1035, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97" ),
                baseNodeElement.getGuid() );
    }

    //@Test
    public void externInheritance() throws Exception {
        SegmentDeclaration lsg = mvis.getSegments().iterator().next();
        EntityDeclaration baseNodeElement = lsg.getEntities().get( 0 );
        assertNotNull( "extends not null", baseNodeElement.getParentType() );
        assertEquals( "extends", "LogicalElementHeaderZLIB", baseNodeElement.getParentType().getName() );
    }

    ////@Test
    public void commentHandling1() throws Exception {
        String comment
                = "/**\n" + //
                " * EntityDeclaration represents the declaration of a single \n" + //
                " * Entity in a Block or Segment. The AST Node contains the Entity name as\n" + //
                " * value.\n" + //
                " */";
        String commentE
                = "EntityDeclaration represents the declaration of a single\n" + //
                "Entity in a Block or Segment. The AST Node contains the Entity name as\n" + //
                "value.\n";

        String comment2 = ModelBuilderVisitor.handleComment( comment );
        log.info( "comment 2 " + comment2 );
        assertEquals( "comment", commentE, comment2 );
    }

    //@Test
    public void commentHandling2() throws Exception {
        String comment = "/** EntityDeclaration represents the declaration of a single */";
        String commentE = "EntityDeclaration represents the declaration of a single";

        String comment2 = ModelBuilderVisitor.handleComment( comment );
        log.info( "comment 2 " + comment2 );
        assertEquals( "comment", commentE, comment2 );
    }

    private BlockDeclaration findBlockDeclaration( String name ) {
        for ( Iterator<BlockDeclaration> it = mvis.getBlocks().iterator(); it.hasNext(); ) {
            BlockDeclaration blockDeclaration = it.next();
            if ( name.equals( blockDeclaration.getName() ) ) {
                return blockDeclaration;
            }
        }
        return null;
    }
}
