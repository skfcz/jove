/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
7.2.6.1.1 Date Property Value
Date Property Value represents the property value when Property Value Type = = 4. Date Property Value data collection represents a date as a combination of year, month, day, hour, minute, and second data fields.

*/
@XmlRootElement(name="DatePropertyValue")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "DatePropertyValue",
    propOrder= {"year", "month", "day", "hour", "minute", "second", }
)
public class DatePropertyValue {

    public static Logger log = LoggerFactory.getLogger( DatePropertyValue.class );

    /**  
     * The variable number 0 : I16 year
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short year;

    /**  
     * The variable number 1 : I16 month
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short month;

    /**  
     * The variable number 2 : I16 day
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short day;

    /**  
     * The variable number 3 : I16 hour
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short hour;

    /**  
     * The variable number 4 : I16 minute
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short minute;

    /**  
     * The variable number 5 : I16 second
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short second;



    /** Getter for year.
     *  
     * 
     * @return Short
     */
    public Short getYear() {
        return year;
    }

     /** Setter for year.
     *  
     * 
     * @param value Short
     */
    public void setYear( Short value ) {
        this.year = value;
    }

    /** Getter for month.
     *  
     * 
     * @return Short
     */
    public Short getMonth() {
        return month;
    }

     /** Setter for month.
     *  
     * 
     * @param value Short
     */
    public void setMonth( Short value ) {
        this.month = value;
    }

    /** Getter for day.
     *  
     * 
     * @return Short
     */
    public Short getDay() {
        return day;
    }

     /** Setter for day.
     *  
     * 
     * @param value Short
     */
    public void setDay( Short value ) {
        this.day = value;
    }

    /** Getter for hour.
     *  
     * 
     * @return Short
     */
    public Short getHour() {
        return hour;
    }

     /** Setter for hour.
     *  
     * 
     * @param value Short
     */
    public void setHour( Short value ) {
        this.hour = value;
    }

    /** Getter for minute.
     *  
     * 
     * @return Short
     */
    public Short getMinute() {
        return minute;
    }

     /** Setter for minute.
     *  
     * 
     * @param value Short
     */
    public void setMinute( Short value ) {
        this.minute = value;
    }

    /** Getter for second.
     *  
     * 
     * @return Short
     */
    public Short getSecond() {
        return second;
    }

     /** Setter for second.
     *  
     * 
     * @param value Short
     */
    public void setSecond( Short value ) {
        this.second = value;
    }


    
}

