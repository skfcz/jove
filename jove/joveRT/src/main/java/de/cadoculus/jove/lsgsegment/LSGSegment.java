/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.lsgsegment;

import de.cadoculus.jove.base.ObjectData;
import de.cadoculus.jove.base.LogicalElementHeaderZLIB;
import de.cadoculus.jove.base.PropertyTable;
import de.cadoculus.jove.fileheader.FileHeader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The LSG Segment contains the declaration of the Logical Scene Graph (the
 * product structure).
 *
 * The toplevel nodes GUID is given in the {@link FileHeader#lsgSegmentId}.
 *
 */
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( name = "LSGSegment" )
@XmlRootElement( name = "LSGSegment" )
@   de.cadoculus.jove.bibi.annotation.BIBICodec( de.cadoculus.jove.bibi.codec.LSGSegmentCodec.class )
public class LSGSegment extends ObjectData {

    @XmlElementWrapper( name = "graphElements" )
    @XmlElements( {
        @XmlElement( name = "BaseAttributeElement", type = BaseAttributeElement.class ),
        @XmlElement( name = "BaseNodeElement", type = BaseNodeElement.class ),
        @XmlElement( name = "BasePropertyAtomElement", type = BasePropertyAtomElement.class ),
        @XmlElement( name = "BaseShapeNodeElement", type = BaseShapeNodeElement.class ),
        @XmlElement( name = "GroupNodeElement", type = GroupNodeElement.class ),
        @XmlElement( name = "InstanceNodeElement", type = InstanceNodeElement.class ),
        @XmlElement( name = "JTObjectReferencePropertyAtomElement", type = JTObjectReferencePropertyAtomElement.class ),
        @XmlElement( name = "LODNodeElement", type = LODNodeElement.class ),
        @XmlElement( name = "LateLoadedPropertyAtomElement", type = LateLoadedPropertyAtomElement.class ),
        @XmlElement( name = "MetaDataNodeElement", type = MetaDataNodeElement.class ),
        @XmlElement( name = "PartNodeElement", type = PartNodeElement.class ),
        @XmlElement( name = "PartitionNodeElement", type = PartitionNodeElement.class ),
        @XmlElement( name = "RangeLODNodeElement", type = RangeLODNodeElement.class ),
        @XmlElement( name = "SwitchNodeElement", type = SwitchNodeElement.class ),
        @XmlElement( name = "VertexShapeNodeElement", type = VertexShapeNodeElement.class )
    } )
    private List<LogicalElementHeaderZLIB> graphElements = new ArrayList<>();

    @XmlElementWrapper( name = "propertyAtomElements" )
    @XmlElements( {
        @XmlElement( name = "BaseAttributeElement", type = BaseAttributeElement.class ),
        @XmlElement( name = "BaseNodeElement", type = BaseNodeElement.class ),
        @XmlElement( name = "BasePropertyAtomElement", type = BasePropertyAtomElement.class ),
        @XmlElement( name = "BaseShapeNodeElement", type = BaseShapeNodeElement.class ),
        @XmlElement( name = "GroupNodeElement", type = GroupNodeElement.class ),
        @XmlElement( name = "InstanceNodeElement", type = InstanceNodeElement.class ),
        @XmlElement( name = "JTObjectReferencePropertyAtomElement", type = JTObjectReferencePropertyAtomElement.class ),
        @XmlElement( name = "LODNodeElement", type = LODNodeElement.class ),
        @XmlElement( name = "LateLoadedPropertyAtomElement", type = LateLoadedPropertyAtomElement.class ),
        @XmlElement( name = "MetaDataNodeElement", type = MetaDataNodeElement.class ),
        @XmlElement( name = "PartNodeElement", type = PartNodeElement.class ),
        @XmlElement( name = "PartitionNodeElement", type = PartitionNodeElement.class ),
        @XmlElement( name = "RangeLODNodeElement", type = RangeLODNodeElement.class ),
        @XmlElement( name = "StringPropertyAtomElement", type = StringPropertyAtomElement.class ),
        @XmlElement( name = "SwitchNodeElement", type = SwitchNodeElement.class ),
        @XmlElement( name = "VertexShapeNodeElement", type = VertexShapeNodeElement.class )
    } )
    private List<LogicalElementHeaderZLIB> propertyAtomElements = new ArrayList<>();

    private PropertyTable propertyTable;

    /**
     * @return the graphElements
     */
    public List<LogicalElementHeaderZLIB> getGraphElements() {
        return graphElements;
    }

    /**
     * @return the propertyAtomElements
     */
    public List<LogicalElementHeaderZLIB> getPropertyAtomElements() {
        return propertyAtomElements;
    }

    public void setPropertyTable( PropertyTable propertyTable ) {
        this.propertyTable = propertyTable;
    }

    public PropertyTable getPropertyTable() {
        return propertyTable;
    }
}
