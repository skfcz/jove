/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.BIBILength;
import de.cadoculus.jove.bibi.annotation.MbString;
import de.cadoculus.jove.bibi.annotation.UChar;
import java.lang.reflect.AnnotatedElement;
import org.slf4j.LoggerFactory;

/**
 * <p>This factory creates codecs for bytes. Supported annotation / Java
 * types:</p>
 *
 * <ul> <li>Uchar / Byte</li></ul>
 *
 * @author Zerbst
 */
public class ByteCodecFactory extends AbstractCodecFactory {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( ByteCodecFactory.class );

    public ByteCodecFactory() {
    }

    @Override
    public <T> Codec<T> create( AnnotatedElement metadata, Class<T> type, WorkingContext ctx ) throws BIBIException {


        if ( !( Byte.class == type ) ) {
            return null;
        }
        if ( metadata == null ) {
            return null;
        }

        UChar ucharA = metadata.getAnnotation( UChar.class );
        BIBILength lengthA = metadata.getAnnotation( BIBILength.class );

        Codec<T> retval = null;


        if ( ucharA != null && Byte.class == type ) {
            retval = (Codec<T>) new ByteCodec( metadata, Byte.class );
        }

        return retval;

    }
}
