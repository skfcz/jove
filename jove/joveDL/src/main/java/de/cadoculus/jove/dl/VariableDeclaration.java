/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Aüache License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents the declaration of one variable in an entity.
 *
 * @author Carsten Zerbst
 */
public class VariableDeclaration {

    private String comment;
    private String name;
    private DataType type;
    private boolean array;
    private String compression;
    private String predictor;
    private Map<String, Object> properties = new HashMap<>();

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName( String name ) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public DataType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType( DataType type ) {
        this.type = type;
    }

    /**
     * @return the array
     */
    public boolean isArray() {
        return array;
    }

    /**
     * @param array the array to set
     */
    public void setArray( boolean array ) {
        this.array = array;
    }

    /**
     * @return the properties
     */
    public Map<String, Object> getProperties() {
        return properties;
    }

    @Override
    public String toString() {
        return "VariableDeclaration{" + "name=" + name + ", type=" + type.getName() + ", array=" + array + ", properties=" + properties + '}';
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment( String comment ) {
        this.comment = comment;
    }

    /**
     *
     * @return the compression
     */

    public String getCompression() {
        return compression;
    }

    /**
     * @param compression the compression to set
     */
    public void setCompression( String compression ) {
        this.compression = compression;
    }

    /**
     * @return the predictor
     */
    public String getPredictor() {
        return predictor;
    }

    /**
     * @param predictor the predictor to set
     */
    public void setPredictor( String predictor ) {
        this.predictor = predictor;
    }
}
