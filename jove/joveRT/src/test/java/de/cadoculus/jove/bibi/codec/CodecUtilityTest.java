/*
 * Copyright (C) 2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.BIBILength;
import de.cadoculus.jove.bibi.annotation.BIBIValid;
import de.cadoculus.jove.fileheader.FileHeader;
import de.cadoculus.jove.metadatasegment.CompressedCADTagData;
import de.cadoculus.jove.tocsegment.TOCSegment;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.*;

/**
 *
 * @author Zerbst
 */
public class CodecUtilityTest {

    public static Logger log = LoggerFactory.getLogger( CodecUtilityTest.class );

    @BeforeClass
    public static void setUpClass() throws Exception {
        BasicConfigurator.resetConfiguration();

        java.io.File logconfig = new java.io.File( "log4j.properties" );

        if ( logconfig.exists() && logconfig.canRead() ) {

            try {
                URL propsURL = new URL( "file:" + logconfig.getName() );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {

                BasicConfigurator.resetConfiguration();
                BasicConfigurator.configure();

            }
        }
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testEvaluateInt() throws Exception {

        WorkingContext ctx = new WorkingContext( new Properties() );

        assertEquals( "FileHeader.version length", 80, CodecUtility.evaluate( FileHeader.class.getDeclaredField( "version" ).getAnnotation( BIBILength.class ), ctx ) );

    }

    @Test
    public void testEvaluateFileHeader() throws Exception {
        WorkingContext ctx = new WorkingContext( new Properties() );

        FileHeader fh = new FileHeader();
        fh.setVersion( "Version 9.0  JT " );
        fh.setReservedFieldI( 0 );
        ctx.setFileHeader( fh );
        ctx.setObj( fh );

//
//        ScriptEngineManager smng = new ScriptEngineManager();
//        ScriptEngine engine = smng.getEngineByName( "JavaScript" );
//        Bindings bindings = engine.getBindings( ScriptContext.ENGINE_SCOPE);
//        bindings.putAll( ctx.setupMap() );
//        bindings.put( "currentTime", new java.util.Date());
//
//        for ( Map.Entry<String, Object> entry : bindings.entrySet() ) {
//            log.info( "entry " + entry.getKey() + " = " + entry.getValue() );
//        }
//
//        String script = "1+2;";
//        log.info( script + " = " + engine.eval( script ) );
//
//        script = "println( currentTime)";
//        log.info( script + " = " + engine.eval( script ) );
        assertEquals( "FileHeader.reserved field", false, CodecUtility.evaluate( FileHeader.class.getDeclaredField( "reservedFieldG" ).getAnnotation( BIBIValid.class ), ctx ) );

    }

    @Test
    public void testEvaluateTocSegment() throws Exception {
        WorkingContext ctx = new WorkingContext( new Properties() );

        FileHeader fh = new FileHeader();
        fh.setVersion( "Version 9.0  JT " );
        fh.setReservedFieldI( 0 );
        ctx.setFileHeader( fh );

        TOCSegment tocSec = new TOCSegment();
        tocSec.setEntryCount( 2 );
        ctx.setObj( tocSec );

        log.info( "vars " + ctx.setupMap() );

        assertEquals( "TOCSegment.entryCount", 2, CodecUtility.evaluate( TOCSegment.class.getDeclaredField( "tocEntries" ).getAnnotation( BIBILength.class ), ctx ) );

    }

    @Test
    public void testValidity1() throws Exception {

        WorkingContext ctx = new WorkingContext( new Properties() );

        CompressedCADTagData cctd = new CompressedCADTagData();
        List<Integer> cadTag = new ArrayList<>();
        cadTag.add( 1 );
        cadTag.add( 1 );
        cctd.setCadTagTypes(cadTag );

        FileHeader fh = new FileHeader();
        fh.setVersion( "Version 9.5  JT " );
        fh.setReservedFieldI( 0 );
        ctx.setFileHeader( fh );

        ctx.setObj( cctd );

        BIBIValid v = new BIBIValid() {

            @Override
            public String value() {
                return "obj.cadTagTypes.contains(1)";
            }

            @Override
            public Class<? extends Annotation> annotationType() {
                return BIBIValid.class;
            }
        };

        log.info( "vars " + ctx.setupMap() );

        assertTrue( v.value(), CodecUtility.evaluate( v, ctx ) );

        cadTag.clear();
        cadTag.add( 2 );
        assertFalse( v.value(), CodecUtility.evaluate( v, ctx ) );
        
    }
}
