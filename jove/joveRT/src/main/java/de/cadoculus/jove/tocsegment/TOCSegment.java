/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.tocsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="TOCSegment")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "TOCSegment",
    propOrder= {"entryCount", "tocEntries", }
)
public class TOCSegment {

    public static Logger log = LoggerFactory.getLogger( TOCSegment.class );

    /**  
     * The variable number 0 : I32 entryCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer entryCount;

    /**  
     * The variable number 1 : TOCEntry[] tocEntries
     * <br>options : <pre>{length : obj.entryCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.entryCount" )
    @XmlElementWrapper(name = "tocEntries")
    @XmlElementRef()    private java.util.List<TOCEntry> tocEntries;



    /** Getter for entryCount.
     *  
     * 
     * @return Integer
     */
    public Integer getEntryCount() {
        return entryCount;
    }

     /** Setter for entryCount.
     *  
     * 
     * @param value Integer
     */
    public void setEntryCount( Integer value ) {
        this.entryCount = value;
    }

    /** Getter for tocEntries.
     *  
     * 
     * @return java.util.List<TOCEntry>
     */
    public java.util.List<TOCEntry> getTocEntries() {
        return tocEntries;
    }

     /** Setter for tocEntries.
     *  
     * 
     * @param value java.util.List<TOCEntry>
     */
    public void setTocEntries( java.util.List<TOCEntry> value ) {
        this.tocEntries = value;
    }


    
}

