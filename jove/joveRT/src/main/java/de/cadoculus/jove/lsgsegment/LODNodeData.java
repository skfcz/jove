/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="LODNodeData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "LODNodeData",
    propOrder= {"versionNumberLND", "reservedFieldsLND", "reservedFieldLND", }
)
public class LODNodeData extends GroupNodeData {

    public static Logger log = LoggerFactory.getLogger( LODNodeData.class );

    /**  
     * The variable number 0 : I16 versionNumberLND
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumberLND;

    /**  
     * The variable number 1 : VecF32 reservedFieldsLND
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.VecF32()    
    private java.util.List<Float> reservedFieldsLND;

    /**  
     * The variable number 2 : I32 reservedFieldLND
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer reservedFieldLND;



    /** Getter for versionNumberLND.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberLND() {
        return versionNumberLND;
    }

     /** Setter for versionNumberLND.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberLND( Short value ) {
        this.versionNumberLND = value;
    }

    /** Getter for reservedFieldsLND.
     *  
     * 
     * @return java.util.List<Float>
     */
    public java.util.List<Float> getReservedFieldsLND() {
        return reservedFieldsLND;
    }

     /** Setter for reservedFieldsLND.
     *  
     * 
     * @param value java.util.List<Float>
     */
    public void setReservedFieldsLND( java.util.List<Float> value ) {
        this.reservedFieldsLND = value;
    }

    /** Getter for reservedFieldLND.
     *  
     * 
     * @return Integer
     */
    public Integer getReservedFieldLND() {
        return reservedFieldLND;
    }

     /** Setter for reservedFieldLND.
     *  
     * 
     * @param value Integer
     */
    public void setReservedFieldLND( Integer value ) {
        this.reservedFieldLND = value;
    }


    
}

