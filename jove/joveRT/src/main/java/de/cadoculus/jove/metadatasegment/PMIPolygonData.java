/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.8 PMI Polygon Data</h4>
The PMI Polygon Data collection contains a list of vertices classified as polygonal primitives. Its composition is shown in the figure 177. Each block of PMI PolygonData contains a list of 0 or more PolygonData elements. Empty PolygonData elements are written with 0 vertices and no additional fields.

*/
@XmlRootElement(name="PMIPolygonData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIPolygonData",
    propOrder= {"versionNumber", "reservedField", "vNumVerts", "polygons", }
)
@de.cadoculus.jove.bibi.annotation.BIBICodec( de.cadoculus.jove.bibi.codec.PMIPolygonDataCodec.class )
public class PMIPolygonData {

    public static Logger log = LoggerFactory.getLogger( PMIPolygonData.class );

    /**  
     * The variable number 0 : I32 versionNumber
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer versionNumber;

    /**  
     * The variable number 1 : I32 reservedField
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer reservedField;

    /**  
     * The variable number 2 : VecI32 vNumVerts
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.VecI32()    
    private java.util.List<Integer> vNumVerts;

    /**  
     * The variable number 3 : PolygonData[] polygons
     * <br>options : <pre>{length : 0}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "0" )
    @XmlElementWrapper(name = "polygons")
    @XmlElementRef()    private java.util.List<PolygonData> polygons;



    /** Getter for versionNumber.
     *  
     * 
     * @return Integer
     */
    public Integer getVersionNumber() {
        return versionNumber;
    }

     /** Setter for versionNumber.
     *  
     * 
     * @param value Integer
     */
    public void setVersionNumber( Integer value ) {
        this.versionNumber = value;
    }

    /** Getter for reservedField.
     *  
     * 
     * @return Integer
     */
    public Integer getReservedField() {
        return reservedField;
    }

     /** Setter for reservedField.
     *  
     * 
     * @param value Integer
     */
    public void setReservedField( Integer value ) {
        this.reservedField = value;
    }

    /** Getter for vNumVerts.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getVNumVerts() {
        return vNumVerts;
    }

     /** Setter for vNumVerts.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setVNumVerts( java.util.List<Integer> value ) {
        this.vNumVerts = value;
    }

    /** Getter for polygons.
     *  
     * 
     * @return java.util.List<PolygonData>
     */
    public java.util.List<PolygonData> getPolygons() {
        return polygons;
    }

     /** Setter for polygons.
     *  
     * 
     * @param value java.util.List<PolygonData>
     */
    public void setPolygons( java.util.List<PolygonData> value ) {
        this.polygons = value;
    }


    
}

