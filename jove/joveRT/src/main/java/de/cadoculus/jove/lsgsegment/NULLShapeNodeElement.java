/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="NULLShapeNodeElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "NULLShapeNodeElement",
    propOrder= {"baseShapeData", "versionNumberNSN", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0xd239e7b6, 0xdd77, 0x4289, 0xa0, 0x7d, 0xb0, 0xee, 0x79, 0xf7, 0x94, 0x94" )
public class NULLShapeNodeElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( NULLShapeNodeElement.class );

    /**  
     * The variable number 0 : BaseShapeData baseShapeData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="BaseShapeData")
    private BaseShapeData baseShapeData;

    /**  
     * The variable number 1 : I16 versionNumberNSN
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumberNSN;



    /** Getter for baseShapeData.
     *  
     * 
     * @return BaseShapeData
     */
    public BaseShapeData getBaseShapeData() {
        return baseShapeData;
    }

     /** Setter for baseShapeData.
     *  
     * 
     * @param value BaseShapeData
     */
    public void setBaseShapeData( BaseShapeData value ) {
        this.baseShapeData = value;
    }

    /** Getter for versionNumberNSN.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberNSN() {
        return versionNumberNSN;
    }

     /** Setter for versionNumberNSN.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberNSN( Short value ) {
        this.versionNumberNSN = value;
    }


    
}

