/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.5 PMI Model Views</h4>
The PMI Model Views data collection defines data for a list of Model Views. A fully annotated part/assembly may contain so much PMI information, that it becomes very difficult to interpret the design intent when viewing a 3D Model (with PMI visible) of the part/assembly. Model Views provide a means to capture and organize PMI information about a 3D model so that the design intent can be clearly interpreted and communicated to others in later stages of the Product Lifecycle Management (PLM) process (e.g. manufacturing, inspection, assembly). This organization is achieved via PMI Associations (see 7.2.6.2.2 PMI Associations), where specific PMI entities are associated as "destinations" to a "source" PMI Model View.

*/
@XmlRootElement(name="PMIModelViews")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIModelViews",
    propOrder= {"modelViewCount", "modelViews", }
)
public class PMIModelViews {

    public static Logger log = LoggerFactory.getLogger( PMIModelViews.class );

    /**  
     * The variable number 0 : I32 modelViewCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer modelViewCount;

    /**  
     * The variable number 1 : PMIModelView[] modelViews
     * <br>options : <pre>{length : obj.modelViewCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.modelViewCount" )
    @XmlElementWrapper(name = "modelViews")
    @XmlElementRef()    private java.util.List<PMIModelView> modelViews;



    /** Getter for modelViewCount.
     *  
     * 
     * @return Integer
     */
    public Integer getModelViewCount() {
        return modelViewCount;
    }

     /** Setter for modelViewCount.
     *  
     * 
     * @param value Integer
     */
    public void setModelViewCount( Integer value ) {
        this.modelViewCount = value;
    }

    /** Getter for modelViews.
     *  
     * 
     * @return java.util.List<PMIModelView>
     */
    public java.util.List<PMIModelView> getModelViews() {
        return modelViews;
    }

     /** Setter for modelViews.
     *  
     * 
     * @param value java.util.List<PMIModelView>
     */
    public void setModelViews( java.util.List<PMIModelView> value ) {
        this.modelViews = value;
    }


    
}

