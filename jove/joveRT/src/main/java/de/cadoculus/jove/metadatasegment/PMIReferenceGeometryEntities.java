/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.11 PMI Reference Geometry Entities</h4>
The PMI Reference Geometry Entities data collection defines data for a list of Reference Geometry. Reference Geometry can be thought of as user-definable datums, which are positioned relative to the topology of an existing entity. Each reference geometry type (point, polyline, polygon) can be implicitly determined by the value of Polyline Segment Index[1] (see 7.2.6.2.1.7.1 PMI 3D Data) as follows:

*/
@XmlRootElement(name="PMIReferenceGeometryEntities")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIReferenceGeometryEntities",
    propOrder= {"referenceGeometryCount", "pmi3dData", }
)
public class PMIReferenceGeometryEntities {

    public static Logger log = LoggerFactory.getLogger( PMIReferenceGeometryEntities.class );

    /**  
     * The variable number 0 : I32 referenceGeometryCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer referenceGeometryCount;

    /**  
     * The variable number 1 : PMI3DData[] pmi3dData
     * <br>options : <pre>{length : obj.referenceGeometryCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.referenceGeometryCount" )
    @XmlElementWrapper(name = "pmi3dData")
    @XmlElementRef()    private java.util.List<PMI3DData> pmi3dData;



    /** Getter for referenceGeometryCount.
     *  
     * 
     * @return Integer
     */
    public Integer getReferenceGeometryCount() {
        return referenceGeometryCount;
    }

     /** Setter for referenceGeometryCount.
     *  
     * 
     * @param value Integer
     */
    public void setReferenceGeometryCount( Integer value ) {
        this.referenceGeometryCount = value;
    }

    /** Getter for pmi3dData.
     *  
     * 
     * @return java.util.List<PMI3DData>
     */
    public java.util.List<PMI3DData> getPmi3dData() {
        return pmi3dData;
    }

     /** Setter for pmi3dData.
     *  
     * 
     * @param value java.util.List<PMI3DData>
     */
    public void setPmi3dData( java.util.List<PMI3DData> value ) {
        this.pmi3dData = value;
    }


    
}

