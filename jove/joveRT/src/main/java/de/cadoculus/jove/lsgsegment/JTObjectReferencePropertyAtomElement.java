/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.1.2.5 JT Object Reference Property Atom Element</h4>
JT Object Reference Property Atom Element represents a property atom whose value is an object ID for another object within the JT file.

*/
@XmlRootElement(name="JTObjectReferencePropertyAtomElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "JTObjectReferencePropertyAtomElement",
    propOrder= {"versionNumber", "referencedObjectID", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0x10dd1004, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97" )
public class JTObjectReferencePropertyAtomElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( JTObjectReferencePropertyAtomElement.class );

    /** 
     * var BasePropertyAtomData basePropertyAtomData;
/** Version Number is the version identifier for this data collection. Version number "0x0001" is currently the only valid value.<p> 
     * The variable number 0 : I16 versionNumber
     * <br>options : <pre>{valid : jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private Short versionNumber;

    /** 
     * Object ID specifies the identifier within the JT file for the referenced object.<p> 
     * The variable number 1 : ObjectID referencedObjectID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.ObjectID()    
    private ObjectID referencedObjectID;



    /** Getter for versionNumber.
     * var BasePropertyAtomData basePropertyAtomData;
/** Version Number is the version identifier for this data collection. Version number "0x0001" is currently the only valid value.<p> 
     * 
     * @return Short
     */
    public Short getVersionNumber() {
        return versionNumber;
    }

     /** Setter for versionNumber.
     * var BasePropertyAtomData basePropertyAtomData;
/** Version Number is the version identifier for this data collection. Version number "0x0001" is currently the only valid value.<p> 
     * 
     * @param value Short
     */
    public void setVersionNumber( Short value ) {
        this.versionNumber = value;
    }

    /** Getter for referencedObjectID.
     * Object ID specifies the identifier within the JT file for the referenced object.<p> 
     * 
     * @return ObjectID
     */
    public ObjectID getReferencedObjectID() {
        return referencedObjectID;
    }

     /** Setter for referencedObjectID.
     * Object ID specifies the identifier within the JT file for the referenced object.<p> 
     * 
     * @param value ObjectID
     */
    public void setReferencedObjectID( ObjectID value ) {
        this.referencedObjectID = value;
    }


    
}

