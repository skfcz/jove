/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.6.1 PMI Property</h4>
A PMI Property data collection consists of a key/value pair and is used to describe attributes of Generic PMI Entity or other specific data.

*/
@XmlRootElement(name="PMIProperty")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIProperty",
    propOrder= {"keyPMIPropertyAtom", "valuePMIPropertyAtom", }
)
public class PMIProperty {

    public static Logger log = LoggerFactory.getLogger( PMIProperty.class );

    /**  
     * The variable number 0 : PMIPropertyAtom keyPMIPropertyAtom
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIPropertyAtom")
    private PMIPropertyAtom keyPMIPropertyAtom;

    /**  
     * The variable number 1 : PMIPropertyAtom valuePMIPropertyAtom
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIPropertyAtom")
    private PMIPropertyAtom valuePMIPropertyAtom;



    /** Getter for keyPMIPropertyAtom.
     *  
     * 
     * @return PMIPropertyAtom
     */
    public PMIPropertyAtom getKeyPMIPropertyAtom() {
        return keyPMIPropertyAtom;
    }

     /** Setter for keyPMIPropertyAtom.
     *  
     * 
     * @param value PMIPropertyAtom
     */
    public void setKeyPMIPropertyAtom( PMIPropertyAtom value ) {
        this.keyPMIPropertyAtom = value;
    }

    /** Getter for valuePMIPropertyAtom.
     *  
     * 
     * @return PMIPropertyAtom
     */
    public PMIPropertyAtom getValuePMIPropertyAtom() {
        return valuePMIPropertyAtom;
    }

     /** Setter for valuePMIPropertyAtom.
     *  
     * 
     * @param value PMIPropertyAtom
     */
    public void setValuePMIPropertyAtom( PMIPropertyAtom value ) {
        this.valuePMIPropertyAtom = value;
    }


    
}

