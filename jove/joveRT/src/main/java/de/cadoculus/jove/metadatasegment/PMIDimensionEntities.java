/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.1 PMI Dimension Entities</h4>
The PMI Dimension Entities data collection defines data for a list of Dimensions.

*/
@XmlRootElement(name="PMIDimensionEntities")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIDimensionEntities",
    propOrder= {"dimensionCount", "pmi2dData", }
)
public class PMIDimensionEntities {

    public static Logger log = LoggerFactory.getLogger( PMIDimensionEntities.class );

    /**  
     * The variable number 0 : I32 dimensionCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer dimensionCount;

    /**  
     * The variable number 1 : PMI2DData[] pmi2dData
     * <br>options : <pre>{length : obj.dimensionCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.dimensionCount" )
    @XmlElementWrapper(name = "pmi2dData")
    @XmlElementRef()    private java.util.List<PMI2DData> pmi2dData;



    /** Getter for dimensionCount.
     *  
     * 
     * @return Integer
     */
    public Integer getDimensionCount() {
        return dimensionCount;
    }

     /** Setter for dimensionCount.
     *  
     * 
     * @param value Integer
     */
    public void setDimensionCount( Integer value ) {
        this.dimensionCount = value;
    }

    /** Getter for pmi2dData.
     *  
     * 
     * @return java.util.List<PMI2DData>
     */
    public java.util.List<PMI2DData> getPmi2dData() {
        return pmi2dData;
    }

     /** Setter for pmi2dData.
     *  
     * 
     * @param value java.util.List<PMI2DData>
     */
    public void setPmi2dData( java.util.List<PMI2DData> value ) {
        this.pmi2dData = value;
    }


    
}

