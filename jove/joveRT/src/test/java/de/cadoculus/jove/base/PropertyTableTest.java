/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.base;

import de.cadoculus.jove.Jove;
import de.cadoculus.jove.fileheader.FileHeader;
import de.cadoculus.jove.lsgsegment.LSGSegment;
import de.cadoculus.jove.tocsegment.TOCSegment;
import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cz
 */
public class PropertyTableTest {

    public static Logger log = LoggerFactory.getLogger( PropertyTableTest.class );
    private static File testDir;

    @BeforeClass
    public static void setUpClass() {
        BasicConfigurator.resetConfiguration();

        java.io.File logconfig = new java.io.File( "log4j.properties" );

        if ( logconfig.exists() && logconfig.canRead() ) {

            try {
                URL propsURL = new URL( "file:" + logconfig.getName() );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {

                BasicConfigurator.resetConfiguration();
                BasicConfigurator.configure();

            }
        }

        testDir = new File( "target/test-out/JoveReaderTest" );
//        Files.delete(testDir.toPath() );
        testDir.mkdirs();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addProperty method, of class PropertyTable.
     */
    @Test
    public void testSerialize() throws Exception {
        System.out.println( "testSerialize" );

        PropertyTable instance = new PropertyTable();

        Element2Properties e2p = new Element2Properties();
        instance.getProperties().add( e2p );
        e2p.setElementObjectId( 1 );
        Key2ValueObjectId key2ValueObjectId = new Key2ValueObjectId();
        key2ValueObjectId.setKeyPropertyAtomObjectId( 2 );
        key2ValueObjectId.setValuePropertyAtomObjectId( 3 );
        e2p.getProperties().add( key2ValueObjectId );

        key2ValueObjectId = new Key2ValueObjectId();
        key2ValueObjectId.setKeyPropertyAtomObjectId( 4 );
        key2ValueObjectId.setValuePropertyAtomObjectId( 5 );
        e2p.getProperties().add( key2ValueObjectId );

        e2p = new Element2Properties();
        instance.getProperties().add( e2p );
        e2p.setElementObjectId( 6 );
        key2ValueObjectId = new Key2ValueObjectId();
        key2ValueObjectId.setKeyPropertyAtomObjectId( 7 );
        key2ValueObjectId.setValuePropertyAtomObjectId( 8 );
        e2p.getProperties().add( key2ValueObjectId );

        File myDir = new File( testDir, "testSerialize" );
        myDir.mkdir();
        File xml = new File( myDir, "propertyTable.xml" );
        FileOutputStream fos = new FileOutputStream( xml );
        JAXBContext context = JAXBContext.newInstance( PropertyTable.class.getPackage().getName() + ":" + //
                FileHeader.class.getPackage().getName() + ":" + //
                TOCSegment.class.getPackage().getName() + ":" + //
                LSGSegment.class.getPackage().getName() + ":" + //
                BBoxF32.class.getPackage().getName() );
        Marshaller m = context.createMarshaller();
        m.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
        m.marshal( instance, fos );
        fos.close();
    }

}
