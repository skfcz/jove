/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.1.1.2.2 Text Polyline Data</h4>
The Text Polyline Data collection defines any polyline segments which are part of the text representation. This existence of this polyline data is conditional (i.e. not all text has it) and is made up of an array of indices into an array of polyline segments packed as 2D vertex coordinates, specifying where each polyline segment begins and ends.

*/
@XmlRootElement(name="TextPolylineData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "TextPolylineData",
    propOrder= {"polylineSegmentIndexCount", "polylineSegmentIndex", "polylineVertexCoords", }
)
public class TextPolylineData {

    public static Logger log = LoggerFactory.getLogger( TextPolylineData.class );

    /**  
     * The variable number 0 : I32 polylineSegmentIndexCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer polylineSegmentIndexCount;

    /**  
     * The variable number 1 : I16[] polylineSegmentIndex
     * <br>options : <pre>{valid : obj.polylineSegmentIndexCount > 0,length : obj.polylineSegmentIndexCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.polylineSegmentIndexCount" )
    @XmlElementWrapper(name = "polylineSegmentIndex")
        @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.polylineSegmentIndexCount > 0" )
    private java.util.List<Short> polylineSegmentIndex;

    /**  
     * The variable number 2 : VecF32 polylineVertexCoords
     * <br>options : <pre>{valid : obj.polylineSegmentIndexCount > 0}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.VecF32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.polylineSegmentIndexCount > 0" )
    private java.util.List<Float> polylineVertexCoords;



    /** Getter for polylineSegmentIndexCount.
     *  
     * 
     * @return Integer
     */
    public Integer getPolylineSegmentIndexCount() {
        return polylineSegmentIndexCount;
    }

     /** Setter for polylineSegmentIndexCount.
     *  
     * 
     * @param value Integer
     */
    public void setPolylineSegmentIndexCount( Integer value ) {
        this.polylineSegmentIndexCount = value;
    }

    /** Getter for polylineSegmentIndex.
     *  
     * 
     * @return java.util.List<Short>
     */
    public java.util.List<Short> getPolylineSegmentIndex() {
        return polylineSegmentIndex;
    }

     /** Setter for polylineSegmentIndex.
     *  
     * 
     * @param value java.util.List<Short>
     */
    public void setPolylineSegmentIndex( java.util.List<Short> value ) {
        this.polylineSegmentIndex = value;
    }

    /** Getter for polylineVertexCoords.
     *  
     * 
     * @return java.util.List<Float>
     */
    public java.util.List<Float> getPolylineVertexCoords() {
        return polylineVertexCoords;
    }

     /** Setter for polylineVertexCoords.
     *  
     * 
     * @param value java.util.List<Float>
     */
    public void setPolylineVertexCoords( java.util.List<Float> value ) {
        this.polylineVertexCoords = value;
    }


    
}

