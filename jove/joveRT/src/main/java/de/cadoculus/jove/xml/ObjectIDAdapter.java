/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.xml;


import de.cadoculus.jove.base.ObjectID;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * @author Zerbst
 */
public class ObjectIDAdapter extends XmlAdapter< String, ObjectID> {

    @Override
    public String marshal( ObjectID v ) throws Exception {

        return v.toString();
    }

    @Override
    public ObjectID unmarshal( String v ) throws Exception {
        return new ObjectID( v );
    }
}
