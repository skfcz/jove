/*
 * Copyright (C) 2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.metadatasegment.PMIPolygonData;
import java.util.List;

/**
 *
 * @author cz
 */
public class PMIPolygonDataCodec extends ObjectCodec<PMIPolygonData> {

    public PMIPolygonDataCodec( Class<PMIPolygonData> type ) throws BIBIException {
        super( type );
    }

    public PMIPolygonDataCodec( Class<PMIPolygonData> type, List<Object> alreadyParsedValues ) throws BIBIException {
        super( type );
        this.alreadyParsedValues.addAll( alreadyParsedValues );
    }

}
