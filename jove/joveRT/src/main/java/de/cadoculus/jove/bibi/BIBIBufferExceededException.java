/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi;

/**
 * This exception is used if a the byte buffer of the complete file, individual
 * blocks or segment is or will be exceeded.
 *
 * @author Zerbst
 */
public class BIBIBufferExceededException extends BIBIException {

    private long currentPos;
    private int bytesToRead;

    /**
     * Constructor for the BIBIException
     *
     * @param msg a human readable description
     * @param cause the underlying cause
     */
    public BIBIBufferExceededException( String msg, long currentPos, int bytesToRead ) {
        super( msg );
        this.currentPos = currentPos;
        this.bytesToRead = bytesToRead;
    }

    /**
     * @return the currentPos
     */
    public long getCurrentPos() {
        return currentPos;
    }

    /**
     * @return the bytesToRead
     */
    public int getBytesToRead() {
        return bytesToRead;
    }

    
}
