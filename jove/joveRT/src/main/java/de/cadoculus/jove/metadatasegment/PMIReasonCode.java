/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;

import de.cadoculus.jove.base.*;

/** 
Reason Code
Reason Code specifies the "reason" for the association. Valid Reason Codes include the following:

 * 
 * This enumeration contains 17 values of type I32
*/

public enum PMIReasonCode {
   
    PRIMARY_ENTITY( 0 ),
    SECONDARY_ENTITY( 1 ),
    DIMESION_PLANE( 2 ),
    Z_AXIS_ENTITY( 5 ),
    ASSOCIATED_TO_PMI_SYMBOL( 10 ),
    ATTACHE_PMI_SYMBOL( 11 ),
    FIRST_ENTITY_TO_ATTACH_PMI_SYMBOL( 12 ),
    SECOND_ENTITY_TO_ATTACH_PMI_SYMBOL( 12 ),
    PMI_GROUPING( 14 ),
    WELD_LINE_ENTITY( 15 ),
    HOT_SPOT( 16 ),
    CHILD_IN_PMI_STACK( 17 ),
    MISCELLANEOUS_RELATION( 72 ),
    PMI_RELATED_ENTITY( 73 ),
    SHOW_MODEL_VIEW( 98 ),
    SHOW_PMI_B( 99 ),
    SHOW_ALL_PARTS_EXPECT_ASSOCIATED( 100 );

    
    private final Integer enumValue;
    private PMIReasonCode( Integer v ) {
        this.enumValue = v;
    }

    /**
     * Get the integer value contained in the enumeration
     */
    public int getValue() {
        return enumValue;
    }
    /**
     * Get a PMIReasonCode for the given enumeration value
     * @param v the enumeration value
     * @return the found value
     * @throws IllegalArgumentException  if no PMIReasonCode was found for the given value
     */
    public static PMIReasonCode valueOf( Integer v ) throws IllegalArgumentException {
        for ( PMIReasonCode test : PMIReasonCode.values() ) {
            if ( test.getValue() == v ) {
                return test;
            }
        }
        throw new IllegalArgumentException( "could not find PMIReasonCode for value '" + v + "'" );

    }       
    
}

