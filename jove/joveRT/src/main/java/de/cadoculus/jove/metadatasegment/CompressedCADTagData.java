/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>8.1.16 Compressed CAD Tag Data</h4>
The Compressed CAD Tag Data collection contains the persistent IDs, as defined in the CAD System, to uniquely identify
individual CAD entities (e.g. Faces and Edges of a JT B-Rep, PMI, etc.). Exactly what CAD entity types have CAD Tags
and what order they are stored in Compressed CAD Tag Data is defined by users of this data collection (e.g. 7.2.3.1.6 B-Rep
CAD Tag Data, 7.2.6.2.7 PMI CAD Tag Data)
What constitutes a CAD Tag is outside the scope of the JT File format and is indeed part of the CAD system. The JT File
format simply provides a way to store any kind of CAD Tag as provided by the CAD system which produced the CAD
entity.

*/
@XmlRootElement(name="CompressedCADTagData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "CompressedCADTagData",
    propOrder= {"versionNumber1", "dataLength", "versionNumber2", "cadTagCount", "cadTagTypes", "cadTagsType1", "compressedCADTagType2Data", }
)
public class CompressedCADTagData {

    public static Logger log = LoggerFactory.getLogger( CompressedCADTagData.class );

    /**  
     * The variable number 0 : I16 versionNumber1
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumber1;

    /**  
     * The variable number 1 : I32 dataLength
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer dataLength;

    /**  
     * The variable number 2 : I32 versionNumber2
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer versionNumber2;

    /**  
     * The variable number 3 : I32 cadTagCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer cadTagCount;

    /**  
     * The variable number 4 : VecI32 cadTagTypes
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.VecI32()    
    @de.cadoculus.jove.bibi.annotation.BIBICompression(compression=CompressionType.Int32CDP2, predictor=PredictorType.Lag1 )
    private java.util.List<Integer> cadTagTypes;

    /**  
     * The variable number 5 : VecI32 cadTagsType1
     * <br>options : <pre>{valid : obj.cadTagTypes.contains(1)}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.VecI32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.cadTagTypes.contains(1)" )
    @de.cadoculus.jove.bibi.annotation.BIBICompression(compression=CompressionType.Int32CDP2, predictor=PredictorType.Lag1 )
    private java.util.List<Integer> cadTagsType1;

    /**  
     * The variable number 6 : CompressedCADTagType2Data compressedCADTagType2Data
     * <br>options : <pre>{valid : obj.cadTagTypes.contains(2)}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="CompressedCADTagType2Data")
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "obj.cadTagTypes.contains(2)" )
    private CompressedCADTagType2Data compressedCADTagType2Data;



    /** Getter for versionNumber1.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumber1() {
        return versionNumber1;
    }

     /** Setter for versionNumber1.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumber1( Short value ) {
        this.versionNumber1 = value;
    }

    /** Getter for dataLength.
     *  
     * 
     * @return Integer
     */
    public Integer getDataLength() {
        return dataLength;
    }

     /** Setter for dataLength.
     *  
     * 
     * @param value Integer
     */
    public void setDataLength( Integer value ) {
        this.dataLength = value;
    }

    /** Getter for versionNumber2.
     *  
     * 
     * @return Integer
     */
    public Integer getVersionNumber2() {
        return versionNumber2;
    }

     /** Setter for versionNumber2.
     *  
     * 
     * @param value Integer
     */
    public void setVersionNumber2( Integer value ) {
        this.versionNumber2 = value;
    }

    /** Getter for cadTagCount.
     *  
     * 
     * @return Integer
     */
    public Integer getCadTagCount() {
        return cadTagCount;
    }

     /** Setter for cadTagCount.
     *  
     * 
     * @param value Integer
     */
    public void setCadTagCount( Integer value ) {
        this.cadTagCount = value;
    }

    /** Getter for cadTagTypes.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getCadTagTypes() {
        return cadTagTypes;
    }

     /** Setter for cadTagTypes.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setCadTagTypes( java.util.List<Integer> value ) {
        this.cadTagTypes = value;
    }

    /** Getter for cadTagsType1.
     *  
     * 
     * @return java.util.List<Integer>
     */
    public java.util.List<Integer> getCadTagsType1() {
        return cadTagsType1;
    }

     /** Setter for cadTagsType1.
     *  
     * 
     * @param value java.util.List<Integer>
     */
    public void setCadTagsType1( java.util.List<Integer> value ) {
        this.cadTagsType1 = value;
    }

    /** Getter for compressedCADTagType2Data.
     *  
     * 
     * @return CompressedCADTagType2Data
     */
    public CompressedCADTagType2Data getCompressedCADTagType2Data() {
        return compressedCADTagType2Data;
    }

     /** Setter for compressedCADTagType2Data.
     *  
     * 
     * @param value CompressedCADTagType2Data
     */
    public void setCompressedCADTagType2Data( CompressedCADTagType2Data value ) {
        this.compressedCADTagType2Data = value;
    }


    
}

