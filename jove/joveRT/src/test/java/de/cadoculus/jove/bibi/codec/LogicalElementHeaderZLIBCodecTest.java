/*
 * Copyright (C) 2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.base.GUID;
import de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID;
import static de.cadoculus.jove.bibi.codec.CodecUtilityTest.log;
import de.cadoculus.jove.lsgsegment.BaseNodeElement;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Zerbst
 */
public class LogicalElementHeaderZLIBCodecTest {

    public LogicalElementHeaderZLIBCodecTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        BasicConfigurator.resetConfiguration();

        java.io.File logconfig = new java.io.File( "log4j.properties" );

        if ( logconfig.exists() && logconfig.canRead() ) {

            try {
                URL propsURL = new URL( "file:" + logconfig.getName() );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {

                BasicConfigurator.resetConfiguration();
                BasicConfigurator.configure();

            }
        }
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testLookupClass() {
        System.out.println( "lookupClass" );
        BIBIObjectTypeID oti = BaseNodeElement.class.getAnnotation( BIBIObjectTypeID.class );

        GUID objectTypeID = new GUID( oti.value() );
        Class expResult = BaseNodeElement.class;
        Class result = LogicalElementHeaderZLIBCodec.lookupClass( objectTypeID );
        assertEquals( expResult, result );

        // Create invalid GUID
        objectTypeID = new GUID( "0x10dd102c, 0x2ac9, 0x11d9, 0x90, 0x60, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00" );

        result = LogicalElementHeaderZLIBCodec.lookupClass( objectTypeID );
        assertNull( result );

    }
}