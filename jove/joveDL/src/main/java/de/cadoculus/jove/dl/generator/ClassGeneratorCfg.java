/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl.generator;

import java.util.Properties;

/**
 *
 * @author Zerbst
 */
class ClassGeneratorCfg {

    private final Properties props;
    public static final String CFG_BASE_PACKAGE = "basePackage";

    ClassGeneratorCfg( Properties cfgProps ) {
        this.props = cfgProps;
    }

    /**
     * Get the base package used to create the classes. Default to
     * <code>de.cadoculus.jove</code>, override using key "basePackage"
     *
     * @return the name of the package
     */
    public String getBasePackage() {
        return props.getProperty( CFG_BASE_PACKAGE, "de.cadoculus.jove" );
    }
}
