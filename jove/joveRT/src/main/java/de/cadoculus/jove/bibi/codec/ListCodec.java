/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.BIBILength;
import java.lang.reflect.AnnotatedElement;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Zerbst
 */
public class ListCodec implements Codec<List> {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( ListCodec.class );
    private final Class type;
    private final Class itemType;
    private final AnnotatedElement metadata;

    /**
     * Construct an ObjectCodec for the given type.
     *
     * @param type
     */
    public ListCodec( AnnotatedElement metadata, Class type ) throws BIBIException {

        log.info( "ListCodec " + metadata + ", " + type );
        if ( metadata == null ) {
            throw new IllegalArgumentException( "expect none null metadata" );
        }
        this.metadata = metadata;
        if ( type == null ) {
            throw new IllegalArgumentException( "expect none null type" );
        }
        if ( !( List.class.isAssignableFrom( type ) ) ) {
            throw new IllegalArgumentException( "expect List.class as type input, but got " + type );
        }
        this.type = type;

        this.itemType = CodecUtility.getItemClass( metadata, type );
        if ( itemType == null ) {
            throw new IllegalArgumentException( "no item type found for " + metadata );
        }

    }

    @Override
    public List decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        // Evaluate the number of items to read
        BIBILength bl = metadata.getAnnotation( BIBILength.class );
        if ( bl == null ) {
            throw new BIBIException( "expect BIBILength annotation" );
        }

        int length = CodecUtility.evaluate( bl, ctx );

        if ( length < 0 ) {
            throw new BIBIException( "evaluated '" + bl.value() + "' to negative value " + length );
        }

        List retval = new ArrayList();

        Codec itemCodec = ctx.getDefaultCodecFactory().create( metadata, itemType, ctx);
        log.info("use " + itemCodec);
        
        for( int i = 0; i< length; i++) {
            Object item = itemCodec.decode( input, ctx );
            retval.add( item);
        }


        return retval;

    }

    @Override
    public String toString() {
        return "ListCodec< " + itemType.getSimpleName() + ">";
    }
    
    
}
