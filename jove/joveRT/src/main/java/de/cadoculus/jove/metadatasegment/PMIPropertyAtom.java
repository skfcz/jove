/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.6.1.1 PMI Property Atom</h4>
PMI Property Atom data collection represents the data format for both the key and value data of a PMI Property key/value pair.

*/
@XmlRootElement(name="PMIPropertyAtom")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIPropertyAtom",
    propOrder= {"value", "hiddenFlag", }
)
public class PMIPropertyAtom {

    public static Logger log = LoggerFactory.getLogger( PMIPropertyAtom.class );

    /**  
     * The variable number 0 : MbString value
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.MbString()    
    private String value;

    /**  
     * The variable number 1 : U32 hiddenFlag
     * <br>options : <pre>{valid : pmiVersionNumber > 6}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber > 6" )
    private Long hiddenFlag;



    /** Getter for value.
     *  
     * 
     * @return String
     */
    public String getValue() {
        return value;
    }

     /** Setter for value.
     *  
     * 
     * @param value String
     */
    public void setValue( String value ) {
        this.value = value;
    }

    /** Getter for hiddenFlag.
     *  
     * 
     * @return Long
     */
    public Long getHiddenFlag() {
        return hiddenFlag;
    }

     /** Setter for hiddenFlag.
     *  
     * 
     * @param value Long
     */
    public void setHiddenFlag( Long value ) {
        this.hiddenFlag = value;
    }


    
}

