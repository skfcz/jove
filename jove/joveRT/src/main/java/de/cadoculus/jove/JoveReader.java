/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove;

import de.cadoculus.jove.base.GUID;
import de.cadoculus.jove.base.LoadingStatus;
import de.cadoculus.jove.base.SegmentHeader;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.codec.DefaultCodecFactory;
import de.cadoculus.jove.fileheader.FileHeader;
import de.cadoculus.jove.tocsegment.TOCEntry;
import de.cadoculus.jove.tocsegment.TOCSegment;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Properties;
import org.slf4j.LoggerFactory;

/**
 * This class is used to read a JT file given as ByteBuffer into the Jove
 * datamodel.
 *
 * @author Zerbst
 */
public class JoveReader {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( JoveReader.class );
    private final Jove jove;
    private final DefaultCodecFactory cfact;
    private final WorkingContext ctx;
    private final ByteBuffer buffer;

    /**
     * Create a new JoveReader
     *
     * @param buffer the ByteBuffer to read from
     * @param cfg the configuration to use. May be null.
     */
    public JoveReader( ByteBuffer buffer, Properties cfg ) {
        this.jove = new Jove();
        this.cfact = new DefaultCodecFactory();
        this.ctx = new WorkingContext( cfg );
        ctx.setDefaultCodecFactory( cfact );
        this.buffer = buffer;
        ctx.setByteBuffer( buffer );
    }

    /**
     * Start readin the JT file.
     *
     * @return the read model
     * @throws BIBIException if decoding fails
     */
    public Jove read() throws BIBIException {
        readFileHeader();
        readTOCSegment();
        readDataSegments();

        return jove;
    }

    /**
     * Get the read data model. In case that the complete read was not
     * successfull, this contains at least the data as read so far.
     *
     * @return a ( potentially partial) data model for the JT files
     */
    public Jove getJove() {
        return jove;
    }

    /**
     * Demand late loading of a data segment not read yet.
     *
     * @param segmentHeader
     * @return
     */
    public SegmentHeader readDataSegment( SegmentHeader segmentHeader ) throws BIBIException {
        if ( segmentHeader == null ) {
            throw new IllegalArgumentException( "expect none null SegmentHeader" );
        }

        log.info( "readDataSegment @" + segmentHeader.getOffset() + ", " + segmentHeader.getSegmentType() );

        if ( jove.getSegments().isEmpty() ) {
            throw new IllegalStateException( "file not loaded yet, first call JoveReader#read before late loading individual segments" );
        }

        // The SegmentHeader has to be from the already read segements
        if ( !jove.getSegments().contains( segmentHeader ) ) {
            throw new IllegalArgumentException( "could not reload SegmentHeader from a different file" );
        }

        if ( segmentHeader.getLoadingStatus() == LoadingStatus.LOADING_FINISHED ) {
            log.info( "alread finished reading segment " + segmentHeader.getSegmentType() + "@" + segmentHeader.getOffset() );
            return segmentHeader;
        } else if ( segmentHeader.getLoadingStatus() == LoadingStatus.LOADING_FAILED ) {
            log.info( "alread failed to load segment " + segmentHeader.getSegmentType() + "@" + segmentHeader.getOffset() );
            return segmentHeader;
        } else if ( segmentHeader.getLoadingStatus() == LoadingStatus.LOADING_POSTPHONED ) {
            // Start the late loading
            int offset = segmentHeader.getOffset();
            int length = segmentHeader.getSegmentLength();
            ctx.setLateLoading( false);
            SegmentHeader reloadedSegment = readDataSegment( offset, length );

            int index = jove.getSegments().indexOf( segmentHeader );
            jove.getSegments().set( index, reloadedSegment );
            return reloadedSegment;
        } else {
            throw new IllegalStateException( "got unsupported loading status " + segmentHeader.getLoadingStatus() );
        }

    }

    /**
     * Read the FileHeader from the JT file
     *
     * @return the read file header
     * @throws BIBIException if decoding fails
     */
    FileHeader readFileHeader() throws BIBIException {

        Codec<FileHeader> codec = cfact.create( null, FileHeader.class, ctx );
        buffer.position( 0 );
        FileHeader fh = codec.decode( buffer, ctx );
        jove.setFileHeader( fh );
        ctx.setFileHeader( fh );
        return fh;

    }

    /**
     * Read the TOCSegment from the JT file
     *
     * @return the read TOCSegment
     * @throws BIBIException if decoding fails
     */
    TOCSegment readTOCSegment() throws BIBIException {
        if ( jove.getFileHeader() == null ) {
            throw new IllegalStateException( "call #readFileHeader before calling #readTOCSegment" );
        }
        buffer.position( jove.getFileHeader().getTocOffset() );
        Codec<TOCSegment> codec = cfact.create( null, TOCSegment.class, ctx );
        TOCSegment toc = codec.decode( buffer, ctx );
        jove.setTocSegment( toc );
        return toc;
    }

    /**
     * Read the data segments from the JT file. By default only the LSG segment
     * is read in full, all other segments have to be late loaded using {@link #read(de.cadoculus.jove.base.SegmentHeader)
     * }.
     *
     * @return a list of all SegmentHeader as found in the TOC
     * @throws BIBIException
     */
    List<SegmentHeader> readDataSegments() throws BIBIException {

        log.info( "readDataSegments" );
        if ( jove.getFileHeader() == null ) {
            throw new IllegalStateException( "called #readLSGSegment before calling #readTOCSegment" );
        }
        GUID lsgSegmentId = jove.getFileHeader().getLsgSegmentId();
        ctx.setLateLoading( true );

        // Loop over segments in the TOC
        TOC:
        for ( TOCEntry segmentEntry : jove.getTocSegment().getTocEntries() ) {

            if ( lsgSegmentId.equals( segmentEntry.getSegmentID() ) ) {
                log.info( "found LSGSegment at offset " + segmentEntry.getSegmentOffset() + ", length " + segmentEntry.getSegmentLength() );
            }

            int offset = segmentEntry.getSegmentOffset();
            int length = segmentEntry.getSegmentLength();

            SegmentHeader segmentHead = readDataSegment( offset, length );

            jove.getSegments().add( segmentHead );

        }
        return jove.getSegments();
    }

    /**
     * Read a single SegmentHeader.
     *
     * @param offset the offset in bytes from the beginning of the file
     * @param length the length of the segment
     * @return the loaded SegmentHeader
     * @throws BIBIException
     */
    SegmentHeader readDataSegment( int offset, int length ) throws BIBIException {

        buffer.position( offset );
        ctx.setMaxPosition( offset + length );
        

        Codec<SegmentHeader> headerCodec = ctx.getDefaultCodecFactory().create( null, SegmentHeader.class, ctx );

        SegmentHeader segHead = headerCodec.decode( buffer, ctx );
        segHead.setOffset( offset );
        return segHead;

    }

}
