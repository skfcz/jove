/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl.generator;

import de.cadoculus.jove.dl.BlockDeclaration;
import de.cadoculus.jove.dl.EntityDeclaration;
import de.cadoculus.jove.dl.EnumerationDeclaration;
import de.cadoculus.jove.dl.SegmentDeclaration;
import de.cadoculus.jove.dl.parser.JoveDL;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.cli.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Zerbst
 */
public class ClassGenerator {

    public static Logger log = LoggerFactory.getLogger( ClassGenerator.class );
    private final File targetDir;
    private final ClassGeneratorCfg cfg;
    private final JoveDL jovedl;
    private Configuration fmCfg;

    public ClassGenerator( JoveDL jovedl, ClassGeneratorCfg cfg, File targetDir ) {
        this.jovedl = jovedl;
        this.cfg = cfg;
        this.targetDir = targetDir;

        fmCfg = new Configuration();
        fmCfg.setClassForTemplateLoading( ClassGenerator.class, "templates" );
        fmCfg.setObjectWrapper( new BeansWrapper() );

    }

    public void generate() throws Exception {

        generateEnumerations();
        generateBlocks();
        generateSegments();
    }

    void generateEnumerations() throws Exception {
        Map<String, Object> values = new HashMap<>();
        String pkgName = "de.cadoculus.jove.enums";
        values.put( "package", pkgName );
        File pkgDir = createPackageDir( pkgName );

        for ( Iterator<EnumerationDeclaration> it = jovedl.getEnumerations().iterator(); it.hasNext(); ) {
            EnumerationDeclaration enu = it.next();

            values.put( "enum", enu );

            log.info( "create " + enu.getName() );

            String fileName = enu.getName() + ".java";
            File f = new File( pkgDir, fileName );

            try {
                FileOutputStream fos = new FileOutputStream( f );
                Writer w = new OutputStreamWriter( fos );
                Template blt = fmCfg.getTemplate( "enum.fm" );

                blt.process( values, w );
                w.close();
            } catch ( Exception exp ) {
                log.error( "failed to create file " + f.getAbsolutePath() );
                log.error( "template 'enum.fm'" );
                log.error( "values " + values );
                log.error( "reason", exp );
                throw new IllegalStateException( "failed to evaluate template" );

            }

        }
    }

    void generateBlocks() throws Exception {
        for ( BlockDeclaration block : jovedl.getBlocks() ) {
            log.info( "generate Block " + block.getName() );

            Map<String, Object> values = new HashMap<>();
            values.put( "block", block );

            String pkgName = createPackageName( block );
            values.put( "package", pkgName );

            File pkgDir = createPackageDir( pkgName );

            if ( block.getComment() != null && block.getComment().trim().length() > 0 ) {
                File pkg = new File( pkgDir, "package.html" );
                values.put( "comment", block.getComment() );
                try {
                    FileOutputStream fos = new FileOutputStream( pkg );
                    Writer w = new OutputStreamWriter( fos );
                    Template blt = fmCfg.getTemplate( "packageHTML.fm" );
                    blt.process( values, w );
                    w.close();
                } catch ( Exception exp ) {
                    log.error( "failed to create package file " + pkg.getAbsolutePath(), exp );
                    throw new IllegalStateException( "failed to create package.html" );

                }
            }

            for ( EntityDeclaration entity : block.getEntities() ) {
                log.info( "create " + entity.getName() );
                values.put( "entity", entity );
                String fileName = entity.getName() + ".java";

                File f = new File( pkgDir, fileName );

                try {
                    FileOutputStream fos = new FileOutputStream( f );
                    Writer w = new OutputStreamWriter( fos );
                    Template blt = fmCfg.getTemplate( "entity.fm" );

                    blt.process( values, w );
                    w.close();
                } catch ( Exception exp ) {
                    log.error( "failed to create file " + f.getAbsolutePath() );
                    log.error( "template 'entity.fm'" );
                    log.error( "values " + values );
                    log.error( "reason", exp );
                    throw new IllegalStateException( "failed to evaluate template" );

                }
            }

        }

    }

    void generateSegments() throws Exception {
        for ( SegmentDeclaration segment : jovedl.getSegments() ) {
            log.info( "generate Segment " + segment.getName() );

            Map<String, Object> values = new HashMap<>();
            values.put( "segment", segment );

            String pkgName = createPackageName( segment );
            values.put( "package", pkgName );

            File pkgDir = createPackageDir( pkgName );

            if ( segment.getComment() != null && segment.getComment().trim().length() > 0 ) {
                File pkg = new File( pkgDir, "package.html" );
                values.put( "comment", segment.getComment() );
                try {
                    FileOutputStream fos = new FileOutputStream( pkg );
                    Writer w = new OutputStreamWriter( fos );
                    Template blt = fmCfg.getTemplate( "packageHTML.fm" );
                    blt.process( values, w );
                    w.close();
                } catch ( Exception exp ) {
                    log.error( "failed to create package file " + pkg.getAbsolutePath(), exp );
                    throw new IllegalStateException( "failed to create package.html" );

                }
            }

            for ( EntityDeclaration entity : segment.getEntities() ) {
                log.info( "create entity " + entity.getName() );
                values.put( "entity", entity );
                String fileName = entity.getName() + ".java";
                File f = new File( pkgDir, fileName );
                try {
                    FileOutputStream fos = new FileOutputStream( f );
                    Writer w = new OutputStreamWriter( fos );
                    Template blt = fmCfg.getTemplate( "entity.fm" );
                    blt.process( values, w );
                    w.close();
                } catch ( Exception exp ) {
                    log.error( "failed to create file " + f.getAbsolutePath() );
                    log.error( "template 'entity.fm'" );
                    log.error( "values " + values );
                    log.error( "reason", exp );
                    throw new IllegalStateException( "failed to evaluate template" );
                }

            }

            for ( EnumerationDeclaration enu : segment.getEnumerations() ) {
                log.info( "create enumeration " + enu.getName() );
                values.put( "enum", enu );
                String fileName = enu.getName() + ".java";
                File f = new File( pkgDir, fileName );

                try {
                    FileOutputStream fos = new FileOutputStream( f );
                    Writer w = new OutputStreamWriter( fos );
                    Template blt = fmCfg.getTemplate( "enum.fm" );

                    blt.process( values, w );
                    w.close();
                } catch ( Exception exp ) {
                    log.error( "failed to create file " + f.getAbsolutePath() );
                    log.error( "template 'enum.fm'" );
                    log.error( "values " + values );
                    log.error( "reason", exp );
                    throw new IllegalStateException( "failed to evaluate template" );

                }

            }

        }

    }

    public static void main( String[] args ) {
        BasicConfigurator.configure();

        Options options = new Options();

        Option helpO = new Option( "h", "help", false, "display help" );
        options.addOption( helpO );

        Option logO = OptionBuilder.withLongOpt( "log" ).hasArg().withDescription(
                "url for log4j.properties file" ).create( "log" );
        options.addOption( logO );

        Option jdlO = OptionBuilder.withArgName( "f" ).withLongOpt( "jdl" ).hasArg()
                .isRequired( true ).withDescription( "the name of the jdl file" ).create( "f" );
        options.addOption( jdlO );

        Option cfgO = OptionBuilder.withArgName( "c" ).withLongOpt( "config" ).hasArg()
                .isRequired( false ).withDescription( "the name of the config file" ).create( "c" );
        options.addOption( cfgO );

        Option dO = OptionBuilder.withArgName( "d" ).withLongOpt( "dir" ).hasArg().isRequired(
                true ).withDescription( "the src directory" ).create( "d" );
        options.addOption( dO );

        // parse the command line
        CommandLine line = null;

        try {
            CommandLineParser cparser = new PosixParser();
            line = cparser.parse( options, args );
        } catch ( org.apache.commons.cli.ParseException exp ) {
            System.err.println( exp.getMessage() );

            // oops, something went wrong
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "ClassGenerator", options );

            System.exit( 66 );
        }

        if ( line.hasOption( "h" ) ) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "ClassGenerator", options );
            System.exit( 0 );
        }

        if ( line.hasOption( "log" ) ) {

            try {
                URL propsURL = new URL( line.getOptionValue( "log" ) );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {
                System.err.println( "invalid properties url " + line.getOptionValue( "log" ) );
                System.err.println( "use standard configuration" );
                BasicConfigurator.resetConfiguration();

            }
        }

        // The JDL file
        String fileName = line.getOptionValue( "f" );
        File jdlFile = new File( fileName );

        if ( !( jdlFile.isFile() && jdlFile.canRead() ) ) {
            log.error( "failed to open " + jdlFile.getAbsolutePath() );
            System.err.println( "got invalid jdl file '" + fileName + "'" );

            System.exit( 66 );
        }

        // The Configuration
        Properties cfgProps = new Properties();
        try {
            cfgProps.load( ClassGenerator.class.getResourceAsStream( "default.properties" ) );
        } catch ( IOException exp ) {
            log.error( "failed to load default configuration", exp );
            System.exit( 33 );
        }
        if ( line.hasOption( "c" ) ) {
            File cfgFile = new File( line.getOptionValue( "c" ) );
            if ( !( cfgFile.isFile() && cfgFile.canRead() ) ) {
                log.error( "failed to open " + cfgFile.getAbsolutePath() );
                System.exit( 66 );
            }
            try {
                try ( FileInputStream fis = new FileInputStream( cfgFile ) ) {
                    cfgProps.load( fis );
                }
            } catch ( IOException exp ) {
                log.error( "an error occured loading the config file", exp );
                System.exit( 66 );
            }
        }

        ClassGeneratorCfg cfg = new ClassGeneratorCfg( cfgProps );
        //
        File targetDir = new File( System.getProperty( "user.dir" ) );
        if ( line.hasOption( "d" ) ) {
            targetDir = new File( line.getOptionValue( "d" ) );
        }
        if ( !( targetDir.isDirectory() && targetDir.canWrite() ) ) {
            log.error( "got invalid directory " + targetDir.getAbsolutePath() );
            System.exit( 66 );
        }

        JoveDL jovedl = null;

        try {
            try ( FileInputStream fis = new FileInputStream( jdlFile ) ) {
                jovedl = new JoveDL( fis );
                jovedl.parse();
            }
        } catch ( Exception exp ) {
            log.error( "an exception occured when parsing", exp );
            System.exit( 1 );
        }
        log.info( "successfully parsed the jdl file" );

        ClassGenerator cg = new ClassGenerator( jovedl, cfg, targetDir );

        try {
            cg.generate();
        } catch ( Exception exp ) {
            log.error( "an exception occured when generating files", exp );
            System.exit( 1 );
        }
        log.info( "successfully generated files" );

    }

    /**
     * Create the full qualified package name for a given BlockDeclaration
     *
     * @param block
     * @return the package name
     */
    String createPackageName( BlockDeclaration block ) {

        StringBuilder buff = new StringBuilder( cfg.getBasePackage() );
        buff.append( "." );
        String name = block.getName();
        for ( int i = 0; i < name.length(); i++ ) {
            char c = name.charAt( i );
            if ( Character.isJavaIdentifierPart( c ) ) {
                buff.append( Character.toLowerCase( c ) );
            } else {
                throw new IllegalArgumentException( "block or segment name '" + name
                        + "' contains invalid character '" + c + "'" );
            }
        }
        return buff.toString();

    }

    /**
     * Creates and returns a new directory for a given package name below the
     * target directory.
     *
     * @param pkgName the name of the package, * * * * * e.g.
     * <code>de.cadoculus.jove.tocheader</code>
     * @return the newly created directory, create as combination of the
     * targetdirectory and the given package name
     */
    File createPackageDir( String pkgName ) {
        File retval = targetDir;
        String[] tokens = pkgName.split( "\\." );
        log.info( "tokens " + Arrays.toString( tokens ) );
        for ( String tok : tokens ) {
            retval = new File( retval, tok );
            log.info( "test " + retval.getAbsolutePath() );
            log.info( "exists " + retval.exists() + ", " + retval.isDirectory() );
            if ( retval.exists() ) {
                if ( !retval.isDirectory() ) {
                    throw new IllegalArgumentException( "file " + retval.getAbsolutePath() + " already exists" );
                }
            } else if ( !retval.mkdirs() ) {
                throw new IllegalArgumentException( "failed to create directory " + retval.getAbsolutePath() );
            }
        }
        return retval;
    }
}
