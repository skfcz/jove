/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
7.2.1.1.2.1.2.1 Shader Parameter
Shader Parameter data collection defines a Shader input and/or output parameter. A list of Shader Parameters represents the runtime linkage of the shader program into the GPU‟s data streams.

*/
@XmlRootElement(name="ShaderParameter")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "ShaderParameter",
    propOrder= {"paramName", "paramType", "valueClass", "direction", "semanticBinding", "variability", "reservedField", "values", }
)
public class ShaderParameter {

    public static Logger log = LoggerFactory.getLogger( ShaderParameter.class );

    /**  
     * The variable number 0 : MbString paramName
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.MbString()    
    private String paramName;

    /**  
     * The variable number 1 : U32 paramType
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    private Long paramType;

    /**  
     * The variable number 2 : U32 valueClass
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    private Long valueClass;

    /**  
     * The variable number 3 : U32 direction
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    private Long direction;

    /**  
     * The variable number 4 : U32 semanticBinding
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    private Long semanticBinding;

    /**  
     * The variable number 5 : U32 variability
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    private Long variability;

    /**  
     * The variable number 6 : U32 reservedField
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    
    private Long reservedField;

    /**  
     * The variable number 7 : U32[] values
     * <br>options : <pre>{length : 16}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.U32()    @de.cadoculus.jove.bibi.annotation.BIBILength( "16" )
    @XmlElementWrapper(name = "values")
        private java.util.List<Long> values;



    /** Getter for paramName.
     *  
     * 
     * @return String
     */
    public String getParamName() {
        return paramName;
    }

     /** Setter for paramName.
     *  
     * 
     * @param value String
     */
    public void setParamName( String value ) {
        this.paramName = value;
    }

    /** Getter for paramType.
     *  
     * 
     * @return Long
     */
    public Long getParamType() {
        return paramType;
    }

     /** Setter for paramType.
     *  
     * 
     * @param value Long
     */
    public void setParamType( Long value ) {
        this.paramType = value;
    }

    /** Getter for valueClass.
     *  
     * 
     * @return Long
     */
    public Long getValueClass() {
        return valueClass;
    }

     /** Setter for valueClass.
     *  
     * 
     * @param value Long
     */
    public void setValueClass( Long value ) {
        this.valueClass = value;
    }

    /** Getter for direction.
     *  
     * 
     * @return Long
     */
    public Long getDirection() {
        return direction;
    }

     /** Setter for direction.
     *  
     * 
     * @param value Long
     */
    public void setDirection( Long value ) {
        this.direction = value;
    }

    /** Getter for semanticBinding.
     *  
     * 
     * @return Long
     */
    public Long getSemanticBinding() {
        return semanticBinding;
    }

     /** Setter for semanticBinding.
     *  
     * 
     * @param value Long
     */
    public void setSemanticBinding( Long value ) {
        this.semanticBinding = value;
    }

    /** Getter for variability.
     *  
     * 
     * @return Long
     */
    public Long getVariability() {
        return variability;
    }

     /** Setter for variability.
     *  
     * 
     * @param value Long
     */
    public void setVariability( Long value ) {
        this.variability = value;
    }

    /** Getter for reservedField.
     *  
     * 
     * @return Long
     */
    public Long getReservedField() {
        return reservedField;
    }

     /** Setter for reservedField.
     *  
     * 
     * @param value Long
     */
    public void setReservedField( Long value ) {
        this.reservedField = value;
    }

    /** Getter for values.
     *  
     * 
     * @return java.util.List<Long>
     */
    public java.util.List<Long> getValues() {
        return values;
    }

     /** Setter for values.
     *  
     * 
     * @param value java.util.List<Long>
     */
    public void setValues( java.util.List<Long> value ) {
        this.values = value;
    }


    
}

