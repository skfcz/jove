/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="RangeLODNodeElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "RangeLODNodeElement",
    propOrder= {"lodNodeData", "versionNumberRLNE", "rangeLimits", "center", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0x10dd104c, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97" )
public class RangeLODNodeElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( RangeLODNodeElement.class );

    /**  
     * The variable number 0 : LODNodeData lodNodeData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="LODNodeData")
    private LODNodeData lodNodeData;

    /**  
     * The variable number 1 : I16 versionNumberRLNE
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumberRLNE;

    /**  
     * The variable number 2 : VecF32 rangeLimits
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.VecF32()    
    private java.util.List<Float> rangeLimits;

    /**  
     * The variable number 3 : CoordF32 center
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.CoordF32()    
    private javax.vecmath.Point3f center;



    /** Getter for lodNodeData.
     *  
     * 
     * @return LODNodeData
     */
    public LODNodeData getLodNodeData() {
        return lodNodeData;
    }

     /** Setter for lodNodeData.
     *  
     * 
     * @param value LODNodeData
     */
    public void setLodNodeData( LODNodeData value ) {
        this.lodNodeData = value;
    }

    /** Getter for versionNumberRLNE.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberRLNE() {
        return versionNumberRLNE;
    }

     /** Setter for versionNumberRLNE.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberRLNE( Short value ) {
        this.versionNumberRLNE = value;
    }

    /** Getter for rangeLimits.
     *  
     * 
     * @return java.util.List<Float>
     */
    public java.util.List<Float> getRangeLimits() {
        return rangeLimits;
    }

     /** Setter for rangeLimits.
     *  
     * 
     * @param value java.util.List<Float>
     */
    public void setRangeLimits( java.util.List<Float> value ) {
        this.rangeLimits = value;
    }

    /** Getter for center.
     *  
     * 
     * @return javax.vecmath.Point3f
     */
    public javax.vecmath.Point3f getCenter() {
        return center;
    }

     /** Setter for center.
     *  
     * 
     * @param value javax.vecmath.Point3f
     */
    public void setCenter( javax.vecmath.Point3f value ) {
        this.center = value;
    }


    
}

