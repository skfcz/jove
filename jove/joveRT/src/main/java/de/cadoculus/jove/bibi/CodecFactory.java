/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi;

import java.lang.reflect.AnnotatedElement;

/**
 * The factory interface for {@link Codec Codecs}. There is no 1:1 relation
 * between the {@link CodecFactory} and the created {@link Codec}.
 *
 * @author cz
 */
public interface CodecFactory {

    /**
     * This method is used to create or reuse a {@link Codec} matching the given
     * input.
     *
     * @param <T> the Java class of the object to read
     * @param metadata annotations used to specify how to read. See
     * de.cadoculus.jove.bibi.annotation.
     * @param type the Java class of the object to read
     * @param ctx the WorkingContext ruling the deserialization
     * @return a {@link Codec} able to perform the deserialization
     * or <code>null</code> if the factory could not provide a Codec matching
     * the expectation.
     */
    <T> Codec<T> create( AnnotatedElement metadata, Class<T> type, WorkingContext ctx ) throws BIBIException;
}
