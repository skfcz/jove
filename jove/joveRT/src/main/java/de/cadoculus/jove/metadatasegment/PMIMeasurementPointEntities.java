/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.9 PMI Measurement Point Entities</h4>
The PMI Measurement Point Entities data collection defines data for a list of Measurement Point symbols. Measurement Points are predefined locations (i.e. geometric entities or theoretical, but measurable points, such as surface locations) which are measured on manufactured parts to verify the accuracy of the manufacturing process.
Several data fields of the PMI Measurement Point Entities data collection are only present if Version Number, as defined in 7.2.6.2PMI Manager Meta Data Element, is greater than or equal to "4".

*/
@XmlRootElement(name="PMIMeasurementPointEntities")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIMeasurementPointEntities",
    propOrder= {"mpCount", "measurePointEntities", }
)
public class PMIMeasurementPointEntities {

    public static Logger log = LoggerFactory.getLogger( PMIMeasurementPointEntities.class );

    /**  
     * The variable number 0 : I32 mpCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer mpCount;

    /**  
     * The variable number 1 : PMIMeasurementPointEntity[] measurePointEntities
     * <br>options : <pre>{length : obj.mpCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.mpCount" )
    @XmlElementWrapper(name = "measurePointEntities")
    @XmlElementRef()    private java.util.List<PMIMeasurementPointEntity> measurePointEntities;



    /** Getter for mpCount.
     *  
     * 
     * @return Integer
     */
    public Integer getMpCount() {
        return mpCount;
    }

     /** Setter for mpCount.
     *  
     * 
     * @param value Integer
     */
    public void setMpCount( Integer value ) {
        this.mpCount = value;
    }

    /** Getter for measurePointEntities.
     *  
     * 
     * @return java.util.List<PMIMeasurementPointEntity>
     */
    public java.util.List<PMIMeasurementPointEntity> getMeasurePointEntities() {
        return measurePointEntities;
    }

     /** Setter for measurePointEntities.
     *  
     * 
     * @param value java.util.List<PMIMeasurementPointEntity>
     */
    public void setMeasurePointEntities( java.util.List<PMIMeasurementPointEntity> value ) {
        this.measurePointEntities = value;
    }


    
}

