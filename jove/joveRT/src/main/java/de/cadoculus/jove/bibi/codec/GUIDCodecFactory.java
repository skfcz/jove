/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.base.GUID;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import java.lang.reflect.AnnotatedElement;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This factory creates codecs for GUIDs. Supported annotation / Java types:</p>
 *
 * <ul> <li>GUID / GUID</li></ul>
 *
 * @author Zerbst
 */
public class GUIDCodecFactory extends AbstractCodecFactory {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( GUIDCodecFactory.class );
    private GUIDCodec codec = null;

    public GUIDCodecFactory() {
    }

    @Override
    public <T> Codec<T> create( AnnotatedElement metadata, Class<T> type, WorkingContext ctx ) throws BIBIException {

        if ( !( GUID.class == type ) ) {
            return null;
        }
        if ( metadata == null ) {
            return null;
        }

        de.cadoculus.jove.bibi.annotation.GUID guidA = metadata.getAnnotation( de.cadoculus.jove.bibi.annotation.GUID.class );
        if ( guidA != null ) {
            if ( codec == null ) {
                codec = new GUIDCodec( metadata, GUID.class );
            }

            return (Codec<T>) codec;
        }
        return null;

    }
}
