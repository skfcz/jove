/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="QuantizationParameters")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "QuantizationParameters",
    propOrder= {"bitsPerVertex", "normalBitsFactor", "bitsPerTextureCoord", "bitsPerColor", }
)
public class QuantizationParameters {

    public static Logger log = LoggerFactory.getLogger( QuantizationParameters.class );

    /**  
     * The variable number 0 : U8 bitsPerVertex
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U8()    
    private Short bitsPerVertex;

    /**  
     * The variable number 1 : U8 normalBitsFactor
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U8()    
    private Short normalBitsFactor;

    /**  
     * The variable number 2 : U8 bitsPerTextureCoord
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U8()    
    private Short bitsPerTextureCoord;

    /**  
     * The variable number 3 : U8 bitsPerColor
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U8()    
    private Short bitsPerColor;



    /** Getter for bitsPerVertex.
     *  
     * 
     * @return Short
     */
    public Short getBitsPerVertex() {
        return bitsPerVertex;
    }

     /** Setter for bitsPerVertex.
     *  
     * 
     * @param value Short
     */
    public void setBitsPerVertex( Short value ) {
        this.bitsPerVertex = value;
    }

    /** Getter for normalBitsFactor.
     *  
     * 
     * @return Short
     */
    public Short getNormalBitsFactor() {
        return normalBitsFactor;
    }

     /** Setter for normalBitsFactor.
     *  
     * 
     * @param value Short
     */
    public void setNormalBitsFactor( Short value ) {
        this.normalBitsFactor = value;
    }

    /** Getter for bitsPerTextureCoord.
     *  
     * 
     * @return Short
     */
    public Short getBitsPerTextureCoord() {
        return bitsPerTextureCoord;
    }

     /** Setter for bitsPerTextureCoord.
     *  
     * 
     * @param value Short
     */
    public void setBitsPerTextureCoord( Short value ) {
        this.bitsPerTextureCoord = value;
    }

    /** Getter for bitsPerColor.
     *  
     * 
     * @return Short
     */
    public Short getBitsPerColor() {
        return bitsPerColor;
    }

     /** Setter for bitsPerColor.
     *  
     * 
     * @param value Short
     */
    public void setBitsPerColor( Short value ) {
        this.bitsPerColor = value;
    }


    
}

