/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="VertexShapeNodeElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "VertexShapeNodeElement",
    propOrder= {"vertexShapeData", }
)
public class VertexShapeNodeElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( VertexShapeNodeElement.class );

    /**  
     * The variable number 0 : VertexShapeData vertexShapeData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="VertexShapeData")
    private VertexShapeData vertexShapeData;



    /** Getter for vertexShapeData.
     *  
     * 
     * @return VertexShapeData
     */
    public VertexShapeData getVertexShapeData() {
        return vertexShapeData;
    }

     /** Setter for vertexShapeData.
     *  
     * 
     * @param value VertexShapeData
     */
    public void setVertexShapeData( VertexShapeData value ) {
        this.vertexShapeData = value;
    }


    
}

