/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.1.2.2 String Property Atom Element <p>String Property Atom Element represents a character string property atom. </h4>

*/
@XmlRootElement(name="StringPropertyAtomElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "StringPropertyAtomElement",
    propOrder= {"basePropertyAtomData", "versionNumberSPA", "value", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0x10dd106e, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97" )
public class StringPropertyAtomElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( StringPropertyAtomElement.class );

    /**  
     * The variable number 0 : BasePropertyAtomData basePropertyAtomData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="BasePropertyAtomData")
    private BasePropertyAtomData basePropertyAtomData;

    /**  
     * The variable number 1 : I16 versionNumberSPA
     * <br>options : <pre>{valid : jtVersion > 81}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "jtVersion > 81" )
    private Short versionNumberSPA;

    /**  
     * The variable number 2 : MbString value
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.MbString()    
    private String value;



    /** Getter for basePropertyAtomData.
     *  
     * 
     * @return BasePropertyAtomData
     */
    public BasePropertyAtomData getBasePropertyAtomData() {
        return basePropertyAtomData;
    }

     /** Setter for basePropertyAtomData.
     *  
     * 
     * @param value BasePropertyAtomData
     */
    public void setBasePropertyAtomData( BasePropertyAtomData value ) {
        this.basePropertyAtomData = value;
    }

    /** Getter for versionNumberSPA.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberSPA() {
        return versionNumberSPA;
    }

     /** Setter for versionNumberSPA.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberSPA( Short value ) {
        this.versionNumberSPA = value;
    }

    /** Getter for value.
     *  
     * 
     * @return String
     */
    public String getValue() {
        return value;
    }

     /** Setter for value.
     *  
     * 
     * @param value String
     */
    public void setValue( String value ) {
        this.value = value;
    }


    
}

