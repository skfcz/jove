/*
 * Copyright (C) 2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.metadatasegment.PMIManagerMetaDataElement;
import java.lang.reflect.Field;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is the standard ObjectCodec. It copies the value of the
 * {@link PMIManagerMetaDataElement#pmiVersionNumber} into the
 * {@link WorkingContext}.
 *
 * @author cz
 */
public class PMIManagerMetaDataElementCodec extends ObjectCodec<PMIManagerMetaDataElement> {

    private static Logger log = LoggerFactory.getLogger( PMIManagerMetaDataElementCodec.class );

    public PMIManagerMetaDataElementCodec( Class<PMIManagerMetaDataElement> type ) throws BIBIException {
        super( type );
    }

    public PMIManagerMetaDataElementCodec( Class<PMIManagerMetaDataElement> type, List<Object> alreadyParsedValues ) throws BIBIException {
        super( type );
        this.alreadyParsedValues.addAll( alreadyParsedValues );
    }

    @Override
    protected void postAction( Field field, Object value, WorkingContext ctx ) {

        if ( "pmiVersionNumber".equals( field.getName() ) ) {

            PMIManagerMetaDataElement obj = (PMIManagerMetaDataElement) ctx.getObj();

            ctx.setPmiVersionNumber( obj.getPmiVersionNumber() );
        }

    }

}
