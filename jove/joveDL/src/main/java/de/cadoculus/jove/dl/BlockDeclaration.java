/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Aüache License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * The BlockDeclaration represents a block from the JT files.
 * @author Carsten Zerbst
 */
public class BlockDeclaration {
    protected String name;
    protected String comment;
    protected List<EntityDeclaration> entities = new ArrayList<>();
    protected Map<String, Object> properties = new HashMap<>();

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName( String name ) {
        this.name = name;
    }

    /**
     * @return the entities
     */
    public List<EntityDeclaration> getEntities() {
        return entities;
    }

    /**
     * @param entities the entities to set
     */
    public void setEntities( List<EntityDeclaration> entities ) {
        this.entities = entities;
    }

    /**
     * @return the properties
     */
    public Map<String, Object> getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties( Map<String, Object> properties ) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        
        StringBuffer buff = new StringBuffer();
        buff.append("BlockDeclaration{ name=").append(name ).append(", entities=[ ");
        for ( Iterator<EntityDeclaration> it = entities.iterator(); it.hasNext(); ) {
            EntityDeclaration entityDeclaration = it.next();
            buff.append( entityDeclaration.getName());
            if ( it.hasNext()) {
                buff.append(", ");
            }
        }
        buff.append("], properties=" );
        buff.append(properties.toString());
        buff.append("}");
        return buff.toString();
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment( String comment ) {
        this.comment = comment;
    }
    
    
}
