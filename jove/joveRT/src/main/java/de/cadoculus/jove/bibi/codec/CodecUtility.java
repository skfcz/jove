/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.annotation.BIBIValid;
import de.cadoculus.jove.bibi.annotation.BIBILength;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.BIBIBufferExceededException;
import de.cadoculus.jove.bibi.WorkingContext;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Zerbst
 */
public class CodecUtility {

    private static org.slf4j.Logger log = LoggerFactory.getLogger( CodecUtility.class );
    public static Class[] NUMERIC_ANNOTATIONS = new Class[]{
        // Simple data types
        de.cadoculus.jove.bibi.annotation.UChar.class, // An unsigned 8-bit byte.
        de.cadoculus.jove.bibi.annotation.U8.class, // An unsigned 8-bit integer value.
        de.cadoculus.jove.bibi.annotation.U16.class, // An unsigned 16-bit integer value.
        de.cadoculus.jove.bibi.annotation.U32.class, // An unsigned 32-bit integer value.
        de.cadoculus.jove.bibi.annotation.U64.class, // An unsigned 64-bit integer value.
        de.cadoculus.jove.bibi.annotation.I16.class, // A signed two‟s complement 16-bit integer value.
        de.cadoculus.jove.bibi.annotation.I32.class, // A signed two‟s complement 32-bit integer value.
        de.cadoculus.jove.bibi.annotation.I64.class, // A signed two's complement 64-bit integer value.
        de.cadoculus.jove.bibi.annotation.F32.class, // An IEEE 32-bit floating point number.
        de.cadoculus.jove.bibi.annotation.F64.class, // An IEEE 64-bit double precision floating point number
    };
    public static Class[] TYPE_ANNOTATIONS = new Class[]{
        // Composite Data Types
        de.cadoculus.jove.bibi.annotation.BBoxF32.class,
        de.cadoculus.jove.bibi.annotation.CoordF32.class, //
        de.cadoculus.jove.bibi.annotation.CoordF64.class, //
        de.cadoculus.jove.bibi.annotation.DirF32.class, //
        de.cadoculus.jove.bibi.annotation.GUID.class, // 
        de.cadoculus.jove.bibi.annotation.HCoordF32.class, //
        de.cadoculus.jove.bibi.annotation.HCoordF64.class, //
        de.cadoculus.jove.bibi.annotation.MbString.class, //
        de.cadoculus.jove.bibi.annotation.Mx4F32.class, //
        de.cadoculus.jove.bibi.annotation.PlaneF32.class, //
        de.cadoculus.jove.bibi.annotation.Quaternion.class, //
        de.cadoculus.jove.bibi.annotation.RGB.class, //
        de.cadoculus.jove.bibi.annotation.RGBA.class, //
        de.cadoculus.jove.bibi.annotation.String.class, //
        de.cadoculus.jove.bibi.annotation.VecF32.class, //
        de.cadoculus.jove.bibi.annotation.VecF64.class, //
        de.cadoculus.jove.bibi.annotation.VecI32.class, //
        de.cadoculus.jove.bibi.annotation.VecU32.class, //
        // object 
        de.cadoculus.jove.bibi.annotation.BIBI.class };
    private final static CUEvaluator evaluator = new CUEvaluator();

    /**
     * Check whether a given field is bound by an binary binding annotation
     *
     * @param field the field to test
     * @return true, if an annotation was found
     */
    public static boolean isBoundField( Field field ) throws BIBIException {

        Set<Annotation> found = new HashSet<>();

        Annotation an = null;

        for ( Class test : NUMERIC_ANNOTATIONS ) {
            if ( ( an = field.getAnnotation( test ) ) != null ) {
                found.add( an );
            }
        }
        for ( Class test : TYPE_ANNOTATIONS ) {
            if ( ( an = field.getAnnotation( test ) ) != null ) {
                found.add( an );
            }
        }

        if ( found.isEmpty() ) {
            return false;
        } else if ( found.size() != 1 ) {
            log.error( "found multiple annotations on " + field.getDeclaringClass().getName() + "#" + field.getName() );
            log.error( "    " + found );
            throw new BIBIException( "found multiple annotation on a field" );
        }

        return true;
    }

    /**
     * Evaluate a BIBILength annotation into an integer value
     *
     * @param length the annotation
     * @param ctx the WorkingContext to be used for evaluation
     * @return the length
     */
    public static int evaluate( BIBILength length, WorkingContext ctx ) throws BIBIException {

        return evaluator.evaluate( length, ctx );
    }

    /**
     * Evaluate a BIBIValid annotation into a boolean
     *
     * @param valid the annotation
     * @param ctx the WorkingContext to be used for evaluation
     * @return the boolean
     */
    public static boolean evaluate( BIBIValid valid, WorkingContext ctx ) throws BIBIException {

        return evaluator.evaluate( valid, ctx );

    }

    /**
     * Check if the given data defines a generic List and returns the class of
     * the item.
     *
     * @param metadata the Field
     * @param type the type of the field
     * @return the type of the item or null
     * @throws BIBIException
     */
    public static Class getItemClass( AnnotatedElement metadata, Class type ) throws BIBIException {
        Class itemClass = null;

        while ( true ) {

            if ( !( metadata instanceof Field ) ) {
                break;
            }

            Field field = (Field) metadata;
            Class fieldType = field.getType();

            if ( !( Collection.class.isAssignableFrom( fieldType ) ) ) {
                break;
            }
            // Need the length annotation
            BIBILength bl = metadata.getAnnotation( BIBILength.class );
            if ( bl == null ) {
                log.warn( "no BIBILength annotation found on field " + field.getName() );
                return null;
            }

            Type returnType = field.getGenericType();
            if ( !( returnType instanceof ParameterizedType ) ) {
                log.warn( "found no generic on field " + field.getName() );
                break;
            }

            ParameterizedType paramType = (ParameterizedType) returnType;
            Type[] typeArguments = paramType.getActualTypeArguments();
            if ( typeArguments.length != 1 ) {
                log.warn( "found multiple generic types " + Arrays.toString( typeArguments ) + " on field " + field.getName() );
                break;
            }
            Type t = typeArguments[0];
            itemClass = (Class) t;
            break;
        }
        return itemClass;
    }

    /**
     * Checks that {@link WorkingContext#maxPosition} does not exceed the
     * current position of the ByteBuffer. Checks that the ByteBufer given as
     * input and in the WorkingContext are the same
     *
     * @param input the ByteBuffer
     * @param ctx the WorkingContext
     */
    public static void checkMaxPosition( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        if ( ctx == null || input == null ) {
            throw new IllegalArgumentException( "expect none null ByteBuffer and WorkingContext as input" );
        }
        if ( ctx.getByteBuffer() == null ) {
            throw new IllegalArgumentException( "no ByteBuffer given in WorkingContext" );
        }
        if ( !( ctx.getByteBuffer().equals( input ) ) ) {
            throw new IllegalArgumentException( "ByteBuffer given in WorkingContext and as input differ" );
        }
        if ( ctx.getMaxPosition() >= 0 ) {
            if ( input.position() > ctx.getMaxPosition() ) {
                throw new BIBIBufferExceededException( "position in ByteBuffer (" + input.position()
                        + ") already exceeds maximal position (" + ctx.getMaxPosition() + ")", input.position(), 0 );
            }
        }
    }

    /**
     * Checks that a future {@link WorkingContext#maxPosition} does not exceed
     * the current position of the ByteBuffer.
     *
     * @param input the ByteBuffer
     * @param ctx the WorkingContext
     */
    public static void checkMaxPosition( ByteBuffer input, WorkingContext ctx, int bytesToRead, String typeToRead ) throws BIBIBufferExceededException {
        if ( ctx == null || input == null ) {
            throw new IllegalArgumentException( "expect none null ByteBuffer and WorkingContext as input" );
        }
        if ( ctx.getByteBuffer() == null ) {
            throw new IllegalArgumentException( "no ByteBuffer given in WorkingContext" );
        }
        if ( !( ctx.getByteBuffer().equals( input ) ) ) {
            throw new IllegalArgumentException( "ByteBuffer given in WorkingContext and as input differ" );
        }

        if ( ctx.getMaxPosition() >= 0 ) {
            if ( input.position() > ctx.getMaxPosition() ) {
                throw new BIBIBufferExceededException( "position in ByteBuffer (" + input.position()
                        + ") already exceeds maximal position (" + ctx.getMaxPosition() + ")", input.position(), 0 );
            }

            if ( input.position() + bytesToRead > ctx.getMaxPosition() ) {
                throw new BIBIBufferExceededException( "continue reading " + typeToRead + " with " + bytesToRead
                        + " from current position would exceed maximal position (" + ctx.getMaxPosition() + ")", input.position(), bytesToRead );

            }
        }

    }

    /**
     * This class is used to evaluate the JavaScript expressions
     */
    private static class CUEvaluator {

        private final ScriptEngine engine;

        public CUEvaluator() {

            ScriptEngineManager smng = new ScriptEngineManager();
            engine = smng.getEngineByName( "JavaScript" );
        }

        public boolean evaluate( BIBIValid valid, WorkingContext ctx ) throws BIBIException {

            java.lang.String expression = valid.value();
            Map<java.lang.String, Object> vars = ctx.setupMap();

            if ( log.isDebugEnabled() ) {
                log.debug( "valid '" + expression + "'" );
                log.debug( "variables " + vars );
            }

            boolean retval = false;
            try {
                Bindings bindings = engine.getBindings( ScriptContext.ENGINE_SCOPE );
//                for ( Iterator<Entry<String, Object>> it = bindings.entrySet().iterator(); it.hasNext(); ) {
//                    it.remove();
//                }

                bindings.putAll( vars );

//                for ( Entry<String, Object> entry : bindings.entrySet() ) {
//                    log.info( "entry " + entry.getKey() + " = " + entry.getValue() );
//                }
                retval = (Boolean) engine.eval( expression );

                log.debug( "evaluated '" + expression + "' to " + retval );
            } catch ( Exception exp ) {
                log.error( "failed to evaluate '" + expression + "' into boolean", exp );
                log.error( "    vars " + vars );
                throw new BIBIException( "failed to evaluate '" + expression + "' into boolean", exp );
            }

            return retval;

        }

        public int evaluate( BIBILength length, WorkingContext ctx ) throws BIBIException {
            java.lang.String expression = length.value();
            Map<java.lang.String, Object> vars = ctx.setupMap();

            if ( log.isDebugEnabled() ) {
                log.debug( "length '" + expression + "'" );
                log.debug( "variables " + vars );
            }

            int retval = 0;

            try {
                Bindings bindings = engine.getBindings( ScriptContext.ENGINE_SCOPE );
                // reset context
//                for ( Iterator<Entry<String, Object>> it = bindings.entrySet().iterator(); it.hasNext(); ) {
//
//                    it.remove();
//                }
                bindings.putAll( vars );
                // JavaScript engine always returns Double value for numeric expressions
                retval = ( (Number) engine.eval( expression ) ).intValue();

                log.debug( "evaluated '" + expression + "' to " + retval );
            } catch ( Exception exp ) {
                log.error( "failed to evaluate '" + expression + "' into integer", exp );
                log.error( "    vars " + vars );
                throw new BIBIException( "failed to evaluate '" + expression + "' into integer", exp );
            }

            return retval;

        }
    }
}
