/*
 * Copyright (C) 2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.base;

import de.cadoculus.jove.bibi.BIBIException;

/**
 * This interface is used to mark elements where an reading error could be
 * confined and reading could contain with other elements. This is typically the
 * case on elements where the position of the next element is given up front,
 * e.g. SegmentHeaders or LogicalElementHeaders.
 *
 * @author cz
 */
public interface RecoverableElement {

    /**
     * Get the exception which occured when reading the element
     *
     * @return the exception
     */
    public BIBIException getException();

    /**
     * Get the LoadingStatus for this element
     *
     * @return the status
     */
    public LoadingStatus getLoadingStatus();

}
