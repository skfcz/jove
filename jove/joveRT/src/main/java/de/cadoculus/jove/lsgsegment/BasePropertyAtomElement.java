/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.1.2.1 Base Property Atom Element</h4>
<p>Base Property Atom Element represents the simplest form of a property that can exist within the LSG and
has no type specific value data associated with it.</p>

*/
@XmlRootElement(name="BasePropertyAtomElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "BasePropertyAtomElement",
    propOrder= {"basePropertyAtomData", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0x10dd104b, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97" )
public class BasePropertyAtomElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( BasePropertyAtomElement.class );

    /**  
     * The variable number 0 : BasePropertyAtomData basePropertyAtomData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="BasePropertyAtomData")
    private BasePropertyAtomData basePropertyAtomData;



    /** Getter for basePropertyAtomData.
     *  
     * 
     * @return BasePropertyAtomData
     */
    public BasePropertyAtomData getBasePropertyAtomData() {
        return basePropertyAtomData;
    }

     /** Setter for basePropertyAtomData.
     *  
     * 
     * @param value BasePropertyAtomData
     */
    public void setBasePropertyAtomData( BasePropertyAtomData value ) {
        this.basePropertyAtomData = value;
    }


    
}

