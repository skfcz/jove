/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.base;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author cz
 */
@XmlRootElement( name = "Element2Properties" )
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType(
         name = "Element2Properties",
        propOrder = { "elementObjectId", "properties" }
)

public class Element2Properties {

    private int elementObjectId;

    @XmlElementWrapper( name = "properties" )
    @XmlElements( {
        @XmlElement( name = "Key2ValueObjectId", type = Key2ValueObjectId.class )
    } )
    private List<Key2ValueObjectId> properties = new ArrayList<>();

    /**
     * @return the properties
     */
    public List<Key2ValueObjectId> getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties( List<Key2ValueObjectId> properties ) {
        this.properties = properties;
    }

    /**
     * @return the elementObjectId
     */
    public int getElementObjectId() {
        return elementObjectId;
    }

    /**
     * @param elementObjectId the elementObjectId to set
     */
    public void setElementObjectId( int elementObjectId ) {
        this.elementObjectId = elementObjectId;
    }

}
