/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl.generator;

import de.cadoculus.jove.dl.parser.JoveDL;
import de.cadoculus.jove.util.Utility;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Zerbst
 */
public class ClassGeneratorTest {

    public static Logger log = LoggerFactory.getLogger( ClassGeneratorTest.class );
    private JoveDL joveDL;
    private ClassGeneratorCfg classGeneratorCfg;
    private File testDir;

    @BeforeClass
    public static void setUpClass() throws Exception {
        BasicConfigurator.resetConfiguration();

        java.io.File logconfig = new java.io.File( "log4j.properties" );

        if ( logconfig.exists() && logconfig.canRead() ) {

            try {
                URL propsURL = new URL( "file:" + logconfig.getName() );
                PropertyConfigurator.configure( propsURL );
                log.info( "using log4j configuration from " + propsURL.toExternalForm() );
            } catch ( MalformedURLException ex ) {

                BasicConfigurator.resetConfiguration();
                BasicConfigurator.configure();

            }
        }
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
        joveDL = new JoveDL( getClass().getResourceAsStream( "/jovedl/jt95.jdl" ) );
        joveDL.parse();

        // The Configuration
        Properties cfgProps = new Properties();
        try {
            cfgProps.load( ClassGenerator.class.getResourceAsStream( "default.properties" ) );
        } catch ( IOException exp ) {
            log.error( "failed to load default configuration", exp );
            System.exit( 33 );
        }
        classGeneratorCfg = new ClassGeneratorCfg( cfgProps );

        //testDir = new File( "target/test-out/ClassGeneratorTest" );
                testDir = new File( "../joveRT/src/main/java" );
      
        testDir.mkdirs();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of generateBlocks method, of class ClassGenerator.
     */
    @Test
    public void testGenerateBlocks() throws Exception {
        log.info( "generateBlocks" );

        File targetDir = testDir;

//        Utility.deleteDirectory( targetDir );
//        assertTrue( "created " + targetDir, targetDir.mkdirs() );
//        log.info( "targetDir " + targetDir );
        ClassGenerator instance = new ClassGenerator( joveDL, classGeneratorCfg, targetDir );
        instance.generateBlocks();
        instance.generateSegments();
        

        File fileHeaderDir = new File( targetDir, "de/cadoculus/jove/fileheader" );
        assertTrue( "created " + fileHeaderDir.getAbsolutePath(), fileHeaderDir.isDirectory() );
        assertEquals( "create one file", 2, fileHeaderDir.list().length );
        

        // Utility.deleteDirectory( targetDir );
    }

    /**
     * Test of generateSegments method, of class ClassGenerator.
     */
    //@Test
    public void testGenerateSegments() throws Exception {

        File targetDir = new File( testDir, "generateSegments" );
        Utility.deleteDirectory( targetDir );
        assertTrue( "created " + targetDir, targetDir.mkdirs() );
        log.info( "targetDir " + targetDir );
        ClassGenerator instance = new ClassGenerator( joveDL, classGeneratorCfg, targetDir );
        instance.generateSegments();

        File fileHeaderDir = new File( targetDir, "de/cadoculus/jove/lsgsegment" );
        assertTrue( "created " + fileHeaderDir.getAbsolutePath(), fileHeaderDir.isDirectory() );

    }
}
