/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.fileheader.FileHeader;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.slf4j.LoggerFactory;

/**
 * The FileHeaderCodec is a specific codec used for the FileHeader. Unlike
 * standard codecs it sets the endianes on the ByteBuffer after reading the
 * FileHeader#byteOrder field
 *
 * @author Zerbst
 */
public class FileHeaderCodec extends ObjectCodec<FileHeader> {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( FileHeaderCodec.class );

    /**
     * Construct an ObjectCodec for the given type.
     *
     * @param type
     */
    public FileHeaderCodec( Class type ) throws BIBIException {
        super( FileHeader.class );
    }

    @Override
    public FileHeader decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        CodecUtility.checkMaxPosition( input, ctx );
        ctx = new WorkingContext( ctx );

        ctx.setMaxPosition( FileHeader.BYTES_PER_OBJECT );

        return super.decode( input, ctx );
    }

    @Override
    protected void postAction( Field field, Object value, WorkingContext ctx ) {
        if ( field.getName().equals( "byteOrder" ) ) {
            FileHeader header = (FileHeader) ctx.getObj();
            log.info( "header " + header );
            log.info( "header.byteOrder " + header.getByteOrder() );
            ByteOrder byteOrder = ( 0 == header.getByteOrder() ) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN;
            ctx.getByteBuffer().order( byteOrder );
            log.info( "continue with " + byteOrder );
        }
    }

    @Override
    public String toString() {
        return "FileHeaderCodec";
    }
}
