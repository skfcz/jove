/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the Aüache License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a simple data type.
 *
 * @author Carsten Zerbst
 */
public class SimpleDataType extends DataType {

    /**
     * An enumeration containing the known simple DataTypes
     */
    public enum SDTType {
        // Simple Data Types
        UChar,
        U8,
        U16,
        U32,
        U64,
        I16,
        ObjectID,
        I32,
        I64,
        F32,
        F64,
        // Composite Data Types
        BBoxF32,
        CoordF32,
        CoordF64,
        DirF32,
        GUID,
        HCoordF32,
        HCoordF64,
        MbString,
        Mx4F32,
        PlaneF32,
        Quaternion,
        RGB,
        RGBA,
        String,
        VecF32,
        VecF64,
        VecI32,
        VecU32

    };
    private final static Map<SDTType, SimpleDataType> instances = new HashMap<>();
    private final SDTType simpleType;

    private SimpleDataType( SDTType type ) {
        this.setName( type.toString() );
        this.simpleType = type;
    }

    static {
        for ( SDTType t : SDTType.values() ) {
            instances.put( t, new SimpleDataType( t ) );
        }
    }

    /**
     * Get an instance of a SimpleDataType for a given name. This is the way of
     * choice to get an instance of SimpleDataType !!!
     *
     * @param name string representation, must be compatible with
     * SDTType#toString()
     * @return the desired instance
     *
     */
    public static SimpleDataType getSimpleType( String name ) {
        SDTType t = SDTType.valueOf( name );
        return instances.get( t );
    }

    /**
     * Get an instance of a SimpleDataType for a given name. This is the way of
     * choice to get an instance of SimpleDataType !!!
     *
     * @param t the enumeration to get a SimpleDataType for
     * @return the desired instance
     */
    public static SimpleDataType getSimpleType( SDTType t ) {

        return instances.get( t );
    }

    @Override
    public String toString() {
        return simpleType.toString();
    }

}
