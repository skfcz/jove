/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;

import de.cadoculus.jove.base.*;

/** 
Polyline Type
Polyline Type specifies the type of polyline segment in Polyline Vertex Coords array. See Figure 146: Constructing Non-Text Polylines from packed 2D data arrays for interpretation of this array of type values relative to the defined polylines. Valid values include the following:

 * 
 * This enumeration contains 10 values of type I16
*/

public enum PMIPolylineType {
   
    GENERA_LINE( (short) 0 ),
    GENERAL_ARROW( (short) 1 ),
    GENERAL_CIRCLE( (short) 2 ),
    GENERAL_ARC( (short) 3 ),
    EXTENDED_LINE_1( (short) 4 ),
    EXTENDED_LINE_2( (short) 5 ),
    EXTENDED_ARC( (short) 6 ),
    EXTENDED_CIRCLE( (short) 7 ),
    TEXT_LINE( (short) 8 ),
    TEXT_STRING( (short) 9 );
    
    
    private final Short enumValue;
    private PMIPolylineType( Short v ) {
        this.enumValue = v;
    }

    /**
     * Get the integer value contained in the enumeration
     */
    public short getValue() {
        return enumValue;
    }
    /**
     * Get a PMIPolylineType for the given enumeration value
     * @param v the enumeration value
     * @return the found value
     * @throws IllegalArgumentException  if no PMIPolylineType was found for the given value
     */
    public static PMIPolylineType valueOf( Short v ) throws IllegalArgumentException {
        for ( PMIPolylineType test : PMIPolylineType.values() ) {
            if ( test.getValue() == v ) {
                return test;
            }
        }
        throw new IllegalArgumentException( "could not find PMIPolylineType for value '" + v + "'" );

    }       
    
}

