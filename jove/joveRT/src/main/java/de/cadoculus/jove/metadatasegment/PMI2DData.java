/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.1.1.1 PMI 2D Data</h4>
The PMI 2D Data collection defines a data format common to all 2D based PMI entities.

*/
@XmlRootElement(name="PMI2DData")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMI2DData",
    propOrder= {"pmiBaseData", "textEntityCount", "j2dTextData", "nonTextPolylineData", }
)
public class PMI2DData {

    public static Logger log = LoggerFactory.getLogger( PMI2DData.class );

    /**  
     * The variable number 0 : PMIBaseData pmiBaseData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMIBaseData")
    private PMIBaseData pmiBaseData;

    /**  
     * The variable number 1 : I32 textEntityCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer textEntityCount;

    /**  
     * The variable number 2 : J2DTextData[] j2dTextData
     * <br>options : <pre>{length : obj.textEntityCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.textEntityCount" )
    @XmlElementWrapper(name = "j2dTextData")
    @XmlElementRef()    private java.util.List<J2DTextData> j2dTextData;

    /**  
     * The variable number 3 : NonTextPolylineData nonTextPolylineData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="NonTextPolylineData")
    private NonTextPolylineData nonTextPolylineData;



    /** Getter for pmiBaseData.
     *  
     * 
     * @return PMIBaseData
     */
    public PMIBaseData getPmiBaseData() {
        return pmiBaseData;
    }

     /** Setter for pmiBaseData.
     *  
     * 
     * @param value PMIBaseData
     */
    public void setPmiBaseData( PMIBaseData value ) {
        this.pmiBaseData = value;
    }

    /** Getter for textEntityCount.
     *  
     * 
     * @return Integer
     */
    public Integer getTextEntityCount() {
        return textEntityCount;
    }

     /** Setter for textEntityCount.
     *  
     * 
     * @param value Integer
     */
    public void setTextEntityCount( Integer value ) {
        this.textEntityCount = value;
    }

    /** Getter for j2dTextData.
     *  
     * 
     * @return java.util.List<J2DTextData>
     */
    public java.util.List<J2DTextData> getJ2dTextData() {
        return j2dTextData;
    }

     /** Setter for j2dTextData.
     *  
     * 
     * @param value java.util.List<J2DTextData>
     */
    public void setJ2dTextData( java.util.List<J2DTextData> value ) {
        this.j2dTextData = value;
    }

    /** Getter for nonTextPolylineData.
     *  
     * 
     * @return NonTextPolylineData
     */
    public NonTextPolylineData getNonTextPolylineData() {
        return nonTextPolylineData;
    }

     /** Setter for nonTextPolylineData.
     *  
     * 
     * @param value NonTextPolylineData
     */
    public void setNonTextPolylineData( NonTextPolylineData value ) {
        this.nonTextPolylineData = value;
    }


    
}

