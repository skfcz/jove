/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.lsgsegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="GeometricTransformAttributeElement")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "GeometricTransformAttributeElement",
    propOrder= {"baseAttributeData", "versionNumberGTA", "storedValuesMask", }
)
@de.cadoculus.jove.bibi.annotation.BIBIObjectTypeID( "0x10dd1083, 0x2ac8, 0x11d1, 0x9b, 0x6b, 0x00, 0x80, 0xc7, 0xbb, 0x59, 0x97" )
@de.cadoculus.jove.bibi.annotation.BIBICodec( de.cadoculus.jove.bibi.codec.GeometricTransformAttributeElementCodec.class )
public class GeometricTransformAttributeElement extends LogicalElementHeaderZLIB {

    public static Logger log = LoggerFactory.getLogger( GeometricTransformAttributeElement.class );

    /**  
     * The variable number 0 : BaseAttributeData baseAttributeData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="BaseAttributeData")
    private BaseAttributeData baseAttributeData;

    /**  
     * The variable number 1 : I16 versionNumberGTA
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I16()    
    private Short versionNumberGTA;

    /**  
     * The variable number 2 : U16 storedValuesMask
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U16()    
    private Integer storedValuesMask;



    /** Getter for baseAttributeData.
     *  
     * 
     * @return BaseAttributeData
     */
    public BaseAttributeData getBaseAttributeData() {
        return baseAttributeData;
    }

     /** Setter for baseAttributeData.
     *  
     * 
     * @param value BaseAttributeData
     */
    public void setBaseAttributeData( BaseAttributeData value ) {
        this.baseAttributeData = value;
    }

    /** Getter for versionNumberGTA.
     *  
     * 
     * @return Short
     */
    public Short getVersionNumberGTA() {
        return versionNumberGTA;
    }

     /** Setter for versionNumberGTA.
     *  
     * 
     * @param value Short
     */
    public void setVersionNumberGTA( Short value ) {
        this.versionNumberGTA = value;
    }

    /** Getter for storedValuesMask.
     *  
     * 
     * @return Integer
     */
    public Integer getStoredValuesMask() {
        return storedValuesMask;
    }

     /** Setter for storedValuesMask.
     *  
     * 
     * @param value Integer
     */
    public void setStoredValuesMask( Integer value ) {
        this.storedValuesMask = value;
    }


    // Verbatim Java Code from DL
      
    // the number of floats to read. Is calculated at runtime based onthe storedValuesMask
    private int numFloats=0;
    // the calculated homogeneous coordinate system transformation
    private   javax.vecmath.Matrix4d hoco = new javax.vecmath.Matrix4d();

    /**
     * Get the homogeneous coordinate transformation 
     */
    public javax.vecmath.Matrix4d getHoco() {
        return hoco;
    }
        
        
    
}

