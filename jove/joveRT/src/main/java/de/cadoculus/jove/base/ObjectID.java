/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.base;

import de.cadoculus.jove.xml.ObjectIDAdapter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * A wrapper class for an integer Object ID
 *
 * @author  cz
 */
@XmlJavaTypeAdapter( ObjectIDAdapter.class )
public class ObjectID {
    private static final String PREFIX = "cadOCID";
    private static final Pattern PATTERN = Pattern.compile( PREFIX + "([0-9]+)" );
    private static int LAST_ID = 0;
    private int id;

    /**
     * Create a new ObjectId object containing a per JVM unique non negative id value
     */
    public ObjectID() {
        this.id = LAST_ID++;
    }

    /**
     * Creates a new ObjectId for a given value
     *
     * @param  id  the encapsulated id
     */
    public ObjectID( int id ) {
        this.id = id;
    }

    /**
     * Creates a new ObjectId for the string representation
     *
     * @param  stringRep  the string to parse, e.g. <code>id122</code>
     */
    public ObjectID( String stringRep ) {
        Matcher m = PATTERN.matcher( stringRep );

        if ( !m.matches() ) {
            throw new IllegalArgumentException( "given string '" + stringRep +
                "' is not matched by pattern '" + PATTERN.pattern() + "'" );
        }

        this.id = Integer.parseInt( m.group( 1 ) );

    }

    /**
     * {@inheritDoc}
     */
    @Override public boolean equals( Object obj ) {

        return ( ( obj instanceof ObjectID ) && ( hashCode() == obj.hashCode() ) );
    }

    /**
     * {@inheritDoc}
     */
    @Override public int hashCode() {
        return 20091210 + id;
    }

    /**
     * {@inheritDoc}
     */
    @Override public String toString() {
        return PREFIX + id;
    }

    /**
     * Get the underlying integr value
     *
     * @return  the value
     */
    public int getValue() {
        return id;
    }

}
