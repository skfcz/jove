/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.3 PMI User Attributes</h4>
The PMI User Attributes collection defines data for a list of user attributes. PMI User Attributes are used to add attribute data to a part/assembly. Each user attribute is composed of key/value pair of strings.

*/
@XmlRootElement(name="PMIUserAttributes")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIUserAttributes",
    propOrder= {"userAttributeCount", "userAttributes", }
)
public class PMIUserAttributes {

    public static Logger log = LoggerFactory.getLogger( PMIUserAttributes.class );

    /**  
     * The variable number 0 : I32 userAttributeCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer userAttributeCount;

    /**  
     * The variable number 1 : PMIUserAttribute[] userAttributes
     * <br>options : <pre>{length : obj.userAttributeCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.userAttributeCount" )
    @XmlElementWrapper(name = "userAttributes")
    @XmlElementRef()    private java.util.List<PMIUserAttribute> userAttributes;



    /** Getter for userAttributeCount.
     *  
     * 
     * @return Integer
     */
    public Integer getUserAttributeCount() {
        return userAttributeCount;
    }

     /** Setter for userAttributeCount.
     *  
     * 
     * @param value Integer
     */
    public void setUserAttributeCount( Integer value ) {
        this.userAttributeCount = value;
    }

    /** Getter for userAttributes.
     *  
     * 
     * @return java.util.List<PMIUserAttribute>
     */
    public java.util.List<PMIUserAttribute> getUserAttributes() {
        return userAttributes;
    }

     /** Setter for userAttributes.
     *  
     * 
     * @param value java.util.List<PMIUserAttribute>
     */
    public void setUserAttributes( java.util.List<PMIUserAttribute> value ) {
        this.userAttributes = value;
    }


    
}

