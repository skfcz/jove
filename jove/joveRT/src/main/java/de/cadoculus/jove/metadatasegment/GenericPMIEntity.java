/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="GenericPMIEntity")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "GenericPMIEntity",
    propOrder= {"pmi2dData", "propertyCount", "properties", "entityTypeNameStringID", "parentTypeNameStringID", "entityType", "parentType", "userFlags", }
)
public class GenericPMIEntity {

    public static Logger log = LoggerFactory.getLogger( GenericPMIEntity.class );

    /**  
     * The variable number 0 : PMI2DData pmi2dData
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @XmlElement( name="PMI2DData")
    private PMI2DData pmi2dData;

    /**  
     * The variable number 1 : I32 propertyCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer propertyCount;

    /**  
     * The variable number 2 : PMIProperty[] properties
     * <br>options : <pre>{length : obj.propertyCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.propertyCount" )
    @XmlElementWrapper(name = "properties")
    @XmlElementRef()    private java.util.List<PMIProperty> properties;

    /**  
     * The variable number 3 : I32 entityTypeNameStringID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer entityTypeNameStringID;

    /**  
     * The variable number 4 : I32 parentTypeNameStringID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer parentTypeNameStringID;

    /**  
     * The variable number 5 : U16 entityType
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U16()    
    private Integer entityType;

    /**  
     * The variable number 6 : U16 parentType
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.U16()    
    private Integer parentType;

    /**  
     * The variable number 7 : U16 userFlags
     * <br>options : <pre>{valid : pmiVersionNumber > 6}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.U16()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber > 6" )
    private Integer userFlags;



    /** Getter for pmi2dData.
     *  
     * 
     * @return PMI2DData
     */
    public PMI2DData getPmi2dData() {
        return pmi2dData;
    }

     /** Setter for pmi2dData.
     *  
     * 
     * @param value PMI2DData
     */
    public void setPmi2dData( PMI2DData value ) {
        this.pmi2dData = value;
    }

    /** Getter for propertyCount.
     *  
     * 
     * @return Integer
     */
    public Integer getPropertyCount() {
        return propertyCount;
    }

     /** Setter for propertyCount.
     *  
     * 
     * @param value Integer
     */
    public void setPropertyCount( Integer value ) {
        this.propertyCount = value;
    }

    /** Getter for properties.
     *  
     * 
     * @return java.util.List<PMIProperty>
     */
    public java.util.List<PMIProperty> getProperties() {
        return properties;
    }

     /** Setter for properties.
     *  
     * 
     * @param value java.util.List<PMIProperty>
     */
    public void setProperties( java.util.List<PMIProperty> value ) {
        this.properties = value;
    }

    /** Getter for entityTypeNameStringID.
     *  
     * 
     * @return Integer
     */
    public Integer getEntityTypeNameStringID() {
        return entityTypeNameStringID;
    }

     /** Setter for entityTypeNameStringID.
     *  
     * 
     * @param value Integer
     */
    public void setEntityTypeNameStringID( Integer value ) {
        this.entityTypeNameStringID = value;
    }

    /** Getter for parentTypeNameStringID.
     *  
     * 
     * @return Integer
     */
    public Integer getParentTypeNameStringID() {
        return parentTypeNameStringID;
    }

     /** Setter for parentTypeNameStringID.
     *  
     * 
     * @param value Integer
     */
    public void setParentTypeNameStringID( Integer value ) {
        this.parentTypeNameStringID = value;
    }

    /** Getter for entityType.
     *  
     * 
     * @return Integer
     */
    public Integer getEntityType() {
        return entityType;
    }

     /** Setter for entityType.
     *  
     * 
     * @param value Integer
     */
    public void setEntityType( Integer value ) {
        this.entityType = value;
    }

    /** Getter for parentType.
     *  
     * 
     * @return Integer
     */
    public Integer getParentType() {
        return parentType;
    }

     /** Setter for parentType.
     *  
     * 
     * @param value Integer
     */
    public void setParentType( Integer value ) {
        this.parentType = value;
    }

    /** Getter for userFlags.
     *  
     * 
     * @return Integer
     */
    public Integer getUserFlags() {
        return userFlags;
    }

     /** Setter for userFlags.
     *  
     * 
     * @param value Integer
     */
    public void setUserFlags( Integer value ) {
        this.userFlags = value;
    }


    
}

