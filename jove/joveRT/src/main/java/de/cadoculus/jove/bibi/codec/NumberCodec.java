/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.F32;
import de.cadoculus.jove.bibi.annotation.F64;
import de.cadoculus.jove.bibi.annotation.I16;
import de.cadoculus.jove.bibi.annotation.I32;
import de.cadoculus.jove.bibi.annotation.U16;
import de.cadoculus.jove.bibi.annotation.U32;
import de.cadoculus.jove.bibi.annotation.U8;
import java.nio.ByteBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Reads a single byte from the given ByteBuffer into a Number or Byte.
 * Supported combinations:
 *
 * <pre>
 * F32 : Float
 * F64 : Double
 * I16 : Short
 * I32 : Integer
 * U16 : Integer
 * U32 : Long
 * U8  : Short
 * U8 : Byte
 * </pre>
 *
 * @author Zerbst
 */
public class NumberCodec<T> implements Codec<T> {

    private static Logger log = LoggerFactory.getLogger( NumberCodec.class );
    private final Class type;
    private final Class annotation;

    NumberCodec( Class annotation, Class type ) {
        this.annotation = annotation;
        this.type = type;
    }

    @Override
    public T decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        MDC.put( WorkingContext.OBJECT_TYPE, type.getSimpleName() );
        MDC.put( WorkingContext.FIELD, "" );
        MDC.put( WorkingContext.POSITION, Long.toString( input.position() ) );
        MDC.put( WorkingContext.CODEC, getClass().getSimpleName() );

        CodecUtility.checkMaxPosition( input, ctx );

        Object retval = null;
        log.debug( "decode " + annotation + ", " + type.getSimpleName() );
        if ( F32.class == annotation ) {
            retval = new Float( input.getFloat() );
        } else if ( F64.class == annotation ) {
            retval = new Double( input.getDouble() );
        } else if ( I16.class == annotation ) {
            retval = new Short( input.getShort() );
        } else if ( I32.class == annotation ) {
            retval = new Integer( input.getInt() );
        } else if ( U16.class == annotation ) {
            retval = new Integer( (int) ( input.getShort() & 0xffff ) );
        } else if ( U32.class == annotation ) {
            retval = new Long( ( (long) input.getInt() & 0xffffffffL ) );
        } else if ( U8.class == annotation ) {
            if ( Byte.class == type ) {
                retval = new Byte( input.get() );
            } else {
                retval = new Short( ( (short) ( input.get() & 0xff ) ) );
            }
        } else {
            throw new IllegalStateException( "got unsupported annotation " + annotation );
        }
        MDC.put( WorkingContext.POSITION, Long.toString( input.position() ) );
        return (T) retval;
    }

    @Override
    public String toString() {
        return "NumberCodec " + annotation.getSimpleName() + ", " + type.getSimpleName();
    }
}
