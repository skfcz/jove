/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl.parser;

import de.cadoculus.jove.dl.BlockDeclaration;
import de.cadoculus.jove.dl.EnumerationDeclaration;
import de.cadoculus.jove.dl.SegmentDeclaration;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class used to extract information from a Jove DL (.jdl) file. It use the
 * automatically generated parser and some tree walkers to parse, validate and
 * extract information from the JDL file. As result one gets POJOs containing
 * the description as found in the .jdl file.
 *
 * @author Carsten Zerbst
 */
public class JoveDL {

    public static Logger log = LoggerFactory.getLogger( JoveDL.class );
    public static String PROP_SEGMENT_TYPE = "segmentType";
    public static String PROP_ZLIB_APPLIED = "zlibApplied";
    private final InputStream in;
    private ModelBuilderVisitor mvis;

    public JoveDL( InputStream in ) {
        if ( in == null ) {
            throw new IllegalArgumentException( "expect non null InputStream" );
        }
        this.in = in;
    }

    /**
     * Get the list of defined enumerations. Only availabel after {@link #parse()
     * }
     * was run successfully.
     *
     * @return the defined enumerations
     */
    public Set<EnumerationDeclaration> getEnumerations() {
        if ( mvis == null ) {
            throw new IllegalStateException( "call JoveDL#parse before retrieving the enumerations" );
        }
        return mvis.getEnumerations();
    }

    /**
     * Get the list of defined blocks. Only availabel after {@link #parse() }
     * was run successfully.
     *
     * @return the defined blocks
     */
    public Set<BlockDeclaration> getBlocks() {
        if ( mvis == null ) {
            throw new IllegalStateException( "call JoveDL#parse before retrieving the blocks" );
        }
        return mvis.getBlocks();
    }

    /**
     * Get the list of defined segments Only availabel after {@link #parse() }
     * was run successfully.
     *
     * @return the defined segments
     */
    public Set<SegmentDeclaration> getSegments() {
        if ( mvis == null ) {
            throw new IllegalStateException( "call JoveDL#parse before retrieving the segments" );
        }
        return mvis.getSegments();
    }

    /**
     * Start the process to parse a jdl file from the given input stream.
     *
     * @throws IllegalStateException if something goes wron
     */
    public void parse() throws IllegalStateException {

        SimpleNode node = null;
        try {
            JoveDLParser parser = new JoveDLParser( in );
            node = parser.JoveDL();
            in.close();
        } catch ( ParseException exp ) {
            log.error( "an error occured during parsing ", exp );
            log.error( "if you want to see more information, set the log level for the logger 'parser' to debug" );
            throw new IllegalStateException( "an error occured during parsing ", exp );
        } catch ( IOException exp ) {
            throw new IllegalStateException( "an IO error occured during parsing ", exp );
        }

        log.info( "finished building AST" );

        TypeCollectorVisitor vis = new TypeCollectorVisitor();
        node.jjtAccept( vis, null );

        log.info( "finished checking types" );

        mvis = new ModelBuilderVisitor( vis.getAllDefinedTypes(), vis.getAllDefinedEnums(), vis.getEnumerationNames() );
        node.jjtAccept( mvis, null );

        log.info( "finished extracting information" );

    }
}
