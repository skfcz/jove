/*
 * Copyright (C) 2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.base.BBoxF32;
import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.BIBICompression;
import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.raida.jcadlib.cadimport.jt.reader.UnsupportedCodecException;
import java.awt.Color;
import java.lang.reflect.AnnotatedElement;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point4d;
import javax.vecmath.Point4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Reads composite types or list of composites types. Supported annotation /
 * java type combination:
 * 
*
 * <table><tr><td>annotation</td><td>java class</td></tr>
 * <tr><td> de.cadoculus.jove.bibi.annotation.BBoxF32 </td><td>
 * de.cadoculus.jove.base.BBoxF32</td></tr>
 * <tr><td> de.cadoculus.jove.bibi.annotation.CoordF32 </td><td>
 * javax.vecmath.Point3f</td></tr>
 * <tr><td> de.cadoculus.jove.bibi.annotation.CoordF64 </td><td>
 * javax.vecmath.Point3d</td></tr>
 * <tr><td> de.cadoculus.jove.bibi.annotation.DirF32 </td><td>
 * javax.vecmath.Vector3f</td></tr>
 * <tr><td>de.cadoculus.jove.bibi.annotation.HCoordF32 </td><td>
 * javax.vecmath.Point4f</td></tr>
 * <tr><td>de.cadoculus.jove.bibi.annotation.HCoordF64 </td><td>
 * javax.vecmath.Point4d</td></tr>
 * <tr><td>de.cadoculus.jove.bibi.annotation.Mx4F32 </td><td>
 * javax.vecmath.Matrix4f</td></tr>
 * <tr><td>de.cadoculus.jove.bibi.annotation.PlaneF32 </td><td>
 * javax.vecmath.Vector4f</td></tr>
 * <tr><td>de.cadoculus.jove.bibi.annotation.Quaternion </td><td>
 * javax.vecmath.Vector4f</td></tr>
 * <tr><td>de.cadoculus.jove.bibi.annotation.RGB </td><td>
 * java.awt.Color</td></tr>
 * <tr><td>de.cadoculus.jove.bibi.annotation.RGBA </td><td>
 * java.awt.Color</td></tr>
 * <tr><td>de.cadoculus.jove.bibi.annotation.VecF32 </td><td>
 * java.util.List</td></tr>
 * <tr><td>de.cadoculus.jove.bibi.annotation.VecF64 </td><td>
 * java.util.List</td></tr>
 * <tr><td>de.cadoculus.jove.bibi.annotation.VecI32 </td><td>
 * java.util.List</td></tr>
 * <tr><td></td></tr>de.cadoculus.jove.bibi.annotation.VecU32 &&
 * java.util.List</td></tr>
 * </table>
 *
 * @author Zerbst
 */
public class CompositeTypeCodec<T> implements Codec<T> {

    private static Logger log = LoggerFactory.getLogger( CompositeTypeCodec.class );
    private final Class type;
    private final AnnotatedElement metadata;
    private final Class annotation;

    CompositeTypeCodec( AnnotatedElement metadata, Class annotation, Class<T> type ) {
        this.metadata = metadata;
        this.annotation = annotation;
        this.type = type;
    }

    @Override
    public T decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        MDC.put( WorkingContext.OBJECT_TYPE, type.getSimpleName() );
        MDC.put( WorkingContext.FIELD, "" );
        MDC.put( WorkingContext.POSITION, Long.toString( input.position() ) );
        MDC.put( WorkingContext.CODEC, getClass().getSimpleName() );

        CodecUtility.checkMaxPosition( input, ctx );

        try {

            if ( annotation == de.cadoculus.jove.bibi.annotation.BBoxF32.class ) {

                int length = 2 * 3 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "BBoxF32" );

                float x0 = input.getFloat();
                float y0 = input.getFloat();
                float z0 = input.getFloat();

                float x1 = input.getFloat();
                float y1 = input.getFloat();
                float z1 = input.getFloat();

                BBoxF32 bbox = new BBoxF32();
                bbox.setMin( new Point3f( x0, y0, z0 ) );
                bbox.setMax( new Point3f( x1, y1, z1 ) );

                return (T) bbox;

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.CoordF32.class ) {

                int length = 3 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "CoordF32" );

                float x0 = input.getFloat();
                float y0 = input.getFloat();
                float z0 = input.getFloat();
                return (T) new Point3f( x0, y0, z0 );

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.CoordF64.class ) {

                int length = 3 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "CoordF64" );

                double x0 = input.getDouble();
                double y0 = input.getDouble();
                double z0 = input.getDouble();
                return (T) new Point3d( x0, y0, z0 );

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.DirF32.class ) {

                int length = 3 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "DirF32" );

                float x0 = input.getFloat();
                float y0 = input.getFloat();
                float z0 = input.getFloat();
                return (T) new Vector3f( x0, y0, z0 );

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.HCoordF32.class ) {

                int length = 4 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "HCoordF32" );

                float x0 = input.getFloat();
                float y0 = input.getFloat();
                float z0 = input.getFloat();
                float w = input.getFloat();
                return (T) new Point4f( x0, y0, z0, w );

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.HCoordF64.class ) {

                int length = 4 * 8;
                CodecUtility.checkMaxPosition( input, ctx, length, "HCoordF64" );

                double x0 = input.getDouble();
                double y0 = input.getDouble();
                double z0 = input.getDouble();
                double w = input.getDouble();
                return (T) new Point4d( x0, y0, z0, w );

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.Mx4F32.class ) {
                int length = 16 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "Mx4F32" );

                Matrix4f mat = new Matrix4f();
                for ( int row = 0; row < 4; row++ ) {
                    for ( int column = 0; column < 4; column++ ) {
                        mat.setElement( row, column, input.getFloat() );
                    }
                }
                return (T) mat;

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.PlaneF32.class ) {

                int length = 4 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "PlaneF32" );

                float x0 = input.getFloat();
                float y0 = input.getFloat();
                float z0 = input.getFloat();
                float w = input.getFloat();
                return (T) new Vector4f( x0, y0, z0, w );

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.Quaternion.class ) {

                int length = 4 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "Quaternion" );

                float x0 = input.getFloat();
                float y0 = input.getFloat();
                float z0 = input.getFloat();
                float w = input.getFloat();
                return (T) new Vector4f( x0, y0, z0, w );

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.RGB.class ) {

                int length = 3 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "RGB" );

                return (T) new Color( input.getFloat(), input.getFloat(), input.getFloat() );

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.RGBA.class ) {

                int length = 4 * 32;
                CodecUtility.checkMaxPosition( input, ctx, length, "RGBA" );

                return (T) new Color( input.getFloat(), input.getFloat(), input.getFloat(), input.getFloat() );

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.VecF32.class ) {
                // TODO: check for further annotation regarding compression
                List<Float> retval = new ArrayList<Float>();

                int length = 1 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "VecF32.count" );

                int count = input.getInt();

                length = count * 4;
                CodecUtility.checkMaxPosition( input, ctx, count, "VecF32.floats" );

                for ( int i = 0; i < count; i++ ) {
                    retval.add( input.getFloat() );
                }

                return (T) retval;
            } else if ( annotation == de.cadoculus.jove.bibi.annotation.VecF64.class ) {
                // TODO: check for further annotation regarding compression
                List<Double> retval = new ArrayList<Double>();

                int length = 1 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "VecF64.count" );

                int count = input.getInt();

                length = count * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "VecF64.doubles" );

                for ( int i = 0; i < count; i++ ) {
                    retval.add( input.getDouble() );
                }

                return (T) retval;

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.VecI32.class ) {

                BIBICompression compression = metadata.getAnnotation( BIBICompression.class );

                if ( compression == null ) {

                    List<Integer> retval = new ArrayList<Integer>();

                    int length = 1 * 4;
                    CodecUtility.checkMaxPosition( input, ctx, length, "VecI32.count" );

                    int count = input.getInt();

                    length = count * 4;
                    CodecUtility.checkMaxPosition( input, ctx, length, "VecI32.ints" );

                    for ( int i = 0; i < count; i++ ) {
                        retval.add( input.getInt() );
                    }

                    return (T) retval;
                } else {
                    CompressionType comprType = compression.compression();
                    PredictorType predType = compression.predictor();

                    return (T) runJCadLib( comprType, predType, input, ctx );

                }

            } else if ( annotation == de.cadoculus.jove.bibi.annotation.VecU32.class ) {
                // TODO: check for further annotation regarding compression
                List<Long> retval = new ArrayList<Long>();

                int length = 1 * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "VecU32.count" );

                int count = input.getInt();

                length = count * 4;
                CodecUtility.checkMaxPosition( input, ctx, length, "VecU32.longs" );

                for ( int i = 0; i < count; i++ ) {
                    retval.add( new Long( ( (long) input.getInt() & 0xffffffffL ) ) );
                }

                return (T) retval;
            }
        } catch ( Exception exp ) {
            log.error( "failed to decode " + annotation, exp );
            throw new BIBIException( "failed to decode " + annotation, exp );
        }
        throw new IllegalStateException( "got unsupported annotation " + annotation );

    }

    private T runJCadLib( CompressionType comprType, PredictorType predType, ByteBuffer input, WorkingContext ctx ) throws UnsupportedCodecException {
        // read the compressed vector using the JCadLib libraries
        de.raida.jcadlib.cadimport.jt.codec.PredictorType jclPredType = null;
        switch ( predType ) {
            case Lag1:
                jclPredType = de.raida.jcadlib.cadimport.jt.codec.PredictorType.PredLag1;
                break;
            case Lag2:
                jclPredType = de.raida.jcadlib.cadimport.jt.codec.PredictorType.PredLag2;
                break;
            case NULL_PRED:
                jclPredType = de.raida.jcadlib.cadimport.jt.codec.PredictorType.PredNULL;
                break;
            case Ramp:
                jclPredType = de.raida.jcadlib.cadimport.jt.codec.PredictorType.PredRamp;
                break;
            case Stride1:
                jclPredType = de.raida.jcadlib.cadimport.jt.codec.PredictorType.PredStride1;
                break;
            case Stride2:
                jclPredType = de.raida.jcadlib.cadimport.jt.codec.PredictorType.PredStride2;
                break;
            case StripIndex:
                jclPredType = de.raida.jcadlib.cadimport.jt.codec.PredictorType.PredStripIndex;
                break;
            case Xor1:
                jclPredType = de.raida.jcadlib.cadimport.jt.codec.PredictorType.PredXor1;
                break;
            case Xor2:
                jclPredType = de.raida.jcadlib.cadimport.jt.codec.PredictorType.PredXor2;
                break;
            default:
                throw new IllegalStateException( "got unsupported predictor type " + predType );
        }

        switch ( comprType ) {
            case Int32CDP:
                if ( annotation == de.cadoculus.jove.bibi.annotation.VecI32.class ) {
                    return (T) de.raida.jcadlib.cadimport.jt.codec.Int32CDP.readVecI32( ctx, jclPredType );
                } else if ( annotation == de.cadoculus.jove.bibi.annotation.VecU32.class ) {
                    return (T) de.raida.jcadlib.cadimport.jt.codec.Int32CDP.readVecU32( ctx, jclPredType );
                } else {
                    throw new IllegalStateException( "got unsupported annotation " + annotation + " with Int32CDP" );
                }
            case Int32CDP2:
                if ( annotation == de.cadoculus.jove.bibi.annotation.VecI32.class ) {
                    return (T) de.raida.jcadlib.cadimport.jt.codec.Int32CDP2.readVecI32( ctx, jclPredType );
                } else if ( annotation == de.cadoculus.jove.bibi.annotation.VecU32.class ) {
                    return (T) de.raida.jcadlib.cadimport.jt.codec.Int32CDP2.readVecU32( ctx, jclPredType );
                } else {
                    throw new IllegalStateException( "got unsupported annotation " + annotation + " with Int32CDP2" );
                }
            // case Float64CDP:
            default:
                throw new IllegalStateException( "got unsupported annotation " + annotation + " with compression " + comprType );
        }

    }

    @Override
    public String toString() {
        return "CompositeTypeCodec " + annotation.getSimpleName() + ", " + type.getSimpleName();
    }
}
