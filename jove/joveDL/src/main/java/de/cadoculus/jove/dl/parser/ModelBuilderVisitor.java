/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.dl.parser;

import static de.cadoculus.jove.dl.parser.JoveDL.*;
import de.cadoculus.jove.dl.BlockDeclaration;
import de.cadoculus.jove.dl.DataType;
import de.cadoculus.jove.dl.EntityDeclaration;
import de.cadoculus.jove.dl.EnumValue;
import de.cadoculus.jove.dl.EnumerationDeclaration;
import de.cadoculus.jove.dl.SegmentDeclaration;
import de.cadoculus.jove.dl.SimpleDataType;
import de.cadoculus.jove.dl.VariableDeclaration;
import de.cadoculus.jove.model.GUID;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ModelBuilderVisitor is used to collect the complete model information
 * from the AST. It needs the input from the TypeCollectorVisitor as input:
 * <pre>
 * JoveDLParser parser = new JoveDLParser(in);
 * SimpleNode node = parser.JoveDL();
 * in.close();
 *
 * TypeCollectorVisitor vis = new TypeCollectorVisitor();
 * node.jjtAccept(vis, null);
 *
 * ModelBuilderVisitor mvis = new ModelBuilderVisitor( vis.getAllDefinedTypes());
 * node.jjtAccept(mvis, null);
 * </pre>
 *
 * @author Carsten Zerbst
 */
class ModelBuilderVisitor implements JoveDLParserVisitor {

    public static Logger log = LoggerFactory.getLogger( ModelBuilderVisitor.class );
    private static final String CURRENT_BL_OR_SEG = "currentBlockOrSegment";
    private final static String NAME_2_ENTITY = "name2entity";
    private final static String NAME_2_ENUM = "name2enum";
    private final static String FORMAL_COMMENT = "formalComment";
    private final static String VARIABLE_DECL = "variableDeclaration";
    private final static String LOGICAL_ELEMENT_HEADER = "LogicalElementHeader";
    private int indent = 0;
    private Map<String, Set<String>> blOrSeg2defEntityNames = new HashMap<>();
    private final Set<BlockDeclaration> blocks = new HashSet<>();
    private final Set<SegmentDeclaration> segments = new HashSet<>();
    private EntityDeclaration elementHeader;
    private Map<String, EnumerationDeclaration> name2enum = new HashMap<>();
    private final Map<String, Set<String>> blOrSeg2defEnumNames;

    /**
     * Create a new ModelBuilderVisitorAdapter
     *
     * @param blOrSeg2defEntityNames the result of the
     * {@link TypeCollectorVisitor#getAllDefinedTypes()}
     */
    public ModelBuilderVisitor( Map<String, Set<String>> blOrSeg2defEntityNames,
            Map<String, Set<String>> blOrSeg2defEnumNames,
            Set<String> enumerationNames ) {

        this.blOrSeg2defEntityNames = blOrSeg2defEntityNames;
        this.blOrSeg2defEnumNames = blOrSeg2defEnumNames;
        this.elementHeader = new EntityDeclaration();
        this.elementHeader.setName( LOGICAL_ELEMENT_HEADER );

        for ( String enumName : enumerationNames ) {

            EnumerationDeclaration entity = new EnumerationDeclaration();
            entity.setName( enumName );
            name2enum.put( enumName, entity );
        }

    }

    /**
     * Get a set of parsed BlockDeclarations
     */
    public Set<BlockDeclaration> getBlocks() {
        return blocks;
    }

    /**
     * Get a set of parsed SegmentDeclaration
     */
    public Set<SegmentDeclaration> getSegments() {
        return segments;
    }

    /**
     * Get the parsed EnumerationDeclaration
     */
    public Set<EnumerationDeclaration> getEnumerations() {
        return new HashSet( name2enum.values() );
    }

    @Override
    public Object visit( SimpleNode node, java.util.Map<String, Object> data ) {
        log.info( "SimpleNode " + node.toString() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public Object visit( ASTJoveDL node, java.util.Map<String, Object> unused ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        Map<String, Object> myData = new HashMap<>();

        for ( int i = 0; i < node.jjtGetNumChildren(); i++ ) {
            Node child = node.jjtGetChild( i );
            Object val = child.jjtAccept( this, myData );
            if ( child instanceof ASTBlockDeclaration ) {
                blocks.add( (BlockDeclaration) val );
            } else if ( child instanceof ASTSegmentDeclaration ) {
                segments.add( (SegmentDeclaration) val );
            } else if ( child instanceof ASTFormalComment ) {
                // OK
            } else if ( child instanceof ASTEnumerationDeclaration ) {
                // OK
            } else {
                log.warn( "unsupported return value for ASTJoveNode child " + child + " : " + val );
            }
        }

        //node.childrenAccept( this, myData );
        indent( -1 );

        check();
        return null;

    }

    @Override
    public Object visit( ASTSegmentDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        String segmentName = (String) node.jjtGetValue();
        data.put( CURRENT_BL_OR_SEG, segmentName );

        // Create empty Entities to be filled later on based on the 
        // information collected by the TypeCollectorVisitorAdapter.
        // These objects also work as forward declaration for the types used
        // in variables.
        Set<String> entityNames = blOrSeg2defEntityNames.get( segmentName );

        Map<String, EntityDeclaration> name2entity = new HashMap<>();
        for ( String entityName : entityNames ) {

            EntityDeclaration entity = new EntityDeclaration();
            entity.setName( entityName );
            name2entity.put( entityName, entity );
        }
        EntityDeclaration entity = new EntityDeclaration();
        entity.setName( "LogicalElementHeaderZLIB" );
        name2entity.put( entity.getName(), entity );
        name2entity.put( LOGICAL_ELEMENT_HEADER, elementHeader );
        data.put( NAME_2_ENTITY, name2entity );

        Set<String> enumNames = blOrSeg2defEnumNames.get( segmentName );

        Map<String, EnumerationDeclaration> name2enum = new HashMap<>();
        for ( String enumName : enumNames ) {

            EnumerationDeclaration enumeration = new EnumerationDeclaration();
            enumeration.setName( enumName );
            name2enum.put( enumName, enumeration );
        }
        data.put( NAME_2_ENUM, name2enum );

        SegmentDeclaration segment = new SegmentDeclaration();
        segment.setName( segmentName );
        segment.setComment( handleComment( (String) data.remove( FORMAL_COMMENT ) ) );

        for ( int i = 0; i < node.jjtGetNumChildren(); i++ ) {
            Node child = node.jjtGetChild( i );
            Object val = child.jjtAccept( this, data );
            log.debug( indent( 0 ) + "child " + child + " : " + val );
            if ( val instanceof EntityDeclaration ) {
                segment.getEntities().add( (EntityDeclaration) val );
            } else if ( val instanceof EnumerationDeclaration ) {
                segment.getEnumerations().add( (EnumerationDeclaration) val );
            } else if ( val instanceof Map ) {
                segment.getProperties().putAll( (Map<? extends String, ? extends Object>) val );
            } else if ( val instanceof String ) {
                // formal comment, not handled here
            } else {
                throw new IllegalStateException( "got unsupported value " + val + " ("
                        + ( val != null ? val.getClass() : "" ) + ") as child of segment declaration" );
            }
        }

        log.debug( indent( 0 ) + " segment " + segment );

        data.put( CURRENT_BL_OR_SEG, null );

        return segment;

    }

    @Override
    public VariableDeclaration visit( ASTVariableDeclaration node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        VariableDeclaration vdec = new VariableDeclaration();
        vdec.setName( (String) node.jjtGetValue() );
        vdec.setComment( handleComment( (String) data.remove( FORMAL_COMMENT ) ) );
        data.put( VARIABLE_DECL, vdec );

        for ( int i = 0; i < node.jjtGetNumChildren(); i++ ) {
            Node child = node.jjtGetChild( i );
            Object val = child.jjtAccept( this, data );
            if ( val instanceof DataType ) {
                vdec.setType( (DataType) val );
            } else if ( child instanceof ASTArrayDeclaration ) {
                vdec.setArray( true );
            } else if ( val instanceof Map ) {
                vdec.getProperties().putAll( (Map<? extends String, ? extends Object>) val );
            }
        }
        data.remove( VARIABLE_DECL );
        log.debug( indent( 0 ) + " var " + vdec );
        indent( -1 );
        return vdec;

    }

    /**
     *
     * @param node an ASTSimpleTypeDeclaration node
     * @param data
     * @return
     */
    @Override
    public SimpleDataType visit( ASTSimpleTypeDeclaration node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        indent( -1 );
        SimpleDataType retval = SimpleDataType.getSimpleType( (String) node.jjtGetValue() );

        node.childrenAccept( this, data );

        return retval;

    }

    /**
     *
     * @param node a ASTGenericParameterDeclaration
     * @param data unused
     * @return a Map containing the collected options
     */
    @Override
    public Map<String, Object> visit( ASTGenericParameterDeclaration node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        Map<String, Object> options = new HashMap<>();

        node.childrenAccept( this, options );

        log.debug( indent( 0 ) + "collected options " + options );

        indent( -1 );
        return options;

    }

    /**
     *
     * @param node a ASTGenericParameterDeclaration
     * @param data a Map which is filled with found key / value pairs
     * @return null
     */
    @Override
    public Object visit( ASTParameterNameAndValue node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        Object value = node.jjtGetChild( 0 ).jjtAccept( this, data );
        data.put( (String) node.jjtGetValue(), value );

        indent( -1 );
        return null;

    }

    /**
     *
     * @param node a ASTParameterValue node
     * @param data unused
     * @return the parsed value
     */
    @Override
    public Object visit( ASTParameterValue node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );

        return node.jjtGetValue();

    }

    @Override
    public BlockDeclaration visit( ASTBlockDeclaration node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        String blockName = (String) node.jjtGetValue();
        // Create empty Entities to be filled later on based on the 
        // information collected by the TypeCollectorVisitorAdapter
        Set<String> entityNames = blOrSeg2defEntityNames.get( blockName );

        Map<String, EntityDeclaration> name2entity = new HashMap<>();
        for ( String entityName : entityNames ) {

            EntityDeclaration entity = new EntityDeclaration();
            entity.setName( entityName );
            name2entity.put( entityName, entity );
        }
        data.put( NAME_2_ENTITY, name2entity );

        // enum declarations are not supported in blocks, but we need the empty
        // list to keep the reference check happy
        Map<String, EnumerationDeclaration> name2blockEnum = new HashMap<>();        
        data.put( NAME_2_ENUM, name2enum );
        
        
        BlockDeclaration block = new BlockDeclaration();
        block.setName( blockName );
        block.setComment( handleComment( (String) data.remove( FORMAL_COMMENT ) ) );

        for ( int i = 0; i < node.jjtGetNumChildren(); i++ ) {
            Node child = node.jjtGetChild( i );
            Object val = child.jjtAccept( this, data );
            log.debug( indent( 0 ) + "child " + child + " : " + val );
            if ( val instanceof EntityDeclaration ) {
                block.getEntities().add( (EntityDeclaration) val );
            } else if ( val instanceof Map ) {
                block.getProperties().putAll( (Map<? extends String, ? extends Object>) val );
            }
        }

        log.debug( indent( 0 ) + " block " + block );

        return block;

    }

    /**
     *
     * @param node the ASTInheritanceDeclaration node
     * @param data expect NAME_2_ENTITY map to lookup right definition
     * @return the parent type, resolved as EntityDeclaration
     */
    @Override
    public EntityDeclaration visit( ASTInheritanceDeclaration node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        String parentName = (String) node.jjtGetValue();
        Map<String, EntityDeclaration> name2entity = (Map<String, EntityDeclaration>) data.get( NAME_2_ENTITY );
        EntityDeclaration parent = name2entity.get( parentName );

        indent( -1 );
        return parent;

    }

    /**
     * A simple marker for arrays
     */
    @Override
    public Object visit( ASTArrayDeclaration node, java.util.Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;

    }

    @Override
    public EntityDeclaration visit( ASTEntityDeclaration node, java.util.Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        String entityName = (String) node.jjtGetValue();
        Map<String, EntityDeclaration> name2entity = (Map<String, EntityDeclaration>) data.get( NAME_2_ENTITY );
        EntityDeclaration entity = name2entity.get( entityName );
        entity.setComment( handleComment( (String) data.remove( FORMAL_COMMENT ) ) );

        for ( int i = 0; i < node.jjtGetNumChildren(); i++ ) {
            Node child = node.jjtGetChild( i );
            Object val = child.jjtAccept( this, data );
            log.info( indent( 0 ) + "  child " + child + " : " + val );
            if ( child instanceof ASTInheritanceDeclaration ) {
                entity.setParentType( (EntityDeclaration) val );
            } else if ( child instanceof ASTVariableDeclaration ) {
                entity.getVariables().add( (VariableDeclaration) val );
            } else if ( child instanceof ASTGenericParameterDeclaration ) {
                entity.getProperties().putAll( (Map<? extends String, ? extends Object>) val );
            } else if ( child instanceof ASTGUIDId ) {
                entity.setGuid( (GUID) val );
            } else if ( child instanceof ASTJavaCode ) {
                entity.setJavaCode( (String) val );
            } else if ( child instanceof ASTFormalComment ) {
                // do nothing
            } else {
                throw new IllegalArgumentException( "got unsupported child " + child );
            }
        }

        log.info( indent( 0 ) + " entity " + entity );

        indent( -1 );

        return entity;

    }

    /**
     * Collects the format comment and puts it into the map using the key
     * FORMAL_COMMENT
     *
     * @param node the ASTFormalComment
     * @param data used to contain comment
     * @return the string
     */
    @Override
    public Object visit( ASTFormalComment node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        indent( -1 );

        data.put( FORMAL_COMMENT, node.jjtGetValue() );
        return node.jjtGetValue();
    }

    /**
     *
     * @param node an ASTGUIDId node
     * @param data unused
     * @return the parsed GUID
     */
    @Override
    public GUID visit( ASTGUIDId node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        GUID guid = new GUID( (String) node.jjtGetValue() );

        indent( -1 );
        return guid;
    }

    @Override
    public String visit( ASTJavaCode node, Map<String, Object> data ) {
        log.info( indent( 1 ) + node.toString() + ", \"" + node.jjtGetValue() + "\"" );
        node.childrenAccept( this, data );
        indent( -1 );
        return (String) node.jjtGetValue();
    }

    @Override
    public DataType visit( ASTReferencedTypeDeclaration node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        Map<String, EntityDeclaration> name2entity = (Map<String, EntityDeclaration>) data.get( NAME_2_ENTITY );
        if ( name2entity == null ) {
            throw new IllegalStateException( "nothing found in given map for key " + NAME_2_ENTITY );
        }

        Map<String, EnumerationDeclaration> name2segEnum = (Map<String, EnumerationDeclaration>) data.get( NAME_2_ENUM );
        if ( name2segEnum == null ) {
            throw new IllegalStateException( "nothing found in given map for key " + NAME_2_ENUM );
        }

        DataType retval = null;
        if ( name2entity.containsKey( node.jjtGetValue() ) ) {
            retval = name2entity.get( node.jjtGetValue() );
        } else if ( name2segEnum.containsKey( node.jjtGetValue() ) ) {
            retval = name2segEnum.get( node.jjtGetValue() );
        } else {
            retval = name2enum.get( node.jjtGetValue() );
        }

        if ( retval == null ) {
            throw new IllegalStateException( "no entity or enum declaration found for name  " + node.jjtGetValue() );
        }
        indent( -1 );

        return retval;

    }

    @Override
    public EnumerationDeclaration visit( ASTEnumerationDeclaration node, Map<String, Object> data
    ) {
        log.info( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        String enumName = (String) node.jjtGetValue();

        EnumerationDeclaration enumDecl = null;

        if ( data.get( CURRENT_BL_OR_SEG ) == null ) {
            // we are in global context
            enumDecl = name2enum.get( enumName );
        } else {
            Map<String, EnumerationDeclaration> name2enum = (Map<String, EnumerationDeclaration>) data.get( NAME_2_ENUM );
            enumDecl = name2enum.get( enumName );
        }
        enumDecl.setComment( handleComment( (String) data.remove( FORMAL_COMMENT ) ) );

        for ( int i = 0; i < node.jjtGetNumChildren(); i++ ) {
            Node child = node.jjtGetChild( i );
            Object val = child.jjtAccept( this, data );
            log.debug( indent( 0 ) + "  " + val );
            if ( val instanceof SimpleDataType ) {
                enumDecl.setType( (DataType) val );
            } else if ( val instanceof EnumValue ) {
                enumDecl.getValues().add( (EnumValue) val );
            } else {
                throw new IllegalStateException( "support only extends <SimpleDataType> or <EnumValue>, got " + val );
            }
        }

        log.debug( indent( 0 ) + " enum " + enumDecl );
        indent( -1 );
        return enumDecl;

    }

    @Override
    public EnumValue visit( ASTEnumConstant node, Map<String, Object> data
    ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        String constName = (String) node.jjtGetValue();
        EnumValue ev = new EnumValue();
        ev.setName( constName );
        ev.setComment( handleComment( (String) data.remove( FORMAL_COMMENT ) ) );
        log.info( indent( 0 ) + "   ev " + constName );

        Object constValue = null;
        for ( int i = 0; i < node.jjtGetNumChildren(); i++ ) {
            Node child = node.jjtGetChild( i );
            Object val = child.jjtAccept( this, data );
            log.info( indent( 0 ) + "      child " + child + " : " + val );
            constValue = val;
            break;
        }

        ev.setValue( constValue );
        ev.setType( constValue instanceof Integer ? SimpleDataType.getSimpleType( SimpleDataType.SDTType.I32 )
                : SimpleDataType.getSimpleType( SimpleDataType.SDTType.GUID ) );

        indent( -1 );
        return ev;

    }

    @Override
    public Integer visit( ASTEnumIntegerValue node, Map<String, Object> data
    ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        Integer constValue = Integer.parseInt( (String) node.jjtGetValue() );
        indent( -1 );
        return constValue;

    }

    @Override
    public GUID visit( ASTEnumGUIDValue node, Map<String, Object> data
    ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );

        GUID guid = new GUID( (String) node.jjtGetValue() );
        indent( -1 );
        return guid;
    }

    @Override
    public Object visit( ASTVectorCompression node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );
        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTCompressionDeclaration node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );

        String compression = (String) node.jjtGetValue();
        VariableDeclaration vdec = (VariableDeclaration) data.get( VARIABLE_DECL );
        vdec.setCompression( compression );

        indent( -1 );
        return null;
    }

    @Override
    public Object visit( ASTPredictorDeclaration node, Map<String, Object> data ) {
        log.debug( indent( 1 ) + node.toString() + ", " + node.jjtGetValue() );
        node.childrenAccept( this, data );

        String predictor = (String) node.jjtGetValue();
        VariableDeclaration vdec = (VariableDeclaration) data.get( VARIABLE_DECL );
        vdec.setPredictor( predictor );

        indent( -1 );
        return null;
    }

    private String indent( int delta ) {
        indent += delta;
        StringBuffer buff = new StringBuffer();

        for ( int i = 0; i < indent; i++ ) {
            buff.append( "    " );
        }
        return buff.toString();
    }

    /**
     * Check the consistency of the parsed data after traversing the complete
     * AST.
     */
    private void check() {

        Set<BlockDeclaration> all = new HashSet<>();
        all.addAll( getBlocks() );
        all.addAll( getSegments() );

        for ( BlockDeclaration block : all ) {
            Set<String> entityNames = new HashSet<>();
            for ( EntityDeclaration entity : block.getEntities() ) {
                // No Duplicate entity names in on block
                if ( !entityNames.add( entity.getName() ) ) {
                    throw new IllegalArgumentException( "found duplicate entity name '" + entity.getName() + "' in '"
                            + block.getName() + "'" );
                }
                // No duplicate variable names in one entity
                Set<String> varNames = new HashSet<>();
                for ( VariableDeclaration var : entity.getVariables() ) {
                    if ( !varNames.add( var.getName() ) ) {
                        throw new IllegalArgumentException( "found duplicate variable name '" + var.getName()
                                + "' in entity '" + entity.getName() + "' in '" + block.getName() + "'" );
                    }
                }

            }

            for ( SegmentDeclaration segment : getSegments() ) {
                // Need Segment type and compression (Table 3: Segment Types) for segments
                Map<String, Object> properties = segment.getProperties();
                if ( !properties.containsKey( PROP_SEGMENT_TYPE ) ) {
                    throw new IllegalArgumentException( "no value found for key '" + PROP_SEGMENT_TYPE
                            + "' in segment " + segment.getName() );
                }
                Object value = properties.get( PROP_SEGMENT_TYPE );
                if ( !( value instanceof Integer ) ) {
                    throw new IllegalArgumentException( "got invalid  value " + value + " for key '" + PROP_SEGMENT_TYPE
                            + "' in segment " + segment.getName() + ", expect integer" );
                }

                if ( !properties.containsKey( PROP_ZLIB_APPLIED ) ) {
                    throw new IllegalArgumentException( "no value found for key '" + PROP_ZLIB_APPLIED
                            + "' in segment " + segment.getName() );
                }
                value = properties.get( PROP_ZLIB_APPLIED );
                if ( !( value instanceof Boolean ) ) {
                    throw new IllegalArgumentException( "got invalid  value " + value + " for key '" + PROP_ZLIB_APPLIED
                            + "' in segment " + segment.getName() + ", expect boolean" );
                }
            }
        }
    }

    static String handleComment( String comment ) {
        if ( comment == null ) {
            return null;
        }
        log.debug( "handleComment '" + comment + "'" );
        StringBuffer buff = new StringBuffer();
        // Split comment into lines
        String[] lines = comment.split( "\n" );
        for ( int i = 0; i < lines.length; i++ ) {
            String line = lines[i].trim();
            log.debug( "line " + line );
            if ( i == 0 ) {
                line = line.replaceAll( "^/\\*\\*", "" ).trim();
                if ( 1 == lines.length ) {
                    line = line.replaceAll( "\\*/$", "" );
                }
                if ( line.length() != 0 ) {
                    buff.append( line.trim() );
                    if ( 1 != lines.length ) {
                        buff.append( "\n" );
                    }
                }
            } else if ( i == lines.length - 1 ) {
                line = line.replaceAll( "\\*/$", "" ).trim();
                if ( line.length() != 0 ) {
                    buff.append( line.trim() );
                }
            } else {
                line = line.replaceAll( "^\\*", "" );
                buff.append( line.trim() + "\n" );
            }

        }
        log.debug( "comment '" + buff.toString() + "'" );
        return buff.toString();
    }

}
