/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import de.cadoculus.jove.bibi.annotation.BIBILength;
import de.cadoculus.jove.bibi.annotation.MbString;
import java.lang.reflect.AnnotatedElement;
import java.nio.ByteBuffer;
import org.slf4j.LoggerFactory;

/**
 * Reads bytes from the given ByteBuffer into a String. Way of reading depends
 * on the given constructor.
 *
 * @author Zerbst
 */
public class StringCodec implements Codec<String> {

    public static org.slf4j.Logger log = LoggerFactory.getLogger( StringCodec.class );
    private final Class type;
    private final AnnotatedElement metadata;
    private final BIBILength bbLength;
    private MbString mbString;
    private de.cadoculus.jove.bibi.annotation.String string;

    /**
     * Reads UChar[] of the given length into String
     *
     * @param length the number of bytes to read
     */
    public StringCodec( AnnotatedElement metadata, Class type ) {
        this.metadata = metadata;
        this.type = type;

        if ( ( mbString = metadata.getAnnotation( MbString.class ) ) != null ) {
            // Ok, use mbstring
            bbLength = null;
        } else if ( ( string = metadata.getAnnotation( de.cadoculus.jove.bibi.annotation.String.class ) ) != null ) {
            // Ok, use mbstring
            bbLength = null;
        } else if ( ( bbLength = metadata.getAnnotation( BIBILength.class ) ) == null ) {
            throw new IllegalArgumentException( "expect BIBILength annotation on " + metadata );
        }

        log.debug( "created StringCodec on " + metadata );

    }

    @Override
    public String decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {

        CodecUtility.checkMaxPosition( input, ctx );

        StringBuffer retval = new StringBuffer();

        if ( mbString != null ) {
            int length = input.getInt();
            log.debug( "MBString length " + length );

            if ( length == 0 ) {
                return null;
            }

            for ( int i = 0; i < length; i++ ) {
                if ( ctx.getMaxPosition() == input.position() ) {
                    log.error( "reading MBString exceeded maximum position " + ctx.getMaxPosition() );
                    log.error( "   expected length " + length );
                    log.error( "   read string " + retval );
                    break;

                    //throw new IllegalStateException( "exceeced maximum position when reading MBString" );
                }
                char c = input.getChar();
                retval.append( c );
                if ( log.isTraceEnabled() ) {
                    log.trace( "read " + retval );
                }

            }
            log.debug( "read string " + retval );
        } else if ( string != null ) {
            int length = input.getInt();
            log.debug( "string length " + length );

            if ( length == 0 ) {
                return null;
            }
            // strings are "proper" C strings and 0 terminated. The 0 is not appended to the java string
            for ( int i = 0; i < length; i++ ) {
                if ( ctx.getMaxPosition() == input.position() ) {
                    log.error( "reading string exceeded maximum position " + ctx.getMaxPosition() );
                    log.error( "   expected length " + length );
                    log.error( "   read string " + retval );

                    throw new IllegalStateException( "exceeced maximum position when reading String" );
                }
                char c = (char) input.get();
                if ( i + 1 != length ) {
                    retval.append( c );
                }

            }
            log.debug( "read string " + retval );
        } else {
            int length = CodecUtility.evaluate( bbLength, ctx );

            if ( length > 0 ) {
                for ( int i = 0; i < length; i++ ) {
                    retval.append( (char) input.get() );
                }
            } else {
                throw new IllegalStateException( "expect length > 0" );
            }
        }

        return retval.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder( "StringCodec " );
        if ( mbString != null ) {
            sb.append( "MbString" );
        } else if ( string != null ) {
            sb.append( "String" );
        } else {
            sb.append( "length " ).append( bbLength.value() );
        }
        return sb.toString();
    }
}
