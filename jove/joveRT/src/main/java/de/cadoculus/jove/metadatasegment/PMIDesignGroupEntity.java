/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 

*/
@XmlRootElement(name="PMIDesignGroupEntity")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIDesignGroupEntity",
    propOrder= {"groupNameStringID", "attributeCount", "designGroupAttributes", }
)
public class PMIDesignGroupEntity {

    public static Logger log = LoggerFactory.getLogger( PMIDesignGroupEntity.class );

    /**  
     * The variable number 0 : I32 groupNameStringID
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer groupNameStringID;

    /**  
     * The variable number 1 : I32 attributeCount
     * <br>options : <pre>{valid : pmiVersionNumber >=3}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    @de.cadoculus.jove.bibi.annotation.BIBIValid( "pmiVersionNumber >=3" )
    private Integer attributeCount;

    /**  
     * The variable number 2 : DesignGroupAttribute[] designGroupAttributes
     * <br>options : <pre>{length : obj.attributeCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.BIBI()
    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.attributeCount" )
    @XmlElementWrapper(name = "designGroupAttributes")
    @XmlElementRef()    private java.util.List<DesignGroupAttribute> designGroupAttributes;



    /** Getter for groupNameStringID.
     *  
     * 
     * @return Integer
     */
    public Integer getGroupNameStringID() {
        return groupNameStringID;
    }

     /** Setter for groupNameStringID.
     *  
     * 
     * @param value Integer
     */
    public void setGroupNameStringID( Integer value ) {
        this.groupNameStringID = value;
    }

    /** Getter for attributeCount.
     *  
     * 
     * @return Integer
     */
    public Integer getAttributeCount() {
        return attributeCount;
    }

     /** Setter for attributeCount.
     *  
     * 
     * @param value Integer
     */
    public void setAttributeCount( Integer value ) {
        this.attributeCount = value;
    }

    /** Getter for designGroupAttributes.
     *  
     * 
     * @return java.util.List<DesignGroupAttribute>
     */
    public java.util.List<DesignGroupAttribute> getDesignGroupAttributes() {
        return designGroupAttributes;
    }

     /** Setter for designGroupAttributes.
     *  
     * 
     * @param value java.util.List<DesignGroupAttribute>
     */
    public void setDesignGroupAttributes( java.util.List<DesignGroupAttribute> value ) {
        this.designGroupAttributes = value;
    }


    
}

