/*
 * Copyright (C) 2013-2014 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the Apache License as published by the Apache Software Foundation 
 * in version 2.
 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the Apache License for more details.
 *
 * You should have received a copy of the Apache License along with
 * Jove; see the file COPYING. If not, see http://www.apache.org/licenses/.
 *
 */
package de.cadoculus.jove.metadatasegment;


import de.cadoculus.jove.bibi.annotation.CompressionType;
import de.cadoculus.jove.bibi.annotation.PredictorType;
import de.cadoculus.jove.base.*;
import de.cadoculus.jove.enums.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
<h4>7.2.6.2.4 PMI String Table</h4>
The PMI String Table data collection defines data for a list of character strings and serves as a central repository for all character strings used by other PMI Entities within the same PMI Manager Meta Data Element. PMI Entities reference into this list/array of character strings to define usage of a particular character string using a simple list/array "index" (i.e. String ID).

*/
@XmlRootElement(name="PMIStringTable")
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( 
    name = "PMIStringTable",
    propOrder= {"stringCount", "pmiString", }
)
public class PMIStringTable {

    public static Logger log = LoggerFactory.getLogger( PMIStringTable.class );

    /**  
     * The variable number 0 : I32 stringCount
     * <br>no options defined
     */     
    @de.cadoculus.jove.bibi.annotation.I32()    
    private Integer stringCount;

    /**  
     * The variable number 1 : String[] pmiString
     * <br>options : <pre>{length : obj.stringCount}</pre>
     */     
    @de.cadoculus.jove.bibi.annotation.String()    @de.cadoculus.jove.bibi.annotation.BIBILength( "obj.stringCount" )
    @XmlElementWrapper(name = "pmiString")
        private java.util.List<String> pmiString;



    /** Getter for stringCount.
     *  
     * 
     * @return Integer
     */
    public Integer getStringCount() {
        return stringCount;
    }

     /** Setter for stringCount.
     *  
     * 
     * @param value Integer
     */
    public void setStringCount( Integer value ) {
        this.stringCount = value;
    }

    /** Getter for pmiString.
     *  
     * 
     * @return java.util.List<String>
     */
    public java.util.List<String> getPmiString() {
        return pmiString;
    }

     /** Setter for pmiString.
     *  
     * 
     * @param value java.util.List<String>
     */
    public void setPmiString( java.util.List<String> value ) {
        this.pmiString = value;
    }


    
}

