/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cadoculus.jove.bibi.codec;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.bibi.Codec;
import de.cadoculus.jove.bibi.WorkingContext;
import java.nio.ByteBuffer;

/**
 *
 * @author Zerbst
 */
public class NullCodec implements Codec<Object> {

    @Override
    public Object decode( ByteBuffer input, WorkingContext ctx ) throws BIBIException {
        return null;
    }
}
