/*
 * Copyright (C) 2013 Carsten Zerbst
 *
 * This file is part of Jove
 *
 * Jove  is free software; you can redistribute it and/or modify it under the
 * terms of the MIT License.

 *
 * Jove is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the MIT License along with
 * Jove; see the file COPYING. If not, see http://opensource.org/licenses/MIT
 *
 */
package de.cadoculus.jove.base;

import de.cadoculus.jove.bibi.BIBIException;
import de.cadoculus.jove.enums.SegmentType;
import de.cadoculus.jove.bibi.annotation.BIBI;
import de.cadoculus.jove.bibi.annotation.I32;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Segment Header contains information that determines how the remainder of the
 * Segment is interpreted by the loader.
 *
 * @author Carsten Zerbst
 */
@XmlRootElement( name = "SegmentHeader" )
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType(
         name = "SegmentHeader",
        propOrder = { "loadingException", "segmentID", "segmentType", "segmentLength", "objectData" } )
@de.cadoculus.jove.bibi.annotation.BIBICodec( de.cadoculus.jove.bibi.codec.SegmentHeaderCodec.class )
public class SegmentHeader implements RecoverableElement {

    @XmlAttribute( name = "loadingStatus" )
    private LoadingStatus loadingStatus = LoadingStatus.UNKNOWN;

    @XmlJavaTypeAdapter( ExceptionXmlAdapter.class )
    private BIBIException loadingException;

    @XmlAttribute( name = "offset" )
    private int offset = 0;

    /**
     * Global Unique Identifier for the segment.
     */
    @de.cadoculus.jove.bibi.annotation.GUID
    private GUID segmentID;
    /**
     * Segment Type defines a broad classification of the segment contents. For
     * example, a Segment Type of “1” denotes that the segment contains Logical
     * Scene Graph material; “2” denotes contents of a B-Rep, etc. The complete
     * list of segment types is as follows. The column labeled "ZLIB Applied?"
     * denotes whether ZLIB compression is conditionally applied to the entirety
     * of the segment's Data payload.<p>
     * <table>
     * <thead><tr><th>Type</th><th>Data Contents</th><th> ZLIB
     * Applied?</th></tr></thead> <tr><td>1</td><td>Logical Scene
     * Graph</td><td>Yes</td>/<tr> <tr><td>2</td><td>JT
     * B-Rep</td><td>Yes</td></tr> <tr><td>3</td><td>PMI
     * Data</td><td>Yes</td></tr> <tr><td>4</td><td>Meta
     * Data</td><td>Yes</td></tr> <tr><td>6</td><td>Shape</td><td>No</td></tr>
     * <tr><td>7</td><td>Shape LOD0</td><td>No</td></tr> <tr><td>8</td><td>Shape
     * LOD1</td><td>No</td></tr> <tr><td>9</td><td>Shape
     * LOD2</td><td>No</td></tr> <tr><td>10</td><td>Shape
     * LOD3</td><td>No</td></tr> <tr><td>11</td><td>Shape
     * LOD4</td><td>No</td></tr> <tr><td>12</td><td>Shape
     * LOD5</td><td>No</td></tr> <tr><td>13</td><td>Shape
     * LOD6</td><td>No</td></tr> <tr><td>14</td><td>Shape
     * LOD7</td><td>No</td></tr> <tr><td>15</td><td>Shape
     * LOD8</td><td>No</td></tr> <tr><td>16</td><td>Shape
     * LOD9</td><td>No</td></tr> <tr><td>17</td><td>XT
     * B-Rep</td><td>Yes</td></tr> <tr><td>18</td><td>Wireframe
     * Representation</td><td>Yes</td></tr>
     * <tr><td>20</td><td>ULP</td><td>Yes</td></tr>
     * <tr><td>24</td><td>LWPA</td><td>Yes</td></tr> </table>
     * <p>
     *
     * Note: Segment Types 7-16 all identify the contents as LOD Shape data,
     * where the increasing type number is intended to convey some notion of how
     * high an LOD the specific shape segment represents. The lower the type in
     * this 7-16 range the more detailed the Shape LOD (i.e. Segment Type 7 is
     * the most detailed Shape LOD Segment). For the rare case when there are
     * more than 10 LODs, LOD9 and greater are all assigned Segment Type 16.
     * Note: The more generic Shape Segment type (i.e. Segment Type 6) is used
     * when the Shape Segment has one or more of the following characteristics:
     * <ul><li> Not a descendant of an LOD node</li><li> Is referenced by (i.e.
     * is a child of) more than one LOD node,</li><li> Shape has its own
     * built-in LODs, and</li><li> No way to determine what LOD a Shape Segment
     * represents.</li></ul>
     */
    @I32
    private SegmentType segmentType;
    /**
     * Segment Length is the total size of the segment in bytes. This length
     * value includes all segment Data bytes plus the Segment Header bytes (i.e.
     * it is the size of the complete segment) and should be equal to the length
     * value stored with this segment‟s TOC Entry.
     */
    @I32
    private Integer segmentLength;
    /**
     * Reference to the data found in the segement after the header
     */
    @BIBI
    @XmlElementRef()
    private ObjectData objectData;

    /**
     * Global Unique Identifier for the segment.
     *
     * @return the segmentID
     */
    public GUID getSegmentID() {
        return segmentID;
    }

    /**
     * Global Unique Identifier for the segment.
     *
     * @param segmentID the segmentID to set
     */
    public void setSegmentID( GUID segmentID ) {
        this.segmentID = segmentID;
    }

    /**
     * Segment Type defines a broad classification of the segment contents. For
     * example, a Segment Type of “1” denotes that the segment contains Logical
     * Scene Graph material; “2” denotes contents of a B-Rep, etc. The complete
     * list of segment types is as follows. The column labeled "ZLIB Applied?"
     * denotes whether ZLIB compression is conditionally applied to the entirety
     * of the segment's Data payload.<p>
     * <table>
     * <thead><tr><th>Type</th><th>Data Contents</th><th> ZLIB
     * Applied?</th></tr></thead> <tr><td>1</td><td>Logical Scene
     * Graph</td><td>Yes</td>/<tr> <tr><td>2</td><td>JT
     * B-Rep</td><td>Yes</td></tr> <tr><td>3</td><td>PMI
     * Data</td><td>Yes</td></tr> <tr><td>4</td><td>Meta
     * Data</td><td>Yes</td></tr> <tr><td>6</td><td>Shape</td><td>No</td></tr>
     * <tr><td>7</td><td>Shape LOD0</td><td>No</td></tr> <tr><td>8</td><td>Shape
     * LOD1</td><td>No</td></tr> <tr><td>9</td><td>Shape
     * LOD2</td><td>No</td></tr> <tr><td>10</td><td>Shape
     * LOD3</td><td>No</td></tr> <tr><td>11</td><td>Shape
     * LOD4</td><td>No</td></tr> <tr><td>12</td><td>Shape
     * LOD5</td><td>No</td></tr> <tr><td>13</td><td>Shape
     * LOD6</td><td>No</td></tr> <tr><td>14</td><td>Shape
     * LOD7</td><td>No</td></tr> <tr><td>15</td><td>Shape
     * LOD8</td><td>No</td></tr> <tr><td>16</td><td>Shape
     * LOD9</td><td>No</td></tr> <tr><td>17</td><td>XT
     * B-Rep</td><td>Yes</td></tr> <tr><td>18</td><td>Wireframe
     * Representation</td><td>Yes</td></tr>
     * <tr><td>20</td><td>ULP</td><td>Yes</td></tr>
     * <tr><td>24</td><td>LWPA</td><td>Yes</td></tr> </table>
     * <p>
     *
     * Note: Segment Types 7-16 all identify the contents as LOD Shape data,
     * where the increasing type number is intended to convey some notion of how
     * high an LOD the specific shape segment represents. The lower the type in
     * this 7-16 range the more detailed the Shape LOD (i.e. Segment Type 7 is
     * the most detailed Shape LOD Segment). For the rare case when there are
     * more than 10 LODs, LOD9 and greater are all assigned Segment Type 16.
     * Note: The more generic Shape Segment type (i.e. Segment Type 6) is used
     * when the Shape Segment has one or more of the following characteristics:
     * <ul><li> Not a descendant of an LOD node</li><li> Is referenced by (i.e.
     * is a child of) more than one LOD node,</li><li> Shape has its own
     * built-in LODs, and</li><li> No way to determine what LOD a Shape Segment
     * represents.</li></ul>
     *
     * @return the segmentType
     */
    public SegmentType getSegmentType() {
        return segmentType;
    }

    /**
     * Segment Type defines a broad classification of the segment contents. For
     * example, a Segment Type of “1” denotes that the segment contains Logical
     * Scene Graph material; “2” denotes contents of a B-Rep, etc. The complete
     * list of segment types is as follows. The column labeled "ZLIB Applied?"
     * denotes whether ZLIB compression is conditionally applied to the entirety
     * of the segment's Data payload.<p>
     * <table>
     * <thead><tr><th>Type</th><th>Data Contents</th><th> ZLIB
     * Applied?</th></tr></thead> <tr><td>1</td><td>Logical Scene
     * Graph</td><td>Yes</td>/<tr> <tr><td>2</td><td>JT
     * B-Rep</td><td>Yes</td></tr> <tr><td>3</td><td>PMI
     * Data</td><td>Yes</td></tr> <tr><td>4</td><td>Meta
     * Data</td><td>Yes</td></tr> <tr><td>6</td><td>Shape</td><td>No</td></tr>
     * <tr><td>7</td><td>Shape LOD0</td><td>No</td></tr> <tr><td>8</td><td>Shape
     * LOD1</td><td>No</td></tr> <tr><td>9</td><td>Shape
     * LOD2</td><td>No</td></tr> <tr><td>10</td><td>Shape
     * LOD3</td><td>No</td></tr> <tr><td>11</td><td>Shape
     * LOD4</td><td>No</td></tr> <tr><td>12</td><td>Shape
     * LOD5</td><td>No</td></tr> <tr><td>13</td><td>Shape
     * LOD6</td><td>No</td></tr> <tr><td>14</td><td>Shape
     * LOD7</td><td>No</td></tr> <tr><td>15</td><td>Shape
     * LOD8</td><td>No</td></tr> <tr><td>16</td><td>Shape
     * LOD9</td><td>No</td></tr> <tr><td>17</td><td>XT
     * B-Rep</td><td>Yes</td></tr> <tr><td>18</td><td>Wireframe
     * Representation</td><td>Yes</td></tr>
     * <tr><td>20</td><td>ULP</td><td>Yes</td></tr>
     * <tr><td>24</td><td>LWPA</td><td>Yes</td></tr> </table>
     * <p>
     *
     * Note: Segment Types 7-16 all identify the contents as LOD Shape data,
     * where the increasing type number is intended to convey some notion of how
     * high an LOD the specific shape segment represents. The lower the type in
     * this 7-16 range the more detailed the Shape LOD (i.e. Segment Type 7 is
     * the most detailed Shape LOD Segment). For the rare case when there are
     * more than 10 LODs, LOD9 and greater are all assigned Segment Type 16.
     * Note: The more generic Shape Segment type (i.e. Segment Type 6) is used
     * when the Shape Segment has one or more of the following characteristics:
     * <ul><li> Not a descendant of an LOD node</li><li> Is referenced by (i.e.
     * is a child of) more than one LOD node,</li><li> Shape has its own
     * built-in LODs, and</li><li> No way to determine what LOD a Shape Segment
     * represents.</li></ul>
     *
     * @param segmentType the segmentType to set
     */
    public void setSegmentType( SegmentType segmentType ) {
        this.segmentType = segmentType;
    }

    /**
     * Segment Length is the total size of the segment in bytes. This length
     * value includes all segment Data bytes plus the Segment Header bytes (i.e.
     * it is the size of the complete segment) and should be equal to the length
     * value stored with this segment‟s TOC Entry.
     *
     * @return the segmentLength
     */
    public int getSegmentLength() {
        return segmentLength;
    }

    /**
     * Segment Length is the total size of the segment in bytes. This length
     * value includes all segment Data bytes plus the Segment Header bytes (i.e.
     * it is the size of the complete segment) and should be equal to the length
     * value stored with this segment‟s TOC Entry.
     *
     * @param segmentLength the segmentLength to set
     */
    public void setSegmentLength( int segmentLength ) {
        this.segmentLength = segmentLength;
    }

    /**
     * @return the data
     */
    public ObjectData getObjectData() {
        return objectData;
    }

    /**
     * @param data the data to set
     */
    public void setObjectData( ObjectData data ) {
        this.objectData = data;
    }

    /**
     * Returns the offset from the beginning of the file to beginning of this
     * SegmentHeader
     *
     * @return the offset in bytes
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Sets the offset from the beginning of the file to beginning of this
     * SegmentHeader
     *
     * @param offset the offset to set in bytes
     */
    public void setOffset( int offset ) {
        this.offset = offset;
    }

    @Override
    public LoadingStatus getLoadingStatus() {
        return loadingStatus;
    }

    public void setLoadingStatus( LoadingStatus status ) {
        this.loadingStatus = status;
    }

    /**
     * Get the exception which occured when loading this SegmentHeader.
     *
     * @return the exception, may be null
     */
    @Override
    public BIBIException getException() {
        return loadingException;
    }

    /**
     * Set the exception which occured when loading this SegmentHeader.
     *
     * @param the exception
     */
    public void setException( BIBIException ex ) {
        this.loadingException = ex;
    }
}
